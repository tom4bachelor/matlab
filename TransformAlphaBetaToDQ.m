function [dXd, dXq] = TransformAlphaBetaToDQ(dXAlpha, dXBeta, dAngle)
	
    dAngle_el = deg2rad(dAngle);

    dXd = cos(dAngle_el)*dXAlpha + sin(dAngle_el)*dXBeta;
    dXq = -sin(dAngle_el)*dXAlpha + cos(dAngle_el)*dXBeta;
  
end