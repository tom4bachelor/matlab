
%% Initalisierung Problem

dLaenge = 100;

dBohrungsdiskretisierungLuftspalt = 0.5;
dBohrungsdiskretisierungBohrung = 30;
dBohrungsdiskretisierungPolschuh = 5;
dBohrungsdiskretisierungJoch = 5;
dBohrungsdiskretisierungWelle = 10;
dBohrungsdiskretisierungPM = 10;

iRotor = 101;
iRotorBohrungen = 102;
sGruppe.iRotor = iRotor;
sGruppe.iRotorBohrungen = iRotorBohrungen;
%% Begrenzung zeichnen (Quadrat)

dBoundaryBox_Kantenlaenge = 250;

%% Stator

% Eingangsparameter
sStator.dPaketBreite = 230;
sStator.dLochkreis = 230;
sStator.dLochkreis2 = 280;
sStator.dBohrungDurchmesser	= 9;
sStator.dBohrungDurchmesser2 = 8;
sStator.dInnendurchmesser = 102;
sStator.dPolbedeckung = 0.89;
sStator.dPolschuhHoeheAussen = 2;
sStator.dPolschuhHoeheInnen = 6;
sStator.dPolschuhWinkel = 15;
sStator.dZahnBreite = 35;
sStator.dZahnHoehe = 20;

sStator.dRadiusIn = 5;                                                       %% �bergang Zahn auf Joch
sStator.dRadiusOut = 160;                                                    %% Radius Joch;

sStator.dBohrungsdiskretisierungBohrung = dBohrungsdiskretisierungBohrung;
sStator.dBohrungsdiskretisierungPolschuh = dBohrungsdiskretisierungPolschuh;
sStator.dBohrungsdiskretisierungJoch = dBohrungsdiskretisierungJoch;
sStator.dBohrungsdiskretisierungLuftspalt = dBohrungsdiskretisierungLuftspalt;


% Wicklung
dDraht = 1;
k = 1/0.4;                                                                  % Korrekturfaktor, Kupferf�llfaktor
iN = 40;
dWickelfensterHoehe = sStator.dZahnHoehe*0.75;                              % Etwas weniger hoch als Wickelfenster
dWickelfensterBreite = (pi/4*dDraht^2*1*k*iN)/dWickelfensterHoehe; 
sStator.dR = 0.5384;                                                        % Messwert                       


% Korrekt, allerdings Anpassung bei Flussaufnahme notwendig
sWicklung.iWickelfenster_ol = 111;
sWicklung.iWickelfenster_or = 112;
sWicklung.iWickelfenster_olu = 113;
sWicklung.iWickelfenster_olo = 114;
sWicklung.iWickelfenster_ulo = 115;
sWicklung.iWickelfenster_ulu = 116;
sWicklung.iWickelfenster_ur = 117;
sWicklung.iWickelfenster_ul = 118;
sWicklung.iWickelfenster_uro = 119;
sWicklung.iWickelfenster_uru = 120;
sWicklung.iWickelfenster_oro = 121;
sWicklung.iWickelfenster_oru = 122;
    
sWicklung.dWickelfensterBreite  = dWickelfensterBreite;
sWicklung.dWickelfensterHoehe = dWickelfensterHoehe;
sWicklung.iN = iN;

%% Rotor inkl. Welle

% Eingangsparameter Welle
sWelle.dDurchmesser = 35;
sWelle.dPassfederBreite = 10;
sWelle.dPassfederHoehe = 3.3;
sWelle.dBohrungsdiskretisierung = dBohrungsdiskretisierungWelle;
                                                 
% Eingangsparameter Rotor
sRotor.dDurchmesser = 100;

% PM mittig
sRotor.dPM_Hoehe = 3.1;
sRotor.dPM_Breite = 51.5;
sRotor.dPM_Abstand = 11.3;                                                     % Abstand Mittelpunkt PM zu Au�enrand Rotor
sRotor.dPM_Winkel = 0;

sRotor.szMaterialPM = 'NdFeB 32 MGOe';
sRotor.dCurrentSheet = 0.1;
sRotor.dBohrungsdiskretisierungLuftspalt = dBohrungsdiskretisierungLuftspalt;
sRotor.dBohrungsdiskretisierungPM = dBohrungsdiskretisierungPM;
sRotor.dWelleDurchmesser = sWelle.dDurchmesser;
sRotor.dPassfederHoehe = sWelle.dPassfederHoehe;