%[gradx,grady] = evaluateGradient(results,xq,yq);
gradB = @(r, phi)(interpolateSolution(result,r.*cos(phi),r.*sin(phi))');
%q = integral2(fun,xmin,xmax,ymin,ymax)

f=@(x,y)(reshape(getB(result,x,y),size(x)));
integral2(f,0,0.01,0,0.01);

M = 1/(4*pi*1e-07) * 

% B hat die x-komponente -ux und die y-komponente uy

getB2=@(phi)(getB(result,0.0506,phi));
M = integral(getB2,0,2*pi)*1/(4*pi*1e-7);

getT2=@(r,phi)(getTorqueVol(result,r,phi));
Mv = integral2(getT2,0.0501,0.0507,0,2*pi)*1/(4*pi*1e-7);

% function [ output_args ] = getTorque( input_args )
% %GETTORQUE Summary of this function goes here
% %   Detailed explanation goes here
%     %https://de.mathworks.com/matlabcentral/answers/314323-pde-toolbox-how-to-integrate-the-solution-only-over-a-subdomain
% 
% 
% 
% end
% 
