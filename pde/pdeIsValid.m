classdef pdeIsValid  
  %PDEISVALID Internal class used to validate input arguments.
  % This undocumented function may be removed in a future release.

  % Copyright 2013-2014 The MathWorks, Inc.
  
  methods(Static)
    function ok=coefficient(c)
      ok = isnumeric(c) || ischar(c) || isa(c,'function_handle');
      % R2015a - Defer this change to avoid risk of backward incompatibility
      % if ok && isa(c,'function_handle') && ~(nargin(c) >= 2 && nargin(c) <= 4)
      %    error(message('pde:pdeIsValid:invalidFunctionHandle'));
      % end
    end
    
    function ok=p2d(p)
      if(size(p,1) < 2)
        error(message('pde:pdeIsValid:pointMatrixRowSize'));
      end
      ok = true;
    end
    
    function ok=t2d(t)
      if(size(t,1) < 4)
        error(message('pde:pdeIsValid:triangleMatrixRowSize'));
      end
      ok = true;
    end
    
    function ok=e(em)
      if(size(em,1) < 7)
        error(message('pde:pdeIsValid:edgeMatrixRowSize'));
      end
      ok = true;
    end
    
    function ok=meshGeomAssoc(e, p)
      if(size(p,1) == 2)
        ok = pdeIsValid.e(e);
      else
        if( ~isa(e, 'pde.FEMeshAssociation'))
		  error(message('pde:pdeIsValid:meshAssociation'));
        end
      end
    end
    
    function ok=onOrOff(v)
      ok = any(validatestring(v,{'on', 'off'}));
    end
    
    function ok=tlist(v)
      if ~isreal(v)
        error(message('pde:pdeIsValid:tlistNotReal'));
      elseif(length(v) < 2)
        error(message('pde:pdeIsValid:tlistTooShort'));
      end
      ok = true;
    end
    
    function ok=t0(v)
      if ~(isnumeric(v) || ischar(v))
        error(message('pde:pdeIsValid:initialConditionType'));
      end
      ok = true;
    end
    
    function ok=b(em)
      ok = pdeIsValid.coefficient(em) || isa(em, 'pde.PDEModel');
    end
    
  end
  
end

