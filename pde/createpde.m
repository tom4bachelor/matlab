function pdem = createpde(varargin)
%CREATEPDE - Creates an analysis model 
%   PDEM = CREATEPDE(PDESystemSize) creates an empty PDE analysis model
%   that can be used to define a system of partial differential equations
%   of size PDESystemSize. If a PDESystemSize is not specified, the default
%   size of 1 is used, giving a scalar system. The output, PDEM, is a
%   PDEModel - a container object that provides functions that support the
%   model creation through the definition of the geometry, generation of
%   the mesh, application of boundary conditions and initial conditions,
%   and solution.
%
%
%   PDEM = CREATEPDE('thermal',AnalysisType) creates an empty thermal
%   analysis model for the analysis type specified using the AnalysisType
%   string. Default AnalysisType is 'steadystate'.
%
%       AnalysisType         Description    
%-----------------------------------------------------------------
%       'steadystate'        Creates a thermal model for steady-state
%                            analysis.
%       'transient'          Creates a thermal model for transient 
%                            analysis.
%
%   Example 1:  Create a scalar PDEModel, import geometry, assign a
%   boundary condition and generate a mesh.
%
%      numberOfPDEs = 1;
%      pdem = createpde(numberOfPDEs);
%      gm = importGeometry(pdem,'Block.stl')
%      pdegplot(gm, 'FaceLabels','on')
%      % Alternatively, pdegplot(pdem,...) plots the geometry
%      
%      % Assign a boundary condition
%      bc1 = applyBoundaryCondition(pdem,'dirichlet','Face',2, 'u', 0);
%      
%      % Generate and plot the mesh
%      msh = generateMesh(pdem);
%      figure;
%      pdemesh(msh);
%      % Alternatively, pdemesh(pdem,...) plots the mesh
%
%   Example 2: Create a thermal model for steady-state analysis.
%
%       thermalmodel = createpde('thermal','steadystate');
%       gm = importGeometry(thermalmodel,'SquareBeam.STL');
%       thermalProperties(thermalmodel,'ThermalConductivity',0.08);
%
%    See also pde.PDEModel, pde.PDEModel/importGeometry, 
%             pde.PDEModel/applyBoundaryCondition, pde.PDEModel/generateMesh
%             pde.ThermalModel, pde.ThermalModel/thermalProperties

%    Copyright 2014-2016 The MathWorks, Inc.

% This is a factory function that may return a more specific model types in
% a future release.

narginchk(0,2);

if ( (nargin == 0) || (nargin == 1 && isnumeric(varargin{1})) )
    % create generic PDEModel
    pdem = pde.PDEModel(varargin{:});
    return
end

physics = varargin{1};
validateattributes(physics, {'char','string'},{'scalartext'})
nc = numel(physics);
if (~strncmpi(physics,'thermal',nc) )
    if isstring(physics) % Needed till string is supported by message
        physics = char(physics);
    end
    error(message('pde:createpde:unsupportedVertical',physics));
else
    pdem = pde.ThermalModel(varargin{2:end});
end


