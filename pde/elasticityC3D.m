function cmat = elasticityC3D(E,nu)
% ELASTICITYC3D Calculate c-matrix for isotropic elastic solid
%   This convenience function is included in the toolbox for the use in
%   several examples. This undocumented function may be removed in a future
%   release.
%
%   This function uses the linearized small-displacement assumption for an
%   isotropic material. Hence, there are only two independent elasticity
%   constants, Young's modulus (E) and Poisson's ratio (nu), which are used
%   to construct c-matrix in PDE Toolbox format.

%   Copyright 2014-2016 The MathWorks, Inc.

G = E/(2*(1+nu));
c1 = E*(1-nu)/((1+nu)*(1-2*nu));
c12 = c1*nu/(1-nu);
C11 = [c1 0 G 0 0 G];
C12 = [0 G 0 c12 0 0 0 0 0];
C13 = [0 0 G 0 0 0 c12 0 0];
C22 = [G 0 c1 0 0 G];
C23 = [0 0 0 0 0 G 0 c12 0];
C33 = [G 0 G 0 0 c1];
cmat  = [C11 C12 C22 C13 C23 C33]';

end
