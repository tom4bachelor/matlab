function [q,g,h,r]=circleb1(~,e,~,~)
%CIRCLEB1       Boundary condition data for a scalar elliptic problem with
%               zero-Dirichlet Boundary conditions.
% 
% This function encapsulates zero-Dirichlet boundary conditions for a
% scalar, elliptic problem. That is, the solution of the associated
% elliptic partial differential equation is zero on the domain's boundary.
% 
% It obeys the calling conventions described in PDEBOUND.
%
% See also PDEBOUND.
%

%       Copyright 1994-2012 The MathWorks, Inc.
 
% The variable 'N' stores the dimensionality of the solution of the PDE.
% Because this boundary file is intended for use with scalar partial
% differential equations, 'N' is assigned the value 1.
%
% The variable 'ne' is the number of edges stored in the matrix 'e'. 
N = 1;
ne = size(e,2); 
 
% For more information on the meaning of the return values and their
% dimensions, please refer to the documentation page for PDEBOUND.
q = zeros(N^2,ne); 
g = zeros(N,ne);
h = ones(N^2,2*ne);
r = zeros(N,2*ne);

