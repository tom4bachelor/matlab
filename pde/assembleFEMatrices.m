function FEMatricesOut = assembleFEMatrices(pdem, varargin)
% assembleFEMatrices - Assembles the intermediate FE matrices 
%    Assembles the intermediate FE matrices for equations of the following 
%    coefficient form:
%  
%              m*d^2u/dt^2 + d*du/dt - div(c*grad(u)) + a*u = f
%
%    The intermediate FE matrices can subsequently be used to compute a 
%    solution to the PDE using linear algebra methods.
%    Note: If the PDE to solve has both 'm' and 'd' coefficients that are
%    non-zero, then 'd' must be a matrix that has the same size as the 
%    discretized mass matrix M, or stiffness matrix Kc mentioned below. 
%
%    FEM = assembleFEMatrices(pdem, bcmethod) Assembles the intermediate 
%    FE matrices. bcmethod option is one of the following:
%       'none'         - (default) 
%       'nullspace'    - nullspace boundary condition method 
%       'stiff-spring' - boundary condition method
%
%    FEM is a struct containing the following fields for 'none' form
%           K: Stiffness matrix 
%           A: Absorbtion/Reaction matrix
%           F: Force matrix  
%           Q: Neumann boundary condition matrix  
%           G: Neumann boundary condition vector 
%           H: Dirichlet boundary condition matrix  
%           R: Dirichlet boundary condition vector 
%           M: Mass matrix  
%
%    FEM is a struct containing the following fields for 'nullspace' form
%           Kc: Stiffness matrix  
%           Fc: Force matrix
%           B:  Dirichlet nullspace matrix
%           ud: Dirichlet vector
%           M: Mass matrix  
%
%    FEM is a struct containing the following fields for 'stiff-spring' form
%           Ks: Stiffness matrix  
%           Fs: Force matrix
%           M: Mass matrix  
%
% See also pde.PDEModel
%

% Copyright 2015 The MathWorks, Inc.

narginchk(1, 2);
BCEnforcementOption = 'none';
if nargin == 2
     if ischar(varargin{1})
        bctype = varargin{1};
        nc = numel(bctype);
        if nc == 1 && strncmpi(bctype,'n',nc)
           error(message('pde:pdeModel:ambiguousBcmethodType')); 
        end
        if ~(strncmpi(bctype,'none',nc) || strncmpi(bctype,'nullspace',nc) || strncmpi(bctype,'stiff-spring',nc))
            error(message('pde:pdeModel:invalidBcmethodType'));   
        end
        if strncmpi(bctype,'nullspace',nc)
            BCEnforcementOption = 'nullspace';         
        elseif strncmpi(bctype,'stiff-spring',nc)
            BCEnforcementOption = 'stiff-spring';
        end        
    else
        error(message('pde:pdeModel:invalidBcmethodType')); 
    end  
end

FEMatricesOut = assembleFEMatricesInternal(pdem,BCEnforcementOption);

end