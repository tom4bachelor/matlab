% FunctionCoefficientFormat - Describes function format for equation coefficients
%     The PDE toolbox can solve equations of the form:
%  
%              m*d^2u/dt^2 + d*du/dt - div(c*grad(u)) + a*u = f
%  
%     and the corresponding eigenvalue equation of the form:
%  
%                   - div(c*grad(u)) + a*u = lamda*d*u
%     or
%                   - div(c*grad(u)) + a*u = (lamda^2)*m*u
%
% The equation coefficients for a PDEModel (m, d, c, a, f) are specified 
% using the specifyCoefficients function. The coefficients can be specified 
% in numeric format or as a MATLAB function handle that returns coefficient 
% values at locations within the domain. This help section describes the
% function coefficient format for the coefficients.
%
% For analyses defined using a PDEModel, a function handle representation 
% of an equation coefficient is required to accept two input argument and 
% return a single output argument. The function is of the form:
%    
%                   coef = coeffcn(location, state)
%
% The location argument is a struct that defines the location within the 
% domain where the coefficients are evaluated. The fields of the location
% struct are as follows: 
%   - location.x - x-coordinates of the evaluation points
%   - location.y - y-coordinates of the evaluation points
%   - location.z - z-coordinates of the evaluation points, 3-D only
%   - location.subdomain - subdomain ID corresponding to the points
%
% The fields of the state struct are as follows: 
%   - state.u    - The solution at the locations 
%   - state.ux   - The x-component of the solution gradient
%   - state.uy   - The y-component of the solution gradient
%   - state.uz   - The z-component of the solution gradient, 3-D only
%   - state.time    - The time at which the solution is computed       
% 
% The output, coef, defines the coefficients at the requested locations.
% Each j'th column of the coef matrix defines the coefficients at the
% corresponding j'th location in the location struct. The coef matrix has 
% a size of MC-by-NL, where MC is the number of entries required to define 
% the coefficient and NL is the number of locations. Refer to the 
% documentation for information on the numeric form of the coefficient
% data or see help on NumericCoefficientFormat.
%
% See also NumericCoefficientFormat, pde.PDEModel/specifyCoefficients

% Copyright 2015 The MathWorks, Inc. 


