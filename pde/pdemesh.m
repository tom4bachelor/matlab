function h=pdemesh(varargin) %p,e,t,u
%PDEMESH Plot a PDE mesh.
%   PDEMESH(P,E,T) plots the mesh specified by P, E, and T.
%
%   PDEMESH(P,E,T,U) plots the solution column vector U using
%   a mesh plot.  If U is a column vector, node data is
%   assumed. If U is a row vector, element data is assumed.
%   This command plots the solution substantially faster than the
%   PDESURF command.
%
%   H=PDEMESH(P,E,T) additionally returns handles to the plotted
%   graphics objects.
%
%   PDEMESH(MSH,...) Allows the mesh to be defined by a pde.FEMesh.
%
%   PDEMESH(PDEM,...) Allows the mesh to be defined by PDEM, a 
%   pde.PDEModel object that contains the mesh.
%
%   PDEMESH(...,'Name1',Value1, 'Name2',Value2) uses name-value pairs to 
%   support the following mesh plot options when plotting the mesh in
%   isolation without the solution.
%   
%   Name        Value/{Default}   Description
%   ----------------------------------------------------------------
%   NodeLabels      {off} | on    - Display node labels.
%   ElementLabels   {off} | on    - Display element labels.
%   FaceAlpha       data          - Transparency of faces.
%                                   Scalar in the range [0, 1], default = 1
%
%
%   See also initmesh, CREATEPDE, pde.PDEModel/generateMesh, pde.FEMesh
%            pdeplot, pdeplot3D

%       A. Nordmark 4-26-94, LL 1-30-95.
%       Copyright 1994-2016 The MathWorks, Inc.

if nargin < 1
  error(message('pde:pdemesh:nargin'))
end

if isa(varargin{1}, 'pde.EquationModel')   
    thepde = varargin{1};
    isTwoDim = thepde.IsTwoD;
    if ~isa(thepde.Mesh, 'pde.FEMesh')
      error(message('pde:pdemesh:UnmeshedPdeModel')) 
    end
    if nargin > 1 && isfloat(varargin{2})
        if isTwoDim
            paramname = 'ZData';
        else
            paramname = 'ColormapData';
        end    
        argsToPass = {varargin{1}, paramname, varargin{2}}; 
    else
      argsToPass = varargin;
    end    
    if isTwoDim
        hh=pdeplot(argsToPass{:});
    else
        hh=pdeplot3D(argsToPass{:});
    end
elseif isa(varargin{1},'pde.FEMesh') 
    argsToPass = varargin;
    themsh = varargin{1};   
    if ~isa(themsh, 'pde.FEMesh')
      error(message('pde:pdemesh:UnmeshedPdeModel')) 
    end
    isTwoDim = (size(themsh.Nodes,1) == 2);
    if nargin > 1 && isfloat(varargin{2})
        if isTwoDim
            paramname = 'ZData';
        else
            paramname = 'ColormapData';
        end       
        argsToPass = {varargin{1}, paramname, varargin{2}};           
    end    
    if isTwoDim
        hh=pdeplot(argsToPass{:});
    else
        hh=pdeplot3D(argsToPass{:});
    end
else
   if nargin < 3
     error(message('pde:pdemesh:InvalidArgs')) 
   end  
   argsToPass = varargin;
   p =  varargin{1};  
   if nargin > 3 
       if isfloat(varargin{4})
           if size(p,1) == 2  
                paramname = 'ZData';
                argsToPass = {varargin{1:3}, paramname, varargin{4}};    
           else
                paramname = 'ColormapData';
                argsToPass = {varargin{1}, varargin{3}, paramname, varargin{4}};    
           end       
       end
   end      
   if size(p,1) == 2  
     hh=pdeplot(argsToPass{:});
   else
     hh=pdeplot3D(argsToPass{:});
   end  
end

if nargout==1
  h=hh;
end






