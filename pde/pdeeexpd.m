function f=pdeeexpd(x,y,s,nx,ny,u,t,f)
%PDEEEXPD Evaluate an expression on edges.

%       A. Nordmark 12-21-94.
%       Copyright 1994-2011 The MathWorks, Inc.

if ischar(f)
  fold=f;
try 
    f=eval(fold);
catch
    error(message('pde:pdeeexpd:InvalidExprForBC', sprintf('%s', fold)));   
end
  if any(size(f)-[1 1]) && any(size(f)-size(x))
    error(message('pde:pdeeexpd:InvalidExprSizeForBC', sprintf('%s', fold)));
  end
end

if length(f)==1
  f=f.*ones(1,length(x));
end

