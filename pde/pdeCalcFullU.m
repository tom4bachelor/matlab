function [uf, isConverged] = pdeCalcFullU(b,p,e,ur,t,H,R, rtol, atol)
%pdeCalcFullU This function is used by the parabolic and hyperbolic
% functions to transform the reduced solution with constrained DOFs
% removed to the full solution with all DOFs at all FE nodes.
%
% When the H or R matrices are functions of the dependent variables
% (i.e. a nonlinear system), multiple iterations may be required to
% obtain a converged solution.
% This undocumented function may be removed in a future release.

%       Copyright 2012 The MathWorks, Inc.

maxIter = 50;
ufi = zeros(size(H,2),1);
for i=1:maxIter
  [null,orth]=pdenullorth(H);
  ud=full(orth*((H*orth)\R));
  uf=null*ur + ud;
  err=max(abs(ufi-uf));
  isConverged = all(abs(ufi-uf) < max(rtol*abs(uf), atol));
  if(isConverged)
    break;
  end
  ufi=uf;
  [~,~,H,R]=assemb(b,p,e,uf,t);
end
if(~isConverged)
  warning(message('pde:pdeCalcFullU:unconverged', ...
    sprintf('%12.3e', err)));
end
end