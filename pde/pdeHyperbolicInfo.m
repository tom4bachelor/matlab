classdef pdeHyperbolicInfo < pdeODEInfo
  %pdeHyperbolicInfo Internal class used to communicate data to ode15s helper functions.
  % This undocumented function may be removed in a future release.
  
  %       Copyright 2012-2013 The MathWorks, Inc.
  
    properties
      C; %global damping matrix
    end

  
  methods
        
    function obj=pdeHyperbolicInfo(u0,ut0,tlist,b,p,e,t,c,a,f,d)
      obj=obj@pdeODEInfo(tlist, nargin<11);
      if(nargin==11 || nargin==12 || nargin==13)
        % Handle case where coefficients are passed to hyperbolic
        obj.b=b; obj.p=p; obj.e=e; obj.t=t; obj.c=c; obj.a=a; obj.f=f; obj.d=d;
        obj=obj.checkFuncDepen(u0, tlist);
        if ~(obj.vq || obj.vg || obj.vh || obj.vr || ...
               obj.vc || obj.va || obj.vf)
             nun=obj.numConstrainedEqns;
             [KK,FF,B,ud]=assempde(obj.K,obj.M,obj.F,obj.Q,obj.G,obj.H,obj.R);
             obj.K=[sparse(nun,nun) -speye(nun,nun); KK sparse(nun,nun)];
             obj.F=[zeros(nun,1);FF];
        end
      elseif(nargin==8 || nargin==9 || nargin==10)
        % Handle case where global matrices are passed to hyperbolic
        obj.K=b;
        obj.F=p;
        obj.B=e;
        obj.ud=t;
        obj.MM=c;
        nun=size(obj.K,2);
        obj.totalNumEqns = nun;
        obj.K=[sparse(nun,nun) -speye(nun,nun); obj.K sparse(nun,nun)];
        obj.F=[zeros(nun,1);obj.F];
      else
        error(message('pde:hyperbolic:nargin'))
      end
    end
        
  end
  
end

