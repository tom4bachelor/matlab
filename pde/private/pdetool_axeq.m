function pdetool_axeq()
% case: set axes equal on/off

%   Copyright 1994-2013 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    set(pde_fig,'CurrentAxes',ax);

    opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
    h=findobj(get(opthndl,'Children'),'flat','Tag','PDEAxeq');
    if pdeumtoggle(h)
        % Axis equal
        axis equal
    else
        % Axis normal
        set(ax,...
            'DataAspectRatioMode','auto',...
            'PlotBoxAspectRatioMode','auto',...
            'CameraViewAngleMode','auto')
    end

    set(get(ax,'ZLabel'),'UserData',[]);
end