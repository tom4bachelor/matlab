function pdetool_circlecntr(flag)
% case: click to draw circle

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    if ~pdeonax, return, end

    user_data=get(pde_fig,'UserData');

    % flag: 1=drag from perimeter, 2=drag from center
    if flag==1
        if strcmp(get(pde_fig,'SelectionType'),'alt') %circle
            set(pde_fig,'WindowButtonMotionFcn','pdemtncb(-3)');
            set(pde_fig,'WindowButtonUpFcn','pdetool(''circledraw'',1)');
        else                                % ellipse
            set(pde_fig,'WindowButtonMotionFcn','pdemtncb(3)');
            set(pde_fig,'WindowButtonUpFcn','pdetool(''circledraw'',2)');
        end
    elseif flag==2                       % circle
        if strcmp(get(pde_fig,'SelectionType'),'alt')
            set(pde_fig,'WindowButtonMotionFcn','pdemtncb(-4)');
            set(pde_fig,'WindowButtonUpFcn','pdetool(''circledraw'',3)');
        else                                % ellipse
            set(pde_fig,'WindowButtonMotionFcn','pdemtncb(4)');
            set(pde_fig,'WindowButtonUpFcn','pdetool(''circledraw'',4)');
        end
    end

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    set(pde_fig,'CurrentAxes',ax)
    pv=get(ax,'CurrentPoint');
    [xc(1),xc(2)]=pdesnap(ax,pv,getappdata(ax,'snap'));

    t=0:pi/50:2*pi;
    radius=0;
    xt=sin(t)*radius+xc(1); yt=cos(t)*radius+xc(2);
    hold on;
    hndl=line(xt,yt,'Color','r');
    hold off;
    set(ax,'UserData',hndl)

    user_data(3:4)=xc;
    set(pde_fig,'UserData',user_data)
end