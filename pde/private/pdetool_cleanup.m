function pdetool_cleanup()
% case: clean up draw-mode features. Called from 'changemode' and
%       when entering a solve mode action through a short-cut path.

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    pde_kids = allchild(pde_fig);
    ax=findobj(pde_kids,'flat','Tag','PDEAxes');

    % reset pushed draw button at mode change
    btst=btnstate(pde_fig,'draw');
    drawhndl=findobj(pde_kids,'flat','Tag','PDEDrawMenu');
    h=[findobj(get(drawhndl,'Children'),'flat','Tag','PDERect')...
        findobj(get(drawhndl,'Children'),'flat','Tag','PDERectc')...
        findobj(get(drawhndl,'Children'),'flat','Tag','PDEEllip')...
        findobj(get(drawhndl,'Children'),'flat','Tag','PDEEllipc')...
        findobj(get(drawhndl,'Children'),'flat','Tag','PDEPoly')];
    for i=find(btst), btnup(pde_fig,'draw',i), set(h(i),'Checked','off'), end

    axKids=get(ax,'Children');
    lines=findobj(axKids,'flat','Tag','PDELine');
    % Erase any unfinished polygon lines
    if ~isempty(lines)
        delete(lines)
        user_data=get(pde_fig,'UserData');
        user_data=[user_data(1:4) zeros(1,4)];
        set(pde_fig,'UserData',user_data)
    end

    % disable 'Rotate...':
    set(findobj(get(drawhndl,'Children'),'flat','Tag','PDERotate'),...
        'Enable','off')

    evalhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEval');
    set(evalhndl,'Enable','off');

    % disable edit menu items:
    edithndl=findobj(pde_kids,'flat','Tag','PDEEditMenu');
    hndls=[findobj(get(edithndl,'Children'),'flat','Tag','PDEUndo')...
        findobj(get(edithndl,'Children'),'flat','Tag','PDECut')...
        findobj(get(edithndl,'Children'),'flat','Tag','PDECopy')...
        findobj(get(edithndl,'Children'),'flat','Tag','PDEPaste')...
        findobj(get(edithndl,'Children'),'flat','Tag','PDEClear')];
    set(hndls,'Enable','off')

    % turn off the drawing objects:
    axKids=get(ax,'Children');
    hndl=[findobj(axKids,'flat','Tag','PDEMinReg')'...
        findobj(axKids,'flat','Tag','PDELine')'...
        findobj(axKids,'flat','Tag','PDESelFrame')'...
        findobj(axKids,'flat','Tag','PDELabel')'...
        findobj(axKids,'flat','Tag','PDELblSel')'];
    set(hndl,'Visible','off')

    % change colormap:
    set(pde_fig,'Colormap',get(get(ax,'Ylabel'),'UserData'))

    % turn off WindowButtonDownFcn
    set(pde_fig,'WindowButtonDownFcn','');
end