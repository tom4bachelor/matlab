function pdetool_showsublbl(flag)
% case: display subdomain labels for decomposed geometry

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(allchild(pde_fig),'flat','Tag','PDEAxes');
    set(pde_fig,'CurrentAxes',ax)

    h=findobj(get(ax,'Children'),'flat','Tag','PDESubLabel');
    delete(h)

    if(flag)     % flag is set: display labels

        bt1=getappdata(pde_fig,'bt1');

        p1=getappdata(pde_fig,'pinit2');
        t1=getappdata(pde_fig,'tinit2');
        numSub=size(bt1,1);
        pdePlotSubLabels(p1, t1, numSub, ax, false);

    end
end