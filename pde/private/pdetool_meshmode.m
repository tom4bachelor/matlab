function pdetool_meshmode()
% case: enter mesh mode (display mesh, create if non-existent)

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    % if no geometry, create a default L-shape:
    pdegd=get(findobj(get(pde_fig,'Children'),'flat',...
        'Tag','PDEMeshMenu'),'UserData');
    if isempty(pdegd), pdetool('membrane'), end

    if btnstate(pde_fig,'zoom',1)
        pdezoom(pde_fig,'off')
        btnup(pde_fig,'zoom',1)
        opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
        set(findobj(get(opthndl,'Children'),'flat','Tag','PDEZoom'),...
            'Checked','off')
    end

    ax=findobj(allchild(pde_fig),'flat','Tag','PDEAxes');
    set(pde_fig,'CurrentAxes',ax,'WindowButtonDownFcn','')

    flg_hndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(flg_hndl,'UserData');
    flag1=flags(3);
    if abs(flag1)
        pdetool('changemode',0)
    end
    flags=get(flg_hndl,'UserData');
    if flags(3)==-1
        % error in decsg
        return;
    elseif ~flags(2)
        pdetool('cleanup')
    end
    flags=get(flg_hndl,'UserData');
    flag2=flags(4);
    if flag2
        pdetool('initmesh')
    else
        % Hide decomposition lines (=subdomain boundaries), boundary lines,
        % labels, and subdomains:
        hKids=get(ax,'Children');
        h=[findobj(hKids,'flat','Tag','PDEBoundLine')'...
            findobj(hKids,'flat','Tag','PDESubLabel')'...
            findobj(hKids,'flat','Tag','PDEEdgeLabel')'...
            findobj(hKids,'flat','Tag','PDESubDom')'...
            findobj(hKids,'flat','Tag','PDEBorderLine')'...
            findobj(hKids,'flat','Tag','PDESelRegion')'];
        set(h,'Visible','off')

        flags=get(flg_hndl,'UserData');
        flags(2)=2;                         % mode_flag=2
        set(flg_hndl,'UserData',flags)

        pdetool('clearsol')

        drawnow

        h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
        hp=findobj(get(h,'Children'),'flat','Tag','PDEInitMesh');
        he=findobj(get(h,'Children'),'flat','Tag','PDERefine');
        ht=findobj(get(h,'Children'),'flat','Tag','PDEMeshParam');
        p=get(hp,'UserData'); e=get(he,'UserData'); t=get(ht,'UserData');

        % erase old mesh
        h=findobj(get(ax,'Children'),'flat','Tag','PDEMeshLine');
        delete(h)

        % create and plot the mesh
        h=pdeplot(p,e,t,'intool','on');
        set(h,'Tag','PDEMeshLine');

        % if requested, label nodes and/or triangles:
        if getappdata(pde_fig,'shownodelbl')
            pdetool('shownodelbl',1)
        end
        if getappdata(pde_fig,'showtrilbl')
            pdetool('showtrilbl',1)
        end
    end

    edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
    set([edithndl findobj(get(edithndl,'Children'),'flat','Tag','PDESelall')],...
        'Enable','off')

    % Enable clicking on mesh to get info about triangle and node no's:
    set(pde_fig,'WindowButtonDownFcn','pdeinfclk(1)',...
        'WindowButtonUpFcn','if pdeonax, pdeinfo; end')

    pdeinfo('Refine or jiggle mesh, label mesh nodes and triangles, display mesh quality plot.');
end