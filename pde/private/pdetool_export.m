function pdetool_export(flag)
% case: export variables to workspace

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    if flag==1
        % export geometry data:
        gd=get(findobj(get(pde_fig,'Children'),'flat',...
            'Tag','PDEMeshMenu'),'UserData');
        ns=getappdata(pde_fig,'objnames');
        evalhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEval');
        sf=get(evalhndl,'String');
        matqueue('put',gd,sf,ns)
        pstr='Variable names for geometry data, set formula, labels:';
        estr='gd sf ns';
    elseif flag==2
        % export decomposed list, boundary conditions:
        dl1=getappdata(pde_fig,'dl1');
        h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEBoundMenu');
        bl=get(findobj(get(h,'Children'),'flat',...
            'Tag','PDEBoundMode'),'UserData');
        matqueue('put',dl1,bl)
        pstr='Variable names for decomposed geometry, boundary cond''s:';
        estr='g b';
    elseif flag==3
        % export mesh:
        h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
        p=get(findobj(get(h,'Children'),'flat','Tag','PDEInitMesh'),...
            'UserData');
        e=get(findobj(get(h,'Children'),'flat','Tag','PDERefine'),...
            'UserData');
        t=get(findobj(get(h,'Children'),'flat','Tag','PDEMeshParam'),...
            'UserData');
        matqueue('put',p,e,t)
        pstr='Variable names for mesh data (points, edges, triangles):';
        estr='p e t';
    elseif flag==4
        % export PDE coefficients:
        params=get(findobj(get(pde_fig,'Children'),'Tag','PDEPDEMenu'),...
            'UserData');
        ns=getappdata(pde_fig,'ncafd');
        nc=ns(1); na=ns(2); nf=ns(3); nd=ns(4);
        c=params(1:nc,:);
        a=params(nc+1:nc+na,:);
        f=params(nc+na+1:nc+na+nf,:);
        d=params(nc+na+nf+1:nc+na+nf+nd,:);
        matqueue('put',c,a,f,d)
        pstr='Variable names for PDE coefficients:';
        estr='c a f d';
    elseif flag==5
        % export solution:
        u=get(findobj(get(pde_fig,'Children'),'flat','Tag','PDEPlotMenu'),...
            'UserData');
        l=get(findobj(get(pde_fig,'Children'),'flat','Tag','winmenu'),...
            'UserData');
        if isempty(l)
            pstr='Variable name for solution:';
            estr='u';
            matqueue('put',u)
        else
            pstr='Variable names for solution and eigenvalues:';
            estr='u l';
            matqueue('put',u,l)
        end
    elseif flag==6
        % export movie:
        M=getappdata(pde_fig,'movie');
        matqueue('put',M)
        pstr='Variable name for PDE solution movie:';
        estr='M';
    end
    pdeinfo('Change the variable name(s) if desired. OK when done.',0);
    matqdlg('buffer2ws','Name','Export','PromptString',pstr,...
        'OKCallback','pdeinfo;','CancelCallback','pdeinfo;','EntryString',estr);
end