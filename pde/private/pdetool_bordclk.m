function pdetool_bordclk()
% case: click on subdomain border to select border

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    % if in Zoom-mode, let Zoom do its thing and return immediately
    if btnstate(pde_fig,'zoom',1), return, end

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    border_sel=getappdata(ax,'bordsel');

    % case: shift-click (allow selection of more than one border segment)
    if strfind(get(pde_fig,'SelectionType'),'extend')

        curr_border=gco;
        set(curr_border,'color','k')

        if isempty(border_sel)
            border_sel = curr_border;
        else
            if isempty(find(curr_border==border_sel, 1))
                border_sel=[border_sel curr_border];
            else
                % if already selected, de-select
                selcol=find(border_sel==curr_border);
                border_sel=[border_sel(1:selcol-1) ...
                    border_sel(selcol+1:length(border_sel))];
                set(curr_border,'color',0.5*ones(1,3))
            end
        end

        setappdata(ax,'bordsel',border_sel)

        % case: select
    elseif strfind(get(pde_fig,'SelectionType'),'normal')
        % if already selected, do nothing; else, select
        curr_border=gco;

        if isempty(border_sel) || isempty(find(border_sel==curr_border, 1))
            set(curr_border,'color','k')
            borders=findobj(get(ax,'Children'),'flat','Tag','PDEBorderLine');
            indx=find(borders~=curr_border)';
            set(borders(indx),'color',0.5*ones(1,3))
            setappdata(ax,'bordsel',curr_border)
        end
    end
end