function pdetool_export_image()
% case: export image

%   Copyright 1994-2013 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    supFileTypes = {
        '*.bmp', getString(message('pde:pdetool:bmpDesc')), '-dbmp'
        '*.eps', getString(message('pde:pdetool:epsDesc')), '-deps'
        '*.emf', getString(message('pde:pdetool:emfDesc')), '-dmeta'
        '*.jpg', getString(message('pde:pdetool:jpgDesc')),  '-djpeg'
        '*.pcx', getString(message('pde:pdetool:pcxDesc')), '-dpcx24b'
        '*.pbm', getString(message('pde:pdetool:pbmDesc')),  '-dpbm'
        '*.pdf', getString(message('pde:pdetool:pdfDesc')),  '-dpdf'
        '*.pgm', getString(message('pde:pdetool:pgmDesc')),  '-dpgm'
        '*.png', getString(message('pde:pdetool:pngDesc')), '-dpng'
        '*.ppm', getString(message('pde:pdetool:ppmDesc')),  '-dppm'
        '*.tif', getString(message('pde:pdetool:tifDesc')),  '-dtiff'
        '*.tif', getString(message('pde:pdetool:tifNoCompDesc')),  '-dtiffn'
        };
    [exFileName,exPathName,exFilterIndex] = uiputfile(supFileTypes(:,1:2),...
        getString(message('pde:pdetool:saveDialogTitle')), '');

    if(exFilterIndex > 0)
        printFig = pdetoolGetViewportFig(pde_fig);
        print(printFig, [exPathName, exFileName], '-noui', supFileTypes{exFilterIndex,3});
        delete(printFig);
    end
end