function pdetool_showtrilbl(flag)
% case: display mesh triangle labels

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(allchild(pde_fig),'flat','Tag','PDEAxes');
    set(pde_fig,'CurrentAxes',ax)

    set(pde_fig,'Pointer','watch')
    drawnow

    h=getappdata(pde_fig,'trihandles');
    delete(h)

    if flag                              % flag is set: display labels

        % get node data for the current mesh
        h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
        hp=findobj(get(h,'Children'),'flat','Tag','PDEInitMesh');
        ht=findobj(get(h,'Children'),'flat','Tag','PDEMeshParam');
        p=get(hp,'UserData'); t=get(ht,'UserData');

        scale=min(diff(get(ax,'Xlim')),diff(get(ax,'Ylim')));
        
        if (isempty(p) || isempty(t))
            return; % return if there is no mesh.
        end
        
        ind=t(:,1)'; ind=[ind(1:3), ind(1)];
        
        trilength=max(mean(abs(diff(p(1,ind)))),mean(abs(diff(p(2,ind)))));
        if trilength/scale>0.06
            fsize=10;
        elseif trilength/scale>0.04
            fsize=8;
        elseif trilength/scale>0.02
            fsize=6;
        elseif trilength/scale>0.01
            fsize=5;
        elseif trilength/scale>0.005
            fsize=4;
        else
            fsize=3;
        end

        n=size(t,2);
        for i=1:n
            xm=mean(p(1,t(1:3,i)));
            ym=mean(p(2,t(1:3,i)));
            txt(i)=text('String',int2str(i),'Units','data',...
                'Clipping','on',...
                'Tag','PDETriLabel','HorizontalAlignment','center',...
                'FontSize',fsize,'pos',[xm ym 1],'color','k',...
                'Parent',ax); %#ok - suppress preallocation warning
        end
        setappdata(pde_fig,'trihandles',txt)
    else
        setappdata(pde_fig,'trihandles',[])
    end

    set(pde_fig,'Pointer','arrow')
    drawnow
end