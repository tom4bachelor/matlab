function pdetool_helponoff()
% case: turn toolbar help on/off

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
    h=findobj(get(opthndl,'Children'),'flat','Tag','PDEHelpoff');

    if getappdata(pde_fig,'toolhelp')
        set(h,'Checked','on')
        setappdata(pde_fig,'toolhelp',0)
        pdeinfo;
    else
        set(h,'Checked','off')
        setappdata(pde_fig,'toolhelp',1)
    end
end