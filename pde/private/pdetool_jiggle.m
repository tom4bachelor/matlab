function pdetool_jiggle()
% case: jiggle mesh

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    % if no geometry, create a default L-shape:
    pdegd=get(findobj(get(pde_fig,'Children'),'flat',...
        'Tag','PDEMeshMenu'),'UserData');
    if isempty(pdegd), pdetool('membrane'), end

    if btnstate(pde_fig,'zoom',1)
        pdezoom(pde_fig,'off')
        btnup(pde_fig,'zoom',1)
        opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
        set(findobj(get(opthndl,'Children'),'flat','Tag','PDEZoom'),...
            'Checked','off')
    end

    flg_hndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(flg_hndl,'UserData');
    flag1=flags(3);
    if abs(flag1)
        pdetool('changemode',0)
    end
    flags=get(flg_hndl,'UserData');
    if flags(3)==-1
        % error in decsg
        return;
    elseif ~flags(2)
        pdetool('cleanup')
    end
    flags=get(flg_hndl,'UserData');
    flag2=flags(4);
    if flag2
        pdetool('initmesh')
    end
    flags=get(flg_hndl,'UserData');
    flags(2)=2;                           % mode_flag=2
    set(flg_hndl,'UserData',flags)

    ax=findobj(allchild(pde_fig),'flat','Tag','PDEAxes');

    pdeinfo('Jiggling mesh...',0);
    set(pde_fig,'CurrentAxes',ax,'Pointer','watch','WindowButtonDownFcn','')
    drawnow

    pdetool('clearsol')

    % Hide decomposition lines (=subdomain boundaries), boundary lines,
    % labels, and subdomains:
    hKids=get(ax,'Children');
    h=[findobj(hKids,'flat','Tag','PDEBoundLine')'...
        findobj(hKids,'flat','Tag','PDESubLabel')'...
        findobj(hKids,'flat','Tag','PDEEdgeLabel')'...
        findobj(hKids,'flat','Tag','PDESubDom')'...
        findobj(hKids,'flat','Tag','PDEBorderLine')'...
        findobj(hKids,'flat','Tag','PDESelRegion')'];
    set(h,'Visible','off')

    drawnow

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
    hp=findobj(get(h,'Children'),'flat','Tag','PDEInitMesh');
    he=findobj(get(h,'Children'),'flat','Tag','PDERefine');
    ht=findobj(get(h,'Children'),'flat','Tag','PDEMeshParam');
    p=get(hp,'UserData'); e=get(he,'UserData'); t=get(ht,'UserData');

    % erase old mesh plot
    h=findobj(get(ax,'Children'),'flat','Tag','PDEMeshLine');
    delete(h)

    % jiggle the mesh
    jiggleparam=getappdata(pde_fig,'jiggle');
    jiggleopt=deblank(jiggleparam(2,:));
    if strcmp(jiggleopt,'on')
        jiggleopt='off';
    end
    jiggleiter=deblank(jiggleparam(3,:));
    if isempty(jiggleiter)
        p=jigglemesh(p,e,t,'opt',jiggleopt);
    else
        p=jigglemesh(p,e,t,'opt',jiggleopt,'iter',str2double(jiggleiter));
    end

    meshhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
    h=findobj(get(meshhndl,'Children'),'flat','Tag','PDEUnrefine');
    set(h,'Enable','on');

    % plot the new mesh
    h=pdeplot(p,e,t,'intool','on');
    set(h,'Tag','PDEMeshLine');

    set(hp,'UserData',p)

    % if requested, label nodes and/or triangles:
    if getappdata(pde_fig,'shownodelbl')
        pdetool('shownodelbl',1)
    end
    if getappdata(pde_fig,'showtrilbl')
        pdetool('showtrilbl',1)
    end

    flags=get(flg_hndl,'UserData');
    flags(1)=1;                           % need_save=1
    flags(5)=1;                           % flag3=1
    flags(6)=0;                           % flag4=0
    set(flg_hndl,'UserData',flags)

    meshstat=getappdata(pde_fig,'meshstat');
    meshstat=[meshstat 3];
    n=length(meshstat);

    setappdata(pde_fig,'meshstat',meshstat);

    % save this generation of the mesh for unrefine purposes
    setappdata(pde_fig,['p' int2str(n)],p);
    setappdata(pde_fig,['e' int2str(n)],e);
    setappdata(pde_fig,['t' int2str(n)],t);

    set(pde_fig,'Pointer','arrow')
    drawnow

    infostr=sprintf('Jiggled mesh consists of %i nodes and %i triangles.',...
        size(p,2),size(t,2));
    pdeinfo(infostr);

    edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
    set([edithndl findobj(get(edithndl,'Children'),'flat','Tag','PDESelall')],...
        'Enable','off')

    % Enable clicking on mesh to get info about triangle and node no's:
    set(pde_fig,'WindowButtonDownFcn','pdeinfclk(1)',...
        'WindowButtonUpFcn','if pdeonax, pdeinfo; end')
end