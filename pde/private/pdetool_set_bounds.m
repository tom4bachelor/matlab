function pdetool_set_bounds()
% case: set boundary condition parameter values

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    appl=getappdata(pde_fig,'application');
    boundequ=getappdata(pde_fig,'boundeq');
    descr=getappdata(pde_fig,'bounddescr');

    systmtx=char('g1','g2','q11, q12','q21, q22','h11, h12','h21, h22',...
        'r1','r2');
    scalarmtx=char('g','q','h','r');
    set(pde_fig,'Pointer','watch')
    drawnow
    if ispc
        pderel
    end
    if appl==1
        pdebddlg('initialize',[],1,1:2,boundequ,scalarmtx,descr)
    elseif appl==2 || appl==3 || appl==4
        pdebddlg('initialize',[],0,1:3,boundequ,systmtx,descr)
    elseif appl>4
        pdebddlg('initialize',[],1,1:2,boundequ,scalarmtx,descr)
    end
    set(pde_fig,'Pointer','arrow')
    drawnow
end