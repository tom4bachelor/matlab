function pdetool_undo()
% case: undo last line

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    user_data=get(pde_fig,'UserData');
    i=user_data(5);
    x=user_data(6:6+i-1); y=user_data(6+i:6+2*i-1);
    line_hndls=user_data(6+2*i:6+3*i-2);

    %lines
    if i>1
        set(line_hndls(i-1),'Visible','off');
        i=i-1;
    end

    if i==1
        edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
        hndl=findobj(get(edithndl,'Children'),'flat','Tag','PDEUndo');
        set(hndl,'Enable','off');
        i=0;
    end

    user_data=[user_data(1:4) i x(1:i) y(1:i) line_hndls(1:i-1)];
    set(pde_fig,'UserData',user_data);
end