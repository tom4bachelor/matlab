function pdetool_boundclk(flag) %#ok, used as switch with nargin
% case: click on boundary to select boundary and
%       open boundary condition dialog box

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    % if in Zoom-mode, let Zoom do its thing and return immediately
    if btnstate(pde_fig,'zoom',1), return, end

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    bndhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEBoundMenu');
    selecth=findobj(get(bndhndl,'Children'),'flat','Tag','PDEBoundSpec');

    % case: select all (flag is set)
    if nargin>0
        hndl=findobj(get(ax,'Children'),'flat','Tag','PDEBoundLine');
        if ~isempty(hndl)
            set(hndl,'color','k');
            set(selecth,'UserData',hndl');
        end

        % case: double-click to open boundary condition dialog box
    elseif strfind(get(pde_fig,'SelectionType'),'open')

        pdetool('set_bounds')

        % case: shift-click (allow selection of more than one boundary segment)
    elseif strfind(get(pde_fig,'SelectionType'),'extend')

        curr_bound=gco;
        set(curr_bound,'color','k')

        pde_bound_sel=get(selecth,'UserData');
        if isempty(pde_bound_sel) || isempty(find(curr_bound==pde_bound_sel, 1))
            pde_bound_sel=[pde_bound_sel curr_bound];
        else
            % if already selected, de-select
            selcol=find(pde_bound_sel==curr_bound);
            pde_bound_sel=[pde_bound_sel(1:selcol-1) ...
                pde_bound_sel(selcol+1:length(pde_bound_sel))];
            col=get(curr_bound,'UserData');
            set(curr_bound,'color',col(2:4))
        end

        set(selecth,'UserData',pde_bound_sel)

        % case: select
    else
        % if already selected, do nothing; else, select
        curr_bound=gco;

        pde_bound_sel=get(selecth,'UserData');
        if isempty(pde_bound_sel) || isempty(find(pde_bound_sel==curr_bound, 1))
            set(curr_bound,'color','k')
            bounds=findobj(get(ax,'Children'),'flat','Tag','PDEBoundLine');
            indx=find(bounds~=curr_bound)';
            for i=indx
                col=get(bounds(i),'UserData');
                set(bounds(i),'color',col(2:4))
            end
            set(selecth,'UserData',curr_bound);
        end
    end
end