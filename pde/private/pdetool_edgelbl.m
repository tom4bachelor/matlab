function pdetool_edgelbl()
% case: manage edge labeling flags and indication

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(h,'UserData'); mode_flag=flags(2);

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEBoundMenu');
    edgeh=findobj(get(h,'Children'),'flat','Tag','PDEShowEdge');

    if pdeumtoggle(edgeh)
        setappdata(pde_fig,'showedgelbl',1);
        if mode_flag==1
            pdetool('showedgelbl',1)
        end
    else
        setappdata(pde_fig,'showedgelbl',0);
        if mode_flag==1
            pdetool('showedgelbl',0)
        end
    end
end