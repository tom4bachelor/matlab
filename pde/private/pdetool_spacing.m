function pdetool_spacing()
% case: spacing

%   Copyright 1994-2013 The MathWorks, Inc.

  pdeinfo('Set x and y spacing. Separate entries using spaces, commas, semi-colons, or brackets.');
  pdespdlg
  pdeinfo;
end