function pdetool_clearsol()
% case: clear solution plot from PDE axes

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    %if solution display is left on screen, remove it.
    h=findobj(get(pde_fig,'Children'),'flat','Tag','Colorbar');
    if ~isempty(h)
        delete(h);
    end
    ax=findobj(allchild(pde_fig),'flat','Tag','PDEAxes');
    hh=get(ax,'UserData');
    if ~isempty(hh)
        delete(hh)
    end
    set(ax,'UserData',[],'Position',getappdata(ax,'axstdpos'))
    titleh=get(ax,'Title');
    if ~isempty(get(titleh,'String'))
        set(titleh,'String','')
    end
end