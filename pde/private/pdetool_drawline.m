function pdetool_drawline()
% case: draw line (polygon)

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(h,'UserData'); mode_flag=flags(2);
    if mode_flag
        pdetool('changemode',0)
    end

    if btnstate(pde_fig,'zoom',1)
        pdezoom(pde_fig,'off')
        btnup(pde_fig,'zoom',1)
        opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
        set(findobj(get(opthndl,'Children'),'flat','Tag','PDEZoom'),...
            'Checked','off')
    end

    btst=btnstate(pde_fig,'draw',1:5);
    if strcmp(get(pde_fig,'SelectionType'),'open')
        btndown(pde_fig,'draw',5);
        sticks=zeros(6,1);
        sticks(5)=1;
        setappdata(pde_fig,'stick',sticks);
        return;
    else
        drawhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEDrawMenu');
        ghndls=get(drawhndl,'UserData');
        my_hndl=findobj(get(drawhndl,'Children'),'flat','Tag','PDEPoly');

        if pdeumtoggle(my_hndl)
            ghndls=ghndls(ghndls~=my_hndl);
            set(ghndls,'Checked','off')
            if ~btst(5)
                btndown(pde_fig,'draw',5)
                btst(5)=1;
            end
            if btst(5)
                btst(5)=0;
                if any(btst)
                    btnup(pde_fig,'draw',find(btst));
                end
            end
        else
            btnup(pde_fig,'draw',5)
            ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
            axKids=get(ax,'Children');
            lines=findobj(axKids,'flat','Tag','PDELine');
            % Erase any unfinished polygon lines
            if ~isempty(lines)
                delete(lines)
                user_data=get(pde_fig,'UserData');
                user_data=[user_data(1:4) zeros(1,4)];
                set(pde_fig,'UserData',user_data)
            end
            set(pde_fig,'WindowButtonDownFcn','pdeselect select',...
                'WindowButtonUpFcn','')
            setappdata(pde_fig,'stick',zeros(6,1));
            pdeinfo('Draw and edit 2-D geometry by using the Draw and Edit menu options.');
            return;
        end
    end

    pdeinfo(['Click and drag to create lines.'...
        ' Click at starting point to close polygon.']);
    set(pde_fig,'WindowButtonDownFcn','pdetool lineclk',...
        'WindowButtonUpFcn','');
    user_data=get(pde_fig,'UserData');
    i=user_data(5);
    if i>0
        % If unfinished polygon on screen, enable undo:
        edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
        hndl=findobj(get(edithndl,'Children'),'flat','Tag','PDEUndo');
        set(hndl,'Enable','on');
    end
end