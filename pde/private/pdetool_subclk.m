function pdetool_subclk(flag) %#ok - flag used as a switch with nargin
% case: click in subdomain to select subdomain and to define PDE coefficients

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    % if in Zoom-mode, let Zoom do its thing and return immediately
    if btnstate(pde_fig,'zoom',1), return, end

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    if ~pdeonax(ax) && nargin==0, return; end
    set(pde_fig,'CurrentAxes',ax)

    subsel=getappdata(ax,'subsel');
    dl1=getappdata(pde_fig,'dl1');
    hndl=findobj(get(ax,'Children'),'flat','Tag','PDESelRegion');

    p1=getappdata(pde_fig,'pinit2');
    t1=getappdata(pde_fig,'tinit2');
    pv=get(ax,'CurrentPoint');
    x=pv(1,1); y=pv(1,2);
    [~,tn,~,~]=tri2grid(p1,t1,zeros(size(p1,2)),x,y);

    % case: select all (flag is set)
    if nargin>0
        if ~isempty(hndl)
            bt1=getappdata(pde_fig,'bt1');
            set(hndl,'color','k')
            setappdata(ax,'subsel',1:size(bt1,1))
        end

        % case: double-click to open PDE Specification dialog box
    elseif strfind(get(pde_fig,'SelectionType'),'open')

        if ~isnan(tn)
            pdetool('set_param')
        end

        % case: shift-click (allow selection of more than one subdomain)
    elseif strfind(get(pde_fig,'SelectionType'),'extend')

        if isnan(tn)
            % clicked outside of geometry objects: deselect all
            set(hndl,'color','w')
            setappdata(ax,'subsel',[])
            return;
        else
            subreg=t1(4,tn);
        end

        s=find(dl1(6,:)==subreg | dl1(7,:)==subreg);
        h=[];
        for i=1:length(s)
            h=[h; findobj(hndl,'flat','UserData',s(i))]; %#ok - suppressing preallocation warning
        end
        if isempty(subsel) || isempty(find(subsel==subreg, 1))
            subsel=[subsel subreg];
            set(h,'color','k')
        else
            % if already selected, de-select
            selcol=find(subsel==subreg);
            subsel=[subsel(1:selcol-1) ...
                subsel(selcol+1:length(subsel))];
            set(h,'color','w')
        end

        setappdata(ax,'subsel',subsel)

        % case: select
    else
        % if already selected, do nothing; else, select

        if isnan(tn)
            % clicked outside of geometry objects: deselect all
            set(hndl,'color','w')
            setappdata(ax,'subsel',[])
            return;
        else
            subreg=t1(4,tn);
        end

        s=find(dl1(6,:)==subreg | dl1(7,:)==subreg);
        h=[];
        for i=1:length(s)
            h=[h; findobj(hndl,'flat','UserData',s(i))]; %#ok - suppress preallocation warning
        end
        if isempty(subsel) || isempty(find(subsel==subreg, 1))
            set(hndl,'color','w')
            set(h,'color','k')
            setappdata(ax,'subsel',subreg);
        end
    end
end