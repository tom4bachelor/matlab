function pdetool_pdetrilbl()
% case: manage mesh triangle labeling flags and indication

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(h,'UserData'); mode_flag=flags(2);

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
    trih=findobj(get(h,'Children'),'flat','Tag','PDEShowTriangles');

    if pdeumtoggle(trih)
        setappdata(pde_fig,'showtrilbl',1);
        if mode_flag==2
            pdetool('showtrilbl',1)
        end
    else
        setappdata(pde_fig,'showtrilbl',0);
        if mode_flag==2
            pdetool('showtrilbl',0)
        end
    end
end