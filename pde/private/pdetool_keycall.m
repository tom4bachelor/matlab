function pdetool_keycall()
% case: handle callback from key presses

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    currchar=get(pde_fig,'CurrentCharacter');

    if ~isempty(currchar) && (abs(currchar)==127 || abs(currchar)==8)
        % case: backspace or delete
        pdetool('clear')
    end
end