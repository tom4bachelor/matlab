function pdetool_axlim()
% case: change axes limits

%   Copyright 1994-2013 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    pdeinfo('Enter axes limits.');

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');

    xmax=get(ax,'XLim');
    xmin=xmax(1); xmax=xmax(2);
    ymax=get(ax,'YLim');
    ymin=ymax(1); ymax=ymax(2);

    PromptString = char('X-axis range:','Y-axis range:');
    OptFlags=[1, 0; 1, 0];
    DefLim = [xmin xmax; ymin ymax];
    figh=pdeaxlimdlg('Axes Limits',OptFlags,PromptString,[double(ax) NaN double(ax)],['x'; 'y'],...
        DefLim,'pdetool(''applyaxlim''); ');
    set(figh,'Tag','PDEAxLimDlg')
end