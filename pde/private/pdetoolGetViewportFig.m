function figHandle = pdetoolGetViewportFig(pdeFigHandle)
% pdetoolGetViewportFig: HELPER FUNCTION FOR PDETOOL
% create a temporary invisible figure and copy axes from pde window there

%   Copyright 1994-2013 The MathWorks, Inc.
    figHandle=figure('ToolBar','none', 'MenuBar','none', 'Visible', 'off');
    axhPde=findobj(get(pdeFigHandle,'Children'),'flat','Tag','PDEAxes');
    copyobj(axhPde,figHandle);
    set(figHandle,'Colormap', get(pdeFigHandle, 'Colormap'));
    cbarH = findobj(pdeFigHandle,'tag','Colorbar');
    if(ishghandle(cbarH))
        cbarLocation = get(cbarH, 'location');
        axhPrint = findobj(get(figHandle,'Children'),'flat','Tag','PDEAxes');
        colorbar(cbarLocation, 'peer', axhPrint, 'UIContextMenu',[]);
    end
end