function pdetool_set_param()
% case: set PDE coefficient values

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    subreg=getappdata(ax,'subsel');

    appl=getappdata(pde_fig,'application');
    equ=getappdata(pde_fig,'equation');
    params=getappdata(pde_fig,'params');
    descr=getappdata(pde_fig,'description');
    par_val=getappdata(pde_fig,'currparam');

    if strfind(par_val(1,:),'!')
        if ~isempty(subreg)
            k=subreg;
        else
            k=1;
        end
        for i=1:size(par_val,1)
            str=par_val(i,:);
            for j=1:k
                [tmps,str]=strtok(str,'!');
            end
            if i==1
                parvalues=tmps;
            else
                parvalues=char(parvalues,tmps);
            end
        end
    else
        parvalues=par_val;
    end

    wbdwnfcn = get(pde_fig,'WindowButtonDownFcn');
    set(pde_fig,'WindowButtonDownFcn','',...
        'Pointer','watch')
    drawnow
    if ispc
        pderel
    end
    typeh=findobj(get(pde_fig,'Children'),'flat','Tag','PDEHelpMenu');
    if appl==1 || appl==2
        pde_type=get(typeh,'UserData');
        pdedlg('initialize',[],pde_type,1:4,equ,params,parvalues,descr)
    elseif appl==3 || appl==4 
        pde_type=get(typeh,'UserData');
        pdedlg('initialize',[],pde_type,[1,4],equ,params,parvalues,descr)
    elseif appl==5 || appl==6 || appl==7 || appl==8
        set(typeh,'Userdata',1)
        pdedlg('initialize',[],1,1,equ,params,parvalues,descr)
    elseif appl==9 || appl==10
        pde_type=get(typeh,'UserData');
        pdedlg('initialize',[],pde_type,1:2,equ,params,parvalues,descr)
    end
    set(pde_fig,'WindowButtonDownFcn',wbdwnfcn,...
        'Pointer','arrow')
    drawnow
end