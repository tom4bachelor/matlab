function [nameOK, hasDotM] = pdetoolCheckFileName(fileName)
% pdetoolCheckFileName: HELPER FUNCTION FOR PDETOOL

%   Copyright 1994-2013 The MathWorks, Inc.
    [ms,me,ti]=regexp(fileName,'[a-zA-Z][a-zA-Z0-9_]*(.[mM])?','once');
    len = length(fileName);
    nameOK = ms==1 && me==len && len<63;
    if(nameOK)
        hasDotM = ti(1)<ti(2);
    elseif(len > 2)
        hasDotM = strcmpi(fileName(len-1:len),'.m');
    else
        hasDotM = false;
    end
end