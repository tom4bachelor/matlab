function pdetool_rectstart(flag)
% case: mouse down to draw rectangle

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    if ~pdeonax, return, end

    set(pde_fig,'WindowButtonUpFcn','pdetool(''rectdraw'')');

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    set(pde_fig,'CurrentAxes',ax)
    pv=get(ax,'CurrentPoint');

    [tmp(1),tmp(2)]=pdesnap(ax,pv,getappdata(ax,'snap'));

    hold on;
    hndl=line([tmp(1) tmp(1) tmp(1) tmp(1) tmp(1)],...
        [tmp(2) tmp(2) tmp(2) tmp(2) tmp(2)],...
        'Color','r');
    hold off;
    set(ax,'UserData',hndl)
    if flag==1
        if strcmp(get(pde_fig,'SelectionType'),'alt')
            set(pde_fig,'WindowButtonMotionFcn','pdemtncb(-1)')
        else
            set(pde_fig,'WindowButtonMotionFcn','pdemtncb(1)')
        end
    elseif flag==2
        if strcmp(get(pde_fig,'SelectionType'),'alt')
            set(pde_fig,'WindowButtonMotionFcn','pdemtncb(-2)')
        else
            set(pde_fig,'WindowButtonMotionFcn','pdemtncb(2)')
        end
    end
end