function pdePlotEdgeLabels( dl1, ax, plotprefix )
%PDEPLOTEDGELABELS(dl1, ax) Plot edge labels on decomposed geometry
%   dl1 is the decomposed geometry matrix obtained from |decsg|
%   ax is the axes handle from the edge plot on which the edge labels
%      are to be plotted.
%   plotprefix is a logical that requests the prefix 'E' to be plotted

%       Copyright 2012-2016 The MathWorks, Inc.

n=size(dl1,2);
for i=1:n
  if dl1(1,i)==1
    % circle
    th1=atan2(dl1(4,i)-dl1(9,i),dl1(2,i)-dl1(8,i));
    th2=atan2(dl1(5,i)-dl1(9,i),dl1(3,i)-dl1(8,i));
    if th2<th1, th2=th2+2*pi; end
    arg=(th2+th1)/2;
    xm=dl1(8,i)+cos(arg)*dl1(10,i);
    ym=dl1(9,i)+sin(arg)*dl1(10,i);
  elseif dl1(1,i)==4
    % ellipse
    ca=cos(dl1(12,i)); sa=sin(dl1(12,i));
    xd=dl1(2,i)-dl1(8,i);
    yd=dl1(4,i)-dl1(9,i);
    x1=ca*xd+sa*yd;
    y1=-sa*xd+ca*yd;
    th1=atan2(y1./dl1(11,i),x1./dl1(10,i));
    xd=dl1(3,i)-dl1(8,i);
    yd=dl1(5,i)-dl1(9,i);
    x1=ca*xd+sa*yd;
    y1=-sa*xd+ca*yd;
    th2=atan2(y1./dl1(11,i),x1./dl1(10,i));
    if th2<th1, th2=th2+2*pi; end
    arg=(th2+th1)/2;
    x1=dl1(10,i)*cos(arg); y1=dl1(11,i)*sin(arg);
    xm=dl1(8,i)+ca*x1-sa*y1;
    ym=dl1(9,i)+sa*x1+ca*y1;
  else
    xm=0.5*(dl1(2,i)+dl1(3,i));
    ym=0.5*(dl1(4,i)+dl1(5,i));
  end
  if plotprefix
      edgelabels =  sprintf('E%d', i);
  else
      edgelabels = int2str(i);
  end
  text('String',edgelabels,'Units','data',...
    'Clipping','on',...
    'Tag','PDEEdgeLabel','HorizontalAlignment','center',...
    'pos',[xm ym 0],'color','k',...
    'Parent',ax);
end

end

