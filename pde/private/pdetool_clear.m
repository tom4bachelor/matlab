function pdetool_clear()
% case: clear patch

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    selected=findobj(get(ax,'Children'),'flat','Tag','PDESelFrame',...
        'Visible','on');
    if isempty(selected), return; end
    n=length(selected);
    geom_cols=zeros(1,n);
    for i=1:n
        geom_cols(i)=get(selected(i),'UserData');
    end

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
    pdegd=get(h,'UserData');
    objmtx=getappdata(pde_fig,'objnames');
    m=size(pdegd,2);
    if n>1 && m>1
        ind=find(~sum(~(ones(n,1)*(1:m)-(ones(m,1)*geom_cols)')));
    else
        ind=find(~(~(ones(n,1)*(1:m)-(ones(m,1)*geom_cols)')));
    end
    pdegd=pdegd(:,ind);
    set(h,'UserData',pdegd);
    objmtx=objmtx(:,ind);
    setappdata(pde_fig,'objnames',objmtx)

    set(pde_fig,'Pointer','watch');
    drawnow

    % First call DECSG to decompose geometry;

    [dl1,bt1,pdedl,bt,msb]=decsg(pdegd);

    pdepatch(pdedl,bt,msb);
    boundh=findobj(get(pde_fig,'Children'),'flat','Tag','PDEBoundMenu');
    set(boundh,'UserData',pdedl)
    set(get(ax,'Title'),'UserData',bt)

    setappdata(pde_fig,'dl1',dl1)
    setappdata(pde_fig,'bt1',bt1)

    set(pde_fig,'Pointer','arrow');
    drawnow

    evalhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEval');

    if isempty(pdegd)
        pdeinfo('Draw 2-D geometry.');
        h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
        flags=get(h,'UserData');
        flags(1)=0; flags(3)=0;
        set(h,'UserData',flags)
        set(evalhndl,'String','')
        drawhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEDrawMenu');
        hndl=findobj(get(drawhndl,'Children'),'flat','Tag','PDEExpGD');
        set(hndl,'Enable','off')
    else
        m=size(pdegd,2);
        for i=1:m
            lbl=deblank(char(objmtx(:,i)'));
            if i==1
                evalstr=lbl;
            else
                evalstr=[evalstr '+' lbl]; %#ok - suppress preallocation warning
            end
        end
        % update set formula string
        set(evalhndl,'String',evalstr,'UserData',evalstr)
    end

    edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
    hndl=findobj(get(edithndl,'Children'),'flat','Tag','PDEClear');
    set(hndl,'Enable','off');
end