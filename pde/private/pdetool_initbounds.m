function pdetool_initbounds(flag)
% case: initialize boundary conditions
%       Initial boundary condition: Dirichlet condition u=0 on the boundary.

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    % flag=1 if scalar, 0 if system
    bndhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEBoundMenu');

    dl1=getappdata(pde_fig,'dl1');
    if isempty(dl1), return; end

    h=findobj(get(bndhndl,'Children'),'flat','Tag','PDEBoundMode');

    bounds=[find(dl1(7,:)==0) find(dl1(6,:)==0)];

    m=size(dl1,2);
    if flag
        pdebound=zeros(10,m);
        pdebound(1,bounds)=ones(1,size(bounds,2));
        pdebound(2:6,:)=ones(5,m);
        pdebound([7:8 10],:)='0'*ones(3,m);
        pdebound(9,:)='1'*ones(1,m);
    else
        pdebound=zeros(26,m);
        pdebound(1,bounds)=2*ones(1,size(bounds,2));
        pdebound(2,:)=2*ones(1,m);
        pdebound(3:14,:)=ones(12,m);
        pdebound([15:20, 22:23, 25:26],:)='0'*ones(10,m);
        pdebound([21 24],:)='1'*ones(2,m);
    end
    set(h,'UserData',pdebound)

    % set color and user data of boundaries to Dirichlet values (red)
    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    bounds=findobj(get(ax,'Children'),'flat','Tag','PDEBoundLine');
    for i=1:length(bounds)
        udata=get(bounds(i),'UserData');
        udata(2:4)=[1 0 0];
        set(bounds(i),'Color','r','UserData',udata)
    end
    % deselect all selected boundaries:
    hndl=findobj(get(bndhndl,'Children'),'flat','Tag','PDEBoundSpec');
    set(hndl,'UserData',[])
end