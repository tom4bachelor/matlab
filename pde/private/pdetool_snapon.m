function pdetool_snapon(flag)
% case: snap to grid on/off

%   Copyright 1994-2013 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    pde_kids = allchild(pde_fig);
    ax=findobj(pde_kids,'flat','Tag','PDEAxes');
    opthndl=findobj(pde_kids,'flat','Tag','PDEOptMenu');
    snaphndl=findobj(get(opthndl,'Children'),'flat','Tag','PDESnap');
    if nargin==1
        % flag contains 'on' or 'off'
        if strcmp(flag,'on')
            setappdata(ax,'snap',1)
        else
            setappdata(ax,'snap',0)
        end
        set(snaphndl,'Checked',flag)
    else
        % toggle state
        setappdata(ax,'snap',pdeumtoggle(snaphndl));
    end
end