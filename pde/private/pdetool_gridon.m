function pdetool_gridon(flag)
% case: grid on/off

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    pde_kids = allchild(pde_fig);
    ax=findobj(pde_kids,'flat','Tag','PDEAxes');
    set(pde_fig,'CurrentAxes',ax);
    opthndl=findobj(pde_kids,'flat','Tag','PDEOptMenu');
    gridhndl=findobj(get(opthndl,'Children'),'flat','Tag','PDEGrid');
    if nargin==1
        set(ax,'XGrid',flag,'YGrid',flag)
        set(gridhndl,'Checked',flag)
    else
        if pdeumtoggle(gridhndl)
            set(ax,'XGrid','on','YGrid','on')
        else
            set(ax,'XGrid','off','YGrid','off')
            snaphndl=findobj(get(opthndl,'Children'),'flat','Tag','PDESnap');
            set(snaphndl,'Checked','off');
            setappdata(ax,'snap',0);
        end
    end
end