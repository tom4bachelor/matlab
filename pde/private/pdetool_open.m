function pdetool_open()
% case: load geometry through MATLAB-file

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    [infile,inpath]=uigetfile('*.m','Open');

    figure(pde_fig);
    set(pde_fig,'Pointer','watch')
    drawnow

    if infile~=0
        nameOK = pdetoolCheckFileName(infile);
        instring=[inpath infile];
        fid = fopen(instring,'r');
        n=length(infile);
        if fid==-1 || n<2
            pdetool('error','  File not found.');
        elseif ~strcmpi(infile(n-1:n),'.m')
            pdetool('error','  File is not an MATLAB-file.');
            fclose(fid);
        elseif(~nameOK)
            msg1 = message('pde:pdetool:badNameOpen').getString();
            msg2 = message('pde:pdetool:validFileName').getString();
            pdetool('error',[msg1 sprintf('\n') msg2]);
            fclose(fid);
        else
            h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
            flags=get(h,'UserData');
            need_save=flags(1);

            if need_save
                curr_file=get(findobj(get(pde_fig,'Children'),'flat',...
                    'Tag','PDEEditMenu'),'UserData');
                if isempty(curr_file)
                    tmp=' Save changes in ''Untitled'' ?';
                else
                    [~,file_name] = fileparts(curr_file);
                    tmp=[' Save changes in ' file_name '?'];
                end

                answr=questdlg(tmp,'New','Yes','No','Cancel','Yes');
            else
                answr = 'No';
            end

            if strcmp(answr,'Cancel')
                set(pde_fig,'Pointer','arrow')
                drawnow
                return;
            end

            if strcmp(answr,'Yes')
                if isempty(curr_file)
                    pdetool('save_as');
                else
                    pdetool('save');
                end
            end

            flags(1)=0;
            set(h,'UserData',flags)

            currdir=pwd;
            if ispc
                % If PC Windows, strip '\' at end (ASCII 92)
                if abs(inpath(length(inpath)))==double(filesep)
                    inpath=inpath(1:length(inpath)-1);
                end
            end
            cd(inpath);
            try
                eval(infile(1:n-2));
            catch
                pdetool('error',sprintf(' Error executing Model MATLAB-file %s',infile));
                set(pde_fig,'Pointer','arrow')
                drawnow
                fclose(fid);
                cd(currdir)
                return;
            end
            name=['PDE Toolbox - ', upper(infile)];
            set(findobj(get(pde_fig,'Children'),'Tag','PDESave'),...
                'Enable','on')
            set(findobj(get(pde_fig,'Children'),...
                'flat','Tag','PDEEditMenu'),'UserData',instring)
            set(pde_fig,'Name',name)

            flags=get(h,'UserData'); flags(1)=0;
            set(h,'UserData',flags)

            fclose(fid);
            cd(currdir)
        end
    end

    set(pde_fig,'Pointer','arrow')
    drawnow
end