function pdetool_appl_cb(flag)
% case: application selection callback

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    kids=get(pde_fig,'Children');
    h=findobj(kids,'flat','Tag','PDEAppl');
    oldval=get(h,'UserData');
    val=get(h,'Value');
    typeh=findobj(kids,'flat','Tag','PDEHelpMenu');
    paramh=findobj(kids,'flat','Tag','PDEPDEMenu');
    pde_typeh=findobj(kids,'flat','Tag','PDEHelpMenu');

    if (flag==0 && val==1) || flag==1
        % Generic scalar
        if oldval==1, return; end
        setappdata(pde_fig,'application',1);
        set(h,'Value',1,'UserData',1)
        menuh=get(findobj(kids,'Tag','PDEAppMenu'),...
            'Children');
        set(menuh,'Checked','off');
        set(findobj(menuh,'UserData',1),'Checked','on')
        setappdata(pde_fig,'equation','-div(c*grad(u))+a*u=f');
        setappdata(pde_fig,'params',char('c','a','f','d'));
        setappdata(pde_fig,'description',char([],[],[],[]));
        setappdata(pde_fig,'bounddescr',char([],[],[],[]));
        setappdata(pde_fig,'boundeq',...
            char('n*c*grad(u)+q*u=g','h*u=r',[]))
        cp=char('1.0','0.0','10','1.0');
        setappdata(pde_fig,'currparam',cp);
        stdparam=pdetrans(cp,1);
        set(paramh,'UserData',stdparam)
        strmtx=char('u|abs(grad(u))|abs(c*grad(u))|user entry',...
            ' -grad(u)| -c*grad(u)| user entry');
        setappdata(pde_fig,'plotstrings',strmtx)
        pdetool('initbounds',1)

    elseif (flag==0 && val==2) || flag==2
        % Generic system
        if oldval==2, return; end
        setappdata(pde_fig,'application',2);
        set(h,'Value',2,'UserData',2)
        menuh=get(findobj(kids,'Tag','PDEAppMenu'),...
            'Children');
        set(menuh,'Checked','off');
        set(findobj(menuh,'UserData',2),'Checked','on')

        setappdata(pde_fig,'equation','-div(c*grad(u))+a*u=f');
        setappdata(pde_fig,'params',char('c11, c12','c21, c22',...
            'a11, a12','a21, a22','f1, f2','d11, d12','d21, d22'));
        setappdata(pde_fig,'description',char([],[],[],[],[],[],[]));
        setappdata(pde_fig,'bounddescr',char([],[],[],[],[],[],[],[]));
        setappdata(pde_fig,'boundeq',...
            char('n*c*grad(u)+q*u=g','h*u=r','n*c*grad(u)+q*u=g+h''*l; h*u=r'))
        cp=char('1.0','0.0','0.0','0.0','1.0','1.0','0.0',...
            '0.0','1.0','0.0','0.0','1.0','0.0','1.0');
        setappdata(pde_fig,'currparam',cp);
        stdparam=pdetrans(cp,2);
        set(paramh,'UserData',stdparam)
        strmtx=char('u|v|abs(u,v)|user entry',...
            '(u,v)|user entry');
        setappdata(pde_fig,'plotstrings',strmtx)
        pdetool('initbounds',0)

    elseif (flag==0 && val==3) || flag==3
        % Structural Mechanics, Plane Stress
        if oldval==3, return; end
        setappdata(pde_fig,'application',3);
        set(h,'Value',3,'UserData',3)
        menuh=get(findobj(kids,'Tag','PDEAppMenu'),...
            'Children');
        set(menuh,'Checked','off');
        set(findobj(menuh,'UserData',3),'Checked','on')
        setappdata(pde_fig,'equation','Structural mechanics, plane stress');
        setappdata(pde_fig,'params',char('E','nu','Kx','Ky','rho'));
        setappdata(pde_fig,'description',char('Young''s modulus',...
            'Poisson ratio','Volume force, x-direction',...
            'Volume force, y-direction','Density'));
        setappdata(pde_fig,'bounddescr',char('Surface tractions','  ''''',...
            'Spring constants','   ''''','Weights','   ''''',...
            'Displacements','   '''''));
        setappdata(pde_fig,'boundeq',...
            char('n*c*grad(u)+q*u=g','h*u=r','n*c*grad(u)+q*u=g+h''*l; h*u=r'))
        cp=char('1E3','0.3','0.0','0.0','1.0');
        setappdata(pde_fig,'currparam',cp);
        stdparam=pdetrans(cp,3);
        set(paramh,'UserData',stdparam)
        str1=['x displacement (u)|y displacement (v)|abs(u,v)|ux|uy|vx|vy|',...
            'x strain|y strain|shear strain|x stress|y stress|shear stress|',...
            '1st principal strain|2nd principal strain|1st principal stress|',...
            '2nd principal stress|von Mises stress|user entry'];

        strmtx=char(str1,'(u,v)|user entry');
        setappdata(pde_fig,'plotstrings',strmtx)
        pdetool('initbounds',0)
        set(pde_typeh,'UserData',1)

    elseif (flag==0 && val==4) || flag==4
        % Structural Mechanics, Plane Strain
        if oldval==4, return; end
        setappdata(pde_fig,'application',4);
        set(h,'Value',4,'UserData',4)
        menuh=get(findobj(kids,'Tag','PDEAppMenu'),...
            'Children');
        set(menuh,'Checked','off');
        set(findobj(menuh,'UserData',4),'Checked','on')
        setappdata(pde_fig,'equation','Structural mechanics, plane strain');
        setappdata(pde_fig,'params',char('E','nu','Kx','Ky','rho'));
        setappdata(pde_fig,'description',char('Young''s modulus',...
            'Poisson ratio','Volume force, x-direction',...
            'Volume force, y-direction','Density'));
        setappdata(pde_fig,'bounddescr',char('Surface tractions','  ''''',...
            'Spring constants','   ''''','Weights','   ''''',...
            'Displacements','   '''''));
        setappdata(pde_fig,'boundeq',...
            char('n*c*grad(u)+q*u=g','h*u=r','n*c*grad(u)+q*u=g+h''*l; h*u=r'))
        cp=char('1E3','0.3','0.0','0.0','1.0');
        setappdata(pde_fig,'currparam',cp);
        stdparam=pdetrans(cp,4);
        set(paramh,'UserData',stdparam)

        str1=['x displacement (u)|y displacement (v)|abs(u,v)|ux|uy|vx|vy|',...
            'x strain|y strain|shear strain|x stress|y stress|shear stress|',...
            '1st principal strain|2nd principal strain|1st principal stress|',...
            '2nd principal stress|von Mises stress|user entry'];

        strmtx=char(str1,'(u,v)|user entry');
        setappdata(pde_fig,'plotstrings',strmtx)
        pdetool('initbounds',0)
        set(pde_typeh,'UserData',1)

    elseif (flag==0 && val==5) || flag==5
        % Electrostatics
        if oldval==5, return; end
        setappdata(pde_fig,'application',5);
        set(h,'Value',5,'UserData',5)
        menuh=get(findobj(kids,'Tag','PDEAppMenu'),...
            'Children');
        set(menuh,'Checked','off');
        set(findobj(menuh,'UserData',5),'Checked','on')
        setappdata(pde_fig,'equation',...
            '-div(epsilon*grad(V))=rho, E=-grad(V), V=electric potential');
        setappdata(pde_fig,'params',char('epsilon','rho'));
        setappdata(pde_fig,'description',char('Coeff. of dielectricity',...
            'Space charge density'));
        setappdata(pde_fig,'bounddescr',...
            char('Surface charge',[],'Weight','Electric potential'));
        setappdata(pde_fig,'boundeq',...
            char('n*epsilon*grad(V)+q*V=g','h*V=r',[]'))
        cp=char('1.0','1.0');
        setappdata(pde_fig,'currparam',cp);
        stdparam=pdetrans(cp,5);
        set(paramh,'UserData',stdparam)
        set(typeh,'UserData',1)
        str1=['electric potential|electric field|electric displacement|',...
            'user entry'];
        str2='electric field|electric displacement|user entry';
        strmtx=char(str1,str2);
        setappdata(pde_fig,'plotstrings',strmtx)
        pdetool('initbounds',1)
        set(pde_typeh,'UserData',1)

    elseif (flag==0 && val==6) || flag==6
        % Magnetostatics
        if oldval==6, return; end
        setappdata(pde_fig,'application',6);
        set(h,'Value',6,'UserData',6)
        menuh=get(findobj(kids,'Tag','PDEAppMenu'),...
            'Children');
        set(menuh,'Checked','off');
        set(findobj(menuh,'UserData',6),'Checked','on')
        setappdata(pde_fig,'equation',...
            '-div((1/mu)*grad(A))=J, B=curl(A), A=magnetic vector potential');
        setappdata(pde_fig,'params',char('mu','J'));
        setappdata(pde_fig,'description',char('Magnetic permeability',...
            'Current density'));
        setappdata(pde_fig,'bounddescr',...
            char('Magnetic field',[],'Weight','Magnetic potential'));
        setappdata(pde_fig,'boundeq',...
            char('n*(1/mu)*grad(A)+q*A=g','h*A=r',[]))
        cp=char('1.0','1.0');
        setappdata(pde_fig,'currparam',cp);
        stdparam=pdetrans(cp,6);
        set(paramh,'UserData',stdparam)
        set(typeh,'UserData',1)
        str1='magnetic potential|magnetic flux density|magnetic field|user entry';
        str2='magnetic flux density|magnetic field|user entry';
        strmtx=char(str1,str2);
        setappdata(pde_fig,'plotstrings',strmtx)
        pdetool('initbounds',1)
        set(pde_typeh,'UserData',1)

    elseif (flag==0 && val==7) || flag==7
        % AC Power Electromagnetics
        if oldval==7, return; end
        setappdata(pde_fig,'application',7);
        set(h,'Value',7,'UserData',7)
        menuh=get(findobj(kids,'Tag','PDEAppMenu'),...
            'Children');
        set(menuh,'Checked','off');
        set(findobj(menuh,'UserData',7),'Checked','on')
        setappdata(pde_fig,'equation',...
            ['-div((1/mu)*grad(E))+(j*omega*sigma-omega^2*epsilon)*E=0, ',...
            'E=electric field']);
        setappdata(pde_fig,'params',char('omega','mu','sigma','epsilon'));
        setappdata(pde_fig,'description',char('Angular frequency',...
            'Magnetic permeability','Conductivity','Coeff. of dielectricity'));
        setappdata(pde_fig,'bounddescr',...
            char('',[],'Weight','Electric field'));
        setappdata(pde_fig,'boundeq',...
            char('n*(1/mu)*grad(E)+q*E=g','h*E=r',[]))
        cp=char('1.0','1.0','1.0','1.0');
        setappdata(pde_fig,'currparam',cp);
        stdparam=pdetrans(cp,7);
        set(paramh,'UserData',stdparam)
        set(typeh,'UserData',1)
        str1=['electric field|magnetic flux density|magnetic field|',...
            'current density|resistive heating rate|user entry'];
        str2='magnetic flux density|magnetic field|user entry';
        strmtx=char(str1,str2);

        setappdata(pde_fig,'plotstrings',strmtx)
        pdetool('initbounds',1)
        set(pde_typeh,'UserData',1)

    elseif (flag==0 && val==8) || flag==8
        % Conductive Media DC
        if oldval==8, return; end
        setappdata(pde_fig,'application',8);
        set(h,'Value',8,'UserData',8)
        menuh=get(findobj(kids,'Tag','PDEAppMenu'),...
            'Children');
        set(menuh,'Checked','off');
        set(findobj(menuh,'UserData',8),'Checked','on')
        setappdata(pde_fig,'equation',...
            '-div(sigma*grad(V))=q, E=-grad(V), V=electric potential');
        setappdata(pde_fig,'params',char('sigma','q'));
        setappdata(pde_fig,'description',char('Conductivity',...
            'Current source'));
        setappdata(pde_fig,'bounddescr',...
            char('Current source','Film conductance','Weight',...
            'Electric potential'));
        setappdata(pde_fig,'boundeq',...
            char('n*sigma*grad(V)+q*V=g','h*V=r',[]))
        cp=char('1.0','1.0');
        setappdata(pde_fig,'currparam',cp);
        stdparam=pdetrans(cp,8);
        set(paramh,'UserData',stdparam)
        set(typeh,'UserData',1)

        str1='electric potential|electric field|current density|user entry';
        str2='electric field|current density|user entry';
        strmtx=char(str1,str2);
        setappdata(pde_fig,'plotstrings',strmtx)
        pdetool('initbounds',1)
        set(pde_typeh,'UserData',1)

    elseif (flag==0 && val==9) || flag==9
        % Heat Transfer
        if oldval==9, return; end
        setappdata(pde_fig,'application',9);
        set(h,'Value',9,'UserData',9)
        menuh=get(findobj(kids,'Tag','PDEAppMenu'),...
            'Children');
        set(menuh,'Checked','off');
        set(findobj(menuh,'UserData',9),'Checked','on')
        setappdata(pde_fig,'equation',...
            'rho*C*T''-div(k*grad(T))=Q+h*(Text-T), T=temperature');
        setappdata(pde_fig,'params',char('rho','C','k','Q','h','Text'));
        ScreenUnits = get(0,'Units');
        set(0,'Unit','pixels');
        ScreenPos = get(0,'ScreenSize');
        set(0,'Unit',ScreenUnits);
        if ScreenPos(3)<=800
            setappdata(pde_fig,'description',char('Density','Heat capacity',...
                'Coeff. of heat conduction','Heat source',...
                'Conv. heat transfer coeff.','External temperature'));
        else
            setappdata(pde_fig,'description',char('Density','Heat capacity',...
                'Coeff. of heat conduction','Heat source',...
                'Convective heat transfer coeff.','External temperature'));
        end
        setappdata(pde_fig,'bounddescr',...
            char('Heat flux','Heat transfer coefficient','Weight',...
            'Temperature'));
        setappdata(pde_fig,'boundeq',...
            char('n*k*grad(T)+q*T=g','h*T=r',[]))
        cp=char('1.0','1.0','1.0','1.0','1.0','0.0');
        setappdata(pde_fig,'currparam',cp);
        stdparam=pdetrans(cp,9);
        set(paramh,'UserData',stdparam)
        set(typeh,'UserData',2)
        str1='temperature|temperature gradient|heat flux|user entry';
        str2='temperature gradient|heat flux|user entry';
        strmtx=char(str1,str2);
        setappdata(pde_fig,'plotstrings',strmtx)

        pdetool('initbounds',1)
        set(pde_typeh,'UserData',1)

    elseif (flag==0 && val==10) || flag==10
        % Diffusion
        if oldval==10, return; end
        setappdata(pde_fig,'application',10);
        set(h,'Value',10,'UserData',10)
        menuh=get(findobj(kids,'Tag','PDEAppMenu'),...
            'Children');
        set(menuh,'Checked','off');
        set(findobj(menuh,'UserData',10),'Checked','on')
        setappdata(pde_fig,'equation','dc/dt-div(D*grad(c))=Q, c=concentration');
        setappdata(pde_fig,'params',char('D','Q'));
        setappdata(pde_fig,'description',char('Diffusion coefficient',...
            'Volume source'));
        setappdata(pde_fig,'bounddescr',...
            char('Flux','Transfer coefficient','Weight','Concentration'));
        setappdata(pde_fig,'boundeq',...
            char('n*D*grad(c)+q*c=g','h*c=r',[]))
        cp=char('1.0','1.0');
        setappdata(pde_fig,'currparam',cp);
        stdparam=pdetrans(cp,10);
        set(paramh,'UserData',stdparam)
        set(typeh,'UserData',2)

        str1='concentration|concentration gradient|flux|user entry';
        str2='concentration gradient|flux|user entry';
        strmtx=char(str1,str2);
        setappdata(pde_fig,'plotstrings',strmtx)

        pdetool('initbounds',1)
        set(pde_typeh,'UserData',1)

    end

    % Restore selected plotflags:
    plotflags=getappdata(pde_fig,'plotflags');
    plotflags(1:6)=ones(1,6);
    plotflags(11:18)=[0 1 1 0 0 0 0 1];
    setappdata(pde_fig,'plotflags',plotflags)
    % [colvar colstyle heightvar heightstyle vectorvar vectorstyle
    % colval doplot xyplot showmesh animationflag popupvalue
    % colflag contflag heightflag vectorflag deformflag deformvar]

    % Restore user entries:
    setappdata(pde_fig,'colstring','')
    setappdata(pde_fig,'arrowstring','')
    setappdata(pde_fig,'deformstring','')
    setappdata(pde_fig,'heightstring','')

    % Set flags to indicate that PDE equation has changed:
    h=findobj(kids,'flat','Tag','PDEFileMenu');
    flags=get(h,'UserData'); flags(6)=0; flags(7)=1;
    set(h,'UserData',flags)

    % If any affected dialog box is open, update it:
    wins=get(0,'Children');
    % Plot selection dialog
    figh=findobj(wins,'flat','Tag','PDEPlotFig');
    if ~isempty(figh)
        if strcmp(get(figh,'Visible'),'on')
            pdeptdlg('initialize',1,getappdata(gcf,'plotstrings'));
        end
    end
    % PDE Specification dialog
    figh=findobj(wins,'flat','Tag','PDESpecFig');
    if ~isempty(figh)
        if strcmp(get(figh,'Visible'),'on')
            close(figh)
            pdetool('set_param')
        end
    end
    % Boundary Condition dialog:
    figh=findobj(wins,'flat','Tag','PDEBoundFig');
    if ~isempty(figh)
        if strcmp(get(figh,'Visible'),'on')
            close(figh)
            pdetool('set_bounds')
        end
    end
end