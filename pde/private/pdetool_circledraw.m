function pdetool_circledraw(flag)
% case: mouse button up to draw ellipse/circle

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    hndl=get(ax,'UserData');
    if isempty(hndl), return; end

    user_data=get(pde_fig,'UserData');
    origin=user_data(3:4);

    set(pde_fig,'WindowButtonMotionFcn','pdemtncb(0)',...
        'WindowButtonUpFcn','');
    sticks=getappdata(pde_fig,'stick');

    pv=get(ax,'CurrentPoint');
    [xcurr,ycurr]=pdesnap(ax,pv,getappdata(ax,'snap'));

    delete(hndl);
    set(ax,'UserData',[]);

    if ~isempty(origin)
        if flag==1                         % circle - perimeter
            dif=max(abs(xcurr-origin(1)),abs(ycurr-origin(2)));
            xsign=sign(pv(1,1)-origin(1));
            ysign=sign(pv(1,2)-origin(2));
            if xsign==0, xsign=1; end
            if ysign==0, ysign=1; end
            xc=0.5*(dif*xsign+2*origin(1));
            yc=0.5*(dif*ysign+2*origin(2));
            radius=0.5*dif;
            if radius==0
                return;
            end
            if ~any(sticks)
                set(pde_fig,'WindowButtonDownFcn','pdeselect select')
            end
            pdecirc(xc,yc,radius);
        elseif flag==2                     % ellipse - perimeter
            origin=0.5*(origin + reshape([xcurr, ycurr],size(origin)));
            radiusx=sqrt((origin(1)-xcurr)*(origin(1)-xcurr));
            radiusy=sqrt((origin(2)-ycurr)*(origin(2)-ycurr));
            if (radiusx==0 || radiusy==0), return; end
            if ~any(sticks)
                set(pde_fig,'WindowButtonDownFcn','pdeselect select')
            end
            small=100*eps*min(diff(get(ax,'Xlim')),diff(get(ax,'Ylim')));
            if abs(radiusx-radiusy)<small
                pdecirc(origin(1),origin(2),radiusx);
            else
                pdeellip(origin(1),origin(2),radiusx,radiusy);
            end
        elseif flag==3                     % circle - center
            dif=max(abs(xcurr-origin(1)),abs(ycurr-origin(2)));
            radius=dif;
            if radius==0, return; end
            if ~any(sticks)
                set(pde_fig,'WindowButtonDownFcn','pdeselect select')
            end
            pdecirc(origin(1),origin(2),radius);
        elseif flag==4                     % ellipse - center
            radiusx=sqrt((origin(1)-xcurr)*(origin(1)-xcurr));
            radiusy=sqrt((origin(2)-ycurr)*(origin(2)-ycurr));
            if (radiusx==0 || radiusy==0), return; end
            if ~any(sticks)
                set(pde_fig,'WindowButtonDownFcn','pdeselect select')
            end
            small=100*eps*min(diff(get(ax,'Xlim')),diff(get(ax,'Ylim')));
            if abs(radiusx-radiusy)<small
                pdecirc(origin(1),origin(2),radiusx);
            else
                pdeellip(origin(1),origin(2),radiusx,radiusy);
            end
        end

        if ~any(sticks)
            drawnow;
            btst=btnstate(pde_fig,'draw');
            if any(btst)
                btnup(pde_fig,'draw',...
                    find(btst))
            end
            drawhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEDrawMenu');
            h=findobj(get(drawhndl,'Children'),'flat','Checked','on');
            set(h,'Checked','off')
            pdeinfo('Draw and edit 2-D geometry by using the Draw and Edit menu options.');
        end
    end
end