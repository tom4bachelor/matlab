function plotAxisTriad(bbox)
% PLOTAXISTRIAD - Plots cartesian axis triad based on input bounding box
%

%   Copyright 2014-2016 The MathWorks, Inc.

maxdim = max([abs(bbox(1,1)-bbox(1,2)), abs(bbox(2,1)-bbox(2,2)), abs(bbox(3,1)-bbox(3,2))]);
orgn = (ones(3,3).*(bbox(:,1) - 0.3*maxdim))';
vMag = .3*maxdim;
vec = vMag*eye(3);
quiver3(orgn(:,1),orgn(:,2),orgn(:,3),vec(:,1),vec(:,2),vec(:,3), 'Color', 'red');
labPts = (orgn + vec);
text(labPts(:,1), labPts(:,2), labPts(:,3), ['x' 'y' 'z']');
end
