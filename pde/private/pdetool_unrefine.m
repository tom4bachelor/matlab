function pdetool_unrefine()
% case: unrefine mesh - undo last mesh change

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    if btnstate(pde_fig,'zoom',1)
        pdezoom(pde_fig,'off')
        btnup(pde_fig,'zoom',1)
        opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
        set(findobj(get(opthndl,'Children'),'flat','Tag','PDEZoom'),...
            'Checked','off')
    end

    flg_hndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(flg_hndl,'UserData');
    flag1=flags(3);
    if abs(flag1)
        pdetool('changemode',0)
    end
    flags=get(flg_hndl,'UserData');
    if flags(3)==-1
        % error in decsg
        return;
    elseif ~flags(2)
        pdetool('cleanup')
    end
    flags=get(flg_hndl,'UserData');
    flag2=flags(4);
    if flag2
        pdetool('initmesh')
    end
    flags=get(flg_hndl,'UserData');
    flags(2)=2;                           % mode_flag=2
    set(flg_hndl,'UserData',flags)

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');

    pdeinfo('Unrefining the mesh...');
    set(pde_fig,'CurrentAxes',ax,'Pointer','watch','WindowButtonDownFcn','')
    drawnow

    pdetool('clearsol')

    % Hide decomposition lines (=subdomain boundaries), boundary lines,
    % labels, and subdomains:
    hKids=get(ax,'Children');
    h=[findobj(hKids,'flat','Tag','PDEBoundLine')'...
        findobj(hKids,'flat','Tag','PDESubLabel')'...
        findobj(hKids,'flat','Tag','PDEEdgeLabel')'...
        findobj(hKids,'flat','Tag','PDESubDom')'...
        findobj(hKids,'flat','Tag','PDEBorderLine')'...
        findobj(hKids,'flat','Tag','PDESelRegion')'];

    set(h,'Visible','off')

    drawnow

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
    hp=findobj(get(h,'Children'),'flat','Tag','PDEInitMesh');
    he=findobj(get(h,'Children'),'flat','Tag','PDERefine');
    ht=findobj(get(h,'Children'),'flat','Tag','PDEMeshParam');

    % erase old mesh
    h=findobj(hKids,'flat','Tag','PDEMeshLine');
    delete(h)

    meshstat=getappdata(pde_fig,'meshstat');
    n=length(meshstat);

    % get previous generation of the mesh
    p=getappdata(pde_fig,['p' int2str(n-1)]);
    e=getappdata(pde_fig,['e' int2str(n-1)]);
    t=getappdata(pde_fig,['t' int2str(n-1)]);

    % plot the unrefined mesh
    h=pdeplot(p,e,t,'intool','on');
    set(h,'Tag','PDEMeshLine');

    set(hp,'UserData',p), set(he,'UserData',e), set(ht,'UserData',t)

    % if requested, label nodes and/or triangles:
    if getappdata(pde_fig,'shownodelbl')
        pdetool('shownodelbl',1)
    end
    if getappdata(pde_fig,'showtrilbl')
        pdetool('showtrilbl',1)
    end

    flags=get(flg_hndl,'UserData');
    flags(1)=1;                           % need_save=1
    flags(5)=1;                           % flag3=1
    flags(6)=0;                           % flag4=0
    set(flg_hndl,'UserData',flags)

    meshstat=meshstat(1:length(meshstat)-1);
    setappdata(pde_fig,'meshstat',meshstat)

    if length(meshstat)==1               % Back to initial mesh;
        % no more undo possible
        meshhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
        h=findobj(get(meshhndl,'Children'),'flat','Tag','PDEUnrefine');
        set(h,'Enable','off');
    end

    % Update max no of triangle default for adaptive solver
    % Double the current no, or 1000, whichever is greatest:
    solveparams=getappdata(pde_fig,'solveparam');
    tristr=int2str(max(1.5*size(t,2),1000));
    setappdata(pde_fig,'solveparam',...
        char(solveparams(1,:),tristr,solveparams(3:11,:)))

    set(pde_fig,'Pointer','arrow')
    drawnow

    infostr=sprintf('Current mesh consists of %i nodes and %i triangles.',...
        size(p,2),size(t,2));
    pdeinfo(infostr);

    edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
    set([edithndl findobj(get(edithndl,'Children'),'flat','Tag','PDESelall')],...
        'Enable','off')

    % Enable clicking on mesh to get info about triangle and node no's:
    set(pde_fig,'WindowButtonDownFcn','pdeinfclk(1)',...
        'WindowButtonUpFcn','if pdeonax, pdeinfo; end')
end