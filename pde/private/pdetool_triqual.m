function pdetool_triqual()
% case: display triangle quality measure

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    if btnstate(pde_fig,'zoom',1)
        pdezoom(pde_fig,'off')
        btnup(pde_fig,'zoom',1)
        opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
        set(findobj(get(opthndl,'Children'),'flat','Tag','PDEZoom'),...
            'Checked','off')
    end

    flg_hndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(flg_hndl,'UserData');
    flag1=flags(3);
    if abs(flag1)
        pdetool('changemode',0)
    end
    flags=get(flg_hndl,'UserData');
    if flags(3)==-1
        % error in decsg
        return;
    elseif ~flags(2)
        pdetool('cleanup')
    end
    flags=get(flg_hndl,'UserData');
    flag2=flags(4);
    if flag2
        pdetool('initmesh')
    end
    flags=get(flg_hndl,'UserData');
    flags(2)=2;                           % mode_flag=2
    set(flg_hndl,'UserData',flags)

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');

    set(pde_fig,'CurrentAxes',ax,'Pointer','watch','WindowButtonDownFcn','')
    drawnow

    pdeinfo('Triangle quality plot.');

    drawnow

    % Clean up axes
    h=get(ax,'UserData');
    if ~isempty(h)
        delete(h)
        set(ax,'UserData',[]);
    end
    set(get(ax,'Children'),'Visible','off')

    cmap=get(get(ax,'YLabel'),'UserData');

    hndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
    p=get(findobj(get(hndl,'Children'),'flat','Tag','PDEInitMesh'),...
        'UserData');
    t=get(findobj(get(hndl,'Children'),'flat','Tag','PDEMeshParam'),...
        'UserData');

    if (isempty(p) || isempty(t))
        return; % return if there is no mesh.
    end
    q=pdetriq(p,t);
    
    h=pdeplot(p,[],t,'xydata',q,'xystyle','flat','intool','on','mesh','on',...
        'colorbar','off','colormap',cmap,'title','Triangle quality');

    % Store handles to all solution plot patches in PDE Toolbox' axes' UserData.
    set(ax,'UserData',h)

    set(0,'CurrentFigure',pde_fig);

    cmin=min(min(real(q)),0.7);
    cmax=max(max(real(q)),1);
    caxis([cmin cmax]);
    colorbar('UIContextMenu',[]); % Disable context menu
    % set(h,'Tag','PDESolBar')

    % if requested, label nodes and/or triangles:
    if getappdata(pde_fig,'shownodelbl')
        pdetool('shownodelbl',1)
    end
    if getappdata(pde_fig,'showtrilbl')
        pdetool('showtrilbl',1)
    end

    set(pde_fig,'Pointer','arrow')
    drawnow

    pdeinfo;

    edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
    set([edithndl findobj(get(edithndl,'Children'),'flat','Tag','PDESelall')],...
        'Enable','off')

    % Enable clicking on mesh to get info about triangle and node no's:
    set(pde_fig,'WindowButtonDownFcn','pdeinfclk(1)',...
        'WindowButtonUpFcn','if pdeonax, pdeinfo; end')
end