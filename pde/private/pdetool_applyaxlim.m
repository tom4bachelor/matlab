function pdetool_applyaxlim()
% case: apply new axes limits (callback via PDEAXLIMDLG)

%   Copyright 1994-2013 The MathWorks, Inc.

    global PDEAXISRATIO;
    axisRatio = PDEAXISRATIO;

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    axlimdlg = findobj(0,'Tag','PDEAxLimDlg');
    hndls=get(axlimdlg,'UserData');
    hndls=hndls(:,2);                     % EditField handles is 2nd column
    xlim=get(hndls(1),'UserData');
    ylim=get(hndls(2),'UserData');

    set(ax,'XLim',xlim, 'YLim',ylim,...
        'DataAspectRatio',[1 axisRatio*diff(ylim)/diff(xlim) 1]);

    set(get(ax,'ZLabel'),'UserData',[]);

    opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
    h=findobj(get(opthndl,'Children'),'flat','Tag','PDEAxeq');
    set(h,'Checked','off')

    pdeinfo;
end