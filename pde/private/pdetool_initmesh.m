function pdetool_initmesh()
% case: initialize mesh

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    % if no geometry, create a default L-shape:
    pdegd=get(findobj(get(pde_fig,'Children'),'flat',...
        'Tag','PDEMeshMenu'),'UserData');
    if isempty(pdegd), pdetool('membrane'), end

    if btnstate(pde_fig,'zoom',1)
        pdezoom(pde_fig,'off')
        btnup(pde_fig,'zoom',1)
        opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
        set(findobj(get(opthndl,'Children'),'flat','Tag','PDEZoom'),...
            'Checked','off')
    end

    flg_hndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(flg_hndl,'UserData');
    flag1=flags(3);
    if abs(flag1)
        pdetool('changemode',0)
    end
    flags=get(flg_hndl,'UserData');
    if flags(3)==-1
        % error in decsg
        return;
    elseif ~flags(2)
        pdetool('cleanup')
    end
    flags=get(flg_hndl,'UserData');
    flags(2)=2;                           % mode_flag=2
    set(flg_hndl,'UserData',flags)

    ax=findobj(allchild(pde_fig),'flat','Tag','PDEAxes');

    pdeinfo('Initializing mesh...',0);
    set(pde_fig,'CurrentAxes',ax,'Pointer','watch','WindowButtonDownFcn','')
    drawnow

    dl1=getappdata(pde_fig,'dl1');

    pdetool('clearsol')

    % Hide decomposition lines (=subdomain boundaries), boundary lines,
    % labels, and subdomains:
    hKids=get(ax,'Children');
    h=[findobj(hKids,'flat','Tag','PDEBoundLine')'...
        findobj(hKids,'flat','Tag','PDESubLabel')'...
        findobj(hKids,'flat','Tag','PDEEdgeLabel')'...
        findobj(hKids,'flat','Tag','PDESubDom')'...
        findobj(hKids,'flat','Tag','PDEBorderLine')'...
        findobj(hKids,'flat','Tag','PDESelRegion')'];

    set(h,'Visible','off')

    drawnow

    trisize=getappdata(pde_fig,'trisize');
    Hgrad=getappdata(pde_fig,'Hgrad');
    jiggleparam=getappdata(pde_fig,'jiggle');
    jiggle=deblank(jiggleparam(1,:));
    if strcmp(jiggle,'on')
        jiggle=deblank(jiggleparam(2,:));
    end
    jiggleiter=deblank(jiggleparam(3,:));
    meshingAlgo = getappdata(pde_fig,'MesherVersion');
    if isempty(meshingAlgo)
        meshingAlgo = 'preR2013a';
    end
    if isempty(trisize)
        if isempty(jiggleiter)
            [p,e,t]=initmesh(dl1,'jiggle',jiggle,'Hgrad',Hgrad, ...
                'MesherVersion', meshingAlgo);
        else
            [p,e,t]=initmesh(dl1,'jiggle',jiggle,'Hgrad',Hgrad,...
                'jiggleiter',str2double(jiggleiter), 'MesherVersion', meshingAlgo);
        end
    else
        if isempty(jiggleiter)
            [p,e,t]=initmesh(dl1,'hmax',trisize,'jiggle',jiggle,'Hgrad',Hgrad, ...
                'MesherVersion', meshingAlgo);
        else
            [p,e,t]=initmesh(dl1,'hmax',trisize,'jiggle',jiggle,'Hgrad',Hgrad,...
                'jiggleiter',str2double(jiggleiter), 'MesherVersion', meshingAlgo);
        end
    end

    meshhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
    h=findobj(get(meshhndl,'Children'),'flat','Tag','PDERefine');
    set(h,'Enable','on');
    h=findobj(get(meshhndl,'Children'),'flat','Tag','PDEUnrefine');
    set(h,'Enable','off');

    % erase old mesh
    h=findobj(get(ax,'Children'),'flat','Tag','PDEMeshLine');
    delete(h)

    % create and plot the mesh
    h=pdeplot(p,e,t,'intool','on');
    set(h,'Tag','PDEMeshLine');

    % save p, e, t
    set(findobj(get(meshhndl,'Children'),'flat','Tag','PDEInitMesh'),...
        'UserData',p)
    set(findobj(get(meshhndl,'Children'),'flat','Tag','PDERefine'),...
        'UserData',e)
    set(findobj(get(meshhndl,'Children'),'flat','Tag','PDEMeshParam'),...
        'UserData',t)

    % if requested, label nodes and/or triangles:
    if getappdata(pde_fig,'shownodelbl')
        pdetool('shownodelbl',1)
    end
    if getappdata(pde_fig,'showtrilbl')
        pdetool('showtrilbl',1)
    end

    % enable export
    set(findobj(get(meshhndl,'Children'),'flat','Tag','PDEExpMesh'),...
        'Enable','on')

    set(pde_fig,'Pointer','arrow')
    drawnow

    infostr=sprintf('Initialized mesh consists of %i nodes and %i triangles.',...
        size(p,2),size(t,2));
    pdeinfo(infostr);

    flags=get(flg_hndl,'UserData');
    flags(1)=1;                           % need_save=1
    flags(4)=0;                           % flag2=0
    flags(5)=1;                           % flag3=1
    flags(6)=0;                           % flag4=0
    set(flg_hndl,'UserData',flags)

    setappdata(pde_fig,'meshstat',1)

    % save this generation of the mesh for unrefine purposes
    setappdata(pde_fig,'p1',p);
    setappdata(pde_fig,'e1',e);
    setappdata(pde_fig,'t1',t);

    % Update max no of triangle default for adaptive solver
    % Double the current no, or 1000, whichever is greatest:
    solveparams=getappdata(pde_fig,'solveparam');
    tristr=int2str(max(1.5*size(t,2),1000));
    setappdata(pde_fig,'solveparam',...
        char(solveparams(1,:),tristr,solveparams(3:11,:)))

    % disable removal of interior subdomain boundaries
    menuhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEBoundMenu');
    hbound(1)=findobj(get(menuhndl,'Children'),'flat','Tag','PDERemBord');
    hbound(2)=findobj(get(menuhndl,'Children'),'flat','Tag','PDERemAllBord');
    set(hbound,'Enable','off')

    edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
    set([edithndl findobj(get(edithndl,'Children'),'flat','Tag','PDESelall')],...
        'Enable','off')

    % Enable clicking on mesh to get info about triangle and node no's:
    set(pde_fig,'WindowButtonDownFcn','pdeinfclk(1)',...
        'WindowButtonUpFcn','if pdeonax, pdeinfo; end')
end