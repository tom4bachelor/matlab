function pdetool_drawrect(flag)
% case: draw rectangle/square
%       (flag=1 = drag from corner, flag=2 = drag from center)

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(h,'UserData'); mode_flag=flags(2);
    if mode_flag
        pdetool('changemode',0)
    end

    if btnstate(pde_fig,'zoom',1)
        pdezoom(pde_fig,'off')
        btnup(pde_fig,'zoom',1)
        opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
        set(findobj(get(opthndl,'Children'),'flat','Tag','PDEZoom'),...
            'Checked','off')
    end

    btst=btnstate(pde_fig,'draw',1:5);
    if strcmp(get(pde_fig,'SelectionType'),'open')
        btndown(pde_fig,'draw',flag);
        sticks=zeros(6,1);
        sticks(flag)=1;
        setappdata(pde_fig,'stick',sticks);
        return;
    else
        drawhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEDrawMenu');
        ghndls=get(drawhndl,'UserData');
        if flag==1
            my_hndl=findobj(get(drawhndl,'Children'),'flat','Tag','PDERect');
        elseif flag==2
            my_hndl=findobj(get(drawhndl,'Children'),'flat','Tag','PDERectc');
        end
        if pdeumtoggle(my_hndl)
            ghndls=ghndls(ghndls~=my_hndl);
            set(ghndls,'Checked','off')
            if ~btst(flag)
                btndown(pde_fig,'draw',flag)
                btst(flag)=1;
            end
            if btst(flag)
                btst(flag)=0;
                if any(btst)
                    btnup(pde_fig,'draw',find(btst));
                    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
                    axKids=get(ax,'Children');
                    lines=findobj(axKids,'flat','Tag','PDELine');
                    % Erase any unfinished polygon lines
                    if ~isempty(lines)
                        delete(lines)
                        user_data=get(pde_fig,'UserData');
                        user_data=[user_data(1:4) zeros(1,4)];
                        set(pde_fig,'UserData',user_data)
                    end
                end
            end

            if flag==1
                pdeinfo('Click at corner and drag to create rectangle/square.');
                set(pde_fig,'WindowButtonDownFcn','pdetool(''rectstart'',1)',...
                    'WindowButtonUpFcn','');
            elseif flag==2
                pdeinfo('Click at center and drag to create rectangle/square.');
                set(pde_fig,'WindowButtonDownFcn','pdetool(''rectstart'',2)',...
                    'WindowButtonUpFcn','');
            end
        else
            btnup(pde_fig,'draw',flag)
            set(pde_fig,'WindowButtonDownFcn','pdeselect select')
            setappdata(pde_fig,'stick',zeros(6,1));
            pdeinfo('Draw and edit 2-D geometry by using the Draw and Edit menu options.');
            return;
        end
    end

    edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
    hndl=findobj(get(edithndl,'Children'),'flat','Tag','PDEUndo');
    set(hndl,'Enable','off');
end