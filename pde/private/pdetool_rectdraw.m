function pdetool_rectdraw()
% case: mouse button up to draw rectangle

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    hndl=get(ax,'UserData');
    if isempty(hndl), return; end

    set(pde_fig,'WindowButtonMotionFcn','pdemtncb(0)',...
        'WindowButtonUpFcn','');

    x=get(hndl,'Xdata'); y=get(hndl,'Ydata');
    delete(hndl);
    set(ax,'UserData',[]);

    if y(3)~=y(1) && x(3)~=x(1)

        sticks=getappdata(pde_fig,'stick');
        if ~any(sticks)
            set(pde_fig,'WindowButtonDownFcn','pdeselect select')
        end

        pderect([x(1:2) y([1 3])]);

        if ~any(sticks)
            drawnow;
            btst=btnstate(pde_fig,'draw');
            if any(btst)
                btnup(pde_fig,'draw',...
                    find(btst))
            end
            drawhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEDrawMenu');
            h=findobj(get(drawhndl,'Children'),'flat','Checked','on');
            set(h,'Checked','off')

            pdeinfo('Draw and edit 2-D geometry by using the Draw and Edit menu options.');
        end

    end
end