function pdetool_pdenodelbl()
% case: manage mesh node labeling flags and indication

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(h,'UserData'); mode_flag=flags(2);

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
    nodeh=findobj(get(h,'Children'),'flat','Tag','PDEShowNode');

    if pdeumtoggle(nodeh)
        setappdata(pde_fig,'shownodelbl',1);
        if mode_flag==2
            pdetool('shownodelbl',1)
        end
    else
        setappdata(pde_fig,'shownodelbl',0);
        if mode_flag==2
            pdetool('shownodelbl',0)
        end
    end
end