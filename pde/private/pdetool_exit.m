function pdetool_exit()
% case: exit

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(h,'UserData');
    need_save=flags(1);

    if need_save
        curr_file=get(findobj(get(pde_fig,'Children'),'flat',...
            'Tag','PDEEditMenu'),'UserData');
        if isempty(curr_file)
            queststr=' Save changes in ''Untitled'' ?';
        else
            [~,file_name] = fileparts(curr_file);
            queststr=[' Save changes in ' file_name '?'];
        end

        answr=questdlg(queststr,'Exit','Yes','No','Cancel','Yes');

    else
        answr = 'No';
    end

    if ~strcmp(answr,'Cancel')
        if strcmp(answr,'Yes')
            if isempty(curr_file)
                pdetool('save_as');
            else
                pdetool('save');
            end
        end

        % Close main PDETOOL figure and all open dialog boxes
        figs=allchild(0);
        for i=1:length(figs)
            figlbl=lower(get(figs(i),'Tag'));
            if ~isempty(strfind(figlbl,'pde'))
                delete(figs(i))
            end
        end
    end
end