function pdetool_drawellipse(flag)
% case: draw ellipse/circle
%       (flag=1 = drag from corner, flag=2 = drag from center)

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(h,'UserData'); mode_flag=flags(2);
    if mode_flag
        pdetool('changemode',0)
    end

    if btnstate(pde_fig,'zoom',1)
        pdezoom(pde_fig,'off')
        btnup(pde_fig,'zoom',1)
        opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
        set(findobj(get(opthndl,'Children'),'flat','Tag','PDEZoom'),...
            'Checked','off')
    end

    btst=btnstate(pde_fig,'draw',1:5);
    if strcmp(get(pde_fig,'SelectionType'),'open')
        btndown(pde_fig,'draw',flag+2);
        sticks=zeros(6,1);
        sticks(flag+2)=1;
        setappdata(pde_fig,'stick',sticks);
        return;
    else
        drawhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEDrawMenu');
        ghndls=get(drawhndl,'UserData');
        if flag==1
            my_hndl=findobj(get(drawhndl,'Children'),'flat','Tag','PDEEllip');
        elseif flag==2
            my_hndl=findobj(get(drawhndl,'Children'),'flat',...
                'Tag','PDEEllipc');
        end

        if pdeumtoggle(my_hndl)
            ghndls=ghndls(ghndls~=my_hndl);
            set(ghndls,'Checked','off')
            if ~btst(flag+2)
                btndown(pde_fig,'draw',flag+2)
                btst(flag+2)=1;
            end
            if btst(flag+2)
                btst(flag+2)=0;
                if any(btst)
                    btnup(pde_fig,'draw',find(btst));
                    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
                    axKids=get(ax,'Children');
                    lines=findobj(axKids,'flat','Tag','PDELine');
                    % Erase any unfinished polygon lines
                    if ~isempty(lines)
                        delete(lines)
                        user_data=get(pde_fig,'UserData');
                        user_data=[user_data(1:4) zeros(1,4)];
                        set(pde_fig,'UserData',user_data)
                    end
                end
            end
        else
            btnup(pde_fig,'draw',flag+2)
            set(pde_fig,'WindowButtonDownFcn','pdeselect select',...
                'WindowButtonUpFcn','');
            setappdata(pde_fig,'stick',zeros(6,1));
            pdeinfo('Draw and edit 2-D geometry by using the Draw and Edit menu options.');
            return;
        end
    end

    if flag==1
        pdeinfo('Click on perimeter and drag to create ellipse/circle.');
        set(pde_fig,'WindowButtonDownFcn','pdetool(''circlecntr'',1)',...
            'WindowButtonUpFcn','');
    elseif flag==2
        pdeinfo('Click at center and drag to create ellipse/circle.');
        set(pde_fig,'WindowButtonDownFcn','pdetool(''circlecntr'',2)',...
            'WindowButtonUpFcn','');
    end

    edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
    hndl=findobj(get(edithndl,'Children'),'flat','Tag','PDEUndo');
    set(hndl,'Enable','off');
end