function pdetool_pdemode()
% case: enter PDE mode (enter PDE coefficients per subdomain)
% if no geometry, create a default L-shape:

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    pdegd=get(findobj(get(pde_fig,'Children'),'flat',...
        'Tag','PDEMeshMenu'),'UserData');
    if isempty(pdegd), pdetool('membrane'), end

    if btnstate(pde_fig,'zoom',1)
        pdezoom(pde_fig,'off')
        btnup(pde_fig,'zoom',1)
        opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
        set(findobj(get(opthndl,'Children'),'flat','Tag','PDEZoom'),...
            'Checked','off')
    end

    flg_hndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(flg_hndl,'UserData');
    flag1=flags(3);
    if abs(flag1)
        pdetool('changemode',0)
    end
    flags=get(flg_hndl,'UserData');
    if flags(3)==-1
        % error in decsg
        return;
    elseif ~flags(2)
        pdetool('cleanup')
    end
    flags(2)=4;           % mode_flag=4
    set(flg_hndl,'UserData',flags)

    pdeinfo('Click on subdomains to select. Double-click to open PDE Specification dialog box.');

    edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
    set(edithndl,'Enable','on')
    set(findobj(get(edithndl,'Children'),'flat','Tag','PDESelall'),...
        'Enable','on','CallBack','pdetool(''subclk'',1)')

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    set(pde_fig,'CurrentAxes',ax,...
        'WindowButtonDownFcn','pdetool(''subclk'')',...
        'WindowButtonUpFcn','')

    pdetool('clearsol')

    % turn off everything else
    kids=get(ax,'Children');
    set(kids,'Visible','off')

    boundh=findobj(get(pde_fig,'Children'),'flat','Tag','PDEBoundMenu');
    dl=get(boundh,'UserData');
    bt=get(get(ax,'Title'),'UserData');
    msb=getappdata(pde_fig,'msb');

    h=findobj(get(ax,'Children'),'flat','Tag','PDESubDom');
    if isempty(h)
        pdepatch(dl,bt,msb,1)
    else
        set(h,'Visible','on');
    end

    % reset selection array and display the subdomain borders:
    setappdata(ax,'subsel',[])
    h=findobj(get(ax,'Children'),'flat','Tag','PDESelRegion');
    if ~isempty(h), delete(h); end
    dl1=getappdata(pde_fig,'dl1');
    pdebddsp(dl1)

    % if requested, label subdomains:
    if getappdata(pde_fig,'showpdesublbl')
        pdetool('showsublbl',1)
    end

    % disable removal of interior subdomain boundaries
    menuhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEBoundMenu');
    hbound(1)=findobj(get(menuhndl,'Children'),'flat','Tag','PDERemBord');
    hbound(2)=findobj(get(menuhndl,'Children'),'flat','Tag','PDERemAllBord');
    set(hbound,'Enable','off')
end