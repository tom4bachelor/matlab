function pdetool_lineclk()
% case: click to draw line (lineclk)

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    if ~pdeonax(ax), return, end

    user_data=get(pde_fig,'UserData');
    i=user_data(5);
    x=user_data(6:6+i-1); y=user_data(6+i:6+2*i-1);
    line_hndls=user_data(6+2*i:6+3*i-2);
    i=i+1;

    set(pde_fig,'CurrentAxes',ax)
    if strcmp(get(pde_fig,'SelectionType'),'alt') && i>1
        x(i)=x(1); y(i)=y(1);
    else
        pv=get(ax,'CurrentPoint');
        [x(i),y(i)]=pdesnap(ax,pv,getappdata(ax,'snap'));
    end

    if i==1
        % Make sure user_data is a row
        user_data = user_data(:)';
        user_data=[user_data(1:4) i x y line_hndls(:)'];
        set(pde_fig,'UserData',user_data);
        i=1+1;
        x(i)=x(i-1); y(i)=y(i-1);
    end

    hold on;
    hndl=line([x(i-1) x(i)],[y(i-1) y(i)],'color','r',...
        'linestyle','-');
    hold off;
    set(ax,'UserData',hndl)

    set(pde_fig,'WindowButtonMotionFcn','pdemtncb(10)',...
        'WindowButtonUpFcn','pdetool linedraw')
end