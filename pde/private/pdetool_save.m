function pdetool_save()
% case: save geometry in MATLAB-file (save to current file)

%   Copyright 1994-2013 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    curr_file=get(findobj(get(pde_fig,'Children'),'flat',...
        'Tag','PDEEditMenu'),'UserData');

    fid=fopen(curr_file,'w');
    if fid>0
        pdetool('write',fid);
        h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
        flags=get(h,'UserData');
        flags(1)=0;                         % need_save=0.
        set(h,'UserData',flags)
    else
        pdetool('error',' Can''t open file');
    end
end