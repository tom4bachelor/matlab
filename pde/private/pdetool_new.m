function pdetool_new()
% case: new geometry

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(h,'UserData');
    need_save=flags(1);

    % A flag value is sent from PDEINIT to prevent
    % question dialog when starting from the command line
    % using a model MATLAB-file.
    if need_save
        curr_file=get(findobj(get(pde_fig,'Children'),'flat',...
            'Tag','PDEEditMenu'),'UserData');
        if isempty(curr_file)
            queststr=' Save changes in ''Untitled'' ?';
        else
            [~,file_name] = fileparts(curr_file);
            queststr=[' Save changes in ' file_name '?'];
        end

        answr=questdlg(queststr,'New','Yes','No','Cancel','Yes');

    else
        answr = 'No';
    end

    if strcmp(answr,'Cancel'), return, end

    if strcmp(answr,'Yes')
        if isempty(curr_file)
            pdetool('save_as');
        else
            pdetool('save');
        end
    end

    pdeinfo('Draw 2-D geometry.');

    % Reset PDEGD matrix
    meshhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
    set(meshhndl,'UserData',[]);
    bndhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEBoundMenu');
    % Reset PDEDL matrix
    set(bndhndl,'UserData',[]);
    % Reset PDEBOUND matrix:
    set(findobj(get(bndhndl,'Children'),'flat','Tag','PDEBoundMode'),...
        'UserData',[]);
    % Set current file name to an empty string
    set(findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu'),...
        'UserData',[]);

    % Disable export of boundary data
    set(findobj(get(bndhndl,'Children'),'flat','Tag','PDEExpBound'),...
        'Enable','off')

    % Disable export of mesh
    set(findobj(get(meshhndl,'Children'),'flat','Tag','PDEExpMesh'),...
        'Enable','off')

    % Turn off zoom
    if btnstate(pde_fig,'zoom',1)
        pdezoom(pde_fig,'out')
        pdezoom(pde_fig,'off')
        btnup(pde_fig,'zoom',1)
        opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
        set(findobj(get(opthndl,'Children'),'flat','Tag','PDEZoom'),...
            'Checked','off')
    end

    ax=findobj(allchild(pde_fig),'flat','Tag','PDEAxes');

    flagh=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(flagh,'UserData'); mode_flag=flags(2);
    % some extra restoration job if New is invoked when in solve mode:
    if mode_flag
        hndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEval');
        set(hndl,'Enable','on');
        set(pde_fig,'Colormap',get(get(ax,'XLabel'),'UserData'))

        % delete colorbar and solution plot
        delete(findobj(get(pde_fig,'Children'),'flat','Tag','Colorbar'))
        h=get(ax,'UserData');
        delete(h)
        set(ax,'UserData',[],'Position',getappdata(ax,'axstdpos'))

        % clear boundary selection vector
        bndhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEBoundMenu');
        set(findobj(get(bndhndl,'Children'),'flat','Tag','PDEBoundSpec'),...
            'UserData',[]);

        % turn off labeling if needed
        if getappdata(pde_fig,'showsublbl')
            pdetool sublbl
        end
        if getappdata(pde_fig,'showpdesublbl')
            pdetool pdesublbl
        end
        if getappdata(pde_fig,'showedgelbl')
            pdetool edgelbl
        end
        if getappdata(pde_fig,'showtrilbl')
            pdetool pdetrilbl
        end
        if getappdata(pde_fig,'shownodelbl')
            pdetool pdenodelbl
        end
    end

    % reset all flags
    set(flagh,'UserData',zeros(7,1));

    filehndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    hndl=findobj(get(filehndl,'Children'),'flat','Tag','PDESave');
    edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
    hndl=[hndl findobj(get(edithndl,'Children'),'flat','Tag','PDEUndo')...
        findobj(get(edithndl,'Children'),'flat','Tag','PDECut')...
        findobj(get(edithndl,'Children'),'flat','Tag','PDECopy')...
        findobj(get(edithndl,'Children'),'flat','Tag','PDEClear')];
    drawhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEDrawMenu');
    hndl=[hndl findobj(get(drawhndl,'Children'),'flat','Tag','PDEExpGD')];
    set(hndl,'Enable','off');
    set(edithndl,'Enable','on')
    set(findobj(get(edithndl,'Children'),'flat','Tag','PDESelall'),...
        'Enable','on','CallBack','pdeselect(''select'',1)')

    set(pde_fig,'name','PDE Toolbox - [Untitled]');

    hndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEval');
    set(hndl,'String','','UserData','');

    snap=getappdata(ax,'snap');

    hndl=[findobj(get(ax,'Children'),'flat','Type','line')',...
        findobj(get(ax,'Children'),'flat','Type','text','Tag','PDELabel')',...
        findobj(get(ax,'Children'),'flat','Type','text','Tag','PDELblSel')',...
        findobj(get(ax,'Children'),'flat','Type','patch')'];
    delete(hndl)

    set(get(ax,'Title'),'String','');

    user_data=get(pde_fig,'UserData');
    user_data(5)=0;                       % pde_i
    user_data(6:7)=[0 0]; user_data=user_data(1:7);

    set(pde_fig,'UserData',user_data)

    set(get(ax,'ZLabel'),'UserData',[])

    % un-select all draw features
    for i=1:5
        btnup(pde_fig,'draw',i);
    end
    h=findobj(get(drawhndl,'Children'),'flat','Checked','on');
    set(h,'Checked','off')

    % enable 'Rotate...':
    set(findobj(get(drawhndl,'Children'),'flat','Tag','PDERotate'),...
        'Enable','on');

    % reset property containers
    set(pde_fig,'WindowButtonDownFcn','pdeselect select');
    setappdata(pde_fig,'objnames',[]);
    setappdata(pde_fig,'nodehandles',[]);
    setappdata(pde_fig,'trihandles',[]);
    setappdata(pde_fig,'meshstat',[])
    setappdata(pde_fig,'bl',[])
    setappdata(ax,'snap',snap);
    setappdata(ax,'subsel',[]);
    setappdata(ax,'bordsel',[]);
    setappdata(ax,'selinit',0);

    % Restore max no of triangles for adaptive solver to 1000
    solveparams=getappdata(pde_fig,'solveparam');
    setappdata(pde_fig,'solveparam',...
        char(solveparams(1,:),'1000',solveparams(3:11,:)))

    set(pde_fig,'CurrentAxes',ax)

    refresh(pde_fig)
end