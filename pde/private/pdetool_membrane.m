function pdetool_membrane()
% case: membrane (create L-shaped geometry as default if no
%       geometry created; solution in the elliptic case will
%       then resemble MATLAB's 'membrane' logo).

%   Copyright 1994-2013 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    mx=max(max(get(ax,'XLim')),max(get(ax,'YLim')));
    l=10^(floor(log10(mx)));
    pdepoly([-l, l, l, 0, 0, -l],...
        [-l, -l, l, l, 0, 0])
end