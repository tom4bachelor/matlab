function pdetool_print()
% case: print pde toolbox figure

%   Copyright 1994-2013 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    printFig = pdetoolGetViewportFig(pde_fig);
    printdlg(printFig);
    delete(printFig);
end