function pdetool_linedraw()
% case: button up function to draw line or completed polygon

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    if ~pdeonax(ax), return, end

    user_data=get(pde_fig,'UserData');
    i=user_data(5);
    x=user_data(6:6+i-1); y=user_data(6+i:6+2*i-1);
    line_hndls=user_data(6+2*i:6+3*i-2);
    i=i+1;

    if strcmp(get(pde_fig,'SelectionType'),'alt') && i>1
        x(i)=x(1); y(i)=y(1);
    else
        pv=get(ax,'CurrentPoint');
        [x(i),y(i)]=pdesnap(ax,pv,getappdata(ax,'snap'));
        % Use a tolerance to check if the current point is different
        if i==2 && abs(diff(x)) < 1000*eps && abs(diff(y))< 1000*eps
            % Click for first point and no drag
            i=1; x=x(1); y=y(1);
            user_data=[user_data(1:4) i x y line_hndls];
            set(pde_fig,'UserData',user_data)
            set(pde_fig,'WindowButtonMotionFcn','pdemtncb(0)','CurrentAxes',ax)
            delete(get(ax,'UserData'));
            set(ax,'UserData',[]);
            return;
        end
    end

    set(pde_fig,'WindowButtonMotionFcn','pdemtncb(0)','CurrentAxes',ax)

    delete(get(ax,'UserData'));
    set(ax,'UserData',[]);

    edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
    undo_hndl=findobj(get(edithndl,'Children'),'flat','Tag','PDEUndo');
    set(undo_hndl,'Enable','on');
    xlim=get(ax,'Xlim');
    ylim=get(ax,'Ylim');
    gobj=[2, length(x), x, y]';
    % Check polygon:
    polystat=csgchk(gobj,diff(xlim),diff(ylim));
    if polystat>1
        % Self-intersecting open or closed polygon
        pdetool('error','  Polygon lines must not overlap or intersect.');
        if i==2
            i=0;
            x=[]; y=[];
        else
            i=i-1;
            x=x(1:i); y=y(1:i);
        end
    elseif polystat==1
        % OK open polygon
        line_hndls(i-1)=line(x(i-1:i),...
            y(i-1:i),...
            'Tag','PDELine',...
            'Color','r');
    elseif polystat==0
        % OK closed polygon
        set(pde_fig,'WindowButtonUpFcn','');
        sticks=getappdata(pde_fig,'stick');
        if ~any(sticks)
            set(pde_fig,'WindowButtonDownFcn','pdeselect select')
        end
        % polygon (lines):
        hndls=findobj(get(ax,'Children'),'flat',...
            'Tag','PDELine');
        delete(hndls)
        pdepoly(x(1:i-1),y(1:i-1));

        if ~any(sticks)
            drawnow;
            btst=btnstate(pde_fig,'draw');
            if any(btst)
                btnup(pde_fig,'draw',...
                    find(btst))
            end
            drawhndl=findobj(get(pde_fig,'Children'),'flat',...
                'Tag','PDEDrawMenu');
            h=findobj(get(drawhndl,'Children'),'flat','Checked','on');
            set(h,'Checked','off')

            pdeinfo('Draw and edit 2-D geometry by using the Draw and Edit menu options.');
        end

        set(undo_hndl,'Enable','off');
        i=0; x=[]; y=[];                    % polygon done; restart.
    end

    user_data=[user_data(1:4) i x y line_hndls];
    set(pde_fig,'UserData',user_data)
end