function pdetool_removeb(flag)
% case: remove border (flag=1: remove all borders)

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(allchild(pde_fig),'flat','Tag','PDEAxes');
    set(pde_fig,'CurrentAxes',ax,'Pointer','watch')
    drawnow

    dl1=getappdata(pde_fig,'dl1');
    bt1=getappdata(pde_fig,'bt1');

    if nargin>0
        if flag==-1
            % Case: remove all borders
            bl=find(dl1(6,:)~=0 & dl1(7,:)~=0);
            if isempty(bl)
                set(pde_fig,'Pointer','arrow')
                drawnow
                return;
            end
            [dl1,bt1]=csgdel(dl1,bt1);
        else
            % flag contains bl; used from startup-file only
            bl=flag;
            [dl1,bt1]=csgdel(dl1,bt1,bl);
        end
    else
        bdsel=getappdata(ax,'bordsel');
        if isempty(bdsel)
            set(pde_fig,'Pointer','arrow')
            drawnow
            return;
        end
        n=length(bdsel);
        bl=[];
        for i=1:n
            bl=[bl get(bdsel(i),'UserData')]; %#ok - suppress preallocation warning
        end
        [dl1new,bt1new]=csgdel(dl1,bt1,bl);
        if (size(dl1new,2)==size(dl1,2))
            set(pde_fig,'Pointer','arrow')
            drawnow
            return;
        else
            dl1=dl1new; bt1=bt1new;
        end
    end

    blsaved=getappdata(pde_fig,'bl');
    if ~isempty(bl)
        i=size(blsaved,1)+1;
        blsaved(i,1:size(bl,2))=bl;
        setappdata(pde_fig,'bl',blsaved)
    end

    % save new dl1, bt1
    setappdata(pde_fig,'dl1',dl1)
    setappdata(pde_fig,'bt1',bt1)

    % compute new coarse initmesh for labeling purposes
    [pinit2,~,tinit2]=initmesh(dl1,'hmax',Inf,'init','on');
    setappdata(pde_fig,'pinit2',pinit2)
    setappdata(pde_fig,'tinit2',tinit2)

    % Set flag 2 to force new mesh initialization:
    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(h,'UserData');
    flags(4)=1;
    set(h,'UserData',flags)

    setappdata(ax,'bordsel',[])

    hh=[findobj(get(ax,'Children'),'flat','Tag','PDEBoundLine')'...
        findobj(get(ax,'Children'),'flat','Tag','PDEBorderLine')'];
    delete(hh)

    appl=getappdata(pde_fig,'application');
    scalarflag=any(appl<2 | appl>4);
    pdetool('initbounds',scalarflag)

    pdetool('drawbounds')

    if getappdata(pde_fig,'showsublbl')
        pdetool('showsublbl',1)
    else
        pdetool('showsublbl',0)
    end
    if getappdata(pde_fig,'showedgelbl')
        pdetool('showedgelbl',1)
    else
        pdetool('showedgelbl',0)
    end

    set(pde_fig,'Pointer','arrow')
    drawnow
end