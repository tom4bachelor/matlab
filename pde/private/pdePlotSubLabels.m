function pdePlotSubLabels( p, t, n, ax, plotprefix )
%PDEPLOTSUBLABELS Plot subdomain labels on decomposed geometry
%   p is the point matrix returned from |initmesh|
%   t is the triangle matrix returned from |initmesh|
%   n is the number of subdomains in the model
%   ax is the axes handle from the edge plot on which the edge labels
%      are to be plotted. 
%   plotprefix is a logical that requests the prefix 'F' to be plotted


%       Copyright 2012-2016 The MathWorks, Inc.

A=pdetrg(p,t);

for i=1:n
  tri=find(t(4,:)==i);
  Amax=find(A(tri)==max(A(tri))); Amax=Amax(1);
  xm=mean(p(1,t(1:3,tri(Amax))));
  ym=mean(p(2,t(1:3,tri(Amax))));
  if plotprefix
      facelabels =  sprintf('F%d', i);
  else
      facelabels = int2str(i);
  end  
  text('String',facelabels,'Units','data',...
    'Clipping','on',...
    'Tag','PDESubLabel','HorizontalAlignment','center',...
    'pos',[xm ym 0],'color','red',...
    'Parent',ax);
end

end

% LocalWords:  subdomain subdomains plotprefix
