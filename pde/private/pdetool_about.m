function pdetool_about()
% case: 'About PDE Toolbox' display

%   Copyright 1994-2013 The MathWorks, Inc.
  
    pdeVer = ver('pde');
    s = [sprintf(' Partial Differential Equation Toolbox %s\n', pdeVer.Version) ...
        sprintf('Copyright 1994-%s The MathWorks, Inc.',pdeVer.Date(end-3:end))];
    msgbox(s,'About PDE Toolbox','modal')
end