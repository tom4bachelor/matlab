function pdetool_boundmode()
% case: enter boundary mode (define boundary conditions, alter subdomains)
%       if no geometry, create a default L-shape:

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    pdegd=get(findobj(get(pde_fig,'Children'),'flat',...
        'Tag','PDEMeshMenu'),'UserData');
    if isempty(pdegd), pdetool('membrane'), end

    if btnstate(pde_fig,'zoom',1)
        pdezoom(pde_fig,'off')
        btnup(pde_fig,'zoom',1)
        opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
        set(findobj(get(opthndl,'Children'),'flat','Tag','PDEZoom'),...
            'Checked','off')
    end

    flg_hndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(flg_hndl,'UserData');
    flag1=flags(3);
    if abs(flag1)
        pdetool('changemode',0)
    end
    flags=get(flg_hndl,'UserData');
    if flags(3)==-1
        % error in decsg
        return;
    elseif ~flags(2)
        pdetool('cleanup')
    end

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    set(pde_fig,'CurrentAxes',ax,...
        'WindowButtonDownFcn','',...
        'WindowButtonUpFcn','')

    flags=get(flg_hndl,'UserData');
    if flags(2)~=1
        flags(2)=1;                         % mode_flag=1
        set(flg_hndl,'UserData',flags)

        pdetool('clearsol')

        % turn off everything else, light up boundary and subdomain lines
        kids=get(ax,'Children'); set(kids,'Visible','off')

        hh=[findobj(get(ax,'Children'),'flat','Tag','PDEBoundLine')'...
            findobj(get(ax,'Children'),'flat','Tag','PDEBorderLine')'];
        if isempty(hh)
            pdetool('drawbounds')
        else
            bndhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEBoundMenu');
            selecth=findobj(get(bndhndl,'Children'),'flat','Tag','PDEBoundSpec');
            selbounds=get(selecth,'UserData');
            set(selecth,'UserData',[])
            for i=1:length(selbounds)
                col=get(selbounds(i),'UserData');
                set(selbounds(i),'color',col(2:4))
            end
            set(hh,'Visible','on')
        end
    end

    pdeinfo('Click to select boundaries. Double-click to open boundary condition dialog box.');

    edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
    set(edithndl,'Enable','on')
    set(findobj(get(edithndl,'Children'),'flat','Tag','PDESelall'),...
        'Enable','on','CallBack','pdetool(''boundclk'',1)')

    % enable removal of interior subdomain boundaries
    menuhndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEBoundMenu');
    hbound(1)=findobj(get(menuhndl,'Children'),'flat','Tag','PDERemBord');
    hbound(2)=findobj(get(menuhndl,'Children'),'flat','Tag','PDERemAllBord');
    set(hbound,'Enable','on')

    % if requested, label subdomains and/or edges:
    if getappdata(pde_fig,'showsublbl')
        pdetool('showsublbl',1)
    end
    if getappdata(pde_fig,'showedgelbl')
        pdetool('showedgelbl',1)
    end
end