function pdePlotVertexLabels( ag, ax )
%PDEPLOTVERTEXLABELS(ag, ax, plotprefix) Plot vertex labels on a 2D
%   AnalyticGeometry. ag is a pdeAnalyticGeometry, ax is the axes handle 
%   from the geometry plot on which the vertex labels are to be plotted.

%    Copyright 2016 The MathWorks, Inc.

vxl = ag.vertexLabelLocations();
x = vxl{1};
y = vxl{2};
z = zeros(size(x));
vxlabels = arrayfun(@(n) {sprintf('V%d', n)}, (1:numel(x))');
text(x, y, z,vxlabels,'Units','data',...
    'Clipping','on',...
    'Tag','PDEVertexLabel','HorizontalAlignment','center',...
    'color','k','Parent',ax);

end

