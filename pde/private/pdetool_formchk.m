function pdetool_formchk()
% case: check if set formula has changed:

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    hndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEval');
    newstr=deblank(get(hndl,'String'));
    oldstr=get(hndl,'UserData');
    change=~strcmp(newstr,oldstr);
    if change
        set(hndl,'UserData',newstr)
        % set flags: need_save and flag1
        h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
        flags=get(h,'UserData');
        flags(1)=1; flags(3)=1;
        set(h,'UserData',flags)
        setappdata(pde_fig,'meshstat',[]);
        setappdata(pde_fig,'bl',[]);
    end
end