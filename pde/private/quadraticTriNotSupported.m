function quadraticTriNotSupported(T)
    if size(T, 1) == 7
        error(message('pde:pdeSanityChecker:QuadraticTriNotSupported'));
    end
end