function pdetool_zoom()
% case: zoom

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    opthndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEOptMenu');
    zoomhndl=findobj(get(opthndl,'Children'),'flat','Tag','PDEZoom');

    if btnstate(pde_fig,'zoom',1) && strcmp(get(zoomhndl,'Checked'),'off')
        set(zoomhndl,'Checked','on');
    elseif btnstate(pde_fig,'zoom',1) && strcmp(get(zoomhndl,'Checked'),'on')
        set(zoomhndl,'Checked','off');
        btnup(pde_fig,'zoom',1)
    elseif ~btnstate(pde_fig,'zoom',1) && strcmp(get(zoomhndl,'Checked'),'on')
        set(zoomhndl,'Checked','off');
    elseif ~btnstate(pde_fig,'zoom',1) && strcmp(get(zoomhndl,'Checked'),'off')
        set(zoomhndl,'Checked','on');
        btndown(pde_fig,'zoom',1)
    end

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    if btnstate(pde_fig,'zoom',1)

        pdeinfo('Click (+drag) to zoom. Double-click to restore.');

        % save WindowButtonDownFcn and WindowButtonUpFcn before invoking zoom:
        setappdata(pde_fig,'WinBtnDownFcn',get(pde_fig,'WindowButtonDownFcn'))
        setappdata(pde_fig,'WinBtnUpFcn',get(pde_fig,'WindowButtonUpFcn'))
        setappdata(ax,'armodes',...
            {get(ax,'DataAspectRatioMode'), get(ax,'PlotBoxAspectRatioMode')})
        set(ax,'DataAspectRatioMode','auto',...
            'PlotBoxAspectRatioMode','auto');
        pdezoom(pde_fig,'on')

    else

        armodes = getappdata(ax,'armodes');
        set(ax,'DataAspectRatioMode',armodes{1},...
            'PlotBoxAspectRatioMode',armodes{2});
        pdezoom(pde_fig,'off')
        % restore WindowButtonDownFcn and WindowButtonUpFcn:
        set(pde_fig,'WindowButtonDownFcn',getappdata(pde_fig,'WinBtnDownFcn'),...
            'WindowButtonUpFcn',getappdata(pde_fig,'WinBtnUpFcn'))
        pdeinfo('Zoom turned off.');
    end
end