function pdetool_drawbounds()
% case: draw the internal and external boundary segments

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    flg_hndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(flg_hndl,'UserData'); flag1=flags(3);

    pde_kids = allchild(pde_fig);
    ax=findobj(pde_kids,'flat','Tag','PDEAxes');

    dl1=getappdata(pde_fig,'dl1');
    bounds=[find(dl1(7,:)==0) find(dl1(6,:)==0)];

    pde_circ=1;
    pde_poly=2;
    pde_ellip=4;
    
    polys=find(dl1(1,:)==pde_poly);
    circles=find(dl1(1,:)==pde_circ);
    ellipses=find(dl1(1,:)==pde_ellip);
    
    %Calculate x and y ranges using a bounding box in order to help with arrow scaling.
    bbXMin = 0;
    bbXMax = 0;
    bbYMin = 0;
    bbYMax = 0;
    if(~isempty(polys))
        xMinFromPolys = min(min(dl1([2,3],polys)));
        xMaxFromPolys = max(max(dl1([2,3],polys)));
        bbXMin = xMinFromPolys;
        bbXMax = xMaxFromPolys;
        yMinFromPolys = min(min(dl1([4,5],polys)));
        yMaxFromPolys = max(max(dl1([4,5],polys)));
        bbYMin = yMinFromPolys;
        bbYMax = yMaxFromPolys;
    end
    if(~isempty(circles))
        xMinFromCircles = min(dl1(8,circles) - dl1(10,circles));
        xMaxFromCircles = max(dl1(8,circles) + dl1(10,circles));
        bbXMin = min(bbXMin,xMinFromCircles);
        bbXMax = max(bbXMax,xMaxFromCircles);
        yMinFromCircles = min(dl1(9,circles) - dl1(10,circles));
        yMaxFromCircles = max(dl1(9,circles) + dl1(10,circles));
        bbYMin = min(bbYMin,yMinFromCircles);
        bbYMax = max(bbYMax, yMaxFromCircles);
    end
    if(~isempty(ellipses))
        xMinFromEllipses = min(dl1(8,ellipses) - dl1(10,ellipses));
        xMaxFromEllipses = max(dl1(8,ellipses) + dl1(10,ellipses));
        bbXMin = min(bbXMin,xMinFromEllipses);
        bbXMax = max(bbXMax,xMaxFromEllipses);
        yMinFromEllipses = min(dl1(9,ellipses) - dl1(11,ellipses));
        yMaxFromEllipses = max(dl1(9,ellipses) + dl1(11,ellipses));
        bbYMin = min(bbYMin,yMinFromEllipses);
        bbYMax = max(bbYMax,yMaxFromEllipses);
    end
    xRange = bbXMax - bbXMin;
    yRange = bbYMax - bbYMin;

    % Allright, if pdebound empty or if the geometry has changed (flag1=1),
    % initialize the boundary conditions to
    % dirichlet cond, u=0.0 on all boundaries:
    bndhndl=findobj(pde_kids,'flat','Tag','PDEBoundMenu');
    h=findobj(get(bndhndl,'Children'),'flat','Tag','PDEBoundMode');
    pdebound=get(h,'UserData');

    appl=getappdata(pde_fig,'application');
    scalarflag=any(appl<2 | appl>4);

    if isempty(pdebound) || flag1
        pdetool('initbounds',scalarflag)
        pdebound=get(h,'UserData');
    end

    % draw the decomposed boundaries:

    set(pde_fig,'CurrentAxes',ax)

    if ~isempty(polys)
        n1=length(polys); n2=length(bounds);
        col=0.5*ones(n1,3);
        tmp1=bounds(max(~(ones(n1,1)*bounds-(ones(n2,1)*polys)')));
        n3=length(tmp1);
        if n3==0
            tmp=[];
        elseif n3==1
            tmp=find(polys==tmp1);
        else
            tmp=find(max(~(ones(n3,1)*polys-(ones(n1,1)*tmp1)')));
        end

        if scalarflag
            dir=find(pdebound(2,polys(tmp))==1);
            m=length(dir);
            col(tmp(dir),:)=[ones(m,1) zeros(m,2)];
            neu=find(pdebound(2,polys(tmp))==0);
            m=length(neu);
            col(tmp(neu),:)=[zeros(m,2) ones(m,1)];
        else
            dir=find(pdebound(2,polys(tmp))==2);
            m=length(dir);
            col(tmp(dir),:)=[ones(m,1) zeros(m,2)];
            neu=find(pdebound(2,polys(tmp))==0);
            m=length(neu);
            col(tmp(neu),:)=[zeros(m,2) ones(m,1)];
            mix=find(pdebound(2,polys(tmp))==1);
            m=length(mix);
            col(tmp(mix),:)=[zeros(m,1) ones(m,1) zeros(m,1)];
        end

        for i=1:n1
            if isempty(find(bounds==polys(i), 1))
                line(dl1(2:3,polys(i)),...
                    dl1(4:5,polys(i)),'Tag','PDEBorderLine',...
                    'ButtonDownFcn','pdetool(''bordclk'')',...
                    'Parent',ax,...
                    'Color',col(i,:),'UserData',polys(i));
            else
                % make the boundary lines into arrows to indicate direction

                arrowX1=dl1(2,polys(i));
                arrowY1=dl1(4,polys(i));
                arrowX2=dl1(3,polys(i));
                arrowY2=dl1(5,polys(i));

                xdif = arrowX2 - arrowX1;
                ydif = arrowY2 - arrowY1;

                theta = atan2(ydif/yRange,xdif/xRange); % the angle has to point according to the (screen) slope

                xx = [arrowX1,arrowX2,... %the boundary line
                    NaN,...
                    (arrowX2+0.02*xRange*cos(theta+3*pi/4)), arrowX2,... %first arrow head line x values
                    NaN,...
                    (arrowX2+0.02*xRange*cos(theta-3*pi/4)), arrowX2]';  %second arrow head line x values
                yy = [arrowY1,arrowY2,...
                    NaN,...
                    (arrowY2+0.02*yRange*sin(theta+3*pi/4)), arrowY2, ... %first arrow head line y values
                    NaN,...
                    (arrowY2+0.02*yRange*sin(theta-3*pi/4)),arrowY2]';    %second arrow head line y values

                line(xx,yy,'Tag','PDEBoundLine',...
                    'Parent',ax,...
                    'Color',col(i,:),...
                    'ButtonDownFcn','pdetool(''boundclk'')',...
                    'UserData',[polys(i) col(i,:)]);

            end
        end
    end

    % draw the decomposed geometry 2 (circles)
    if ~isempty(circles)
        n1=length(circles); n2=length(bounds);
        col=0.5*ones(n1,3);
        if n1==1
            tmp1=find(bounds==circles);
        else
            tmp1=bounds(max(~(ones(n1,1)*bounds-(ones(n2,1)*circles)')));
        end
        n3=length(tmp1);
        if n3==0
            tmp=[];
        elseif n3==1
            if n1==1
                tmp=1;
            else
                tmp=find(circles==tmp1);
            end
        else
            tmp=find(max(~(ones(n3,1)*circles-(ones(n1,1)*tmp1)')));
        end
        if scalarflag
            dir=find(pdebound(2,circles(tmp))==1);
            m=length(dir);
            col(tmp(dir),:)=[ones(m,1) zeros(m,2)];
            neu=find(pdebound(2,circles(tmp))==0);
            m=length(neu);
            col(tmp(neu),:)=[zeros(m,2) ones(m,1)];
        else
            dir=find(pdebound(2,circles(tmp))==2);
            m=length(dir);
            col(tmp(dir),:)=[ones(m,1) zeros(m,2)];
            neu=find(pdebound(2,circles(tmp))==1);
            m=length(neu);
            col(tmp(neu),:)=[zeros(m,2) ones(m,1)];
            mix=find(pdebound(2,circles(tmp))==0);
            m=length(mix);
            col(tmp(mix),:)=[zeros(m,1) ones(m,1) zeros(m,1)];
        end

        delta=2*pi/100;
        for i=1:n1
            arg1=atan2(dl1(4,circles(i))-dl1(9,circles(i)),...
                dl1(2,circles(i))-dl1(8,circles(i)));
            arg2=atan2(dl1(5,circles(i))-dl1(9,circles(i)),...
                dl1(3,circles(i))-dl1(8,circles(i)));
            if arg2<0 && arg1>0, arg2=arg2+2*pi; end
            darg=abs(arg2-arg1);
            ns=max(2,ceil(darg/delta));
            arc=linspace(arg1,arg2,ns);
            x(1:ns)=(dl1(8,circles(i))+dl1(10,circles(i))*cos(arc))';
            y(1:ns)=(dl1(9,circles(i))+dl1(10,circles(i))*sin(arc))';
            if isempty(find(bounds==circles(i), 1))
                line(x(1:ns),y(1:ns),'Tag','PDEBorderLine',...
                    'Color',col(i,:),...
                    'Parent',ax,...
                    'ButtonDownFcn','pdetool(''bordclk'')',...
                    'UserData',circles(i));
            else
                % make the boundary lines into arrows to indicate direction

                arrowX1=x(ns-1); arrowY1=y(ns-1);
                arrowX2 =x(ns); arrowY2=y(ns);

                xdif = arrowX2 - arrowX1;
                ydif = arrowY2 - arrowY1;

                theta = atan2(ydif/yRange,xdif/xRange); % the angle has to point according to the (screen) slope

                xx = [x(1:ns),... %the boundary line
                    NaN,...
                    (arrowX2+0.02*xRange*cos(theta+3*pi/4)), arrowX2,... %first arrow head line x values
                    NaN,...
                    (arrowX2+0.02*xRange*cos(theta-3*pi/4)), arrowX2]';  %second arrow head line x values
                yy = [y(1:ns),...
                    NaN,...
                    (arrowY2+0.02*yRange*sin(theta+3*pi/4)), arrowY2, ... %first arrow head line y values
                    NaN,...
                    (arrowY2+0.02*yRange*sin(theta-3*pi/4)),arrowY2]';    %second arrow head line y values

                line(xx,yy,'Tag','PDEBoundLine',...
                    'Color',col(i,:),...
                    'Parent',ax,...
                    'ButtonDownFcn','pdetool(''boundclk'')',...
                    'UserData',[circles(i) col(i,:)]);

            end
        end
    end

    % draw the decomposed geometry 3 (ellipses)
    if ~isempty(ellipses)
        n1=length(ellipses); n2=length(bounds);
        col=0.5*ones(n1,3);
        if n1==1
            tmp1=find(bounds==ellipses);
        else
            tmp1=bounds(max(~(ones(n1,1)*bounds-(ones(n2,1)*ellipses)')));
        end
        n3=length(tmp1);
        if n3==0
            tmp=[];
        elseif n3==1
            if n1==1
                tmp=1;
            else
                tmp=find(ellipses==tmp1);
            end
        else
            tmp=find(max(~(ones(n3,1)*ellipses-(ones(n1,1)*tmp1)')));
        end
        if scalarflag
            dir=find(pdebound(2,ellipses(tmp))==1);
            m=length(dir);
            col(tmp(dir),:)=[ones(m,1) zeros(m,2)];
            neu=find(pdebound(2,ellipses(tmp))==0);
            m=length(neu);
            col(tmp(neu),:)=[zeros(m,2) ones(m,1)];
        else
            dir=find(pdebound(2,ellipses(tmp))==2);
            m=length(dir);
            col(tmp(dir),:)=[ones(m,1) zeros(m,2)];
            neu=find(pdebound(2,ellipses(tmp))==1);
            m=length(neu);
            col(tmp(neu),:)=[zeros(m,2) ones(m,1)];
            mix=find(pdebound(2,ellipses(tmp))==0);
            m=length(mix);
            col(tmp(mix),:)=[zeros(m,1) ones(m,1) zeros(m,1)];
        end

        delta=2*pi/100;
        for i=1:n1
            k=ellipses(i);
            ca=cos(dl1(12,k)); sa=sin(dl1(12,k));
            yd=dl1(4,k)-dl1(9,k);
            xd=dl1(2,k)-dl1(8,k);
            x1=ca.*xd+sa.*yd;
            y1=-sa.*xd+ca.*yd;
            arg1=atan2(y1./dl1(11,k),x1./dl1(10,k));
            yd=dl1(5,k)-dl1(9,k);
            xd=dl1(3,k)-dl1(8,k);
            x1=ca.*xd+sa.*yd;
            y1=-sa.*xd+ca.*yd;
            arg2=atan2(y1./dl1(11,k),x1./dl1(10,k));
            if arg2<0 && arg1>0, arg2=arg2+2*pi; end
            darg=abs(arg2-arg1);
            ns=max(2,ceil(darg/delta));
            arc=linspace(arg1,arg2,ns);
            xd=(dl1(10,k)*cos(arc))';
            yd=(dl1(11,k)*sin(arc))';
            x1=ca*xd-sa*yd;
            y1=sa*xd+ca*yd;
            x(1:ns)=dl1(8,k)+x1;
            y(1:ns)=dl1(9,k)+y1;
            if isempty(find(bounds==k, 1))
                line(x(1:ns),y(1:ns),zeros(1,ns),'Tag','PDEBorderLine',...
                    'Color',col(i,:),...
                    'Parent',ax,...
                    'ButtonDownFcn','pdetool(''bordclk'')',...
                    'UserData',k);
            else
                % make the boundary lines into arrows to indicate direction

                arrowX1=x(ns-1); arrowY1=y(ns-1);
                arrowX2 =x(ns); arrowY2=y(ns);

                xdif = arrowX2 - arrowX1;
                ydif = arrowY2 - arrowY1;

                theta = atan2(ydif/yRange,xdif/xRange); % the angle has to point according to the (screen) slope

                xx = [x(1:ns),... %the boundary line
                    NaN,...
                    (arrowX2+0.02*xRange*cos(theta+3*pi/4)), arrowX2,... %first arrow head line x values
                    NaN,...
                    (arrowX2+0.02*xRange*cos(theta-3*pi/4)), arrowX2]';  %second arrow head line x values
                yy = [y(1:ns),...
                    NaN,...
                    (arrowY2+0.02*yRange*sin(theta+3*pi/4)), arrowY2, ... %first arrow head line y values
                    NaN,...
                    (arrowY2+0.02*yRange*sin(theta-3*pi/4)),arrowY2]';    %second arrow head line y values

                line(xx,yy,zeros(size(xx)),'Tag','PDEBoundLine',...
                    'Color',col(i,:),...
                    'Parent',ax,...
                    'ButtonDownFcn','pdetool(''boundclk'')',...
                    'UserData',[k col(i,:)]);
            end
        end
    end
end