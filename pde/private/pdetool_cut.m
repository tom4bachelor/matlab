function pdetool_cut(flag)
% case: cut selected object(s) out (flag=1) or copy to 'clipboard' (flag=2)

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(get(pde_fig,'Children'),'flat','Tag','PDEAxes');
    selected=findobj(get(ax,'Children'),'flat','Tag','PDELblSel','Visible','on');
    if ~isempty(selected)

        edithndl=findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu');
        hndl=findobj(get(edithndl,'Children'),'flat','Tag','PDEPaste');
        set(hndl,'Enable','on')

        h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEMeshMenu');
        pdegd=get(h,'UserData');

        n=length(selected);
        for i=1:n
            column=get(selected(i),'UserData');
            clipboard(:,i)=pdegd(:,column); %#ok - suppress preallocation warning
        end
        set(findobj(get(edithndl,'Children'),'flat','Tag','PDECut'),...
            'UserData',clipboard);

        if flag==1
            % case: cut selected objects out
            pdetool('clear')
        end
    end
end