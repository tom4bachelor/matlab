function pdetool_save_as()
% case: save geometry in MATLAB-file (save as...)

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    [outfile,outpath]=uiputfile('*.m','Save As');

    if outfile~=0

        [nameOK, hasDotM] = pdetoolCheckFileName(outfile);
        if(~nameOK)
            msg1 = message('pde:pdetool:badNameSave').getString();
            msg2 = message('pde:pdetool:validFileName').getString();
            msg3 = message('pde:pdetool:continue').getString();
            titl = message('pde:pdetool:badNameSaveTitle').getString();
            yesBtn = message('pde:pdetool:Yes').getString();
            noBtn = message('pde:pdetool:No').getString();
            resp = questdlg([msg1 sprintf('\n') msg2 sprintf('\n') msg3], ...
                titl, yesBtn, noBtn, noBtn);
            if(strcmp(resp,noBtn))
                return;
            end

            if(~hasDotM)
                outfile=[outfile '.m'];
            end
        end

        outfilefull=[outpath outfile];

        fid=fopen(outfilefull,'w');
        if fid>0
            pdetool('write',fid);
            set(pde_fig,'Name',['PDE Toolbox - ' upper(outfile)]);

            h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
            flags=get(h,'UserData');
            flags(1)=0;                               % need_save=0.
            set(h,'UserData',flags)

            set(findobj(get(pde_fig,'Children'),'flat','Tag','PDEEditMenu'),...
                'UserData',outfilefull)
            filemenu=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
            hndl=findobj(get(filemenu,'Children'),'flat','Tag','PDESave');
            set(hndl,'Enable','on');
        else
            pdetool('error',' Can''t open file');
        end
    end
end