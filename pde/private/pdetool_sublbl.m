function pdetool_sublbl()
% case: manage subdomain labeling flags and indication

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEFileMenu');
    flags=get(h,'UserData'); mode_flag=flags(2);

    h=findobj(get(pde_fig,'Children'),'flat','Tag','PDEBoundMenu');
    subh=findobj(get(h,'Children'),'flat','Tag','PDEShowSub');

    if pdeumtoggle(subh)
        setappdata(pde_fig,'showsublbl',1);
        if mode_flag==1
            pdetool('showsublbl',1)
        end
    else
        setappdata(pde_fig,'showsublbl',0);
        if mode_flag==1
            pdetool('showsublbl',0)
        end
    end
end