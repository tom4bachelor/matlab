function pdetool_refresh()
% case: refresh PDE toolbox figure

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    pde_kids = allchild(pde_fig);
    ax=findobj(pde_kids,'flat','Tag','PDEAxes');
    h=findobj(pde_kids,'flat','Tag','PDEFileMenu');
    flags=get(h,'UserData'); mode_flag=flags(2);

    if mode_flag~=3
        set(ax,'SortMethod','depth'),
        refresh(pde_fig);
        set(ax,'SortMethod','childorder')
    end
end