function pdetool_showedgelbl(flag)
% case: display edge labels for decomposed geometry

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    ax=findobj(allchild(pde_fig),'flat','Tag','PDEAxes');
    set(pde_fig,'CurrentAxes',ax)

    h=findobj(get(ax,'Children'),'flat','Tag','PDEEdgeLabel');
    delete(h)

    if(flag)   % flag is set: display labels
        dl1=getappdata(pde_fig,'dl1');
        pdePlotEdgeLabels(dl1, ax, false);
    end
end