function pdetool_solve_param()
% case: set PDE solution parameters
%       (adaption algorithm/time step/eigenvalue range)

%   Copyright 1994-2016 The MathWorks, Inc.

    pde_fig=findobj(allchild(0),'flat','Tag','PDETool');

    pde_type=get(findobj(get(pde_fig,'Children'),'flat','Tag','PDEHelpMenu'),...
        'UserData');

    if pde_type==1                       % elliptic PDE
        pdesldlg
    elseif pde_type==2                   % parabolic PDE
        pdetrdlg('parabolic')
    elseif pde_type==3                   % hyperbolic PDE
        pdetrdlg('hyperbolic')
    elseif pde_type==4                   % eigenvalue PDE
        pdetrdlg('eigenvalue')
    end
end