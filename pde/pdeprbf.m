function f=pdeprbf(t,u)
%PDEPRBF  Right hand side for PARABOLIC.

%       Copyright 1994-2014 The MathWorks, Inc.

global pdeprb


if ~(pdeprb.vq || pdeprb.vg || pdeprb.vh || pdeprb.vr || ...
     pdeprb.vc || pdeprb.va || pdeprb.vf)
  f=-pdeprb.K*u+pdeprb.F;
  return
end

if(pdeprb.numConstrainedEqns == pdeprb.totalNumEqns)
  uFull = u;
else
  uFull = pdeprb.B*u + pdeprb.ud;
end

if pdeprb.vq || pdeprb.vg || pdeprb.vh || pdeprb.vr
  [pdeprb.Q,pdeprb.G,pdeprb.H,pdeprb.R]=assemb(pdeprb.b,pdeprb.p,pdeprb.e,uFull,t);
end

if pdeprb.vc || pdeprb.va || pdeprb.vf
  [pdeprb.K,pdeprb.M,pdeprb.F]=pdeprb.thePde.assema(pdeprb.p,pdeprb.t,pdeprb.c,pdeprb.a,pdeprb.f,uFull,t);
end


if ~(pdeprb.vh || pdeprb.vr)
  % neither H nor R are functions of t or u
  K = pdeprb.K + pdeprb.M + pdeprb.Q;
  F = pdeprb.B'*(pdeprb.F + pdeprb.G - K*pdeprb.ud);
  K = pdeprb.B'*K*pdeprb.B;
else
  dt=pdeprb.ts*sqrt(eps);
  t1=t+dt;
  dt=t1-t;
  if pdeprb.vd
    f0 = zeros(pdeprb.N,1);
    [~,pdeprb.MM]=pdeprb.thePde.assema(pdeprb.p,pdeprb.t,0,pdeprb.d,f0,uFull,t);
  end
  if pdeprb.vh
    [N,O]=pdenullorth(pdeprb.H);
    ud=O*((pdeprb.H*O)\pdeprb.R);
    [Q,G,H,R]=assemb(pdeprb.b,pdeprb.p,pdeprb.e,uFull,t1);
    [N1,O1]=pdenullorth(H);
    ud1=O1*((H*O1)\R);
    K=N'*((pdeprb.K+pdeprb.M+pdeprb.Q)*N + pdeprb.MM*(N1-N)/dt);
    F=N'*((pdeprb.F+pdeprb.G)-(pdeprb.K+pdeprb.M+pdeprb.Q)*ud - ...
      pdeprb.MM*(ud1-ud)/dt);
  else
    HH=pdeprb.H*pdeprb.Or;
    ud=pdeprb.Or*(HH\pdeprb.R);
    [Q,G,H,R]=assemb(pdeprb.b,pdeprb.p,pdeprb.e,uFull,t1);
    ud1=pdeprb.Or*(HH\R);
    K=pdeprb.Nu'*(pdeprb.K+pdeprb.M+pdeprb.Q)*pdeprb.Nu;
    F=pdeprb.Nu'*((pdeprb.F+pdeprb.G)-(pdeprb.K+pdeprb.M+pdeprb.Q)*ud - ...
                 pdeprb.MM*(ud1-ud)/dt);
  end
  pdeprb.ud=ud;
end

f=-K*u+F;

end