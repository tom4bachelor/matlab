% MULTICUBOID   Geometry formed by several cubic cells
%   gm = MULTICUBOID(W, D, H) creates a geometry by combining several cubic 
%   cells. W(i), D(i) and H(i) are the width, depth, and height of the ith cell. 
%   W, D, and H can be scalars or vectors of the same length. For a combination 
%   of scalar and vector inputs, multicuboid replicates the scalar arguments 
%   into vectors of the same length.
%  
%   gm = MULTICUBOID(...,'Name1',Value1, 'Name2',Value2, ...) uses name/value
%   pairs to provide the following options for cells within the geometry.
%   
%   Name        Value/{Default}     Description
%   ---------------------------------------------------------------------- 
%   ZOffset     numeric {0}         Vector of values specifying the Z offset
%                                   of each cell. If ZOffset is a scalar,
%                                   each geometric cell has the same offset. 
%  
%   Void        logical {false}     Vector of logicals specifying which 
%                                   geometric cells if any should be empty.
%  
%   Example: Thick rectangular plate with two subdomains in Z-direction
%  
%       gm = MULTICUBOID(40, 20, [10, 10], 'ZOffset', [0, 10])
%       %Assign the geometry to a PDEModel
%       pdem = createpde
%       pdem.Geometry = gm
%       %Plot the geometry
%       pdegplot(pdem, 'CellLabels', 'on', 'FaceAlpha', 0.7)
%       %Mesh the geometry
%       generateMesh(pdem)
%  
%  
%   See also, multicylinder, multisphere, createpde, pde.PDEModel

%   Copyright 2016 The MathWorks, Inc.
%   Built-in function.