function f=pdetexpd(x,y,sd,u,ux,uy,t,f)
%PDETEXPD Evaluate an expression on triangles.

%       A. Nordmark 12-19-94.
%       Copyright 1994-2016 The MathWorks, Inc.

if ischar(f)
  ic=strfind(f,'!');
  nf=length(ic)+1;
  if nf>1
    X=x;
    Y=y;
    SD=sd;
    U=u;
    UX=ux;
    UY=uy;
    nt=length(x);
    fff=zeros(1,nt);
    lf=length(f);
    ic=[0 ic lf+1];
    for ii=1:nf
      ff=f(ic(ii)+1:ic(ii+1)-1);
      iii=find(SD==ii);
      x=X(iii);
      y=Y(iii);
      sd=SD(iii);
      if size(U)
        u=U(:,iii);
        ux=UX(:,iii);
        uy=UY(:,iii);
      end
     ffold=ff;
      try
          ff=eval(ffold);
      catch ME
        error(message('pde:pdetexpd:InvalidExpressionSubDomain', ME.message, sprintf('%s', ffold),ii));
      end
      if any(size(ff)-[1 1]) && any(size(ff)-size(x))
        error(message('pde:pdetexpd:InvalidSizeFromExpressionSubDomain', sprintf('%s', ffold), ii));
      end
      fff(iii)=ff.*ones(size(iii));
    end
    f=fff;
  else
   fold=f;
    try
        f=eval(fold);
    catch ME
      error(message('pde:pdetexpd:InvalidExpression', ME.message, sprintf('%s', fold)));
    end
    if any(size(f)-[1 1]) && any(size(f)-size(x))
      error(message('pde:pdetexpd:InvalidSizeFromExpression', sprintf('%s', fold)));
    end
  end
end

