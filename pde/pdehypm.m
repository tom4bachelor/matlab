function m=pdehypm(t, u)
%PDEHYPM Mass matrix for HYPERBOLIC.

%       Copyright 1994-2014 The MathWorks, Inc.

  global pdehyp
  
  if(pdehyp.vd || pdehyp.vh)
    nu=pdehyp.numConstrainedEqns;
    if(nu == pdehyp.totalNumEqns)
      uFull = u;
    else
      uFull = pdehyp.B*u(1:nu) + pdehyp.ud;
    end
  end
  
  if(pdehyp.vd)
    [~,pdehyp.MM]=pdehyp.thePde.assema(pdehyp.p,pdehyp.t,0,pdehyp.d,zeros(pdehyp.N,1),uFull,t);
  end
  
  if(pdehyp.vh)
    [~,~,H,~]=assemb(pdehyp.b,pdehyp.p,pdehyp.e,uFull,t);
    pdehyp.B=pdenullorth(H);
  end
  
  nu=size(pdehyp.B,2);
  if(~isempty(pdehyp.C))
    dampMat = pdehyp.B'*pdehyp.C*pdehyp.B;
  else
    dampMat = sparse(nu,nu);
  end
  m=[speye(nu,nu) sparse(nu,nu); dampMat pdehyp.B'*pdehyp.MM*pdehyp.B];
  
end


