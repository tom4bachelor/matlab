% Partial Differential Equation Toolbox
% Version 2.4 (R2017a) 16-Feb-2017
%
% Define and Solve PDEs using a structured workflow.
%   createpde               - Creates a PDE analysis model.
%   geometryFromEdges       - Create 2D geometry from DECSG or PDEGEOM.
%   importGeometry          - Create 3D geometry from file (STL only).
%   geometryFromMesh        - Create 3D geometry from a triangulated mesh.
%   specifyCoefficients     - Specify all PDE coefficients over a domain or subdomain.
%   applyBoundaryCondition  - Apply a boundary condition to the geometry.
%   setInitialConditions    - Set the initial conditions for the PDE.
%   generateMesh            - Generate a mesh from the geometry.
%   solvepde                - Solve the PDE.
%   solvepdeeig             - Solve the PDE eigenvalue problem.
%   assembleFEMatrices      - Assembles the intermediate FE matrices 
%   createPDEResults        - Creates a results object for postprocessing.
%   pdegplot                - Plot a PDE geometry representation.
%   pdemesh                 - Plot a PDE mesh.
%   pdeplot                 - Plot 2D PDE mesh and results.
%   pdeplot3D               - Plot 3D PDE mesh and results.
%  
% Solve PDEs using an unstructured workflow.
%   adaptmesh   - Adaptive mesh generation and PDE solution.
%   assema      - Assemble area integral contributions.
%   assemb      - Assemble boundary condition contributions.
%   assempde    - Assemble a PDE problem.
%   hyperbolic  - Solve hyperbolic problem.
%   parabolic   - Solve parabolic problem.
%   pdeeig      - Solve eigenvalue PDE problem.
%   pdenonlin   - Solve nonlinear PDE problem.
%   poisolv     - Fast solution of Poisson's equation on a rectangular grid.
%
% User interface algorithms and utilities.
%   pdecirc     - Draw circle.
%   pdeellip    - Draw ellipse.
%   pdemdlcv    - Convert MATLAB 4.2c Model MATLAB-files for use with MATLAB 5.
%   pdepoly     - Draw polygon.
%   pderect     - Draw rectangle.
%   pdetool     - PDE Toolbox graphical user interface (GUI).
%
% Geometry algorithms.
%   csgchk      - Check validity of Geometry Description matrix.
%   csgdel      - Delete borders between minimal regions.
%   decsg       - Decompose Constructive Solid Geometry into minimal regions.
%   initmesh    - Build an initial triangular mesh.
%   jigglemesh  - Jiggle internal points of a triangular mesh.
%   pdearcl     - Interpolation between parametric representation and arc length.
%   poimesh     - Make regular mesh on a rectangular geometry.
%   refinemesh  - Refines a triangular mesh.
%   wbound      - Write boundary condition specification data file.
%   wgeom       - Write geometry specification data file.
%
% Plot functions.
%   pdecont     - Shorthand command for contour plot.
%   pdegplot    - Plot a PDE geometry.
%   pdemesh     - Plot a PDE triangular mesh.
%   pdeplot     - Generic PDE Toolbox plot function.
%   pdesurf     - Shorthand command for surface plot.
%
% Utility algorithms.
%   dst         - Discrete sine transform.
%   idst        - Inverse discrete sine transform.
%   pdeadgsc    - Pick bad triangles using a relative tolerance criterion.
%   pdeadworst  - Pick bad triangles relative to the worst value.
%   pdecgrad    - Compute the flux of a PDE solution.
%   pdeent      - Indices of triangles neighboring a given set of triangles.
%   pdegrad     - Compute the gradient of the PDE solution.
%   pdeintrp    - Interpolate function values to triangle midpoints.
%   pdejmps     - Error estimates for adaption.
%   pdeprtni    - Interpolate function values to mesh nodes.
%   pdesde      - Indices of edges adjacent to a set of subdomains.
%   pdesdp      - Indices of points in a set of subdomains.
%   pdesdt      - Indices of triangles in a set of subdomains.
%   pdesmech    - Compute structural mechanics tensor functions.
%   pdetrg      - Triangle geometry data.
%   pdetriq     - Measure the quality of mesh triangles.
%   poiasma     - Boundary point matrix contributions for Poisson's equation.
%   poicalc     - Fast solution of Poisson's equation on a rectangular grid.
%   poiindex    - Indices of points in canonical ordering for rectangular grid.
%   sptarn      - Solve generalized sparse eigenvalue problem.
%   tri2grid    - Interpolate from PDE triangular mesh to rectangular mesh.
%
% User defined algorithms.
%   pdebound    - Boundary MATLAB-file.
%   pdegeom     - Geometry MATLAB-file.
% 
% Object creation functions. These functions are not called directly.
%   PDEModel          - A container that represents a PDE model
%   GeometricModel    - Geometric representation of model boundary
%   AnalyticGeometry  - 2D geometry object from PDEGEOM or DECSG geometry matrix
%   DiscreteGeometry  - Geometric representation of a facetted boundary
%   BoundaryCondition - Defines a boundary condition for the PDE
%   CoefficientAssignmentRecords  - Assignments of the equation coefficients
%   CoefficientAssignment         - Specify all PDE coefficients over a domain or subdomain
%   InitialConditionsRecords   - Records the assignment of Initial Conditions
%   GeometricInitialConditions - Initial Conditions over a region or region boundary
%   PDEResults           - PDE solution and its derived quantities
%   StationaryResults    - PDE solution and its derived quantities
%   TimeDependentResults - PDE solution and its derived quantities
%   EigenResults         - PDE solution representation
%

% Undocumented classes and functions
%   pdeCalcFullU
%   pdeODEInfo
%   pdeParabolicInfo
%   pdeHyperbolicInfo

%   Copyright 2017 The MathWorks, Inc. 
