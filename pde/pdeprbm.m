function m=pdeprbm(t, u)
%PDEPRBM Mass matrix for PARABOLIC.

%       Copyright 1994-2014 The MathWorks, Inc.

global pdeprb

if(pdeprb.vd || pdeprb.vh)
  if(pdeprb.numConstrainedEqns == pdeprb.totalNumEqns)
    uFull = u;
  else
    uFull = pdeprb.B*u + pdeprb.ud;
  end
end

if(pdeprb.vd)
  [~,pdeprb.MM]=pdeprb.thePde.assema(pdeprb.p,pdeprb.t,0,pdeprb.d,zeros(pdeprb.N,1),uFull,t);
end

if(pdeprb.vh)
  [~,~,H,~]=assemb(pdeprb.b,pdeprb.p,pdeprb.e,uFull,t);
  pdeprb.B=pdenullorth(H);
end
m=pdeprb.B'*pdeprb.MM*pdeprb.B;

end
