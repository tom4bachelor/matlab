% Partial Differential Equation Toolbox Examples
%
% Examples.
%   pdedemo1 - Exact solution of Poisson's equation on unit disk.
%   pdedemo2 - Solve Helmholtz's equation and study the reflected waves.
%   pdedemo3 - Solve a minimal surface problem.
%   pdedemo4 - Solve PDE problem using subdomain decomposition.
%   pdedemo5 - Solve a parabolic PDE (the heat equation).
%   pdedemo6 - Solve a hyperbolic PDE (the wave equation).
%   pdedemo7 - Adaptive solution with point source.
%   pdedemo8 - Solve Poisson's equation on rectangular grid.
%   clampedSquarePlateExample - Clamped, Square Isotropic Plate With a Uniform Pressure Load
%   heatTransferThinPlateExample - Nonlinear Heat Transfer In a Thin Plate
%   deflectionPiezoelectricActuator - Deflection of a Piezoelectric Actuator
%   radioactiveRod - Heat Distribution in a Circular Cylindrical Rod
%   eigsExample - Vibration Of a Circular Membrane Using The MATLAB eigs Function
%   cantileverBeamDampedTransient - Dynamics of a Damped Cantilever Beam
%   heatedBlockWithSlot - Solving a Heat Transfer Problem With Temperature-Dependent Properties
%   clampedBeamDynamics - Dynamic Analysis of a Clamped Beam
%   StrainedBracketExample - Deflection Analysis of a Bracket
%   Eigenvaluesofa3DPlateExample - Vibration of a Square Plate
%   ContourSlices3DExample - Contour Slices Through a 3-D Solution

% Internally Used Utility Routines
%
%   plotAlongY - create a plot of the solution along Y at a prescribed x-value
%   calcCMatPiezoActuator - c matrix function for deflectionPiezoelectricActuator
%   cantileverBeamTransientPlot - plot function for cantileverBeamDampedTransient
%   boundaryFileHeatedBlock - boundary condition function for heatedBlockWithSlot
%   cCoefficientLagrangePlaneStress - calculate c-coefficient for clampedBeamDynamics
%   allzerobc - set solution to zero on the entire boundary
%   elasticityC3D - calculate the c-coefficient matrix for 3D elasticity

%   Copyright 2017 The MathWorks, Inc. 
