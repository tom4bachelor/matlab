function pdeDefinition = allzerobc(g,N)
% ALLZEROBC set solution to zero on all boundaries
% g - decomposed geometry matrix or function handle to "geometry file" 
% N - number of PDE in the system

%       Copyright 2014-2016 The MathWorks, Inc.

if(nargin < 2)
  N = 1;
end

pdeDefinition = createpde(N);
pg = geometryFromEdges(pdeDefinition,g);
applyBoundaryCondition(pdeDefinition,'dirichlet','Edge',(1:pg.NumEdges),'u', zeros(N,1));

end

