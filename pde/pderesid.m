function r=pderesid(time,u)
%PDERESID Residual for nonlinear solver

%       Copyright 1994-2014 The MathWorks, Inc.

global pdenonb pdenonp pdenone pdenont pdenonc pdenona pdenonf pdenonn
global pdenonK pdenonF

u=full(u);
[K,M,F,Q,G,H,R]=assempde(pdenonb,pdenonp,pdenone,pdenont,...
  pdenonc,pdenona,pdenonf,u(1:pdenonn));
pdenonK=K+M+Q;pdenonF=F+G;
if any(any(H))
  if size(u,1)<(size(pdenonK,2)+size(H,1))
    u=[u;H'\(pdenonF-pdenonK*u)];
  end
  pdenonK=[pdenonK H';H zeros(size(H,1))];
  pdenonF=[pdenonF;R];
end
r=pdenonK*u-pdenonF;

