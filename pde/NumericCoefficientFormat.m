% NumericCoefficientFormat - Describes numeric format for equation coefficients
%     The PDE toolbox can solve equations of the form:
%  
%              m*d^2u/dt^2 + d*du/dt - div(c*grad(u)) + a*u = f
%  
%     and the corresponding eigenvalue equation of the form:
%  
%                   - div(c*grad(u)) + a*u = lamda*d*u
%     or
%                   - div(c*grad(u)) + a*u = (lamda^2)*m*u
%
% The equation coefficients for a PDEModel (m, d, c, a, f) are specified 
% using the specifyCoefficients function. The coefficients can be specified 
% in numeric format or as a MATLAB function handle that returns coefficient 
% values at locations within the domain. This help section describes the
% numeric coefficient format for the coefficients.
%
% m-Coefficient Format
%   The d coefficient is an N-by-N matrix matrix, where N is the number of 
%   equations - the PDEModel/PDESystemSize. This matrix can be represented 
%   more concisely in one of the following ways:
%   - A scalar value - MATLAB intrprets this as a diagonal matrix with
%     the the scalar value representing the diagonal entries.
%   - An N-element column vector - MATLAB intrprets this as a diagonal matrix 
%     with the column vector representing the diagonal entries.
%   - An N(N+1)/2-Element Column Vector - This vector represents the 
%     upper triangular entries of a symmetric matrix. MATLAB intrprets this
%     as a symmetric matrix.
%   - An N^2 column vector - This vector represents the column-wise entries
%     of the NxN matrix.
%
% d-Coefficient Format
%   The m coefficient matrix has the same format as the m coefficient
%   matrix described previously.
%
% c-Coefficient Format
%   The c coefficient can represent a vector, matrix or tensor. This matrix 
%   can be represented in several different formats. These formats will be
%   summarized here, the documentation should be consulted for a broader
%   description. 
%   For a system of N equations, the c coefficient formats for 2-D are
%   as follows:
%   - A scalar value
%   - A column vector of length 2, 3, 4, N, 2N, 3N, 4N, 2N(2N+1)/2, 4N^2
%   For a system of N equations, the c coefficient formats for 3-D are
%   as follows:
%   - A scalar value
%   - A column vector of length 3, 6, 9, N, 3N, 6N, 9N, 3N(3N+1)/2, 9N^2
% 
% a-Coefficient Format
%   The a coefficient matrix has the same format as the m coefficient
%   matrix described previously.
%
% f-Coefficient Format
%   The f coefficient is represented by a column vector whose length equals
%   the number of equations, PDEModel/PDESystemSize. For a scalar PDE, the
%   f coefficient is a matrix. If the equation to solve does not have an 
%   f coefficient, then set f to zero. Note: if the equation to solve has
%   an f coefficient it is ignored when formulating and solving an 
%   eigenvalue problem.
%
% See also FunctionCoefficientFormat, pde.PDEModel/specifyCoefficients

% Copyright 2015 The MathWorks, Inc.

