% pdeInterpolant Interpolate nodal data to selected locations
% pdeInterpolant is used to interpolate data at selected locations within
% the mesh. The sample data is defined at the mesh nodes and typically
% represents the solution at the nodes.
%
% F = pdeInterpolant(p, t, u) Creates an interpolant for the mesh defined
% by the nodal coordinate array, p, and the triangulation connectivity 
% array, t. 
%
% The array u is the data defined at each node several forms of u are allowed:
%
%     In the simplest case, u is a column vector with number of rows equal 
%     to the number of points in the p-array, np. 
%
%     u may be a column vector with number of rows equal to an integer
%     multiple of np. In this case, every range of np values in this vector 
%     corresponds to a different variable defined on the mesh. This is the 
%     form returned by assempde, for example, for a PDE system (N>1).
%
%     u may have multiple columns where, for example, each column corresponds 
%     to the solution at a particular time. This is the form returned by the 
%     parabolic and hyperbolic functions.
%
% Example 1 - Interpolate a 2D Mesh:
% 	[p,e,t] = initmesh('squareg');
%   % Plot the mesh
%   pdemesh(p, e, t);
% 	% u is a simple linear function of x and y
% 	f = @(xy) (2*xy(1,:) + 3*xy(2,:))';
% 	u = f(p);
% 	pdeInt = pdeInterpolant(p, t, u);
% 	% Compute the interpolated value at coordinates (0.2, 0.3) 
%   % within the mesh
%   uq = pdeInt.evaluate(0.2, 0.3)
%
% Example 2 - Interpolate a 3D Mesh:
%   pdem = createpde();
%   importGeometry(pdem,'Block.stl');
%   msh = generateMesh(pdem,'Hmax',20);
%   pdeplot3D(pdem);
%   axis on
% 	f2 = @(xyz) (2*xyz(1,:) + 3*xyz(2,:) + 3*xyz(3,:))';
%   [p, ~, t] = meshToPet(msh);
%   u = f2(p);
% 	pdeInt = pdeInterpolant(p, t, u)
% 	% Compute the interpolated value at coordinates (0.2, 0.3) 
%   % within the mesh
%   uq = pdeInt.evaluate(30, 10, 25)
%
%   pdeInterpolant methods:
%       evaluate - Interpolate nodal solution vector to the input points
%
%    See also initmesh tri2grid

%    Copyright 2014 The MathWorks, Inc.
%    Built-in function.