% evaluate Interpolate nodal data to selected locations
%
% outputData = evaluate(F, queryPointMatrix)
%
% F is the interpolant returned from pdeInterpolant and queryPointMatrix
% is an array of query-point locations where the pdeInterpolant is to be 
% evaluated. queryPointMatrix has Nep columns representing Nep evaluation 
% points and NDIM rows, where NDIM = 2 or 3. If the size of u is N*np rows 
% by nt columns, the size of outputData will be N*Nep rows by nt columns.
%
% outputData = evaluate(F, xq, yq) - Evaluates a 2D interpolant at query
% locations xq, yq. xq and yq may be vectors or matrices with the same
% number of elements.
%
% outputData = evaluate(F, xq, yq, Zq) - Evaluates a 3D interpolant at query
% locations xq, yq, zq. xq, yq and zq may be vectors or matrices with the 
% same number of elements.
%
% 	Example 1 - Interpolate a 2D Mesh:
% 	[p,e,t] = initmesh('squareg');
% 	% u is a simple linear function of x and y
% 	f = @(xy) (2*xy(1,:) + 3*xy(2,:))';
% 	u = f(p);
% 	pdeInt = pdeInterpolant(p, t, u);
% 	% calculate u at arbitrary point (.2,.5)
% 	pOut = [.2 .5]'; 
% 	uInt = evaluate(pdeInt, pOut);
% 	fprintf('Exact u = %g, interpolated u = %g\n', f(pOut), uInt);
%
%   Example 2 - Interpolate a 3D Mesh:
%   pdem = createpde();
%   importGeometry(pdem,'Block.stl');
%   msh = generateMesh(pdem,'Hmax',20);
%   pdeplot3D(pdem);
%   axis on
% 	f2 = @(xyz) (2*xyz(1,:) + 3*xyz(2,:) + 3*xyz(3,:))';
%   [p, ~, t] = meshToPet(msh);
%   u = f2(p);
% 	pdeInt = pdeInterpolant(p, t, u)
% 	% Compute the interpolated value at coordinates (0.2, 0.3) 
%   % within the mesh
%   uq = pdeInt.evaluate(30, 10, 25)

%    Copyright 2014 The MathWorks, Inc.
%    Built-in function.
