% MULTISPHERE    Geometry formed by several spherical cells
%   gm = MULTISPHERE(R) creates a geometry by combining several spherical 
%   cells. R is a vector that represent the radius the geometric cells, where 
%   R(i) is the radius of the i'th cell.
%  
%   gm = MULTISPHERE(...,'Name1',Value1, ...) uses name/value pairs to 
%   provide the following options for cells within the geometry.
%   
%   Name        Value/{Default}     Description
%   ----------------------------------------------------------------------
%   Void        logical {false}     Vector of logicals specifying which 
%                                   geometric cells if any should be empty.
%  
%
%   Example: Thick-walled sphere with two subdomains in the radial direction
%  
%       gm = multisphere([10, 15, 20], 'Void', [true, false, false])
%       %Assign the geometry to a PDEModel
%       pdem = createpde
%       pdem.Geometry = gm
%       %Plot the geometry
%       pdegplot(pdem, 'CellLabels', 'on', 'FaceAlpha', 0.7)
%       %Mesh the geometry
%       generateMesh(pdem)
%   
%
%   See also, multicuboid, multicylinder, createpde, pde.PDEModel

%   Copyright 2016 The MathWorks, Inc.
%   Built-in function.