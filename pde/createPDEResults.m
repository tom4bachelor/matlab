function pdes = createPDEResults(pdem,u, varargin)
%createPDEResults - Creates a PDEResults object for postprocessing
%   The createPDEResults function creates a PDEResults object from the
%   solution output from assempde, parabolic, hyperbolic, or pdeeig. The
%   PDEResults object formats the results data into a convenient form and
%   provides useful query functions, such as value and gradient.
%
%   R = createPDEResults(pdem,u) or R = createPDEResults(pdem,u,
%   'stationary') creates a StationaryResults object for the PDEModel,
%   pdem, and the solution, u, where u is the output from assempde.
%
%   R = createPDEResults(pdem, u, utimes,'time-dependent') creates an
%   TimeDependentResults object for the PDEModel, pdem, and the solution u,
%   and solution times utimes, where u is the output from parabolic or
%   hyperbolic functions and utimes is the input requested solution times.
%
%   R = createPDEResults(pdem, eigenvectors, eigenvalues,'eigen') creates
%   an EigenResults object for the PDEModel, pdem, and the solution
%   eigenvectors and eigenvalues output from pdeeig.
%
%   Example: Solve a structural dynamics eigenvalue problem on a square
%   plate and visualize a mode shape.
% 
%     %Create a PDEModel, import geometry, and assign coefficients.
%     N = 3; % system of three PDEs
%     pdem = createpde(N);
%     importGeometry(pdem,'Plate10x10x1.stl');
%     E = 200e9; % Modulus of elasticity in Pascal
%     nu = .3; % Poisson's ratio
%     d = 8000; % Material density in kg/m^3
%     c = elasticityC3D(E,nu);
%     a = 0;
% 
%     %Apply boundary conditions and generate mesh.
%     applyBoundaryCondition(pdem,'mixed','Face',1:4,'u',0,'EquationIndex',3);
%     generateMesh(pdem,'Hmax',1.2,'GeometricOrder','quadratic');
%     
%     %Solve for eigenvalues and eigenvectors of the system of PDEs.
%     [v,l] = pdeeig(pdem,c,a,d,[-.1 2e+06]);
%     
%     %Create a PDEResults object using eigenvectors matrix v.
%     R = createPDEResults(pdem,v,l,'eigen');
%     
%     %Plot the z-displacement of the twelfth mode.
%     pdeplot3D(pdem,'colormapdata',R.NodalSolution(:,3,12))
%     
%
%   See also pde.StationaryResults, pde.TimeDependentResults,
%   pde.EigenResults, createpde, assempde, parabolic, hyperbolic, pdeeig

% Copyright 2015-2016 The MathWorks, Inc.


narginchk(2,4);

if nargin == 2
    pdes = pde.StationaryResults(pdem,u);
elseif nargin == 3 && ischar(varargin{1})
    restype = varargin{1};
    nc = numel(restype);
    if ~(strncmpi(restype,'stationary',nc))
        error(message('pde:PDEResults:invalidResultsType'));   
    end
    pdes = pde.StationaryResults(pdem,u);
elseif nargin == 4 && ischar(varargin{2})
    restype = varargin{2};
    nc = numel(restype);
    if ~(strncmpi(restype,'eigen',nc) || strncmpi(restype,'time-dependent',nc))
        error(message('pde:PDEResults:invalidResultsType'));   
    end
    if strncmpi(restype,'eigen',nc)
        pdes = pde.EigenResults(pdem,u, varargin{1});            
    else
        pdes = pde.TimeDependentResults(pdem,u, varargin{1});
    end
else
    error(message('pde:PDEResults:invalidResultsType'));
end  
    
end






