function uu=pdeuxpd(p,u,dim)
% PDEUXPD Expand solution u to node points
%
%       U=PDEUXPD(P,U) expands a scalar valued U to node points defined by
%       point matrix P.
%       U can be a scalar or a string describing U as a function of x and y.
%
%       U=PDEUXPD(P,U,N) expands U to node points for a system with
%       dimension N (default is 1).
%
%       PDEUXPD returns the expanded U as a column vector of length
%       N * size(P,2).

%       M. Ringh 3-01-95. MR, AN 9-11-95.
%       Copyright 1994-2016 The MathWorks, Inc.

if nargin<3
    dim=1;
end
np=size(p,2);
if ischar(u) 
    
    if (size(u,1) ~=  dim)  % Check for the required number strings for IC 
      error(message('pde:pdeuxpd:SizeUString', dim)) 
    end
    
    try
        % Coordinate values for string case
        x=p(1,:)';
        y=p(2,:)';
        if(size(p,1)==3)
            % 3D case
            z = p(3,:)';
        end
        
        u0 = zeros(size(p,2),dim);
        for i=1:dim
            u0(:,i) = eval(u(i,:)); %evaluaate initial condition for each equation.
        end
        u = u0(:); % convert to column form
    catch me
        error(message('pde:pdeuxpd:CannotEvalU'));
    end
end
[~,m]=size(u);
if m==np
    u=u';
end
u=u(:);
n=length(u);
if n==1
    n=dim*np;
    uu=u*ones(n,1);
else
    uu=u;
end

if rem(n,np)~=0
    error(message('pde:pdeuxpd:SizeU'))
end

