function cc=pdebsplit(c)
%PDEBSPLIT Splits space separated string into separate rows.
%
%       CC=PDEBSPLIT(C) splits the string C into its space separated
%       components. A maximum of four space separated string components
%       is allowed. The separated string components are returned as
%       rows in a string matrix.

%       Magnus Ringh 9-12-95.
%       Copyright 1994-2016 The MathWorks, Inc.

if ~ischar(c)
  error(message('pde:pdebsplit:InputNotString'))
end
if size(c,1)~=1
  error(message('pde:pdebsplit:StringNot1Line'))
end
c=[fliplr(deblank(fliplr(deblank(c)))), ' '];
whites=find(isspace(c));
c(whites(find(diff(whites)==1)+1))=[];
whites=find(isspace(c));
nc=length(whites);
if nc>4
  error(message('pde:pdebsplit:TooManyStrings'))
end
if nc==1
  cc=c(1:whites-1);
else
  cc=c(1:whites(1)-1);
  for k=1:nc-1
    cc=char(cc,c(whites(k)+1:whites(k+1)-1));
  end
end

