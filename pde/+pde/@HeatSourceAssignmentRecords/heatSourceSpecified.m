function tf = heatSourceSpecified(self, varargin)
% heatSourceSpecified True if heat source is specified for domain or subdomain

% Copyright 2016 The MathWorks, Inc.

narginchk(1,3);  
tf = false;
if isempty(self.ParentModel.Geometry)
    return;
end

parser = inputParser;
parser.addParameter('face', [], @pde.EquationModel.isValidEntityID);   
parser.addParameter('cell', [], @pde.EquationModel.isValidEntityID);   
parser.parse(varargin{:});


globalquery=false;

if self.ParentModel.IsTwoD
    if  ismember('face',parser.UsingDefaults)
        regionid = 1:self.ParentModel.Geometry.NumFaces;
        tf = false(size(regionid));
        globalquery=true;
    else
        regionid = parser.Results.face;
        if any(regionid > self.ParentModel.Geometry.NumFaces)
            error(message('pde:heatSourceSpecification:invalidRegionID'));
        end
        tf = false(size(regionid));
    end
else
    if  ismember('cell',parser.UsingDefaults)
        regionid = 1:self.ParentModel.Geometry.NumCells;
        tf = false(size(regionid));
        globalquery=true;
    else
        regionid = parser.Results.cell;
        if any(regionid > self.ParentModel.Geometry.NumCells)
            error(message('pde:heatSourceSpecification:invalidRegionID'));
        end
        tf = false(size(regionid));
    end
end

numqueries = numel(regionid);
numassigns = numel(self.HeatSourceAssignments);
for i = 1:numqueries
    rid = regionid(i);
    for j = 1:numassigns
       thisca =  self.HeatSourceAssignments(j);
       if ismember(rid, thisca.RegionID) 
          tf(i) = true;
          break       
       end
    end    
end

if globalquery
   tf = all(tf == true);    
end

end
