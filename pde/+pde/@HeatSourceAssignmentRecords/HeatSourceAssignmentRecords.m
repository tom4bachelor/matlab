classdef (Sealed) HeatSourceAssignmentRecords < handle & matlab.mixin.internal.CompactDisplay
% HeatSourceAssignmentRecords  Assignments of the internal heat generation/source
%    The HeatSourceAssignmentRecords holds a record of all the assignments
%    of internal heat generation across the geometric domain. This may be a
%    single assignment throughout the domain or multiple assignments that
%    vary across various subdomains. This object cannot be created
%    directly, it can only be created by the ThermalModel class.
%
% MaterialAssignmentRecords methods:
%    globalHeatSource - True if consistent across the geometric domain
%    findHeatSource  - Find heat source assignment for a geometric region
%
% HeatSourceAssignmentRecords properties:
%    HeatSourceAssignments - Vector of heat source assignments
%
% See also pde.ThermalModel, pde.ThermalModel/internalHeatSource

% Copyright 2016 The MathWorks, Inc.

properties
    % HeatSourceAssignments - Vector of heat source assignments
    %    A vector containing the heat source assignments to the geometric
    %    domain or subdomains. Each entry in the vector is a
    %    pde.HeatSourceAssignment object. This object defines the internal
    %    heat generation or source in a designated domain or subdomain of a
    %    thermal model. heat source values and host domains are defined
    %    using the internalHeatSource method of the ThermalModel.
    HeatSourceAssignments;
end

methods
    % Method declaration
    mp = findHeatSource(self, varargin)
    tf = heatSourceSpecified(self, varargin)
    tf = globalHeatSource(self, varargin)
end


methods
    function self = HeatSourceAssignmentRecords(tpdem)
        self.HeatSourceAssignments = [];
        self.ParentModel = tpdem;
    end

    function tf = allHeatSourceNumeric(self)
        tf = true;
        if ~self.heatSourceSpecified()
            error(message('pde:heatSourceSpecification:subdomainsWithoutHeatSrc'));
        end

        for i = 1:self.nRegion
            hs = self.findHeatSource(self.qregiontype,i);
            if ~numericHeatSource(hs)
                tf = false;
                return
            end
        end

    end


    function performSolverPrechecks(self)
        if ~self.heatSourceSpecified()
            error(message('pde:heatSourceSpecification:subdomainsWithoutHeatSrc'));
        end
        if ~consistentHeatSourceDefinition(self)
            error(message('pde:heatSourceSpecification:inconsistentHeatSrc'));
        end
        
        syssize = 1;

        nf = self.ParentModel.Geometry.NumFaces;
        nc = self.ParentModel.Geometry.NumCells;

        for i = 1:self.nRegion
            ca = self.findHeatSource(self.qregiontype,i);
            ca.performSolverPrecheck(syssize, nf, nc);
        end

    end

    function coefstruct = packHeatSource(self)
        [loc, state] = getSampleLocState(self);
        msh = self.ParentModel.Mesh;
        ma = msh.MeshAssociation;
        numelems = size(msh.Elements,2);
        if self.globalHeatSource()
            coefstruct.ElementsInSubdomain = (1:numelems)';
            coefstruct.NumElementsInSubdomain = numelems;
            ca = self.HeatSourceAssignments(end);
            thisstruct.f{1} = ca.HeatSource;
            coefstruct.Coefficients = thisstruct;
            coefstruct.IsComplex = hasComplexCoefficient(ca, loc, state);
        else
            if ma.PetFormat()
                eidtofid = ma.RegionAssociativity; % Element ID to face ID.
                coefstruct.ElementsInSubdomain = zeros(numelems,1)';
                coefstruct.NumElementsInSubdomain = zeros(self.nRegion,1);
                complexcoef = false;
                endidx = 0;
                for i = 1:self.nRegion
                    felems = find(eidtofid==i);
                    numfelems = numel(felems);
                    startidx = endidx;
                    endidx = startidx+numfelems;
                    startidx = startidx+1;
                    coefstruct.NumElementsInSubdomain(i) = numfelems;
                    coefstruct.ElementsInSubdomain(startidx:endidx)=felems;
                    ca = self.findHeatSource(self.qregiontype,i);
                    if isempty(ca)
                        thisstruct.f{i}  = 0;
                        complexcoef = false;
                    else
                        thisstruct.f{i} = ca.HeatSource;
                        complexcoef = complexcoef | hasComplexCoefficient(ca, loc, state);
                    end
                end
            else
                eIDtocellID = ma.SubdomainAssociativity;
                coefstruct.ElementsInSubdomain = zeros(numelems,1)';
                coefstruct.NumElementsInSubdomain = zeros(self.nRegion,1);
                complexcoef = false;
                endidx = 0;
                for i = 1:self.nRegion
                    celems = eIDtocellID(2,i)-eIDtocellID(1,i)+1;
                    numcelems = celems;
                    startidx = endidx;
                    endidx = startidx+numcelems;
                    startidx = startidx+1;
                    coefstruct.NumElementsInSubdomain(i) = numcelems;
                    coefstruct.ElementsInSubdomain(startidx:endidx)=eIDtocellID(1,i):eIDtocellID(2,i);
                    ca = self.findHeatSource(self.qregiontype,i);
                    if isempty(ca)
                        thisstruct.f{i}  = 0;
                        complexcoef = false;
                    else
                        thisstruct.f{i} = ca.HeatSource;
                        complexcoef = complexcoef | hasComplexCoefficient(ca, loc, state);
                    end
                end
            end
            coefstruct.Coefficients = thisstruct;
            coefstruct.IsComplex = complexcoef;
        end
        if any(coefstruct.IsComplex)
            error(message('pde:heatSourceSpecification:invalidHeatSrcValueComplex'));
        end
    end
    
    
end


methods(Hidden=true, Access={?pde.ThermalModel,?pde.ThermalICRecords})
    function tf = HeatSourceDefined(self)
        tf = false;
        numassigns = numel(self.HeatSourceAssignments);
        if numassigns
            ca = self.HeatSourceAssignments(end);
            tf = ca.HeatSourceDefined();
        end
    end
end

methods(Hidden=true, Access=private)
    % Returns true if each coef has consistent definition across all
    % subdomains. For example, m coefficient defined in one region is
    % also defined in all other regions.
    function tf = consistentHeatSourceDefinition(self)
        tf = globalHeatSource(self);
        if tf
            return
        end
        tf = true;
        
        thiscoef = self.findHeatSource(self.qregiontype, 1);
        for i = 2:self.nRegion
            othercoef = self.findHeatSource(self.qregiontype, i);
            if ~thiscoef.coefficientsMatch(othercoef)
                tf = false;
                return;
            end
        end
    end
    
    
    function [location, state] = getSampleLocState(self)
        msh = self.ParentModel.Mesh;
        nodecoords =msh.Nodes(:,1);
        location.x = nodecoords(1);
        location.y = nodecoords(2);
        if numel(nodecoords) == 3
            location.z = nodecoords(3);
        else
            location.z = 0;
        end
        location.subdomain=1;
        N = self.ParentModel.PDESystemSize;
        state.u(1:N,1)=0;   % state.u(1:NSystemSize, NumLocations)
        state.ux(1:N,1)=0;
        state.uy(1:N,1)=0;
        state.uz(1:N,1)=0;
        state.time=0;
    end
    
    
    function rType = qregiontype(self)
        if self.ParentModel.IsTwoD
            rType = 'face';
        else
            rType = 'cell';
        end
    end
    
    function n = nRegion(self)
        if self.ParentModel.IsTwoD
            n = self.ParentModel.Geometry.NumFaces;
        else
            n = self.ParentModel.Geometry.NumCells;
        end
    end
    
    
end


methods(Hidden=true, Access={?pde.HeatSourceAssignment})
    function delistHeatSourceAssignments(self, hstodelete)
        numhs = numel(self.HeatSourceAssignments);
        for i = 1:numhs
            thishs = self.HeatSourceAssignments(i);
            if thishs == hstodelete
                self.HeatSourceAssignments(i) = [];
                break
            end
        end
        numhs = numel(self.HeatSourceAssignments);
        if numhs == 0 && isvalid(self.ParentModel)
            self.ParentModel.delistHeatSources();
        end
    end
end


methods(Static, Hidden=true)
    function delete(self,~)
        numhs = numel(self.HeatSourceAssignments);
        for i = 1:numhs
            if isvalid(self.HeatSourceAssignments(i))
                delete(self.HeatSourceAssignments(i));
            end
        end           
        
        if isvalid(self.ParentModel)
            self.ParentModel.delistHeatSources();
        end
    end
end

properties (Hidden = true, SetAccess='private')
    ParentModel;
end



end
