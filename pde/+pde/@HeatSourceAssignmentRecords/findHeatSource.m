function mp = findHeatSource(self,varargin)
% findHeatSource Find heat source assignment for a geometric region
% 

% Copyright 2016 The MathWorks, Inc.

narginchk(3,3);  
if isempty(self.ParentModel.Geometry)
    return;
end

parser = inputParser;
parser.addParameter('cell', [], @pde.EquationModel.isValidEntityID); 
parser.addParameter('face', [], @pde.EquationModel.isValidEntityID);  
parser.parse(varargin{:});

if ~ismember('face', parser.UsingDefaults)
    if ~self.ParentModel.IsTwoD
      error(message('pde:heatSourceSpecification:noBoundaryHeatSrc')); 
   end    
    qregiontype = 'face';
    qregionid = parser.Results.face;
    maxid = self.ParentModel.Geometry.NumFaces;       
else
    qregiontype = 'cell';
    qregionid = parser.Results.cell;
    maxid = self.ParentModel.Geometry.NumCells;
   
end


if any(qregionid > maxid)
    error(message('pde:heatSourceSpecification:invalidRegionID'));
end

numassigns = numel(self.HeatSourceAssignments);
numqueries = numel(qregionid);

if ~heatSourceSpecified(self,qregiontype,qregionid)
mp = [];
return;
end
       
mp = repmat(self.HeatSourceAssignments(1),size(qregionid));
for i = 1:numqueries
    rid = qregionid(i);
    %mpfound = false;
    for j = numassigns:-1:1
       thisca =  self.HeatSourceAssignments(j);   
       if ~heatSourceSpecified(self,qregiontype,rid)
           continue
       end
       if strcmp(qregiontype,thisca.RegionType) && ismember(rid, thisca.RegionID)        
          mp(i) = thisca;
     %     mpfound = true;
          break
       end
    end
% It is not necessary to have heat source assignment to all region.
%     if ~mpfound
%       error(message('pde:heatSourceSpecification:entitiesWithoutCoefficients'));
%     end   
end

end

