function tf = globalHeatSource(self,varargin)
% globalHeatSource  True if consistent across the geometric domain

% Copyright 2016 The MathWorks, Inc.

narginchk(1,2); 
tf = true;

if ~self.heatSourceSpecified()   
    tf = false;
    return;
end


if self.nRegion==1
    % 3-D geometry with one cell or 2-D geometry with one face
    return;
end

if nargin == 2
   coefname =  varargin{1};
   iscoef = false;
   if ischar(coefname)
       iscoef = (strcmp(coefname, 'HeatSource'));
   end
   if ~iscoef
      error(message('pde:heatSourceSpecification:invalidHeatSrcName')); 
   end
end

thiscoef = self.findHeatSource(self.qregiontype, 1);
for i = 2:self.nRegion
    othercoef = self.findHeatSource(self.qregiontype, i);           
    if ~thiscoef.sameCoefficients(othercoef, varargin{:})
        tf = false;
        return;
    end            
end   



end