function [dudxIntrp, dudyIntrp, dudzIntrp] = evaluateGradient(obj,varargin)
%evaluateGradient - evaluate gradients of solution at specified spatial locations
%
% [dudxIntrp, dudyIntrp, dudzIntrp] = evaluateGradient(R,QP, Ti, EQj) 
% evaluates the gradients in a PDEResults object R, for a system of equations, 
% at the query point locations QP. The points QP are represented as a matrix 
% of size 3-by-Numq, where 3 represents 3 spatial dimensions and Numq is the 
% number of query points. Ti is a scalar or vector of indices in the range 
% 1 to tlist, where tlist is the solution time requests passed to the solvers. 
% EQj is a scalar or vector of equation indices in the range 1 to PDESystemSize, 
% where PDESystemSize is the total number of equation in the system of PDEs.
% The output dudxIntrp, dudyIntrp, dudzIntrp represents the components of 
% the gradients at the query points QP.
%
% [dudxIntrp, dudyIntrp] = evaluateGradient(R,QP, Ti, EQj) evaluates the 
% gradients in a PDEResults object R, for a system of equations, at the query 
% point locations QP. The points QP are represented as a matrix of size 
% 2-by-Numq, where 2 represents 2 spatial dimensions and Numq is the number 
% of query points. Ti is a scalar or vector of indices in the range 1 to tlist, 
% where tlist is the solution time requests passed to the solvers. EQj is a 
% scalar or vector of equation indices in the range 1 to PDESystemSize, 
% where PDESystemSize is the total number of equation in the system of PDEs.
% The output dudxIntrp, dudyIntrp represents the components of the gradients
% at the query points QP.
%
% [__] = evaluateGradient(R,QP, Ti) evaluates the gradients in a PDEResults 
% object R, for a scalar PDE, at the query point locations QP. The points 
% QP are represented as a matrix of size 2-by-Numq, where 2 represents 2 
% spatial dimensions and Numq is the number of query points. Ti is a scalar 
% or vector of indices in the range 1 to tlist, where tlist is the solution 
% time requests passed to the solvers. The output dudxIntrp, dudyIntrp 
% represents the components of the gradients at the query points QP.
%
% [__] = evaluateGradient(R, xq, yq, __ )  and evaluateGradient(R, xq, yq, zq, __ ) 
% allow the query points QP tobe specified in coordinate vector format.
%
% Example: Solve a system of second-order time-dependent PDEs and evaluate
% gradients of the solution.
%     %Create a PDEModel, import geometry, and specify coefficients.
%     model = createpde(3);
%     importGeometry(model,'Plate10x10x1.stl');
%     E = 200e9;
%     nu = 0.3;
%     c = elasticityC3D(E,nu);
%     specifyCoefficients(model,'m',1,'d',0,'c',c,'a',0,'f',[0;0;0]);   
%    
%     %Apply fixed boundary condition on one edge of the slab and a traction load on
%     %the edge opposite to the fixed edge, and generate mesh.
%     applyBoundaryCondition(model,'dirichlet','face',2,'u',[0,0,0]);
%     applyBoundaryCondition(model,'neumann','face',5,'g',[0,0,1e3]);
%     generateMesh(model,'Hmax',1);
%
%     %Set initial conditions and solve the PDE at specified times.
%     setInitialConditions(model,0,0);
%     tlist = 0:5e-4:5e-3;
%     results = solvepde(model,tlist);
% 
%     %Evaluate the gradients of the solution at fixed x- and z-coordinates
%     %in the centers of their ranges, 5 and 0.5 respectively. Evaluate for
%     %y from 0 through 10 in steps of 0.2. Obtain just component 3, the
%     %z-component.
%     yy = 0:0.2:10;
%     zz = 0.5*ones(size(yy));
%     xx = 10*zz;
%     component = 3;
%     [gradx,grady,gradz] = evaluateGradient(results,xx,yy,zz,component,1:length(tlist));
% 
%     gradx = squeeze(gradx);
%     grady = squeeze(grady);
%     gradz = squeeze(gradz);
% 
%     figure
%     t = [1:2:11];
%     for i = t
%       p(i) = plot(yy,grady(:,i),'DisplayName', strcat('t=', num2str(tlist(i))));
%       hold on
%     end
%     ylim([-5e-6, 20e-6]) 
%     legend(p(t))
%     xlabel('y')
%     ylabel('grady')
%
%    See also pde.TimeDependentResults, createPDEResults, solvepde, meshgrid, surf

% Copyright 2015-2016 The MathWorks, Inc.


[dudxIntrp, dudyIntrp, dudzIntrp] = pde.PDEResults.evalGradInternal(obj,varargin{:}) ;
 
end       

