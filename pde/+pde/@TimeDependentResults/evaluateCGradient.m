function [cdudx, cdudy, cdudz] = evaluateCGradient(obj,varargin)
% evaluateCGradient - Evaluate flux of a time-dependent PDE solution
%
% [cdudx,cdudy] = evaluateCGradient(R) computes the tensor product of
% c-coefficient and spatial gradients of solution for a 2-D problem. R is
% an object of type TimeDependentResults. The shape of output arrays, cdudx
% and cdudy, depends on the number of PDEs for which R is the results
% object. The first array dimension of each of the output array represents
% node index. The second array dimension represents either time-step for a
% single PDE or the equation index for a system of PDEs. The third array
% dimension represents time-step index for a system of PDEs. The
% c-coefficients used to obtain the results object R are used internally to
% compute the tensor product.
%
% [cdudx,cdudy,cdudz] = evaluateCGradient(R) evaluates flux for a 3-D
% problem.
%
% [__] = evaluateCGradient(R,QP,Ti) evaluates flux for a single PDE with
% results object R at time-steps Ti. The points QP is a matrix of size
% dim-by-Numq, where dim represents spatial dimensions, dim is 2 for 2-D
% and 3 for 3-D problems, and Numq is the number of query points. Ti is a
% scalar or vector of indices in the range 1 to length(tlist), where tlist
% is the solution time vector passed as input to the solver. The first
% array dimension of each of the output array corresponds to the number of
% spatial points defined by QP. The second array dimension corresponds to
% time-steps Ti.
%
% [__] = evaluateCGradient(R,QP,EQi,Tj) evaluates flux for a system of PDEs
% with results object R, for equations given by EQi and time-steps Ti. EQi
% is a scalar or vector of equation indices in the range 1 to
% PDESystemSize, the total number of equation in the system of PDEs. Tj is
% a scalar or vector of indices in the range 1 to length(tlist), where
% tlist is the solution time vector passed as input to the solver. The
% first array dimension of each of the output array corresponds to the
% number of spatial points defined by QP. The second array dimension
% corresponds equation indices EQi. The third array dimension corresponds
% to time-steps Tj.
%
% [__] = evaluateCGradient(R,xq,yq,__ ) and [__] =
% evaluateCGradient(R,xq,yq,zq, __ ) allow the query points QP to be
% specified as coordinate vector or N-D grid. The first array dimension of
% each of the output array corresponds to spatial points defined by
% [x(:),y(:)] in 2-D space or [x(:),y(:),z(:)] in 3-D space.
%
% Example: Solve a 2-D transient heat transfer problem on a square domain
% and compute heat flow across convective boundary.
%
%     %Create a PDEModel and square geometry.
%     numberOfPDE = 1;
%     pdem = createpde(numberOfPDE);
%     g = @squareg;
%     geometryFromEdges(pdem,g);
%     pdegplot(pdem,'EdgeLabels','on')
%
%     %Specify material properties and ambient conditions.
%     rho = 7800;
%     cp = 500;
%     k = 100;
%     Text = 25;
%     hext = 5000;
%     specifyCoefficients(pdem,'m',0,'d',rho*cp,'c',k,'a',0,'f',0);
%
%     %Apply insulated boundary conditions on three edges.
%     applyBoundaryCondition(pdem,'neumann','Edge', [1,3,4],'q',0,'g',0);
%     %Apply free convection boundary conditions on the right edge.
%     applyBoundaryCondition(pdem,'neumann','Edge', 2,'q',hext,'g',Text*hext);
%
%     %Set uniform room temperature across domain.
%     setInitialConditions(pdem,25);
%     %Set higher temperature on the left edge.
%     setInitialConditions(pdem, 100, 'Edge', 4);
%
%     %Generate mesh and solve.
%     generateMesh(pdem);
%     tlist = 0:1000:200000;
%     R = solvepde(pdem,tlist);
%
%     %Define a line at convective boundary to compute heat flux across it.
%     yg = -1:0.1:1;
%     xg = ones(size(yg));
%
%     %Evaluate the product of c-coefficient and spatial gradients at
%     %(xg,yg).
%     [qx,qy] = R.evaluateCGradient(xg,yg,1:length(tlist));
%
%     %Spatially integrate gradients to obtain heat flow for each
%     %time-step. Negate qx, as positive heat flows is in the direction of
%     %negative temperature gradient.
%     HeatFlowX(1:length(tlist)) = trapz(yg,-qx(:,1:length(tlist)));
%
%     %Plot convective heat flow over time.
%     figure
%     plot(tlist,HeatFlowX)
%     title('Heat flow across convection boundary')
%     xlabel('Time')
%     ylabel('Heat flow')
%
%    See also solvepde, pde.TimeDependentResults, createPDEResults

% Copyright 2016 The MathWorks, Inc.

N = obj.PDESystemSize;
Nt = obj.NumTimeEig;
Np = size(obj.Mesh.Nodes,2);

cdudx=zeros(Np,N,Nt);
cdudy=zeros(Np,N,Nt);
cdudz=zeros(Np,N,Nt);

for ti = 1:Nt
    cCoeffAtNodes = pde.PDEResults.evalCCoeffAtNodes(obj,obj.SolutionTimes(ti));
    
    dudx = obj.XGradients;
    dudy = obj.YGradients;
    % Shape to 3-D arrays for uniformity in case of scalar PDEs
    dudx = reshape(dudx,[Np,N,Nt]);
    dudy = reshape(dudy,[Np,N,Nt]);
    
    
    if obj.IsTwoD
        % 2-D implementation code
        [cdudx(:,:,ti), cdudy(:,:,ti)] = pde.PDEResults.cgradImpl2D(obj,cCoeffAtNodes,dudx(:,:,ti)',dudy(:,:,ti)');
    else
        % Shape to 3-D arrays for uniformity in case of scalar PDEs
        dudz = obj.ZGradients;
        dudz = reshape(dudz,[Np,N,Nt]);
        % 3-D implementation code
        [cdudx(:,:,ti), cdudy(:,:,ti), cdudz(:,:,ti)] = pde.PDEResults.cgradImpl3D(obj,cCoeffAtNodes,dudx(:,:,ti)',dudy(:,:,ti)',dudz(:,:,ti)');
    end
    
    
end % End of loop over time-steps
% Remove second singleton dimension in case of scalar PDEs.
cdudx = squeeze(cdudx);
cdudy = squeeze(cdudy);
cdudz = squeeze(cdudz);

% Perform interpolation if user specifies required additional arguments.
if (nargin > 1)
    [cdudx,cdudy,cdudz] = pde.PDEResults.evalCGradInternal(obj,cdudx,cdudy,cdudz,varargin{:});
end

end