function uintrp = interpolateSolution(obj,varargin)
%interpolateSolution - Interpolate solution at specified spatial locations
%
% uintrp = interpolateSolution(R, QP, EQi, Tj) interpolates the solution in a 
% results object R, for a system of equations, at the query point 
% locations QP. The points QP are represented as a matrix of size Ndim-by-Numq,
% where Ndim is the number of spatial dimensions and Numq is the number of 
% query points. Tj is a scalar or vector of indices in the range 1 to tlist, 
% where tlist is the solution time requests passed to the solvers. EQi is a 
% scalar or vector of equation indices in the range 1 to PDESystemSize, 
% where PDESystemSize is the total number of equation in the system of PDEs.
% uintrp is the interpolated solution at the query points QP.
%
% uintrp = interpolateSolution(R, QP, Ti) interpolates the solution in a 
% results object R, for a scalar PDE, at the query point locations QP. 
% The points QP are represented as a matrix of size Ndim-by-Numq, where Ndim 
% is the number of spatial dimensions and Numq is the number of query points.
% Ti is a scalar or vector of indices in the range 1 to tlist, where tlist 
% is the solution time requests passed to the solvers. uintrp is the 
% interpolated solution at the query points QP.
%
% uintrp = interpolateSolution(R, xq, yq, __ ) 
% and interpolateSolution(R, xq, yq, zq, __ ) allow the query points QP to
% be specified in coordinate vector format.
%
%   Example 1: Solve a parabolic PDE on a square plate and visualize the
%   interpolated solution on a mid-plane.
%
%     %Create a PDEModel, import geometry, and specify coefficients. 
%     N = 1; % single PDE
%     pdem = createpde(N);
%     g = importGeometry(pdem,'Plate10x10x1.stl');
%     c = 1;
%     a = 0;
%     d = 1;
%     f = @(region,state) 1+10*region.z.^2+region.y;
%     specifyCoefficients(pdem,'m',0,'d',d,'c',c,'a',a,'f',f);
%
%     %Apply boundary conditions and generate the mesh.
%     applyBoundaryCondition(pdem,'dirichlet','Face',1:6,'u',0);
%     generateMesh(pdem,'Hmax',0.6);
%     
%     %Set initial conditions and solve the PDE at specified times.
%     setInitialConditions(pdem,0);
%     tlist = 0:0.02:0.2;
%     R = solvepde(pdem,tlist);
%     
%     %Define a grid of points representing the mid-plane of the plate.
%     xp = 0:0.5:10; yp = 0:0.5:10;
%     [XX, YY] = meshgrid(xp,yp);
%     ZZ = 0.5*ones(size(XX));
%     
%     %Interpolate the solution on mid-plane grid for three time instants.
%     uintrp = interpolateSolution(R,XX,YY,ZZ,[3,7,11]);
%     
%     %Reshape the solution to visualize using the MATLAB surf function.
%     ures1 = reshape(uintrp(:,1),size(XX));
%     ures2 = reshape(uintrp(:,2),size(XX));
%     ures3 = reshape(uintrp(:,3),size(XX));
%     
%     %Visualize the temporal evolution of the solution on the mid-plane.
%     figure
%     subplot(1,3,1)
%     surf(XX,YY,ures1) 
%     zlim([0 1.5])
%     xlabel('x'); ylabel('y'); zlabel('u');
%     
%     subplot(1,3,2)
%     surf(XX,YY,ures2) 
%     zlim([0 1.5])
%     xlabel('x'); ylabel('y'); zlabel('u');
%     
%     subplot(1,3,3)
%     surf(XX,YY,ures3)
%     zlim([0 1.5])
%     xlabel('x'); ylabel('y'); zlabel('u');
%
%    See also pde.TimeDependentResults, createPDEResults, solvepde, meshgrid, surf
 
% Copyright 2015-2016 The MathWorks, Inc.

uintrp = pde.PDEResults.interpolateSolutionInternal(obj,varargin{:});

end




