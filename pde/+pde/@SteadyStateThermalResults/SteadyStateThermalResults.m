classdef SteadyStateThermalResults < pde.ThermalResults
% pde.SteadyStateThermalResults - Steady-state thermal model solution and its derived quantities
%   A SteadyStateThermalResults object provides a convenient representation
%   of steady-state thermal model solution. It also provides functions to
%   interpolate temperature, evaluate temperature gradients, heat flux, and
%   heat rate.
%
% SteadyStateThermalResults methods:
%   interpolateTemperature      - Interpolate temperature at specified spatial locations
%   evaluateTemperatureGradient - Evaluate gradients of temperature at specified spatial locations
%   evaluateHeatFlux            - Evaluate heat flux at specified spatial locations
%   evaluateHeatRate            - Evaluate integrated heat flux normal to a given boundary
%
% SteadyStateThermalResults properties:
%   Temperature          - Temperature at nodal locations
%   XGradients           - Spatial gradient of temperature along x-direction
%   YGradients           - Spatial gradient of temperature along y-direction
%   ZGradients           - Spatial gradient of temperature along z-direction
%   Mesh                 - Discretization of the domain
%
%    See also pde.TransientThermalResults

% Copyright 2016 The MathWorks, Inc.
     
    properties(SetAccess = protected)
        % Temperature - Temperature at nodal locations 
        % The Temperature vector can be indexed into to extract a
        % sub-vector of interest.
        Temperature;
         
        % XGradients - Spatial gradient of temperature along x-direction
        % The shape of XGradients is same as the shape of Temperature.
        XGradients;

        % YGradients - Spatial gradient of temperature along y-direction
        % The shape of YGradients is same as the shape of Temperature.
        YGradients;
        
        % ZGradients - Spatial gradient of temperature along z-direction
        % The shape of ZGradients is same as the shape of Temperature.
        ZGradients;
    end
    
    

    methods
        function obj = SteadyStateThermalResults(varargin)
            obj@pde.ThermalResults(varargin{:})
            if nargin == 0
                return
            end
            narginchk(2,2);
            if (obj.IsTimeEig)
                error(message('pde:PDEResults:notStationaryResults'));
            end
            
            pdem = varargin{1};
            u = varargin{2};
            
            %Since N = 1 for thermal model, no reshaping is required.
            ureshaped = u;
            obj.Temperature = u;
            if ~isempty(pdem.MaterialProperties)
                obj.Coefficients = pdem.MaterialProperties.packProperties;
            else
                obj.Coefficients = [];
            end
            
            obj.XGradients = [];
            obj.YGradients = [];
            obj.ZGradients = [];
            obj.InterpolantdUdx = [];
            obj.InterpolantdUdy = [];
            obj.InterpolantdUdz = [];
            if isempty(obj.Temperature)
                return;
            end
            
            obj.Interpolant   = pde.ThermalResults.constructInterpolat(pdem.Mesh,...
                ureshaped, pdem.PDESystemSize, obj.NumTimeEig);
            
            % Calculate gradients, assign nodal gradients as properties and
            % construct interpolants for gradients
            [ux,uy,uz] = nodalGradients(obj.Interpolant);
            obj.XGradients = pde.ThermalResults.reshapePDESolution(ux, ...
                obj.IsTimeEig, pdem.PDESystemSize, obj.NumTimeEig);
            obj.YGradients = pde.ThermalResults.reshapePDESolution(uy, ...
                obj.IsTimeEig, pdem.PDESystemSize, obj.NumTimeEig);
            obj.ZGradients = pde.ThermalResults.reshapePDESolution(uz, ...
                obj.IsTimeEig, pdem.PDESystemSize, obj.NumTimeEig);
            
            obj.InterpolantdUdx   = pde.ThermalResults.constructInterpolat(pdem.Mesh,...
                obj.XGradients, pdem.PDESystemSize, obj.NumTimeEig);
            
            obj.InterpolantdUdy   = pde.ThermalResults.constructInterpolat(pdem.Mesh,...
                obj.YGradients, pdem.PDESystemSize, obj.NumTimeEig);
            
            if ~obj.IsTwoD
                obj.InterpolantdUdz   = pde.ThermalResults.constructInterpolat(pdem.Mesh,...
                    obj.ZGradients, pdem.PDESystemSize, obj.NumTimeEig);
            end
            
        end
        
        
        % Methods
        function Tintrp = interpolateTemperature(obj,varargin)
            %interpolateTemperature - Interpolate temperature at specified spatial locations
            %
            % Tintrp = interpolateTemperature(R, QP) interpolates the
            % temperature in a steady-state thermal results object R at the
            % query point locations QP. The points QP are represented as a
            % matrix of size Ndim-by-Numq, where Ndim is the number of
            % spatial dimensions and Numq is the number of query points.
            % Tintrp is the interpolated temperature at the query points QP.
            %
            % Tintrp = interpolateTemperature(R, xq, yq, __ ) and
            % interpolateTemperature(R, xq, yq, zq, __ ) interpolates the
            % temperature using the query points QP specified as vectors of
            % coordinates or an N-D grid.
            %
            % Example: Evaluate temperature distribution across the wall of
            % an insulated pipe carrying hot fluid in a cold atmosphere.
            %
            % %Create a steady-state thermal model.
            % thermalmodel = createpde('thermal');
            %
            % %Create a nested cylinders geometry to model the two layered
            % %insulated pipe section, consisting of an inner metal pipe
            % %surrounded by insulation material.
            % gm = multicylinder([20,25,35], 20, 'Void', [1,0,0]);
            %
            % %Assign geometry to the thermal model and generate mesh.
            % thermalmodel.Geometry = gm;
            % generateMesh(thermalmodel,'Hmax',4); 
            %
            % %Specify thermal conductivities for metal and insulation.
            % thermalProperties(thermalmodel,'Cell',1,'ThermalConductivity',0.40);
            % thermalProperties(thermalmodel,'Cell',2,'ThermalConductivity',0.0015); 
            %
            % %Define the temperature of the pipe inner wall to be 85 C and outer
            % %wall temperature to be 4 C.
            % thermalBC(thermalmodel,'Face',3,'Temperature',85);
            % thermalBC(thermalmodel,'Face',7,'Temperature',4); 
            %
            % %Solve the thermal model and plot temperature distribution.
            % result = solve(thermalmodel);
            % figure
            % pdeplot3D(thermalmodel,'ColormapData',result.Temperature)
            %
            % %Interpolate the temperature along a radial line.
            % xg = 20:0.5:35;
            % yg = zeros(size(xg));
            % zg = 10*ones(size(xg));
            % Tradial = interpolateTemperature(result,xg,yg,zg);
            %
            % %Plot the temperature variation along the thickness of pipe.
            % figure
            % plot(xg,Tradial)
            % title('Variation of temperature across the pipe thickness')
            % xlabel('Radial distance')
            % ylabel('Temperature')
            %
            % See also pde.TransientThermalResults/interpolateTemperature            
            
            Tintrp = pde.ThermalResults.interpolateSolutionInternal(obj,varargin{:});
        end
        
        function [TxInt, TyInt, TzInt] = evaluateTemperatureGradient(obj,varargin)
            %evaluateTemperatureGradient - Evaluate gradients of temperature at specified spatial locations
            %
            % [TxInt, TyInt, TzInt] = evaluateTemperatureGradient(R,QP)
            % evaluates the spatial gradients of temperature in a
            % SteadyStateThermalResults object R at the query point
            % locations QP. The points QP are represented as a matrix of
            % size Ndim-by-Numq, where Ndim is the number of spatial
            % dimensions and Numq is the number of query points. The output
            % TxInt, TyInt, TzInt represents the spatial components of
            % temperature gradients at the query points QP.
            %
            % [__] = evaluateTemperatureGradient(R, xq, yq, __ ) and [__] =
            % evaluateTemperatureGradient(R, xq, yq, zq, __ ) evaluates the
            % spatial temperature gradient using the query points QP
            % specified as vectors of coordinates or an N-D grid.
            %
            % Example: Evaluate temperature distribution across the wall of
            % an insulated pipe carrying hot fluid in a cold atmosphere.
            %
            % %Create a steady-state thermal model.
            % thermalmodel = createpde('thermal');
            %
            % %Create a nested cylinders geometry to model the two layered
            % %insulated pipe section, consisting of an inner metal pipe
            % %surrounded by insulation material.
            % gm = multicylinder([20,25,35], 20, 'Void', [1,0,0]);
            %
            % %Assign the geometry to the thermal model and generate mesh.
            % thermalmodel.Geometry = gm;
            % generateMesh(thermalmodel,'Hmax',4); 
            %
            % %Specify thermal conductivities for metal and insulation.
            % thermalProperties(thermalmodel,'Cell',1,'ThermalConductivity',0.40);
            % thermalProperties(thermalmodel,'Cell',2,'ThermalConductivity',0.0015); 
            %
            % %Define the temperature of the pipe inner wall to be 85 C and outer
            % %wall temperature to be 4 C.
            % thermalBC(thermalmodel,'Face',3,'Temperature',85);
            % thermalBC(thermalmodel,'Face',7,'Temperature',4); 
            %
            % %Solve the thermal model and visualize temperature distribution.
            % result = solve(thermalmodel);
            % figure
            % pdeplot3D(thermalmodel,'ColormapData',result.Temperature)
            %
            % %Compute temperature gradients around the circumference 
            % %and at mid-way along insulation thickness.
            % r = 30;
            % theta = 0:0.1:2*pi;
            % xg = r*cos(theta)';
            % yg = r*sin(theta)';
            % zg = 10*ones(size(xg));
            % [Tx,Ty,Tz] = evaluateTemperatureGradient(result,xg,yg,zg);
            % figure
            % quiver(xg,yg,Tx,Ty)
            % title({'Temperature gradients around the circumference', ...
            %                    'Mid-way along insulation thickness'})
            % xlabel('x');
            % ylabel('y');
            %
            % See also pde.TransientThermalResults/evaluateTemperatureGradient  
            
            [TxInt, TyInt, TzInt] = pde.ThermalResults.evalGradInternal(obj,varargin{:});
        end
        
        function [qx, qy, qz] = evaluateHeatFlux(obj,varargin)
            % evaluateHeatFlux - Evaluate heat flux at specified spatial locations
            %
            % [qx,qy] = evaluateHeatFlux(R) evaluates the heat flux of a
            % thermal solution for a 2-D problem. R is an object of type
            % SteadyStateThermalResults. The output vectors, qx and qy,
            % contain nodal heat flux density.
            %
            % [qx,qy,qz] = evaluateHeatFlux(R) evaluates the heat flux of a
            % thermal solution for a 3-D problem.
            %
            % [__] = evaluateHeatFlux(R,QP) evaluates the heat flux using
            % results object R at points QP. The points QP is a matrix of
            % size Ndim-by-Numq, where Ndim is the number of spatial
            % dimensions and Numq is the number of query points. The output
            % vectors contains heat flux density at spatial points
            % defined by QP.
            %
            % [__] = evaluateHeatFlux(R,xq,yq,__ ) and [__] =
            % evaluateHeatFlux(R,xq,yq,zq, __ ) evaluates the heat flux
            % using the query points QP specified as vectors of coordinates
            % or an N-D grid. Elements in each of the output vectors
            % corresponds to spatial points defined by [x(:),y(:)] in 2-D
            % space or [x(:),y(:),z(:)] in 3-D space.
            %
            %
            % Example: Evaluate temperature distribution across the wall of
            % an insulated pipe carrying hot fluid in a cold atmosphere.
            %
            % %Create a steady-state thermal model.
            % thermalmodel = createpde('thermal');
            %
            % %Create a nested cylinders geometry to model the two layered
            % %insulated pipe section, consisting of an inner metal pipe
            % %surrounded by insulation material.
            % gm = multicylinder([20,25,35], 20, 'Void', [1,0,0]);
            %
            % %Assign the geometry to the thermal model and generate mesh.
            % thermalmodel.Geometry = gm;
            % generateMesh(thermalmodel,'Hmax',4); 
            %
            % %Specify thermal conductivities for metal and insulation.
            % thermalProperties(thermalmodel,'Cell',1,'ThermalConductivity',0.40);
            % thermalProperties(thermalmodel,'Cell',2,'ThermalConductivity',0.0015); 
            %
            % %Define the temperature of the pipe inner wall to be 85 C and outer
            % %wall temperature to be 4 C.            
            % thermalBC(thermalmodel,'Face',3,'Temperature',85);
            % thermalBC(thermalmodel,'Face',7,'Temperature',4); 
            %
            % %Solve the thermal model and plot temperature distribution.
            % result = solve(thermalmodel);
            % figure
            % pdeplot3D(thermalmodel,'ColormapData',result.Temperature)
            %             
            % %Compute the heat flux around the circumference and at mid-way
            % %along insulation thickness.
            % r = 30;
            % theta = 0:0.1:2*pi;
            % xg = r*cos(theta)';
            % yg = r*sin(theta)';
            % zg = 10*ones(size(xg));
            % 
            % [qx,qy,qz] = evaluateHeatFlux(result,xg,yg,zg);
            % figure
            % quiver(xg,yg,qx,qy)
            % title({'Heat flux around the circumference', ...
            %                      'Mid-way along insulation thickness'})
            %
            % See also pde.TransientThermalResults/evaluateHeatFlux  
            
            cCoeffAtNodes = pde.ThermalResults.evalCCoeffAtNodes(obj);
            
            dudx = obj.XGradients';
            dudy = obj.YGradients';
            
            if obj.IsTwoD
                % 2-D implementation code
                [qx, qy] = pde.ThermalResults.cgradImpl2D(obj,cCoeffAtNodes,dudx,dudy);
                qz = [];
            else
                dudz = obj.ZGradients';
                % 3-D implementation code
                [qx, qy, qz] = pde.ThermalResults.cgradImpl3D(obj,cCoeffAtNodes,dudx,dudy,dudz);
            end
            
            % Perform interpolation if user specifies required additional arguments.
            if (nargin > 1)
                [qx, qy, qz] = pde.ThermalResults.evalCGradInternal(obj,qx,qy,qz,varargin{:});
            end
                
            %Negate c-gradients to get heat flux direction.
            qx = -qx;
            qy = -qy;
            qz = -qz;
        end
        
        %Declaration of method to compute heat rate.
        Qn = evaluateHeatRate(obj,varargin);
        
    end
    
end

