function Qn= evaluateHeatRate(self,varargin)
% evaluateHeatRate - Evaluate integrated heat flux normal to a given boundary
%
% Qn = evaluateHeatRate(R,'REGIONTYPE',REGIONID) evaluates the integrated
% heat flow normal to the boundary specified by using REGIONTYPE and
% REGIONID, for the SteadyStateThermalResults object R. Here, REGIONTYPE is
% 'Face' for a 3-D model, and 'Edge' for a 2-D model. REGIONID is an
% integer specifying the ID of the geometric entity. The output, Qn, is a
% scalar quantity representing the integrated heat flow rate, energy per
% unit time, flowing in the direction normal to the boundary. Qn is
% positive if the heat flows out of the domain, and negative if the heat
% flows into the domain.
% 
%
% Example: Evaluate the heat flow from the face of a block.
%
% %Create a steady-state thermal model.
% thermalmodel = createpde('thermal','steadystate'); 
%
% %Import the block geometry and generate mesh.
% importGeometry(thermalmodel,'Block.stl');
% generateMesh(thermalmodel,'GeometricOrder','linear');
%
% %Assign material properties.
% thermalProperties(thermalmodel,'ThermalConductivity',0.80);
%
% %Apply fixed temperature on the opposite ends of the
% %block. All other faces are insulated by default.
% thermalBC(thermalmodel,'Face',1,'Temperature',100); 
% thermalBC(thermalmodel,'Face',3,'Temperature',50); 
%
% %Solve the thermal model
% result = solve(thermalmodel); 
%
% %Compute the heat flow rate across face 3 of the block.
% Qn = evaluateHeatRate(result,'Face',3);
% 
% See also pde.ThermalModel/solve,
% pde.SteadyStateThermalResults/evaluateHeatFlux,
% pde.TransientThermalResults/evaluateHeatRate
 
%       Copyright 2016 The MathWorks, Inc.


Qn = pde.ThermalResults.evaluateHeatRateInternal(self,varargin{:});

