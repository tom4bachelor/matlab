function mtl = thermalProperties(self,varargin)
%thermalProperties - Assign thermal properties of the material to a thermal model
%   mtl = thermalProperties(thermalmodel,PROPERTY,VALUE) assigns material
%   properties to a thermal model. VALUE can be specified as
%        (1) A numerical scalar for a constant material properties.
%        (2) MATLAB function format for a non-constant or nonlinear material.
%
%   PROPERTY                 VALUE
%-------------------------------------------------------------------------
%   'ThermalConductivity'    Thermal conductivity of the material.
%   'MassDensity'            Mass density of the material.
%   'SpecificHeat'           Specific heat of the material.
%
%  For steady-state analysis, thermal conductivity is the only required
%  material property. Whereas, for transient analysis all three material
%  properties are required.
%
%  mtl = thermalProperties(__,'Face',FACEID) assigns thermal properties for
%  the specifed face of a 2-D geometry. FACEID is a positive integer in the
%  range of 1 to thermalmodel.Geometry.NumFaces.
%
%  mtl = thermalProperties(__,'Cell',CELLID) assigns thermal properties for
%  the specifed cell of a 3-D geometry. CELLID is a positive integer in the
%  range of 1 to thermalmodel.Geometry.NumCells.
%
%   Example 1: Assign thermal properties for a steady-state analysis.
%
%       thermalmodel = createpde('thermal','steadystate');
%       gm = importGeometry(thermalmodel,'SquareBeam.STL');
%       thermalProperties(thermalmodel,'ThermalConductivity',0.08);
%
%   Example 2: Assign thermal properties for a transient analysis.
%
%       thermalmodel = createpde('thermal','transient');
%       gm = importGeometry(thermalmodel,'SquareBeam.STL');
%       thermalProperties(thermalmodel,'ThermalConductivity',0.2,...
%                                      'MassDensity',2.7E-6,...
%                                      'SpecificHeat',920);
%
%
%    See also pde.ThermalModel, pde.ThermalModel/thermalBC
%             pde.ThermalModel/internalHeatSource,pde.ThermalModel/thermalIC,
%             pde.ThermalModel/solve

%    Copyright 2016 The MathWorks, Inc.


narginchk(3,9);
nargoutchk(0,1);

if isempty(self.Geometry)
    error(message('pde:ThermalModel:thermalModelHasNoGeom'));
end

parser = inputParser;
parser.addParameter('face', [], @pde.EquationModel.isValidEntityID);
parser.addParameter('cell', [], @pde.EquationModel.isValidEntityID);
parser.addParameter('MassDensity', []);
parser.addParameter('SpecificHeat', []);
parser.addParameter('ThermalConductivity', []);

parser.parse(varargin{:});
argsToPass = varargin;
%If a face (for 2-D) or a cell (for 3-D) is not explicitly specified then
%use the same material for all faces and edges.

if self.IsTwoD
    if ~isempty(parser.Results.cell)
        error(message('pde:ThermalModel:noMtlOnCellFor2D'));
    end
    if isempty(parser.Results.face)
        argsToPass = [{'face',1:self.Geometry.NumFaces}, varargin];
    else
        self.isValidEntityID(parser.Results.face);
        if any(parser.Results.face > self.Geometry.NumFaces)
            error(message('pde:pdeModel:invalidFaceIndex'));
        end
    end
else
    if ~isempty(parser.Results.face)
        error(message('pde:ThermalModel:noMtlOnBoundary'));
    end    
    if isempty(parser.Results.cell)
        argsToPass = [{'cell',1:self.Geometry.NumCells}, varargin];
    else
        self.isValidEntityID(parser.Results.cell);
        if any(parser.Results.cell > self.Geometry.NumCells)
            error(message('pde:pdeModel:invalidCellIndex'));
        end
    end
    
end


if (strcmp(self.AnalysisType,'transient'))
    if (isempty(parser.Results.MassDensity) || isempty(parser.Results.SpecificHeat))
        error(message('pde:ThermalModel:incompleteMtlForTransient'));
    end
end

if (strcmp(self.AnalysisType,'steadystate'))
    if (~isempty(parser.Results.MassDensity) || ~isempty(parser.Results.SpecificHeat))
        warning(message('pde:ThermalModel:extraMtlPropsForSteadyState'));
    end
end


if isempty(self.MaterialProperties)
    mtlContainer = pde.MaterialAssignmentRecords(self);
    mtl = pde.ThermalMaterialAssignment(mtlContainer,argsToPass{:});
    mtlContainer.MaterialAssignments = mtl;
    self.MaterialProperties = mtlContainer;
else
    mtl = pde.ThermalMaterialAssignment(self.MaterialProperties,argsToPass{:});
    self.MaterialProperties.MaterialAssignments(end+1) = mtl;
end


end