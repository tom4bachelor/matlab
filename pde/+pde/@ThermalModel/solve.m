function R = solve(self,varargin)
% solve - Solve the thermal model
% R = solve(thermalmodel) solves a steady-state thermal model and
% returns the result in a SteadyStateThermalResults object.
%
% R = solve(thermalmodel, tlist) solves a transient thermal model and
% returns the result in a TransientThermalResults object. The input
% argument tlist is a monotonic increasing or decreasing vector that
% specifies the times at which the solution is requested.
%
% Example 1: Solve a steady-state thermal model.
%
%     %Create a thermal model for this problem.
%     thermalmodel = createpde('thermal');
%
%     %Import and plot the block geometry.
%     importGeometry(thermalmodel,'Block.stl');
%
%     %Assign material properties.
%     thermalProperties(thermalmodel,'ThermalConductivity',0.80);
%
%     % Apply a constant temperature of 100 to the left side of the block
%     %(face 1) and a constant temperature of 300 at the right side of the
%     %block (face 3). All other faces are insulated by default.
%     thermalBC(thermalmodel,'Face',1,'Temperature',100);
%     thermalBC(thermalmodel,'Face',3,'Temperature',300);
%
%     %Mesh the geometry and solve the problem.
%     generateMesh(thermalmodel);
%     thermalresults = solve(thermalmodel)
%
%     %Plot temperature at nodal locations.
%     pdeplot3D(thermalmodel,'ColorMapData',thermalresults.Temperature)
%
%  Example 2: Solve transient thermal model.
%
%     thermalmodel = createpde('thermal','transient');
%     gm = @squareg;
%     geometryFromEdges(thermalmodel,gm);
%     generateMesh(thermalmodel);
%     thermalProperties(thermalmodel,'ThermalConductivity',50,...
%                                     'MassDensity',2500,...
%                                     'SpecificHeat',600);
%     bc = thermalBC(thermalmodel,'Edge',[1,3],'Temperature',100);
%     qs = internalHeatSource(thermalmodel,2);
%     ic = thermalIC(thermalmodel,0);
%     tlist = 0:0.1:1;
%     thermalresults = solve(thermalmodel,tlist)
%     pdeplot(thermalmodel,'xydata',thermalresults.Temperature)
%
% See also pde.ThermalModel, pde.ThermalModel/thermalBC,
%           pde.ThermalModel/thermalIC, pde.ThermalModel/internalHeatSource
%           pde.ThermalModel/thermalProperties

% Copyright 2016 The MathWorks, Inc.



narginchk(1,2);
checkics = true;

performSolverPrecheck(self, checkics);
if nargin == 2 && ~self.IsTimeDependent
    warning(message('pde:pdeModel:stationaryTlistIgnored'))
elseif nargin == 1 && self.IsTimeDependent
    error(message('pde:pdeModel:timeDependentNoTlist'))
end

tlist = [];
if self.IsTimeDependent
    tlist = varargin{1};
    validateattributes(tlist,{'numeric'},{'real', 'finite', 'nonsparse', 'nonnan'});
end
u0 = [];
if self.IsTimeDependent
    u0 = self.InitialConditions.packInitialConditions();
end


mtlstruct = self.MaterialProperties.packProperties();
if ~isempty(self.HeatSources)
    heatSrcStr = self.HeatSources.packHeatSource();
else % No heat source was specified
    heatSrcStr.Coefficients.f = {0};
    heatSrcStr.IsComplex =false;
end

coefstruct.ElementsInSubdomain = mtlstruct.ElementsInSubdomain;
coefstruct.NumElementsInSubdomain = mtlstruct.NumElementsInSubdomain;
coefstruct.Coefficients.m = mtlstruct.Coefficients.m;
coefstruct.Coefficients.d = mtlstruct.Coefficients.d;
coefstruct.Coefficients.c = mtlstruct.Coefficients.c;
coefstruct.Coefficients.a = mtlstruct.Coefficients.a;
if numel(heatSrcStr.Coefficients.f) == numel(coefstruct.Coefficients.c)
    coefstruct.Coefficients.f = heatSrcStr.Coefficients.f;
else
    coefstruct.Coefficients.f = repmat(heatSrcStr.Coefficients.f,1,numel(coefstruct.Coefficients.c));
end


coefstruct.IsComplex = [mtlstruct.IsComplex;false;heatSrcStr.IsComplex];

allbcsnumeric = true;
if ~isempty(self.BoundaryConditions)
    numbcs = numel(self.BoundaryConditions.ThermalBCAssignments);
    for i = 1:numbcs
        thisbc = self.BoundaryConditions.ThermalBCAssignments(i);
        if ~thisbc.numericCoefficients()
            allbcsnumeric = false;
            break;
        end
    end
end


if self.IsTimeDependent
    u = self.solveTimeDependent(coefstruct, u0, [], tlist, ...
        false);
    R = pde.TransientThermalResults(self,u,tlist);
else
    if (self.MaterialProperties.allMaterialPropertiesNumeric() && allbcsnumeric)
        if ~isempty(self.InitialConditions)
            warning(message('pde:ThermalModel:noICforSteadyStateLinearModel'));
        end
        u = self.solveStationary(coefstruct);
        R = pde.SteadyStateThermalResults(self,u);
    else
        [p,e,t] = self.Mesh.meshToPet();
        u0 = pdeuxpd(p,0,self.PDESystemSize);
        if ~isempty(self.InitialConditions)
            u0 = self.InitialConditions.packInitialConditions();
        end
        femodel = pde.DiscretizedPDEModel(self,p,e,t,coefstruct,u0);
        if (~femodel.IsStationaryNonlinear)
            if ~isempty(self.InitialConditions)
                warning(message('pde:ThermalModel:noICforSteadyStateLinearModel'));
            end
            u = self.solveStationary(coefstruct,femodel);
            R = pde.SteadyStateThermalResults(self,u);
        else
            u = self.solveStationaryNonlinear(coefstruct, u0);
            R = pde.SteadyStateThermalResults(self,u);
        end
    end
    
end


end

