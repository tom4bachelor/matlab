function bc = thermalBC(self,varargin)
%thermalBC - Apply boundary conditions to a thermal model
%   bc = thermalBC(thermalmodel,'REGIONTYPE',REGIONID,'Name',Value) applies
%   a thermal boundary condition (BC) to a thermal model. BC is applied on
%   a region of the domain defined by REGIONTYPE and REGIONID. Here,
%   REGIONTYPE is 'Face' for 3-D model, and 'Edge' for 2-D model. REGIONID
%   is an integer specifying the ID of the geometric region.
%
%   The default boundary condition is a zero heat flux (insulated) through
%   the boundary.
%
%   The name-value pairs for various types of thermal BCs are:
%
% -------------------------------------------------------------------------
% Name			      	    Description
% -------------------------------------------------------------------------
%
%'Temperature'	            Applies temperature boundary condition using a
%                           scalar value of temperature on the boundary.
%                           Spatially and/or temporally varying temperature
%                           can be specified using a function handle.
%
%
%'HeatFlux'                 Applies general heat flux boundary condition
%                           using a scalar value of heat flux on the
%                           boundary. Spatially and/or temporally varying
%                           heat flux and nonlinear heat flux can be
%                           specified using function handle. For insulated
%                           boundaries, HeatFlux is 0.
%
%'ConvectionCoefficient'    Applies convection to ambient boundary condition
%                           using a scalar value of convection heat
%                           transfer coefficient on the boundary. For
%                           problems with convective boundary, specify
%                           ambient temperature by using
%                           'AmbientTemperature'. Spatially and/or
%                           temporally varying convection coefficient and
%                           nonlinear convection coefficient can be
%                           specified using function handle.
%
% 'Emissivity'              Applies radiation boundary condition using a
%                           scalar value of emissivity in the range (0,1)
%                           representing radiation emissivity coefficient.
%                           For problems with radiation boundary, specify
%                           ambient temperature by using
%                           'AmbientTemperature' and the Stefan-Boltzmann
%                           constant by using the corresponding thermal
%                           model property. Spatially and/or temporally
%                           varying emissivity and nonlinear emissivity can
%                           be specified using function handle.
%
% 'AmbientTemperature'	    Specify ambient temperature for convection and
%                           radiation boundary conditions. Only scalar
%                           value of ambient temperature is supported.
%
%--------------------------------------------------------------------------
%
%   Example 1: Apply temperature BC on two edges of a square.
%
%       thermalmodel = createpde('thermal','steadystate');
%       gm = @squareg;
%       geometryFromEdges(thermalmodel,gm);
%       bc = thermalBC(thermalmodel,'Edge',[1,3],'Temperature',100);
%
%   Example 2: Apply heat flux BC on the face of a block.
%
%       thermalmodel = createpde('thermal','transient');
%       gm = importGeometry(thermalmodel,'Block.STL');
%       bc = thermalBC(thermalmodel,'Face',[1,3],'HeatFlux',20);
%
%   Example 3: Apply convection BC on four faces of a block.
%
%       thermalmodel = createpde('thermal','transient');
%       gm = importGeometry(thermalmodel,'Block.STL');
%       bc = thermalBC(thermalmodel,'Edge',[2,4,5,6], ...
%                                   'ConvectionCoefficient',100, ...
%                                   'AmbientTemperature',27);
%
%   Example 4: Apply radiation BC on four faces of a block.
%
%       thermalmodel = createpde('thermal','transient');
%       gm = importGeometry(thermalmodel,'Block.STL');
%       thermalmodel.StefanBoltzmannConstant = 5.670373E-8;
%       bc = thermalBC(thermalmodel,'Edge',[2,4,5,6],'Emissivity',0.1,...
%                                   'AmbientTemperature',300);
%
%    See also pde.ThermalModel, pde.ThermalModel/thermalIC
%             pde.ThermalModel/internalHeatSource,
%             pde.ThermalModel/thermalProperties, pde.ThermalModel/solve

%    Copyright 2016 The MathWorks, Inc.


if isempty(self.Geometry)
    error(message('pde:ThermalModel:thermalModelHasNoGeom'));
end

argsToPass = varargin;

if ~isempty(self.Geometry) && numel(varargin) > 1
    parser = inputParser;
    parser.KeepUnmatched=true;
    parser.addParameter('Face', []);
    parser.addParameter('Edge', []);
    parser.parse(argsToPass{:});
    
    if isempty(parser.Results.Face) && isempty(parser.Results.Edge)
        error(message('pde:ThermalModel:noRegionSpecified'));
    end
    
    if ~isempty(parser.Results.Face) && ~isempty(parser.Results.Edge)
        error(message('pde:ThermalModel:noRegionSpecified'));
    end
    
    if self.IsTwoD
        if ~isempty(parser.Results.Face)
            error(message('pde:ThermalModel:noRegionSpecified'))
        end
    else
        if ~isempty(parser.Results.Edge)
            error(message('pde:ThermalModel:noRegionSpecified'))
        end
    end
       
    
    if ~isempty(parser.Results.Face)
        validateattributes(parser.Results.Face,{'numeric'},...
            {'integer','positive', 'nonzero','real', 'nonsparse'});
        if any(parser.Results.Face > self.Geometry.NumFaces)
            error(message('pde:pdeModel:invalidFaceIndex'));
        end
    elseif ~isempty(parser.Results.Edge)
        validateattributes(parser.Results.Edge,{'numeric'},...
            {'integer','positive','nonzero','real','nonsparse'});
        if any(parser.Results.Edge > self.Geometry.NumEdges)
            error(message('pde:pdeModel:invalidEdgeIndex'));
        end
    end
end


if isempty(self.BoundaryConditions)
    BCcont = pde.ThermalBCRecords(self);
    bc = pde.ThermalBC(BCcont,argsToPass{:});
    BCcont.ThermalBCAssignments = bc;
    self.BoundaryConditions = BCcont;
else
    bc = pde.ThermalBC(self.BoundaryConditions,argsToPass{:});
    self.BoundaryConditions.ThermalBCAssignments(end+1) = bc;
end
end
