classdef (Sealed) ThermalICRecords  < handle & matlab.mixin.internal.CompactDisplay 
%ThermalICRecords  Records the assignment of Initial Temperature
%   The ThermalICRecords holds a record of all the assignments of thermal
%   initial condition (IC), temperature, across the geometric domain. This may be a 
%   single assignment that represents a single IC throughout the domain or 
%   multiple assignments where the ICs differ across geometric regions. 
%   This object cannot be created directly, it can only be created by the 
%   ThermalModel class.
%  
%   ThermalICRecords methods:
%      findThermalIC - Find IC assignment for a geometric region
%  
%   ThermalICRecords properties:
%      ThermalICAssignments � Vector of IC assignments
%  
%   See also pde.ThermalModel, pde.ThermalModel/thermalIC

% Copyright 2016 The MathWorks, Inc.

properties
        
% ThermalICAssignments � Vector of thermal initial condition assignments
%    A vector containing the IC assignments to the geometry or geometric
%    boundaries. Each entry in the vector is a PDEInitialConditions object. 
%    This object defines the ICs that should be set on a designated region
%    or region boundary. ICs and host regions are set using the 
%    thermalIC method of the ThermalModel.
    ThermalICAssignments;
  
end

methods  
   ca = findThermalIC(self, varargin)   
   tf = thermalICSpecified(self, varargin)  
end

methods(Hidden=true, Access={?pde.EquationModel})  
    function obj=ThermalICRecords(tpdem) 
       obj.ThermalICAssignments = [];
       obj.ParentPdemodel = tpdem;
    end         
    
    % Check that the domain or each subdomain has initial conditions set if
    % they are required. Also perform a basic sanity check of the initial 
    % conditions in the event that they were edited since assigned.
  
    function performSolverPrechecks(self)
   
        if (self.ParentPdemodel.IsTimeDependent)
            if ~self.icValueDefined()
                error(message('pde:thermalInitialConditions:transientNeedsIcValue')); 
            end
        end
        
        icassigntype = classifyICAssignment(self);
        if(strcmp(icassigntype, 'GEOMETRIC'))
            performGeometricPrechecks(self);
        elseif(strcmp(icassigntype, 'NODAL'))
            performNodalPrechecks(self);
        else
            performHybridPrechecks(self);
        end
       
               
    end
    
    % Check Geometric IC assignments - no nodal assignments
    % This check ensures all faces(2-D) and cells(3-D) have an IC.
    % It then checks all ICs to ensure entities on edges/vertices
    % are valid.
    function performGeometricPrechecks(self)
        systemsize = self.ParentPdemodel.PDESystemSize;
        nv = self.ParentPdemodel.Geometry.NumVertices;
        ne = self.ParentPdemodel.Geometry.NumEdges;    
        nf = self.ParentPdemodel.Geometry.NumFaces;
        nc = self.ParentPdemodel.Geometry.NumCells;    
        if self.ParentPdemodel.IsTwoD 
            for i = 1:nf
                thisic = self.findThermalIC('face',i);                
                thisic.performSolverPrecheck(systemsize, nv, ne, nf, nc);
            end             
        else      
            for i = 1:nc
                thisic = self.findThermalIC('cell',i);                
                thisic.performSolverPrecheck(systemsize, nv, ne, nf, nc);
            end                               
        end  
        numic = numel(self.ThermalICAssignments);
        for idx = 1:numic
            thisic = self.ThermalICAssignments(idx);
            if strcmp(thisic.RegionType, 'edge') || ...
                    strcmp(thisic.RegionType, 'vertex')
              thisic.performSolverPrecheck(systemsize, nv, ne, nf, nc);
            end 
        end 
    end
   
    % Check Nodal IC assignments - no geometric assignments
    function performNodalPrechecks(self)
        % The last assignment must be Nodal        
        systemsize = self.ParentPdemodel.PDESystemSize;
        numnodes = size(self.ParentPdemodel.Mesh.Nodes,2);
        thisic = self.ThermalICAssignments(end);
        thisic.performSolverPrecheck(systemsize, numnodes);    
    end
    
    % performHybridPrechecks, find the most recent nodal assignment
    % and sanity check that assignment, then check all subsequent
    % geometric assignments
    function performHybridPrechecks(self)        
        numic = numel(self.ThermalICAssignments);                
        idx = findIndexOfNodalIC(self);
        systemsize = self.ParentPdemodel.PDESystemSize;
        numnodes = size(self.ParentPdemodel.Mesh.Nodes,2);
        thisic = self.ThermalICAssignments(idx);
        thisic.performSolverPrecheck(systemsize, numnodes);    
        nv = self.ParentPdemodel.Geometry.NumVertices;
        ne = self.ParentPdemodel.Geometry.NumEdges;    
        nf = self.ParentPdemodel.Geometry.NumFaces;
        nc = self.ParentPdemodel.Geometry.NumCells; 
        idx = idx+1;
        for i = idx:numic
            thisic = self.ThermalICAssignments(i);
            thisic.performSolverPrecheck(systemsize, nv, ne, nf, nc);
        end 
    end
    
    % Returns a string 'GEOMETRIC', 'NODAL' or 'MIXED'
    % to clasify the nature of the IC assignment
    function icassigntype = classifyICAssignment(self)
        thisic = self.ThermalICAssignments(end);
        if isa(thisic, 'pde.NodalThermalICs')
            icassigntype = 'NODAL';
            return;
        end
        numic = numel(self.ThermalICAssignments);
        for i = 1:numic
            thisic = self.ThermalICAssignments(i);
            if isa(thisic, 'pde.NodalThermalICs')
                icassigntype = 'MIXED';
                return;
            end 
        end           
        icassigntype = 'GEOMETRIC';
    end
    
    function u0 = packInitialConditions(self)
       icassigntype = classifyICAssignment(self);
        if(strcmp(icassigntype, 'GEOMETRIC'))
            u0 = packGeometricInitialConditions(self);
        elseif(strcmp(icassigntype, 'NODAL'))
            u0 = packNodalInitialConditions(self);
        else
            u0 = packMixedInitialConditions(self);
        end
    end
end
  
methods(Hidden=true, Access = private) 
% Declaration
tf = activeGeometricIC(self, varargin);

function entcnt = CountEntities(self, entitytype)
    switch entitytype
        case 'cell'
            entcnt = self.ParentPdemodel.Geometry.NumCells; 
        case 'face'
            entcnt = self.ParentPdemodel.Geometry.NumFaces; 
        case 'edge'
            entcnt = self.ParentPdemodel.Geometry.NumEdges; 
        case 'vertex'
            entcnt = self.ParentPdemodel.Geometry.NumVertices;                            
    end        
end

% Finds the index of the last nodal-IC.
function idx = findIndexOfNodalIC(self)
    numic = numel(self.ThermalICAssignments);                
    for idx = numic:-1:1
        thisic = self.ThermalICAssignments(idx);
        if isa(thisic, 'pde.NodalThermalICs')
          return;
        end 
    end     
end


function u0 = packGeometricInitialConditions(self)
    nodes = self.ParentPdemodel.Mesh.Nodes;
    syssize = self.ParentPdemodel.PDESystemSize;
    numnodes = size(nodes,2);
    u0 = ones(numnodes,syssize);
    % Fill the array of IC values         
    u0 = packEntityInitialConditions(self, 'cell', u0);
    u0 = packEntityInitialConditions(self, 'face', u0);
    u0 = packEntityInitialConditions(self, 'edge', u0);
    u0 = packEntityInitialConditions(self, 'vertex', u0);                           
    u0 = u0(:);
end

function u0 = packMixedInitialConditions(self)
    u0 = packNodalInitialConditions(self);
    u0 = packEntityInitialConditions(self, 'cell', u0);
    u0 = packEntityInitialConditions(self, 'face', u0);
    u0 = packEntityInitialConditions(self, 'edge', u0);
    u0 = packEntityInitialConditions(self, 'vertex', u0);                           
    u0 = u0(:);
end

function u0 = packNodalInitialConditions(self)
   idx = findIndexOfNodalIC(self);
   thisic = self.ThermalICAssignments(idx);
   u0 = thisic.InitialTemperature;
   u0 = u0(:);
end


function u0 = packEntityInitialConditions(self, entitytype, u0) 
   entitycount = CountEntities(self, entitytype);
   if entitycount == 0
       return
   end
   is3D = ~self.ParentPdemodel.IsTwoD;
   msh = self.ParentPdemodel.Mesh;
   ma = msh.MeshAssociation;  
   hasic = self.activeGeometricIC(entitytype,1:entitycount);
   icentities = find(hasic);
   numicentities = numel(icentities);
   for i = 1:numicentities
       thisentid = icentities(i);       
       thisic = self.findThermalIC(entitytype,thisentid);  
       fnid = ma.getNodes(entitytype,thisentid);      
       if thisic.numericIcValue()
           if isscalar(thisic.InitialTemperature)
               u0(fnid,:) = thisic.InitialTemperature;
           else
               u0(fnid,:) = bsxfun(@times,ones(size(u0(fnid,:))),(thisic.InitialTemperature)');               
           end       
       else
           fhdl = thisic.InitialTemperature;
           locations.x = msh.Nodes(1,fnid);
           locations.y = msh.Nodes(2,fnid);
           if is3D
            locations.z = msh.Nodes(3,fnid);
           end
           u0(fnid,:) = (fhdl(locations))';
       end
   end
end    

end


methods(Hidden=true)     
    function tf = icValueDefined(self)
       tf = false;
       numassigns = numel(self.ThermalICAssignments);
       if numassigns
         ic = self.ThermalICAssignments(end);
         tf = ic.hasICValue();
       end   
    end
end

    
methods(Hidden=true, Access={?pde.GeometricThermalICs, ?pde.NodalThermalICs, ?pde.PDEInitialConditions})
    function delistThermalICAssignments(self, ictodelete)
        numic = numel(self.ThermalICAssignments);
        for i = 1:numic
            thisic = self.ThermalICAssignments(i);
            if thisic == ictodelete
                self.ThermalICAssignments(i) = [];
                break
            end
        end   
        numic = numel(self.ThermalICAssignments);
        if numic == 0 && isvalid(self.ParentPdemodel)
           self.ParentPdemodel.delistInitialConditions(); 
        end
    end 
end


methods(Static, Hidden=true)
    function delete(self,~)
        
        numic = numel(self.ThermalICAssignments);
        for i = 1:numic
            if isvalid(self.ThermalICAssignments(i))
                delete(self.ThermalICAssignments(i));
            end
        end
        
        if isvalid(self.ParentPdemodel)
            self.ParentPdemodel.delistInitialConditions();
        end
    end
end

properties (Hidden = true, SetAccess='private')
    ParentPdemodel;
end  
  
end
