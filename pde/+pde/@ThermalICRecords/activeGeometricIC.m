function tf = activeGeometricIC(self, regiontype, regionid)  
%activeGeometricIC true if geometric ICs are active for given region. 
%   TF = activeGeometricIC(CR,REGIONTYPE, REGIONID) returns true if 
%   Initial conditions are specified for regions defined by REGIONTYPE, 
%   REGIONID. The IC is active if it is not superseded by a nodal IC.
%  
%   See also pde.PDEModel/setInitialConditions
% 

% Copyright 2016 The MathWorks, Inc.


narginchk(3,3)

parser = inputParser; 
parser.addParameter('cell', [], @pde.EquationModel.isValidEntityID); 
parser.addParameter('face', [], @pde.EquationModel.isValidEntityID);  
parser.addParameter('edge', [], @pde.EquationModel.isValidEntityID);
parser.addParameter('vertex', [], @pde.EquationModel.isValidEntityID);
parser.parse(regiontype, regionid);
if isempty(self.ParentPdemodel.Geometry)       
   tf = false;
   return;
end
   

if ~ismember('vertex', parser.UsingDefaults)
   qregiontype = 'vertex';
   qregionid = parser.Results.vertex;
   if any(qregionid > self.ParentPdemodel.Geometry.NumVertices)
       error(message('pde:pdeInitialConditions:invalidVertexID'));
   end      
elseif ~ismember('edge', parser.UsingDefaults)
   qregiontype = 'edge';
   qregionid = parser.Results.edge;
   if any(qregionid > self.ParentPdemodel.Geometry.NumEdges)
       error(message('pde:pdeInitialConditions:invalidEdgeID'));
   end        
elseif ~ismember('face', parser.UsingDefaults)
   qregiontype = 'face';
   qregionid = parser.Results.face;
   if any(qregionid > self.ParentPdemodel.Geometry.NumFaces)
       error(message('pde:pdeInitialConditions:invalidFaceID'));
   end
elseif ~ismember('cell', parser.UsingDefaults)
    qregiontype = 'cell';
    qregionid = parser.Results.cell;   
    if any(qregionid > self.ParentPdemodel.Geometry.NumCells)
       error(message('pde:pdeInitialConditions:invalidCellID'));
    end    
end

tf = false(size(qregionid)); 
icassigntype = classifyICAssignment(self);
if strcmp(icassigntype, 'NODAL')
    return;
end

endindex = 1;
if strcmp(icassigntype, 'MIXED')
   endindex = findIndexOfNodalIC(self);
end

numassigns = numel(self.ThermalICAssignments);
numqueries = numel(qregionid);
for i = 1:numqueries
    rid = qregionid(i);
    for j = numassigns:-1:endindex
       thisic =  self.ThermalICAssignments(j);
       if isa(thisic, 'pde.GeometricThermalICs') && strcmp(qregiontype,thisic.RegionType) && ismember(rid, thisic.RegionID) 
          tf(i) = true;
          break               
       end
    end    
end


end