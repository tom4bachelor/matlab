function tf = materialPropertiesSpecified(self, varargin)
% materialPropertiesSpecified True if material properties are specified for domain or subdomain

% Copyright 2016 The MathWorks, Inc.

narginchk(1,3);  
tf = false;
if isempty(self.ParentModel.Geometry)
    return;
end

parser = inputParser;
parser.addParameter('face', [], @pde.EquationModel.isValidEntityID);
parser.addParameter('cell', [], @pde.EquationModel.isValidEntityID);
parser.parse(varargin{:});


if ~ismember('face', parser.UsingDefaults)
    if ~self.ParentModel.IsTwoD
        error(message('pde:materialPropertySpecification:noBoundaryMaterialProps'));
    end
    qregionid = parser.Results.face;
    maxid = self.ParentModel.Geometry.NumFaces;
else
    qregionid = parser.Results.cell;
    maxid = self.ParentModel.Geometry.NumCells;
end

if any(qregionid > maxid)
    error(message('pde:materialPropertySpecification:invalidRegionID'));
end

tf = false(size(qregionid));

globalquery=false;

% Global query
if ~isempty(parser.UsingDefaults) && ...
        ( strcmp(parser.UsingDefaults{1},'face') ||  ...
                 strcmp(parser.UsingDefaults{1},'cell') )
    regionid = 1:self.nRegion;
    tf = false(size(regionid));
    globalquery=true;
end

numqueries = numel(regionid);
numassigns = numel(self.MaterialAssignments);
for i = 1:numqueries
    rid = regionid(i);
    for j = 1:numassigns
        thisca =  self.MaterialAssignments(j);
        if ismember(rid, thisca.RegionID)
            tf(i) = true;
            break
        end
    end
end

if globalquery
    tf = all(tf == true);
end

end
