function tf = globalMaterialProperties(self,varargin)
% globalMaterialProperties  True if consistent across the geometric domain

% Copyright 2016 The MathWorks, Inc.

narginchk(1,2); 
tf = true;

if ~self.materialPropertiesSpecified()
    tf = false;
    return;
end

if self.nRegion==1
    % 3-D geometry with one cell or 2-D geometry with one face
    return;
end


if nargin == 2
    coefname =  varargin{1};
    iscoef = false;
    if ischar(coefname)
        iscoef = (strcmp(coefname, 'SpecificHeat') ||  strcmp(coefname, 'MassDensity') || ...
            strcmp(coefname, 'ThermalConductivity') );
    end
    if ~iscoef
        error(message('pde:materialPropertySpecification:invalidMtlPropName'));
    end
end

thiscoef = self.findThermalProperties(self.qregiontype, 1);
for i = 2:self.nRegion
    othercoef = self.findThermalProperties(self.qregiontype, i);
    if ~thiscoef.sameCoefficients(othercoef, varargin{:})
        tf = false;
        return;
    end
end

end