function mp = findThermalProperties(self,varargin)
% findThermalProperties - Find thermal properties assignment for a geometric region
% 

% Copyright 2016 The MathWorks, Inc.
narginchk(3,3);
if isempty(self.ParentModel.Geometry)
    return;
end

parser = inputParser;
parser.addParameter('cell', [], @pde.EquationModel.isValidEntityID);
parser.addParameter('face', [], @pde.EquationModel.isValidEntityID);
parser.parse(varargin{:});

if ~ismember('face', parser.UsingDefaults)
    if ~self.ParentModel.IsTwoD
        error(message('pde:materialPropertySpecification:invalidQueryRegionTypeFace'));
    end
    qregiontype = 'face';
    qregionid = parser.Results.face;
    maxid = self.ParentModel.Geometry.NumFaces;
else
    if self.ParentModel.IsTwoD
        error(message('pde:materialPropertySpecification:invalidQueryRegionTypeCell'));
    end
    qregiontype = 'cell';
    qregionid = parser.Results.cell;
    maxid = self.ParentModel.Geometry.NumCells;
end

if any(qregionid > maxid)
    error(message('pde:materialPropertySpecification:invalidRegionID'));
end

numassigns = numel(self.MaterialAssignments);
numqueries = numel(qregionid);
mp = repmat(self.MaterialAssignments(1),size(qregionid));
for i = 1:numqueries
    rid = qregionid(i);
    mpfound = false;
    for j = numassigns:-1:1
        thisca =  self.MaterialAssignments(j);
        if strcmp(qregiontype,thisca.RegionType) && ismember(rid, thisca.RegionID)
            mp(i) = thisca;
            mpfound = true;
            break
        end
    end
    if ~mpfound
        error(message('pde:materialPropertySpecification:entitiesWithoutMtlProps'));
    end
end

end

