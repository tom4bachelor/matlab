classdef EigenResults < pde.PDEResults
    % pde.EigenResults PDE solution representation
    %   A EigenResults object provides a convenient representation of
    %   results data for an eigenvalue analysis. Create a EigenResults object
    %   using the createPDEResults function. 
    %  
    % EigenResults methods:
    %   interpolateSolution  - Interpolate solution at specified spatial
    %                          locations
    %
    % EigenResults properties:
    %   Eigenvectors         - Solution to the eigenvalue problem
    %   Eigenvalues          - Eigenvalues corresponding to the Eigenvectors  
    %   Mesh                 - Discretization of the domain
    %
    %    See also createPDEResults
    
    % Copyright 2015 The MathWorks, Inc.
    
    
    properties(SetAccess = protected, Dependent = true)
        % Eigenvectors - Solution to the eigenvalue problem
        % Solution array which can be conveniently indexed into to extract
        % a sub-array of interest. The shape of Eigenvectors is as follows:
        %      
        %   matrix        - for a scalar eigenvalue problem
        %   3-D array     - for a system of eigenvalue problems
        %
        % The first array dimension of Eigenvectors represents node index.
        % The second array dimension represents the eigenvector index for 
        % a single PDE, or the equation index for a system of PDEs. 
        % The third array dimension represents the eigenvector index for an 
        % eigenvalue problem involving a system of PDEs.
        Eigenvectors;   
    end
    
    properties(SetAccess = protected)
        % Eigenvalues - Times at which the Eigenvectors was computed
        Eigenvalues;
                       
    end
    
    properties(SetAccess = protected, Hidden = true)        
        NodalSolution;             
        XGradients;      
        YGradients;       
        ZGradients;
    end
    
    methods      
        function obj = EigenResults(varargin)
            obj@pde.PDEResults(varargin{:});
            if nargin == 0
                %Will result in a default empty object.
                return
            end
            
            narginchk(3,3);
            pdem = varargin{1};
            u = varargin{2};
            evalinterval = varargin{3};
            
            % Case when there were eigen-solver did not find a solution.
            if (isempty(u) && isempty(evalinterval))
               return
            end
            
            if ( ~isnumeric(evalinterval) || issparse(evalinterval) || ...
                    ~isvector(evalinterval) || any(isnan(evalinterval)) )
                error(message('pde:PDEResults:invalidEigenvalueVector'))
            end
            
            %Eigenvalue solution can have only one eigenvector in some case,
            %topologically this solution similar to the solution of a
            %elliptic problem, for which case NumTimeEig is treated as empty
            %in base PDEResults class.
            if (isempty(obj.NumTimeEig) && (numel(evalinterval) ~= 1))
                error(message('pde:PDEResults:eigvecEigvalResultMismatch'))
            end
            
            if numel(evalinterval) ~= obj.NumTimeEig
                error(message('pde:PDEResults:eigvecEigvalResultMismatch'))
            end  
            
            obj.Eigenvalues = evalinterval;
            ureshaped = pde.PDEResults.reshapePDESolution(u, ...
                obj.IsTimeEig, pdem.PDESystemSize, obj.NumTimeEig);

            obj.NodalSolution = ureshaped;
            
            obj.Interpolant   = pde.PDEResults.constructInterpolat(pdem.Mesh,...
                ureshaped, pdem.PDESystemSize, obj.NumTimeEig);                       
        end

          % Method declaration
          uintrp = interpolateSolution(obj,varargin)
          
          function ev = get.Eigenvectors(obj)
            ev = obj.NodalSolution;
          end                        
    end           
               
    
end
