function uintrp = interpolateSolution(obj,varargin)
%interpolateSolution - Interpolate solution at specified spatial locations
%
% uintrp = interpolateSolution(R, QP, EQi, EVj) interpolates the
% eigenvectors in a results object R, for a system of equations, at the
% query point locations QP. The points QP are represented as a matrix of
% size Ndim-by-Numq, where Ndim is the number of spatial dimensions and
% Numq is the number of query points. EVj is a scalar or vector of indices
% in the range 1 to neigenvalue, where neigenvalue is the number of
% eigenvalues returned by the eigen solver. EQi is a scalar or vector of
% equation indices in the range 1 to PDESystemSize, where PDESystemSize is
% the total number of equation in the system of PDEs. uintrp is the
% interpolated eigenvectors at the query points QP.
%
% uintrp = interpolateSolution(R, QP, EVi) interpolates the eigenvectors in
% a results object R, for a scalar PDE, at the query point locations QP.
% The points QP are represented as a matrix of size Ndim-by-Numq, where
% Ndim is the number of spatial dimensions and Numq is the number of query
% points. EVi is a scalar or vector of indices in the range 1 to
% neigenvalue, where neigenvalue is the number of eigenvalues returned by
% the eigen solver. uintrp is the interpolated eigenvectors at the query
% points QP.
%
% uintrp = interpolateSolution(R, xq, yq, __ ) 
% and interpolateSolution(R, xq, yq, zq, __ ) allow the query points QP to
% be specified in coordinate vector format.
%
%     Example: Solve a structural eigenvalue problem on a square
%     plate and visualize a mode shape on the mid-plane.
%   
%       %Create a PDEModel, import geometry, and specify coefficients.
%       N = 3; % system of three PDEs
%       pdem = createpde(N);
%       importGeometry(pdem,'Plate10x10x1.stl');
%       E = 200e9; % Modulus of elasticity in Pascal
%       nu = .3; % Poisson's ratio
%       m = 8000; % Material density in kg/m^3
%       c = elasticityC3D(E,nu);
%       a = 0;
%       specifyCoefficients(pdem,'m',m,'d',0,'c',c,'a',a,'f',0);   
%
%       %Apply boundary conditions and generate mesh.
%       applyBoundaryCondition(pdem,'mixed','Face',1:4,'u',0,'EquationIndex',3);
%       generateMesh(pdem,'Hmax',1.2,'GeometricOrder','quadratic');
%       
%       %Solve for eigenvalues and eigenvectors of the system of PDEs.
%       R = solvepdeeig(pdem,[-.1 2e+06]);
%       
%       %Define a grid of points representing mid-plane of the plate.
%       xp = 0:0.2:10; yp = 0:0.2:10;
%       [XX, YY] = meshgrid(xp,yp);
%       ZZ = 0.5*ones(size(XX));
%   
%       %Interpolate the 12th mode shape along z-direction (3) on the
%       %mid-plane and visualize using surf.
%       uintrp = interpolateSolution(R,XX,YY,ZZ,3,12);
%       ures = reshape(uintrp,size(XX));
%       surf(XX,YY,ures)
%       shading flat
%       xlabel('x'); ylabel('y'); zlabel('z-displacement');
%       title('Mode 12')
%
%    See also pde.EigenResults, solvepdeeig, meshgrid, surf
 
% Copyright 2015-2016 The MathWorks, Inc.

uintrp = pde.PDEResults.interpolateSolutionInternal(obj,varargin{:});

end




