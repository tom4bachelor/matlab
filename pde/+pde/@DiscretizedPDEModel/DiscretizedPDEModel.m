classdef DiscretizedPDEModel
%DiscretizedPDEModel - Finite Element form of PDE
%This undocumented function may be removed in a future release.
%
%       Copyright 2015-2016 The MathWorks, Inc.

    properties
        % input parameters
        p, e, t, coefstruct;
        % Current values of global matrices
        K, A, F, Q, G, H, R;
        B, ud, Nu, Or;
        totalNumEqns, numConstrainedEqns;
        thePde, emptyPDEModel;
        nrp;        
        % Flags indicating whether coefficient is dependent on u
        qu, gu, hu, ru, cu, au, fu;
        IsStationaryNonlinear;
    end
    
    
    methods(Access=public)
        function obj=DiscretizedPDEModel(thePde,p,e,t,coefstruct,u0)
            obj.qu=false; obj.gu=false; obj.hu=false; obj.ru=false; obj.cu=false;
            obj.au=false; obj.fu=false;
            obj.p = p;
            obj.e = e;
            obj.t = t;
            obj.thePde = thePde;
            obj.emptyPDEModel = pde.PDEModel(thePde.PDESystemSize);
            obj.nrp = size(p,1);
            obj.coefstruct = coefstruct;
            tdummy = 0;
            obj = obj.checkSpatialCoefsForUDependence(u0,tdummy);
            obj.IsStationaryNonlinear = (obj.qu || obj.gu || obj.hu || obj.ru ...
                        || obj.cu || obj.au || obj.fu);
        end
    end
    
    methods(Access=protected)
        
        function self=checkSpatialCoefsForUDependence(self,u0,tdummy)
            
            %checkFuncDepen determine whether coefficients are functions on u
            [K0,A0,F0,Q0,G0,H0,R0] = self.getStationaryFEMatrices(u0,tdummy);
            u1 = NaN*ones(size(u0,1), 1);
            [K1,A1,F1,Q1,G1,H1,R1] = self.getStationaryFEMatrices(u1,tdummy);
            % check if coefficients are functions of u
            [self.cu,self.au,self.fu, ...
                self.qu,self.gu,self.hu,self.ru] = self.checkNaNFEMatrix(K1,A1,F1,Q1,G1,H1,R1);
            
            self.K = K0;
            self.A = A0;
            self.F = F0;
            self.Q = Q0;
            self.G = G0;
            self.H = H0;
            self.R = R0;
            
            [self.Nu,self.Or]=pdenullorth(H0);
            if size(self.Or,2)==0
                ud0=zeros(size(K0,2),1);
            else
                ud0=full(self.Or*((H0*self.Or)\R0));
            end
            
            [self.totalNumEqns, self.numConstrainedEqns]=size(self.Nu);
            
            self.B=self.Nu;
            self.ud=ud0;
        end
        
    end
    
    
    methods(Access=protected)
        
        function [K,A,F,Q,G,H,R] = getStationaryFEMatrices(self, u,time)
            if(self.nrp==2)
                [K, F] = formGlobalKF2D(self.emptyPDEModel, self.p, self.t, self.coefstruct,u,time);
                A = formGlobalM2D(self.emptyPDEModel, self.p, self.t, self.coefstruct,u,time,'a');
                
            elseif(self.nrp==3)
                [K, F] = formGlobalKF3D(self.emptyPDEModel, self.p, self.t, self.coefstruct,u,time);
                A = formGlobalM3D(self.emptyPDEModel, self.p, self.t, self.coefstruct,u,time,'a');
            end
            [Q,G,H,R]=self.thePde.assembleBoundary(u,time);
        end
        
        
        function [IsNaNK,IsNaNA,IsNaNF,IsNaNQ,IsNaNG,IsNaNH,IsNaNR] = checkNaNFEMatrix(~,K,A,F,Q,G,H,R)
            IsNaNK = false;
            IsNaNA = false;
            IsNaNF = false;
            IsNaNQ = false;
            IsNaNG = false;
            IsNaNH = false;
            IsNaNR = false;
            
            if any(isnan(K(:)))
                IsNaNK=true;
            end
            if any(isnan(A(:)))
                IsNaNA=true;
            end
            if any(isnan(F(:)))
                IsNaNF=true;
            end
            if any(isnan(Q(:)))
                IsNaNQ=true;
            end
            if any(isnan(G(:)))
                IsNaNG=true;
            end
            if any(isnan(H(:)))
                IsNaNH=true;
            end
            if any(isnan(R(:)))
                IsNaNR=true;
            end
        end
       
       
    end
    
end

