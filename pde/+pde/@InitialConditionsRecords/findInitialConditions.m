function ca = findInitialConditions(self, regiontype, regionid)  
%findInitialConditions - Find ICs assignment for a geometric region
%   IC = findInitialConditions(CC, REGIONTYPE, REGIONID) finds and returns 
%   the PDEInitialConditions object IC that defines the Initial Conditions
%   that apply to the given REGIONTYPE and REGIONID. Where REGIONTYPE is one 
%   of the following geometric entities: 'cell', 'face', 'edge' or 'vertex'
%   and REGIONID is the ID of the geometric region.
%  
%   See also pde.PDEModel/setInitialConditions


% Copyright 2015-2016 The MathWorks, Inc.


if isempty(self.ParentPdemodel.Geometry)
    return;
end

parser = inputParser;
parser.addParameter('cell', [], @pde.PDEModel.isValidEntityID); 
parser.addParameter('face', [], @pde.PDEModel.isValidEntityID);  
parser.addParameter('edge', [], @pde.PDEModel.isValidEntityID);
parser.addParameter('vertex', [], @pde.PDEModel.isValidEntityID);
parser.parse(regiontype, regionid);

if ~ismember('vertex', parser.UsingDefaults)
   qregiontype = 'vertex';
   qregionid = parser.Results.vertex;
   maxid = self.ParentPdemodel.Geometry.NumVertices;   
elseif ~ismember('edge', parser.UsingDefaults)
   qregiontype = 'edge';
   qregionid = parser.Results.edge;
   maxid = self.ParentPdemodel.Geometry.NumEdges;   
elseif ~ismember('face', parser.UsingDefaults)
   qregiontype = 'face';
   qregionid = parser.Results.face;
   maxid = self.ParentPdemodel.Geometry.NumFaces;
else       
    qregiontype = 'cell';
    qregionid = parser.Results.cell;
    if self.ParentPdemodel.IsTwoD
        maxid = 0;
    else
        maxid = self.ParentPdemodel.Geometry.NumCells;
    end       
end
if any(qregionid > maxid)
       error(message('pde:pdeInitialConditions:invalidRegionID'));
end    
    

numqueries = numel(qregionid);
numassigns = numel(self.InitialConditionAssignments);
ca = repmat(self.InitialConditionAssignments(1),size(qregionid));
for i = 1:numqueries
    rid = qregionid(i);
    icfound = false;
    for j = numassigns:-1:1
       thisic =  self.InitialConditionAssignments(j);
        if isa(thisic, 'pde.NodalInitialConditions') || (strcmp(qregiontype,thisic.RegionType) && ismember(rid, thisic.RegionID))        
          ca(i) = thisic;
          icfound = true;
          break
       end
    end
    if ~icfound
      error(message('pde:pdeInitialConditions:entitiesWithoutICs'));
    end   
end

end