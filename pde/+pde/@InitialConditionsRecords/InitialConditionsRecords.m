classdef (Sealed) InitialConditionsRecords  < handle & matlab.mixin.internal.CompactDisplay 
%InitialConditionsRecords  Records the assignment of Initial Conditions
%   The InitialConditionsRecords holds a record of all the assignments of 
%   initial conditions (ICs) across the geometric domain. This may be a 
%   single assignment that represents a single IC throughout the domain or 
%   multiple assignments where the ICs differ across geometric regions. 
%   This object cannot be created directly, it can only be created by the 
%   PDEModel class.
%  
%   InitialConditionsRecords methods:
%      findInitialConditions - Find ICs assignment for a geometric region
%  
%   InitialConditionsRecords properties:
%      InitialConditionAssignments � Vector of IC assignments
%  
%   See also pde.PDEModel, pde.PDEModel/setInitialConditions

% Copyright 2015-2016 The MathWorks, Inc.

properties
        
% InitialConditionAssignments � Vector of initial condition assignments
%    A vector containing the IC assignments to the geometry or geometric
%    boundaries. Each entry in the vector is a PDEInitialConditions object. 
%    This object defines the ICs that should be set on a designated region
%    or region boundary. ICs and host regions are set using the 
%    setInitialConditions method of the PDEModel.
    InitialConditionAssignments;
  
end

methods  
   ca = findInitialConditions(self, varargin)   
   tf = initialConditionsSpecified(self, varargin)  
end

methods(Hidden=true, Access={?pde.EquationModel})  
    function obj=InitialConditionsRecords(pdem) 
       obj.InitialConditionAssignments = [];
       obj.ParentPdemodel = pdem;
    end         
    
    
    % Check that the domain or each subdomain has initial conditions set if
    % they are required. Also perform a basic sanity check of the initial 
    % conditions in the event that they were edited since assigned.
  
    function performSolverPrechecks(self)
   
        if (self.ParentPdemodel.EquationCoefficients.mDefined())
            [icDerivativeDefined,ic] = self.icDerivativeValueDefined();
            
            if (isa(ic,'pde.NodalInitialConditions') && ~ icDerivativeDefined)
                error(message('pde:pdeInitialConditions:noTimeDerivativeInResults')); 
            end
            
            if ~icDerivativeDefined
                error(message('pde:pdeInitialConditions:mCoefNeedsIcDerivative')); 
            end
        end
        if (self.ParentPdemodel.EquationCoefficients.dDefined())
            if ~self.icValueDefined()
                error(message('pde:pdeInitialConditions:dCoefNeedsIcValue')); 
            end
        end
        if ~(self.ParentPdemodel.EquationCoefficients.mDefined()) && ...
                self.icDerivativeValueDefined()
           warning(message('pde:pdeInitialConditions:icDerivativeIgnored')); 
        end 
        
        icassigntype = classifyICAssignment(self);
        if(strcmp(icassigntype, 'GEOMETRIC'))
            performGeometricPrechecks(self);
        elseif(strcmp(icassigntype, 'NODAL'))
            performNodalPrechecks(self);
        else
            performHybridPrechecks(self);
        end
       
               
    end
    
    % Check Geometric IC assignments - no nodal assignments
    % This check ensures all faces(2-D) and cells(3-D) have an IC.
    % It then checks all ICs to ensure entities on edges/vertices
    % are valid.
    function performGeometricPrechecks(self)
        systemsize = self.ParentPdemodel.PDESystemSize;
        nv = self.ParentPdemodel.Geometry.NumVertices;
        ne = self.ParentPdemodel.Geometry.NumEdges;    
        nf = self.ParentPdemodel.Geometry.NumFaces;
        nc = self.ParentPdemodel.Geometry.NumCells;    
        if self.ParentPdemodel.IsTwoD 
            for i = 1:nf
                thisic = self.findInitialConditions('face',i);                
                thisic.performSolverPrecheck(systemsize, nv, ne, nf, nc);
            end             
        else      
            for i = 1:nc
                thisic = self.findInitialConditions('cell',i);                
                thisic.performSolverPrecheck(systemsize, nv, ne, nf, nc);
            end                               
        end  
        numic = numel(self.InitialConditionAssignments);
        for idx = 1:numic
            thisic = self.InitialConditionAssignments(idx);
            if strcmp(thisic.RegionType, 'edge') || ...
                    strcmp(thisic.RegionType, 'vertex')
              thisic.performSolverPrecheck(systemsize, nv, ne, nf, nc);
            end 
        end 
    end
   
    % Check Nodal IC assignments - no geometric assignments
    function performNodalPrechecks(self)
        % The last assignment must be Nodal        
        systemsize = self.ParentPdemodel.PDESystemSize;
        numnodes = size(self.ParentPdemodel.Mesh.Nodes,2);
        thisic = self.InitialConditionAssignments(end);
        thisic.performSolverPrecheck(systemsize, numnodes);    
    end
    
    % performHybridPrechecks, find the most recent nodal assignment
    % and sanity check that assignment, then check all subsequent
    % geometric assignments
    function performHybridPrechecks(self)        
        numic = numel(self.InitialConditionAssignments);                
        idx = findIndexOfNodalIC(self);
        systemsize = self.ParentPdemodel.PDESystemSize;
        numnodes = size(self.ParentPdemodel.Mesh.Nodes,2);
        thisic = self.InitialConditionAssignments(idx);
        thisic.performSolverPrecheck(systemsize, numnodes);    
        nv = self.ParentPdemodel.Geometry.NumVertices;
        ne = self.ParentPdemodel.Geometry.NumEdges;    
        nf = self.ParentPdemodel.Geometry.NumFaces;
        nc = self.ParentPdemodel.Geometry.NumCells; 
        idx = idx+1;
        for i = idx:numic
            thisic = self.InitialConditionAssignments(i);
            thisic.performSolverPrecheck(systemsize, nv, ne, nf, nc);
        end 
    end
    
    % Returns a string 'GEOMETRIC', 'NODAL' or 'MIXED'
    % to clasify the nature of the IC assignment
    function icassigntype = classifyICAssignment(self)
        thisic = self.InitialConditionAssignments(end);
        if isa(thisic, 'pde.NodalInitialConditions')
            icassigntype = 'NODAL';
            return;
        end
        numic = numel(self.InitialConditionAssignments);
        for i = 1:numic
            thisic = self.InitialConditionAssignments(i);
            if isa(thisic, 'pde.NodalInitialConditions')
                icassigntype = 'MIXED';
                return;
            end 
        end           
        icassigntype = 'GEOMETRIC';
    end
    
    function [u0, ut0] = packInitialConditions(self)
       icassigntype = classifyICAssignment(self);
        if(strcmp(icassigntype, 'GEOMETRIC'))
            [u0, ut0] = packGeometricInitialConditions(self);
        elseif(strcmp(icassigntype, 'NODAL'))
            [u0, ut0] = packNodalInitialConditions(self);
        else
            [u0, ut0] = packMixedInitialConditions(self);
        end
    end
end
  
methods(Hidden=true, Access = private) 
% Declaration
tf = activeGeometricIC(self, varargin);

function entcnt = CountEntities(self, entitytype)
    switch entitytype
        case 'cell'
            entcnt = self.ParentPdemodel.Geometry.NumCells; 
        case 'face'
            entcnt = self.ParentPdemodel.Geometry.NumFaces; 
        case 'edge'
            entcnt = self.ParentPdemodel.Geometry.NumEdges; 
        case 'vertex'
            entcnt = self.ParentPdemodel.Geometry.NumVertices;                            
    end        
end

% Finds the index of the last nodal-IC.
function idx = findIndexOfNodalIC(self)
    numic = numel(self.InitialConditionAssignments);                
    for idx = numic:-1:1
        thisic = self.InitialConditionAssignments(idx);
        if isa(thisic, 'pde.NodalInitialConditions')
          return;
        end 
    end     
end


function [u0, ut0] = packGeometricInitialConditions(self)
    nodes = self.ParentPdemodel.Mesh.Nodes;
    syssize = self.ParentPdemodel.PDESystemSize;
    numnodes = size(nodes,2);
    u0 = ones(numnodes,syssize);
    ut0 = [];        
    if self.icDerivativeValueDefined()
        ut0 = ones(numnodes,syssize);
    end
    % Fill the array of IC values         
    [u0, ut0] = packEntityInitialConditions(self, 'cell', u0, ut0);
    [u0, ut0] = packEntityInitialConditions(self, 'face', u0, ut0);
    [u0, ut0] = packEntityInitialConditions(self, 'edge', u0, ut0);
    [u0, ut0] = packEntityInitialConditions(self, 'vertex', u0, ut0);                           
    u0 = u0(:);
    ut0 = ut0(:);
end

function [u0, ut0] = packMixedInitialConditions(self)
    [u0, ut0] = packNodalInitialConditions(self);
    [u0, ut0] = packEntityInitialConditions(self, 'cell', u0, ut0);
    [u0, ut0] = packEntityInitialConditions(self, 'face', u0, ut0);
    [u0, ut0] = packEntityInitialConditions(self, 'edge', u0, ut0);
    [u0, ut0] = packEntityInitialConditions(self, 'vertex', u0, ut0);                           
    u0 = u0(:);
    ut0 = ut0(:);    
end

function [u0, ut0] = packNodalInitialConditions(self)
   idx = findIndexOfNodalIC(self);
   thisic = self.InitialConditionAssignments(idx);
   u0 = thisic.InitialValue;
   ut0 = thisic.InitialDerivative;
   u0 = u0(:);
   ut0 = ut0(:);    
end


function [u0, ut0] = packEntityInitialConditions(self, entitytype, u0, ut0) 
   entitycount = CountEntities(self, entitytype);
   if entitycount == 0
       return
   end
   is3D = ~self.ParentPdemodel.IsTwoD;
   msh = self.ParentPdemodel.Mesh;
   ma = msh.MeshAssociation;  
   hasic = self.activeGeometricIC(entitytype,1:entitycount);
   icentities = find(hasic);
   numicentities = numel(icentities);
   for i = 1:numicentities
       thisentid = icentities(i);       
       thisic = self.findInitialConditions(entitytype,thisentid);  
       fnid = ma.getNodes(entitytype,thisentid);      
       if thisic.numericIcValue()
           if isscalar(thisic.InitialValue)
               u0(fnid,:) = thisic.InitialValue;
           else
               u0(fnid,:) = bsxfun(@times,ones(size(u0(fnid,:))),(thisic.InitialValue)');               
           end       
       else
           fhdl = thisic.InitialValue;
           locations.x = msh.Nodes(1,fnid);
           locations.y = msh.Nodes(2,fnid);
           if is3D
            locations.z = msh.Nodes(3,fnid);
           end
           u0(fnid,:) = (fhdl(locations))';
       end
       if self.icDerivativeValueDefined()
           if thisic.numericIcDerivativeValue() 
               if isscalar(thisic.InitialDerivative)
                   ut0(fnid,:) = thisic.InitialDerivative;
               else                 
                    ut0(fnid,:) = bsxfun(@times,ones(size(ut0(fnid,:))),(thisic.InitialDerivative)');         
               end     
           else
               fhdl = thisic.InitialDerivative;
               locations.x = msh.Nodes(1,fnid);
               locations.y = msh.Nodes(2,fnid);
               if is3D
                locations.z = msh.Nodes(3,fnid);
               end
               ut0(fnid,:) = (fhdl(locations))';
           end    
       end
   end
end    

end
    
methods(Hidden=true, Access={?pde.GeometricInitialConditions, ?pde.NodalInitialConditions, ?pde.PDEInitialConditions})
    function delistPDEInitialConditions(self, ictodelete)
        numic = numel(self.InitialConditionAssignments);
        for i = 1:numic
            thisic = self.InitialConditionAssignments(i);
            if thisic == ictodelete
                self.InitialConditionAssignments(i) = [];
                break
            end
        end   
        numic = numel(self.InitialConditionAssignments);
        if numic == 0 && isvalid(self.ParentPdemodel)
           self.ParentPdemodel.delistInitialConditions(); 
        end
    end 
end

methods(Hidden=true)     
    function tf = icValueDefined(self)
       tf = false;
       numassigns = numel(self.InitialConditionAssignments);
       if numassigns
         ic = self.InitialConditionAssignments(end);
         tf = ic.hasICValue();
       end   
    end
    function [tf,ic] = icDerivativeValueDefined(self)
       tf = false;
       numassigns = numel(self.InitialConditionAssignments);
       if numassigns
         ic = self.InitialConditionAssignments(end);
         tf = ic.hasICDerivativeValue();
       end   
    end
end

% methods(Static, Access = private, Hidden=true)     
%     function ok = isValiEntityID(faceids) 
%        validateattributes(faceids,{'numeric'},{'integer','positive', 'nonzero','real', 'nonsparse'});             
%        ok=true;
%     end
% end

methods(Static, Hidden=true)
    function preDelete(self,~)
        if isvalid(self.ParentPdemodel)
            self.ParentPdemodel.delistInitialConditions();
        end        
    end    
end

properties (Hidden = true, SetAccess='private')
    ParentPdemodel;
end  
  
end
