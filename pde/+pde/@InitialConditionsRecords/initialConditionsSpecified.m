function tf = initialConditionsSpecified(self, varargin)  
%initialConditionsSpecified true if ICs are specified for domain or subdomain
%   TF = initialConditionsSpecified(CR,REGIONTYPE, REGIONID) returns true if 
%   Initial conditions are specified on a region of the doamin defined by 
%   REGIONTYPE and REGIONID. Where REGIONTYPE is one of the following: 
%   'cell', 'face', 'edge' or 'vertex' and REGIONID is the corresponding ID.
%  
%   TF = initialConditionsSpecified(CR) returns true if initial conditions
%   are specified throughout the domain.
%  
%   See also pde.PDEModel/setInitialConditions
% 

% Copyright 2015-2016 The MathWorks, Inc.


narginchk(1,3)

parser = inputParser; 
parser.addParameter('cell', [], @pde.PDEModel.isValidEntityID); 
parser.addParameter('face', [], @pde.PDEModel.isValidEntityID);  
parser.addParameter('edge', [], @pde.PDEModel.isValidEntityID);
parser.addParameter('vertex', [], @pde.PDEModel.isValidEntityID);
parser.parse(varargin{:});

if isempty(self.ParentPdemodel.Geometry)       
   tf = false;
   return;
end

globalquery=false;    

if ~ismember('vertex', parser.UsingDefaults)
   qregiontype = 'vertex';
   qregionid = parser.Results.vertex;
   if any(qregionid > self.ParentPdemodel.Geometry.NumVertices)
       error(message('pde:pdeInitialConditions:invalidVertexID'));
   end      
elseif ~ismember('edge', parser.UsingDefaults)
   qregiontype = 'edge';
   qregionid = parser.Results.edge;
   if any(qregionid > self.ParentPdemodel.Geometry.NumEdges)
       error(message('pde:pdeInitialConditions:invalidEdgeID'));
   end        
elseif ~ismember('face', parser.UsingDefaults)
   qregiontype = 'face';
   qregionid = parser.Results.face;
   if any(qregionid > self.ParentPdemodel.Geometry.NumFaces)
       error(message('pde:pdeInitialConditions:invalidFaceID'));
   end
elseif ~ismember('cell', parser.UsingDefaults)
    qregiontype = 'cell';
    qregionid = parser.Results.cell;   
    if any(qregionid > self.ParentPdemodel.Geometry.NumCells)
       error(message('pde:pdeInitialConditions:invalidCellID'));
    end    
else
    globalquery=true;    
    if (self.ParentPdemodel.IsTwoD)
        qregiontype = 'face';
        qregionid = 1:self.ParentPdemodel.Geometry.NumFaces;      
    else
        qregiontype = 'cell';
        qregionid = 1:self.ParentPdemodel.Geometry.NumCells;            
    end   
end

icassigntype = classifyICAssignment(self);
if(strcmp(icassigntype, 'NODAL') || strcmp(icassigntype, 'MIXED') )
    tf = true(size(qregionid));
    if globalquery
        tf = true;
    end
    return;
end
tf = false(size(qregionid)); 
numassigns = numel(self.InitialConditionAssignments);
numqueries = numel(qregionid);
for i = 1:numqueries
    rid = qregionid(i);
    for j = numassigns:-1:1
       thisic =  self.InitialConditionAssignments(j);
       if isa(thisic, 'pde.NodalInitialConditions')
          tf(i) = true;
          break  
       elseif strcmp(qregiontype,thisic.RegionType) && ismember(rid, thisic.RegionID) 
          tf(i) = true;
          break               
       end
    end    
end

if globalquery
   tf = all(tf == true);    
end

end