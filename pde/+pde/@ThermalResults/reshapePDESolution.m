function pdeu = reshapePDESolution(u,IsTimeEig, PDESystemSize,NumTimeEig)
%reshapePDESolution - Internal function to reshape the solution into a matrix or 3-D array.

% Copyright 2016 The MathWorks, Inc.

if    (PDESystemSize == 1)
    pdeu = u;
else
    if( ~IsTimeEig )
        pdeu = reshape(u,[], PDESystemSize);
    else
        pdeu = reshape(u,[], PDESystemSize, NumTimeEig);
    end
end

end