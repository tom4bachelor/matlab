function qcoords = validateXYZArrays(IsTwoD, xyzdata)
%validateXYZArrays - Internal function to validate  x-, y-, and z-coordinates.

% Copyright 2016 The MathWorks, Inc.

if IsTwoD
    xq = xyzdata{1};
    yq = xyzdata{2};
    if ~(isequal(size(xq), size(yq)) )
        error(message('pde:PDEResults:mismatchXYquery'));
    end
    qcoords = {xq(:), yq(:)};
else
    xq = xyzdata{1};
    yq = xyzdata{2};
    zq = xyzdata{3};
    if (~(isequal(size(xq), size(yq))) || ~(isequal(size(yq), size(zq))))
        error(message('pde:PDEResults:mismatchXYZquery'));
    end
    qcoords = {xq(:), yq(:), zq(:)};
end

for n = 1:numel(qcoords)
    if (any(any(~isfinite(qcoords{n}))))
        error(message('pde:PDEResults:nonFiniteCoordinates'))
    end
end

end