function Qn = heatFluxIntegral3D(self,boundaryID)
%heatFluxIntegral2D - Internal function to integrate heat flux across a face of a 3-D model.
%

%       Copyright 2016 The MathWorks, Inc.

Nt = self.NumTimeEig;
if isempty(Nt)
    Nt = 1;
end

domainDim = 3;
boundaryDim = domainDim-1;
ma = self.Mesh.MeshAssociation;
elemFacets = ma.getElementFaces(boundaryID);
[numFacetNodes, numFacets] = size(elemFacets);
Qn = zeros(Nt,1);

import pde.internal.elements.shapeTri3
import pde.internal.elements.dShapeTri3
import pde.internal.elements.shapeTri6
import pde.internal.elements.dShapeTri6
import pde.internal.elements.GaussianIntegrationRule

if(numFacetNodes == 3)
    sfCalc = @shapeTri3;
    dsfCalc = @dShapeTri3;
    numGaussPoints = 1;
elseif(numFacetNodes == 6)
    sfCalc = @shapeTri6;
    dsfCalc = @dShapeTri6;
    numGaussPoints = 3;
else
    error(message('pde:pde3DBCImpl:illegalFaceDefn', numFacetNodes));
end
% compute shape functions and derivatives at Gauss points
intRule=GaussianIntegrationRule(GaussianIntegrationRule.Triangle, numGaussPoints);
intWts = intRule.wts;
intPoints = intRule.points;
dsPts = zeros(numFacetNodes, boundaryDim, numGaussPoints);
for n=1:numGaussPoints
    dsPts(:,:,n) = dsfCalc(intPoints(:,n));
end
sPts = sfCalc(intPoints);
xyzAllFaceNodes = self.Mesh.Nodes(:, elemFacets(:));
xyzAllFaceNodes = reshape(xyzAllFaceNodes, domainDim, numFacetNodes, numFacets);

% Compute flux density for the solution.
[qx,qy,qz] = self.evaluateHeatFlux;

for nt = 1:Nt
    facetHeatFlux = [qx(elemFacets(:),nt).';
        qy(elemFacets(:),nt).';
        qz(elemFacets(:),nt).'
        ];
    facetHeatFlux = reshape(facetHeatFlux, domainDim, numFacetNodes, numFacets);
    
    for i=1:numFacets
        xyz = xyzAllFaceNodes(:,:,i);
        for n=1:numGaussPoints
            dxyzDrs = xyz*dsPts(:,:,n);
            v = pde.ThermalResults.cross3(self,dxyzDrs(:,1),dxyzDrs(:,2));
            facetNorm = norm(v); % This norm is also equal to area of the triangle.
            facetUnitNormal = v/facetNorm;
            detJ = facetNorm;
            shn = sPts(:,n);
            wt = intWts(n);
            detWt = detJ*wt;
            gaussPtHeatFlux = facetHeatFlux(:,:,i)*shn;
            normalFluxDensity = gaussPtHeatFlux'*facetUnitNormal;
            Qn(nt) = Qn(nt) + detWt*normalFluxDensity;
        end
    end
    
end
end
