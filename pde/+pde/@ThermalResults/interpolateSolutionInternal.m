function uintrp = interpolateSolutionInternal(obj,varargin)
%interpolateSolutionInternal - Internal function to support
%interpolateTemperature
%
%    See also pde.ThermalResults, meshgrid, surf
 
% Copyright 2016 The MathWorks, Inc.
 
%valid number of arguments covering all types of PDE solutions and input formats
narginchk(2,6);
%find valid number of arguments for p-matrix and xyz formats of inputs
[numargsp, numargsxyz] = pde.ThermalResults.validnumargs(obj);
nargs = nargin;
if ((nargs ~= numargsp) && (nargs ~= numargsxyz))
    error(message('pde:PDEResults:invalidNumArgs'));
end

%sanity check for the input arguments
for n =1:nargs-1
    if (~isnumeric(varargin{n}) || ~isreal(varargin{n}) || ...
            isempty(varargin{n}) )
        error(message('pde:PDEResults:invalidArgs'));
    end
end

dim = pde.ThermalResults.spatialDimension(obj); % dim = 2 for 2-D and dim = 3 for 3-D problems.

%validate the query point coordinates and store them in qcoords cell array.
if (nargs == numargsp) %p-matrix format of query points
    qcoords = pde.ThermalResults.validatePointsMatrix(obj.IsTwoD, varargin{1}    );
else %xyz-arrays format of query points
    qcoords = pde.ThermalResults.validateXYZArrays(    obj.IsTwoD, varargin(1:dim));
end

%validate the time-step/mode IDs and equation IDs, if any.
[qtimeID, qeqnID] = pde.ThermalResults.validateVectorInputs(obj, varargin);

uintrp = [];
if isempty(obj.Interpolant)
    return;
end

%interpolate the solution on to query points
uintrpFull = evaluate(obj.Interpolant, qcoords{:});

% reshape the interpolated solution.
uintrpFullFormatted = pde.ThermalResults.reshapePDESolution(uintrpFull, ...
    obj.IsTimeEig, obj.PDESystemSize, obj.NumTimeEig);

% select the required solution subset
uintrp = pde.ThermalResults.selectPDESolution(obj,uintrpFullFormatted, qtimeID, qeqnID);

end % End of interpolateSolutionInternal



