function dim = spatialDimension(obj)
%spatialDimension - Internal function that returns spatial dimension of the analysis model.

% Copyright 2016 The MathWorks, Inc.

if obj.IsTwoD
    dim = 2;
else
    dim = 3;
end
end