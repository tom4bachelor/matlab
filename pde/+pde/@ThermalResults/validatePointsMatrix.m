function qcoords = validatePointsMatrix(IsTwoD, pQuery)
%validatePointsMatrix - Internal function to validate the points matrix.

% Copyright 2016 The MathWorks, Inc.

if     (  IsTwoD && ~(size(pQuery,1)==2) )
    error(message('pde:PDEResults:invalidQueryPointMatrix2D'));
elseif ( ~IsTwoD && ~(size(pQuery,1)==3) )
    error(message('pde:PDEResults:invalidQueryPointMatrix3D'));
end
qcoords = {pQuery};

for n = 1:numel(qcoords)
    if (any(any(~isfinite(qcoords{n}))))
        error(message('pde:PDEResults:nonFiniteCoordinates'))
    end
end

end


