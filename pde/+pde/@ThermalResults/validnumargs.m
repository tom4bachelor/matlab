function [numargsp, numargsxyz]  = validnumargs(obj)
%validnumargs - Internal function to determine the valid number of input
%arguments based on the analysis type.

% Copyright 2016 The MathWorks, Inc.

if(obj.PDESystemSize==1)
    if (~obj.IsTimeEig)
        if (obj.IsTwoD) % 2-D time-independent scalar PDE
            numargsp =2;
            numargsxyz =3;
        else  % 3-D time-independent scalar PDE
            numargsp =2;
            numargsxyz =4;
        end
    else
        if (obj.IsTwoD) % 2-D time-dependent scalar PDE
            numargsp =3;
            numargsxyz =4;
        else % 3-D time-dependent scalar PDE
            numargsp =3;
            numargsxyz =5;
        end
    end
else % system of PDEs
    if (~obj.IsTimeEig) % time-independent system of PDE, requires equation ID vector
        if (obj.IsTwoD) % 2-D time-independent system of PDEs
            numargsp =3;
            numargsxyz =4;
        else % 3-D time-independent system of PDEs
            numargsp =3;
            numargsxyz =5;
        end
    else
        if (obj.IsTwoD)% 2-D time-dependent system of PDEs
            numargsp =4;
            numargsxyz =5;
        else % 3-D time-dependent system of PDEs
            numargsp =4;
            numargsxyz =6;
        end
    end
end
end
