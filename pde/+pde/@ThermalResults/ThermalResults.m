classdef (Abstract) ThermalResults
    % pde.ThermalResults Thermal solution and its derived quantities
    %   A ThermalResults object provides a convenient interface for
    %   postprocessing a thermal solution. It also provides functions to
    %   interpolate temperature, evaluate temperature gradients, heat flux,
    %   and heat rate.
    %
    % ThermalResults properties:   
    %   Mesh                 - Discretization of the domain
    %
    %    See also pde.ThermalModel
    
    % Copyright 2016 The MathWorks, Inc.
        
  
    
  properties(SetAccess = protected, Hidden=true, Abstract=true)
        % Temperature - Temperature at nodal locations
        % Temperature vector which can be indexed into to extract
        % a sub-array of interest. The shape of Temperature depends on
        % the type of ThermalModel. It will be a:
        %
        %   column vector - for a steady-state ThermalModel
        %   matrix        - for a transient ThermalModel
        %
        % The first array dimension of Temperature represents node index.
        % The second array dimension represents the time-step.
        Temperature;           
    end
                
    methods
        function obj = ThermalResults(varargin)        
             if nargin == 0
                return
             end
             narginchk(2,3);
             tpdem = varargin{1};
             u = varargin{2};       
             if (~isa(tpdem, 'pde.ThermalModel'))
                error(message('pde:PDEResults:invalidPDEModel'))
            end
            
            if ( ~isnumeric(u) || issparse(u) || isrow(u) || isscalar(u) ...
                    || any(isnan(u(:))) || ~any(isfinite(u(:))))
                error(message('pde:PDEResults:invalidSolution'))
            end
            
            if ( ~ismatrix(u) && ~iscolumn(u))
                error(message('pde:PDEResults:invalidSolution'))
            end

            if ~isa(tpdem.Mesh, 'pde.FEMesh')
                error(message('pde:pdeplot:UnmeshedPdeModel'))
            end
            
            [IsTimeEig, NumTimeEig] = classifyPDEModel(tpdem, u);                       
            obj.Mesh = pde.ThermalResults.cloneMesh(tpdem.Mesh);
            
            % Metadata classifying the PDEmodel.
            obj.PDESystemSize = tpdem.PDESystemSize;
            obj.IsTwoD        = tpdem.IsTwoD;
            obj.IsTimeEig     = IsTimeEig; % true if time-dependent or eigenvectors
            obj.NumTimeEig    = NumTimeEig; % number of time-steps or eigenvectors                                                                                                                         
        end
        
        
        function obj = set.Mesh(obj,msh)
            if(~isempty(msh) && ~isa(msh, 'pde.FEMesh'))
                error(message('pde:pdeModel:invalidMesh'));
            end
            obj.Mesh = msh;
        end
       
    end
    properties(SetAccess = private)       
        % Mesh - Discretization of the domain
        % Mesh object containing the node and element data that were used
        % in solving the thermal model.
        Mesh;      
    end
    
    methods (Hidden = true, Static = true, Access = protected)
        % Method declaration
        [dudxInt, dudyInt, dudzInt] = evalGradInternal(obj,varargin)
        [dudxInt, dudyInt, dudzInt] = evalCGradInternal(obj,nodalCGradX,nodalCGradY,nodalCGradZ,varargin)
        uinterp = interpolateSolutionInternal(obj,varargin)
        Qn = evaluateHeatRateInternal(self,varargin)
        Qn = heatFluxIntegral2D(obj,boundaryID)
        Qn = heatFluxIntegral3D(obj,boundaryID)
        
        % This highly specialized version of the cross function with no error
        % checking is used to improve performance.
        function normVec = cross3(~,a,b)
            normVec =[a(2)*b(3)-a(3)*b(2);
                a(3)*b(1)-a(1)*b(3);
                a(1)*b(2)-a(2)*b(1)];
        end
    end
    
    methods (Hidden = true, Static = true, Access = protected)        
        function obj = loadobj(obj)
            obj.Interpolant = pde.ThermalResults.constructInterpolat(obj.Mesh, ...
                   obj.Temperature, obj.PDESystemSize, obj.NumTimeEig);     
        end
        
        function resultsmsh = cloneMesh(msh)
            %Create an independent FEMesh object that has shared copies of the properties.
            p           = msh.Nodes;
            t           = msh.Elements;
            Hmax        = msh.MaxElementSize;
            Hmin        = msh.MinElementSize;
            geomOrder   = msh.GeometricOrder;
            assoc       = msh.MeshAssociation;
            resultsmsh  = pde.FEMesh(p, t, Hmax, Hmin, geomOrder, assoc);
        end
        
        function Interpolant = constructInterpolat(mesh, NodalValues, ...
                ~, ~)
            Interpolant = [];
            if isempty(NodalValues)
                return;
            end
            
            %Since N = 1 for thermal model, no reshaping is required.
            u = NodalValues;
            
            [p,~,t] = meshToPet(mesh);
            Interpolant = pdeInterpolant(p,t,u);
        end
        
        %Private methods declaration
        pdeu = reshapePDESolution(u,IsTimeEig, PDESystemSize,NumTimeEig)
        [numargsp, numargsxyz]  = validnumargs(obj)
        qcoords = validatePointsMatrix(IsTwoD, pQuery)
        qcoords = validateXYZArrays(IsTwoD, xyzdata)
        [qtimeID, qeqnID] = validateVectorInputs(obj, vectorargs)
        uintrp = selectPDESolution(obj,uintrpFullFormatted, qtimeID, qeqnID)
        dim = spatialDimension(obj)
        cCoeffAtNodes = evalCCoeffAtNodes(obj,varargin)
        [cdudx, cdudy] = cgradImpl2D(obj,c,dudx,dudy)
        [cdudx, cdudy, cdudz] = cgradImpl3D(obj,c,dudx,dudy,dudz)
        
    end
    
    properties (Hidden = true, SetAccess=protected)
        PDESystemSize;
        IsTwoD;
        IsTimeEig;
        NumTimeEig;
        Coefficients;
    end
    
    properties (Hidden = true, Transient=true)
        Interpolant;                              
        InterpolantdUdx;
        InterpolantdUdy;
        InterpolantdUdz;
    end
    
end


% classify the type of PDE using the solution and PDEModel data.
function  [IsTimeEig, NumTimeEig] = classifyPDEModel(thepde, thesol)

numNodes = size(thepde.Mesh.Nodes,2);
N = thepde.PDESystemSize;

IsTimeEig = false;
NumTimeEig = [];
if isempty(thesol)
    return
end
if ( mod(numel(thesol), (numNodes*N)) ~= 0) % mismatch between the mesh and solution
    error(message('pde:PDEResults:modelSolMismatch'));
end

if (iscolumn(thesol) && (size(thesol,1) ~= numNodes*N))
    error(message('pde:PDEResults:modelSolMismatch'));
end

if (ismatrix(thesol) && ~iscolumn(thesol))
    if (size(thesol,1)== numNodes*N)
        IsTimeEig  = true;
        NumTimeEig = size(thesol,2);
    else
        error(message('pde:PDEResults:modelSolMismatch'));
    end
end

end
