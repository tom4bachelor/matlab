function uintrp = selectPDESolution(obj,uintrpFullFormatted, qtimeID, qeqnID)
%selectPDESolution - Internal function to select a sub-array of solution.

% Copyright 2016 The MathWorks, Inc.

if  (obj.PDESystemSize == 1)
    if (~obj.IsTimeEig )
        uintrp = uintrpFullFormatted;
    else
        uintrp = uintrpFullFormatted(:,qtimeID);
    end
else
    if ( ~obj.IsTimeEig )
        uintrp = uintrpFullFormatted(:,qeqnID);
    else
        uintrp = uintrpFullFormatted(:,qeqnID,qtimeID);
    end
end

end