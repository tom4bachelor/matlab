function Qn = heatFluxIntegral2D(self,boundaryID)
%heatFluxIntegral2D - Internal function to integrate heat flux across an edge of a 2-D model.
%

%       Copyright 2016 The MathWorks, Inc.

Nt = self.NumTimeEig;
if isempty(Nt)
    Nt = 1;
end

domainDim = 2;
boundaryDim = domainDim-1;
ma = self.Mesh.MeshAssociation;
elemFacets = ma.discreteEdge(boundaryID);
[numFacetNodes, numFacets] = size(elemFacets);
Qn = zeros(Nt,1);

import pde.internal.elements.shapeLine2
import pde.internal.elements.shapeLine3
import pde.internal.elements.dShapeLine2
import pde.internal.elements.dShapeLine3
import pde.internal.elements.GaussianIntegrationRule

if(numFacetNodes == 3)
    sfCalc = @shapeLine3;
    dsfCalc = @dShapeLine3;
    numGaussPoints = 3;
elseif (numFacetNodes == 2)
    sfCalc = @shapeLine2;
    dsfCalc = @dShapeLine2;
    numGaussPoints = 2;
else
    error(message('pde:pde2DBCImpl:illegalEdgeDefn', numFacetNodes));
end

% compute shape functions and derivatives at Gauss points
intRule=GaussianIntegrationRule(GaussianIntegrationRule.Curve, numGaussPoints);
intWts = intRule.wts;
intPoints = intRule.points;
dsPts = zeros(numFacetNodes, boundaryDim, numGaussPoints);
for n=1:numGaussPoints
    dsPts(:,:,n) = dsfCalc(intPoints(:,n));
end
dsPts = squeeze(dsPts); % Remove singleton dimension in case of 2-D problems.
sPts = sfCalc(intPoints);
xyzAllFaceNodes = self.Mesh.Nodes(:, elemFacets(:));
xyzAllFaceNodes = reshape(xyzAllFaceNodes, domainDim, numFacetNodes, numFacets);

% Compute flux density for the solution.
[qx,qy,~] = self.evaluateHeatFlux;

for nt = 1:Nt
    facetHeatFlux = [qx(elemFacets(:),nt).';
        qy(elemFacets(:),nt).';
        ];
    facetHeatFlux = reshape(facetHeatFlux, domainDim, numFacetNodes, numFacets);
    
    for i=1:numFacets
        xyz = xyzAllFaceNodes(:,:,i);
        for n=1:numGaussPoints
            dxyDr = xyz*dsPts(:,n);
            v = pde.ThermalResults.cross3(self,[dxyDr;0],[0,0,1]);
            v = v(1:2);
            facetNorm = norm(v);
            facetUnitNormal = v/facetNorm;
            detJ = getElemLength(xyz(:,1), xyz(:,end));
            shn = sPts(:,n);
            wt = intWts(n);
            detWt = detJ*wt;
            gaussPtHeatFlux = facetHeatFlux(:,:,i)*shn;
            normalFluxDensity = gaussPtHeatFlux'*facetUnitNormal;
            Qn(nt) = Qn(nt) + detWt*normalFluxDensity;
        end
    end
    
end
end

function elmLength=getElemLength(p1,p2)
% compute length of a line segment given two end point coordinates.
elmLength = sqrt((p2(1)-p1(1))^2 + (p2(2)-p1(2))^2);

end

