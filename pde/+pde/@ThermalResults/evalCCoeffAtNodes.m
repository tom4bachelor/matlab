function cCoeffAtNodes = evalCCoeffAtNodes(obj,varargin)
%evalCCoeffAtNodes - Evaluate values of c-coefficients at nodal locations
%
% Copyright 2016-2017 The MathWorks, Inc.
 
N = obj.PDESystemSize;
Np = size(obj.Mesh.Nodes,2);
nodeRefCount = zeros(1,Np);
dim = pde.ThermalResults.spatialDimension(obj);
 
%For time-dependent problems, a scalar value of time is passed as input.
if (obj.IsTimeEig && ~isempty(varargin))
    state.time = varargin{1};
end
 
if isempty(obj.Coefficients)
    error(message('pde:PDEResults:cCoeffEmpty'))
end
 
% Loop through all subdomains to compute c-coefficient values. For numeric
% c-coefficients, with nrc rows, the output will be of size nrc x Np. Each
% subdomain would have a set of nrc x NpSubDomain c-values, where
% NpSubDomain is the number of nodes in the subdomain. Nodes forming the
% internal boundary of subdomains would get the averaged value of c. For
% functional form of c-coefficient specification, state and region
% structures are constructed with fields that contain solution state and
% nodal coordinates for all elements of the subdomain. User defined
% function is invoked with these structures as inputs.
 
% Declare maximum sized array with zeros to store c-coefficients, condense
% it later.
cCoeffAtNodes = zeros(dim^2*N^2,size(obj.Mesh.Nodes,2));
efirst = 1;
for sd = 1:length(obj.Coefficients.Coefficients.c) % Loop over subdomains
    elast = efirst + obj.Coefficients.NumElementsInSubdomain(sd)-1;
    subDomainElements = obj.Coefficients.ElementsInSubdomain(efirst:elast);
    subDomainNodeIDs = unique(obj.Mesh.Elements(:,subDomainElements));
    % Node reference count for averaging across subdomains.
    nodeRefCount(subDomainNodeIDs) = nodeRefCount(subDomainNodeIDs) + 1;
    if isnumeric(obj.Coefficients.Coefficients.c{sd})
        % Code to expand c-coefficient to all nodes.
        csd = obj.Coefficients.Coefficients.c{sd};
        nrc = size(csd,1);
        cCoeffAtNodes(1:nrc,subDomainNodeIDs) = ...
            cCoeffAtNodes(1:nrc,subDomainNodeIDs)+csd;
        efirst = elast+1;
    else
        % This implies c-coefficient is specified as a function.
        userCfunc = obj.Coefficients.Coefficients.c{sd};
        nodes = obj.Mesh.Nodes;
        
        region.x = nodes(1,subDomainNodeIDs);
        region.y = nodes(2,subDomainNodeIDs);
        %Create state structure with transposed solution and gradients.
        state.u  = obj.Temperature(subDomainNodeIDs,:)'; 
        state.ux =    obj.XGradients(subDomainNodeIDs,:)';
        state.uy =    obj.YGradients(subDomainNodeIDs,:)';
 
        % case of 3-D model
        if ~obj.IsTwoD
            region.z = nodes(3,subDomainNodeIDs);
            state.uz = obj.ZGradients(subDomainNodeIDs,:)';
        end
       
        csd = feval(userCfunc,region,state);
        nrc = size(csd,1);
        cCoeffAtNodes(1:nrc,subDomainNodeIDs) = ...
            cCoeffAtNodes(1:nrc,subDomainNodeIDs) + csd;
        efirst = elast+1;
    end
end % End of loop over subdomains.
 
% condense cCoeffAtNodes by retaining only nrc rows.
cCoeffAtNodes = cCoeffAtNodes(1:nrc,:);
 
if ~all(nodeRefCount)
    %In case if there was an unconnected node(s) in the mesh.
    error(message('pde:PDEResults:hangingNodes'))
else
    % Perform average across the boundary.
    cCoeffAtNodes = cCoeffAtNodes(1:nrc,:)./nodeRefCount;
end
 
end
