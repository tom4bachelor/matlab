function Qn = evaluateHeatRateInternal(self,varargin)
%evaluateHeatRateInternal - Internal function to compute integrated heat flow across a given boundary
%

%       Copyright 2016 The MathWorks, Inc.
narginchk(3,3);
ma = self.Mesh.MeshAssociation;

parser = inputParser;
parser.addParameter('Edge', [], @pde.EquationModel.isValidEntityID);
parser.addParameter('Face', [], @pde.EquationModel.isValidEntityID);
parser.parse(varargin{:});

if self.IsTwoD
    if ~ismember('Face', parser.UsingDefaults)
        error(message('pde:ThermalResults:no2DBoundryIntIn2DModel'));
    end
    
    if ~isempty(parser.Results.Edge)
        pde.EquationModel.isValidEntityID(parser.Results.Edge);
        if any(parser.Results.Edge > ma.numDiscreteEdges())
            error(message('pde:pdeModel:invalidEdgeIndex'));
        end
    else
        error(message('pde:ThermalResults:noEdgeFor2DHeatFlow'));
    end
    
    Qn = pde.ThermalResults.heatFluxIntegral2D(self,parser.Results.Edge);
else
    if ~ismember('Edge', parser.UsingDefaults)
        error(message('pde:ThermalResults:no1DBoundryIntIn3DModel'));
    end
    
    if ~isempty(parser.Results.Face)
        pde.EquationModel.isValidEntityID(parser.Results.Face);
        if any(parser.Results.Face > ma.numDiscreteFaces)
            error(message('pde:pdeModel:invalidFaceIndex'));
        end
    else
        error(message('pde:ThermalResults:noFaceFor3DHeatFlow'));
    end
    Qn = pde.ThermalResults.heatFluxIntegral3D(self,parser.Results.Face);
end

end
