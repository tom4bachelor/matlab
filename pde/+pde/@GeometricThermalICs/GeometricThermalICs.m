classdef (Sealed) GeometricThermalICs  < pde.PDEInitialConditions & matlab.mixin.internal.CompactDisplay 
% GeometriThermalICs Initial Temperature over a region or region boundary
%     Initial condition (IC), temperature, for transient thermal problem or
%     the initial guess for nonlinear thermal problem. The thermal model in
%     PDE toolbox case solve equation of the form:
%    
%      (MassDensity*SpecificHeat)*dT/dt - div(ThermalConductivity*grad(T)) = HeatSource
%  
%     The equation to solve is defined in terms of the coefficients m, d, c, 
%     a, f. If the thermal model is set up as transient and valid non-zero
%     MassDensity and SpecificHeat are provided, then Initial Conditions
%     must be specified. Alternatively, if thermal model is set up to solve
%     steady state problem, however, if the ThermalConductivity and/or
%     HeatSource happens to be function of temperature then initial guess
%     for temperature must be provided as Initial Condition the model.
%     If no initial guess is provided for a nonlinear steady state thermal
%     problem, then a default initial guess of zero is assumed.
%  
%     Instances of this class can only be created by calling the 
%     thermalIC method of the ThermalModel class. 
%
%
%   See also pde.ThermalModel, pde.ThermalModel/thermalIC
 
% Copyright 2016 The MathWorks, Inc.


properties (SetAccess = private)
% RegionType - Type of geometric region the ICs are assigned to.
%    A string specifying the type of region the ICs are assigned to. The
%    RegionType can be one of the following geometric entities: 'face',
%    or 'edge'.
    RegionType;
end


properties
% RegionID - ID of the geometric regions the ICs are assigned to. Where 
%      RegionID satisfies the following:
%        RegionType 'face'   0 < RegionID(j) < NumFaces
%        RegionType 'edge'   0 < RegionID(j) < NumEdges
%        RegionType 'vertex' 0 < RegionID(j) < NumVertices
%   j is the j'th element of the RegionID column vector. NumFaces and 
%   NumEdges are the properties of the geometric model.
    RegionID;

% InitialTemperature - The initial temperature or guess
%    The initial temperature for a transient problem or the
%    initial-guess for steady state nonlinear thermal problems. The initial
%    temperature can be one of the following:
%     (1) A scalar representing a constant initial Temperature/guess throughout 
%         the region.
%     (2) A function handle representing a spatially varying initial
%     temperature/guess across the region.
    InitialTemperature;
    
end

methods(Hidden=true, Access={?pde.ThermalModel})
    function obj=GeometricThermalICs(varargin) 
      obj.RecordOwner = varargin{1};
      parser = inputParser;
      obj.InitialTemperature = [];
      inputargs = varargin;
      if isnumeric(varargin{2}) || isa(varargin{2}, 'function_handle') 
          obj.InitialTemperature = varargin{2};
          inputargs = {varargin{3:end}};
      end
         
      parser.addParameter('vertex', []);    
      parser.addParameter('edge', []);     
      parser.addParameter('face', []);     
      parser.addParameter('cell', []);            
      parser.parse(inputargs{:});
     
      if ~isempty(parser.Results.vertex)
              obj.RegionType = 'vertex';
              obj.RegionID = parser.Results.vertex;
      elseif ~isempty(parser.Results.edge)
              obj.RegionType = 'edge';
              obj.RegionID = parser.Results.edge;
       elseif ~isempty(parser.Results.face)
              obj.RegionType = 'face';
              obj.RegionID = parser.Results.face;
      elseif ~isempty(parser.Results.cell)
             obj.RegionType = 'cell';
             obj.RegionID = parser.Results.cell;    
      end            
      systemsize = 1;
      obj.validateICs(systemsize);      
    end
end

methods    
    function set.RegionType(self, rtype)     
        self.RegionType = rtype;       
    end
    
    function set.RegionID(self, rids)      
        self.ValidateRegionID(rids);      
        self.RegionID = rids;        
    end        
    
    function set.InitialTemperature(self, val)    
      self.precheckIC(val);     
      self.InitialTemperature = val;     
    end
    

end

methods(Hidden=true, Access = {?pde.ThermalICRecords})      
             
    function tf=numericIcValue(self)                
         tf = ~isa(self.InitialTemperature, 'function_handle');                                        
    end
      
    function performSolverPrecheck(self, systemsize, numvertices, numedges, numfaces, numcells)  
        if strcmp(self.RegionType, 'vertex')
            if any(self.RegionID > numvertices)
                error(message('pde:thermalInitialConditions:invalidVertexIndexPresolve')); 
            end
        elseif strcmp(self.RegionType, 'edge')
            if any(self.RegionID > numedges)
                error(message('pde:thermalInitialConditions:invalidEdgeIndexPresolve')); 
            end
         elseif strcmp(self.RegionType, 'face')
            if any(self.RegionID > numfaces)
                error(message('pde:thermalInitialConditions:invalidFaceIndexPresolve')); 
            end
         else
            if any(self.RegionID > numcells)
                error(message('pde:thermalInitialConditions:invalidCellIndexPresolve')); 
            end        
         end  
         validateICs(self, systemsize);     
    end         
end

methods(Hidden=true)
    function tf = hasICValue(self) 
        tf = ~isempty(self.InitialTemperature);
    end
  
end

methods(Hidden=true, Access = private) 
    %
    % validateICs - check the size of the IC matrices
    % 
    function validateICs(self, systemsize)
       if isempty(self.InitialTemperature)
         error(message('pde:pdeInitialConditions:emptyICValue'));      
       end      
       self.checkIcSize(self.InitialTemperature, systemsize);
    end       
end




methods(Static, Access = private)

    function ok=ValidateRegionID(rgnid)
      % Must be real(non-complex), full, natural number.        
      if ~isreal(rgnid) || ~all(rgnid(:) > 0) || issparse(rgnid) || any(mod(rgnid(:),1)~=0)
        error(message('pde:pdeInitialConditions:invalidRegionID'));   
      end      
      ok = true; 
    end
                
    function precheckIC(icval)   
      if isempty(icval)          
          return
      end
      if isfloat(icval)          
        if issparse(icval)
            error(message('pde:thermalInitialConditions:invalidICValueSparse'));   
        elseif any(isnan(icval))
            error(message('pde:thermalInitialConditions:invalidICValueNaN'));
        elseif any(isinf(icval))
            error(message('pde:thermalInitialConditions:invalidICValueInf'));      
        elseif ~isscalar(icval) && ~(iscolumn(icval))
            error(message('pde:thermalInitialConditions:invalidICColVector'));
        elseif any(~isreal(icval))
            error(message('pde:thermalInitialConditions:invalidICValueComplex'));                 
        end
      elseif isa(icval, 'function_handle')    
         pde.GeometricThermalICs.checkIcFcnHdlArgCounts(icval);             
      end          
    end
                    
    function checkIcSize(icval, systemsize)  
         pde.GeometricThermalICs.precheckIC(icval);
         if (~isscalar(icval) && numel(icval) ~= systemsize)
            error(message('pde:thermalInitialConditions:invalidICColVector'));    
         end                              
    end
   
    
%
%   Coefficient function-handle argument checks
%   
    function checkIcFcnHdlArgCounts(icfcn)        
        location.x = 0;
        location.y = 0;          
        location.z = 0;
        location.subdomain=1;                                 
        try
            icfcn(location);
        catch 
            error(message('pde:thermalInitialConditions:invalidFcnHandleArgs'));
        end 
    end  
    
end   

methods(Static, Hidden=true)
    function delete(self,~)
        if isvalid(self.RecordOwner)          
            self.RecordOwner.delistThermalICAssignments(self);
        end
    end
end

   properties (Hidden = true, Access='private')
      RecordOwner;
   end   
end
