classdef PDEModel < pde.EquationModel
% pde.PDEMODEL  A container that represents a PDE model
%    A PDEModel is a container that stores the geometry, mesh, and boundary
%    conditions that are required to define a PDE. A PDEModel is generally 
%    not created directly, it is created using the CREATEPDE function. 
%    The PDEModel provides functions that allow you to: define 2D geometry 
%    or import 3D geometry, generate and store a mesh for these geometries, 
%    and apply and store boundary conditions that define the behavior at 
%    the boundary of the geometry. The properties of the PDEModel allow you 
%    to access this information easily.
%
%    PDEM = pde.PDEMODEL(PDESystemSize) creates a PDE model for a system of
%    PDESystemSize partial differential equations. If PDESystemSize is
%    omitted, it defaults to one (scalar system).
%
% PDEModel methods:
%    geometryFromEdges       - Create 2D geometry from DECSG or PDEGEOM
%    importGeometry          - Create 3D geometry from file (STL only)
%    geometryFromMesh        - Create 3D geometry from a triangulated mesh
%    specifyCoefficients     - Specify all PDE coefficients over a domain or subdomain
%    applyBoundaryCondition  - Apply a boundary condition to the geometry
%    setInitialConditions    - Set the initial conditions for the PDE
%    generateMesh            - Generate a mesh from the geometry
%    solvepde                - Solve the PDE
%    solvepdeeig             - Solve the PDE eigenvalue problem
%
% PDEModel properties:
%    PDESystemSize        - Number of PDEs in the system
%    IsTimeDependent      - True if the PDE is time-dependent
%    Geometry             - Geometric representation of the domain
%    EquationCoefficients - Equation coefficients within the domain
%    BoundaryConditions   - Boundary conditions applied to the geometry
%    InitialConditions    - Initial conditions/guess
%    Mesh                 - Discretization of the domain
%    SolverOptions        - Algorithm options for the PDE solvers
%
%
%    See also CREATEPDE, pde.GeometricModel, pde.BoundaryCondition,
%    pde.FEMesh, pde.ThermalModel

% Copyright 2014-2016 The MathWorks, Inc.

    
  properties (Dependent = true, SetAccess='protected')
 % IsTimeDependent - True if the PDE is time-dependent
    IsTimeDependent;  
  end
  
  
    
    properties
        % EquationCoefficients - Equation coefficients within the domain
        %    An object that contains the equation coefficient assignments within the
        %    geometric domain. Equation coefficients are specified and assigned to
        %    the geometry using the specifyCoefficients function. If multiple assignments
        %    are made, they are all recorded within this object.
        %
        %    See also pde.CoefficientAssignmentRecords/findCoefficients,
        %             pde.CoefficientAssignmentRecords/globalCoefficients
        EquationCoefficients;
        
        
        % BoundaryConditions - Boundary conditions applied to the geometry
        %    An object that contains the boundary condition assignments on the
        %    boundary of the geometric. Boundary conditions are specified and assigned to
        %    the geometry using the applyBoundaryCondition function. If multiple assignments
        %    are made, they are all recorded within this object.
        %
        %    See also pde.BoundaryConditionRecords/findBoundaryConditions
        BoundaryConditions;
        
        % InitialConditions - Initial conditions/guess
        %    An object that contains the initial condition assignments within the
        %    geometric domain. Initial conditions are specified and assigned to
        %    the geometry using the setInitialConditions function. If multiple assignments
        %    are made, they are all recorded within this object.
        %
        %    See also pde.InitialConditionsRecords/findInitialConditions,
        %             pde.GeometricInitialConditions, pde.NodalInitialConditions
        InitialConditions;
        
    end
    
    
    methods
        function obj = PDEModel(numEqns)
            if(nargin < 1)
                numEqns = 1;
            end
            validateattributes(numEqns,{'numeric'},...
                {'scalar','integer','positive'});
            obj@pde.EquationModel(numEqns)
        end
        
        % Method declaration
        coef = specifyCoefficients(self, varargin)
        bc = applyBoundaryCondition(self,varargin)
        ic = setInitialConditions(self, varargin)
        sol = solvepde(self, varargin)
        [eval, evec] = solvepdeeig(self, varargin)
        
        
        function set.EquationCoefficients(self, eqc)
            if(~isempty(eqc) && ~isa(eqc, 'pde.CoefficientAssignmentRecords'))
                error(message('pde:pdeModel:invalidEquationCoefficients'));
            end
            if ~isscalar(eqc) && ~isempty(eqc)
                error(message('pde:pdeModel:nonScalarEquationCoefficients'));
            end
            self.EquationCoefficients = eqc;
        end
        
        
        function set.BoundaryConditions(self, bc)
            if(~isempty(bc) && ~isa(bc, 'pde.BoundaryConditionRecords'))
                error(message('pde:pdeModel:invalidBoundaryConditions'));
            end
            if ~isscalar(bc) && ~isempty(bc)
                bcTemp = bc(1);
                for i = 2:length(bc)
                    bcTemp.BoundaryConditionAssignments(end+1) =  bc(i).BoundaryConditionAssignments;
                end
                bc = bcTemp;
            end
            if isa(bc,'pde.BoundaryConditionRecords') && isempty(bc.ParentPdemodel.Geometry)
                bc.ParentPdemodel = self;
            end
            self.BoundaryConditions = bc;
        end
        
        function set.InitialConditions(self, icr)
            if(~isempty(icr) && ~isa(icr, 'pde.InitialConditionsRecords'))
                error(message('pde:pdeModel:invalidInitialConditions'));
            end
            if ~isscalar(icr) && ~isempty(icr)
                error(message('pde:pdeModel:nonScalarInitialConditions'));
            end
            self.InitialConditions = icr;
        end
        

     function tf = get.IsTimeDependent(self)
        tf = false;
        if ~isempty(self.EquationCoefficients)  
            tf = self.EquationCoefficients.timeDependent();
        end            
     end
     
        
    end
    
    
    
    methods(Hidden = true, Access = {?pde.CoefficientAssignmentRecords})
        function delistCoefficientAssignments(self)
            if isvalid(self)
                self.EquationCoefficients = [];
            end
        end
    end
    
    
  methods(Hidden = true, Access = {?pde.InitialConditionsRecords})
    function delistInitialConditions(self)
         if isvalid(self)
            self.InitialConditions = [];  
         end
    end         
  end
  
  methods(Hidden = true, Access = {?pde.BoundaryConditionRecords})
    function delistBoundaryConditions(self)
         if isvalid(self)
            self.BoundaryConditions = [];  
         end
    end         
  end
  
% For custom display  
methods (Access = protected, Hidden = true)
    function group = getPropertyGroups(~)
        group = matlab.mixin.util.PropertyGroup({'PDESystemSize','IsTimeDependent', ...
                            'Geometry','EquationCoefficients',...
                            'BoundaryConditions','InitialConditions','Mesh',...
                            'SolverOptions'});
    end
end
    
end
