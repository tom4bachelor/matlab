classdef (Sealed) ThermalBC < handle & matlab.mixin.internal.CompactDisplay
%ThermalBC Defines a boundary condition (BC) for the thermal model
%
%     Thermal model supports Dirichlet BC (specified temperature) of the form:
%    
%                T = Temperature
%      
%     and Neumann BCs or heat fluxes of forms:
% 							
%      Heat Flux:  n*(ThermalConductivity*grad(T)) = HeatFlux
%      Convection: n*(ThermalConductivity*grad(T)) = ConvectionCoefficient(T - AmbientTemperature)
%      Radiation:  n*(ThermalConductivity*grad(T)) = Emissivity*StefanBoltzmannConstant(T^4 - AmbientTemperature^4)
%
%     ThermalBC objects is defined for each region of the boundary 
%     where boundary conditions are required. If multiple regions have identical 
%     boundary conditions, they can be defined in a single ThermalBC. 
%     The default boundary condition for a region of boundary is zero heat flux or insulated. A ThermalBC is generally not created directly, it 
%     is created using the thermalBC method of the ThermalModel class. 
%     The BC object is appended to the ThermalBCAssignments. 
% 
%     BoundaryCondition properties:
%          RegionType - Type of region the BC is assigned to 
%          RegionID - ID of the region the BC is assigned to
%          Temperature - Temperature on the boundary.
%          HeatFlux - Heat flux crossing a boundary.
%          ConvectionCoefficient - Convection heat transfer coefficient at a boundary.
%          Emissivity - Emissivity of a boundary.
%          AmbientTemperature - Ambient temperature specified along with ConvectionCoefficient and Emissivity.
%          Vectorized - Indicates whether a user-defined function accepts vector arguments.
% 
%     See also pde.ThermalModel, pde.ThermalModel/thermalBC

%     Copyright 2016 The MathWorks, Inc.
    
    properties
        % RegionType - Type of region the BC is assigned to
        RegionType;
        % RegionID - ID of the region the BC is assigned to
        RegionID;
        %Temperature - Temperature on the boundary.
        Temperature;
        %HeatFlux - Heat flux across the boundary.
        HeatFlux;
        %ConvectionCoefficient - Convection heat transfer coefficient at the boundary.
        ConvectionCoefficient;
        %Emissivity - Emissivity of the boundary.
        Emissivity;
        %AmbientTemperature - Ambient temperature specified along with ConvectionCoefficient and Emissivity.
        AmbientTemperature
        % Vectorized - Indicates whether a user-written function will return
        %              boundary condition matrices at multiple locations.
        Vectorized;
    end
    
    properties (Hidden = true, Access='private')
        RecordOwner;
    end
    
    properties (Hidden = true, Access={?pde.internal.pde2DBCImpl, ?pde.internal.pde3DBCImpl,  ?pde.internal.pde2DBCSfnImpl,?pde.ThermalBCRecords})
        h = 1;
        r;
        q;
        g;
    end
    
    
    methods
        function obj=ThermalBC(bcr,varargin)
            obj.RecordOwner = bcr;
            import pde.*
            import pde.internal.*
            narginchk(5,13);
            obj.CoeffsSet = obj.noneSet;
            parser = inputParser;
            parser.addParameter('Face', [], @ThermalBC.ValidateRegionID);
            parser.addParameter('Edge', [], @ThermalBC.ValidateRegionID);
            parser.addParameter('Vectorized', 'off', @pdeIsValid.onOrOff);
            parser.addParameter('Temperature', [], @ThermalBC.ValidateBCVal);
            parser.addParameter('HeatFlux', [], @ThermalBC.ValidateBCVal);
            parser.addParameter('ConvectionCoefficient', [], @ThermalBC.ValidateBCVal);
            parser.addParameter('Emissivity', [], @ThermalBC.ValidateBCVal);
            parser.addParameter('AmbientTemperature', [], @ThermalBC.ValidateBCValNumericOnly);
            parser.parse(varargin{:});
            
                           
            if ~isempty(parser.Results.Face)
                obj.RegionType = 'Face';
                obj.RegionID = parser.Results.Face;
            elseif ~isempty(parser.Results.Edge)
                obj.RegionType = 'Edge';
                obj.RegionID = parser.Results.Edge;
            end
            
            
            if ~isempty(parser.Results.Temperature)
                obj.Type = 'dirichlet';
            else
                obj.Type = 'neumann';
            end
            
            obj.Vectorized = parser.Results.Vectorized;
            obj.Temperature = parser.Results.Temperature;
            obj.HeatFlux = parser.Results.HeatFlux;
            obj.ConvectionCoefficient = parser.Results.ConvectionCoefficient;
            obj.Emissivity = parser.Results.Emissivity;
            obj.AmbientTemperature = parser.Results.AmbientTemperature;
            
            
            if ~isempty(obj.Temperature)
                if (~isempty(obj.ConvectionCoefficient) || ...
                        ~isempty(obj.Emissivity) || ...
                        ~isempty(obj.AmbientTemperature) || ...
                        ~isempty(obj.HeatFlux)       )
                    error(message('pde:ThermalModel:noBCsAlongWithTemperature'));
                end
            end
            
        end
        
        
        function set.RegionType(self, rtype)
            self.ValidateRegionType(rtype);
            self.RegionType = rtype;
        end
        
        function set.RegionID(self, rids)
            self.ValidateRegionID(rids);
            self.RegionID = rids;
        end
        
        
        function set.Temperature(self, coef)
            self.ValidateCoeff(coef, self.TemperatureSet, self.HeatFluxSet|self.ConvectionCoefficientSet|self.EmissivitySet|self.AmbientTemperatureSet);
            self.BCValPrecheck(coef);
            if (numel(coef)> 1)
                error(message('pde:ThermalModel:noVecForTemperatureBC'));
            end
            self.Temperature = coef;
        end
        %
        
        function set.HeatFlux(self, coef)
            self.ValidateCoeff(coef, self.HeatFluxSet, ~self.noneSet);
            self.BCValPrecheck(coef);
            if (numel(coef)> 1)
                error(message('pde:ThermalModel:noVecForHeatFluxBC'));
            end
            self.HeatFlux = coef;
        end
        
        function set.ConvectionCoefficient(self, coef)
            self.ValidateCoeff(coef, self.ConvectionCoefficientSet, ~self.noneSet);
            self.BCValPrecheck(coef);
            if (numel(coef)> 1)
                error(message('pde:ThermalModel:noVecForConvectionBC'));
            end
            
            self.ConvectionCoefficient = coef;
        end
        
        
        function set.Emissivity(self, coef)
            self.ValidateCoeff(coef, self.EmissivitySet, ~self.noneSet);
            self.BCValPrecheck(coef);
            if (numel(coef)> 1)
                error(message('pde:ThermalModel:noVecForEmissivityBC'));
            end
            
           self.Emissivity = coef;
        end
        
        function set.AmbientTemperature(self, coef)
            self.ValidateCoeff(coef, self.AmbientTemperatureSet, ~self.noneSet);
            self.BCValPrecheck(coef);
            
            if ~isnumeric(coef)
                error(message('pde:ThermalModel:noVecOrFuncForAmbientTBC'));
            end
            
            if (numel(coef)> 1)
                error(message('pde:ThermalModel:noVecOrFuncForAmbientTBC'));
            end
            self.AmbientTemperature = coef;
        end
        
        %
        function set.Vectorized(self, vec)
            % This property is pre-validate by input parser.
            self.Vectorized = vec;
        end
        
    end % methods
    
    
    properties(Access = {?pde.internal.pde2DBCImpl, ?pde.internal.pde3DBCImpl, ?pde.internal.pde2DBCSfnImpl})
        % Type - string indicating type of boundary condition
        %        Allowable values are 'Value', 'Neumann', 'Dirichlet',
        %        'mixedvalue' and 'mixeddirichlet'
        Type;
        % CoeffsSet - Boolean array indicating which coefficients have been set
        CoeffsSet;
    end
    
    methods(Access = private)
        
        function ValidateCoeff(self, coeff, cInd, cSet)
            if(~isempty(coeff))
                self.ValidateBCVal(coeff);
                % Is this coefficient compatible with the ones already set?
                if(any((~cSet & self.CoeffsSet)))
                    msg = '';
                    startMsg = true;
                    for i=1:length(self.CoeffsSet)
                        ci = self.CoeffsSet(i);
                        if(ci)
                            if ~startMsg
                                % separate parameter names with commas
                                msg = [msg ',']; %#ok
                            end
                            msg = [msg ' ' self.coefNames{i}]; %#ok
                            startMsg = false;
                        end
                    end
                    error(message('pde:pdeBoundaryConditions:invalidParameter', ...
                        self.coefNames{cInd}, msg));
                end
                % Flag this coefficient as having been set.
                self.CoeffsSet(cInd) = true;
            else
                % Coefficient is empty so flag it as not set.
                self.CoeffsSet(cInd) = false;
            end
        end
    end
    
    methods(Static, Access = private)
        
        function ok=BCValPrecheck(coef)
            if isfloat(coef)
                if any(isnan(coef))
                    error(message('pde:thermalBCSpecification:invalidBCValueNaN'));
                elseif any(isinf(coef))
                    error(message('pde:thermalBCSpecification:invalidBCValueInf'));
                elseif(~isreal(coef))
                    error(message('pde:thermalBCSpecification:invalidBCValueComplex'));
                end
            end
            ok = true;
        end
        
    end
    
    methods(Hidden=true, Access = {?pde.ThermalModel})
        function tf=numericCoefficients(self,varargin)
            tf = ~any(isa(self.Temperature, 'function_handle')  || ...
                isa(self.HeatFlux, 'function_handle')  || ...
                isa(self.ConvectionCoefficient, 'function_handle')  || ...
                isa(self.Emissivity, 'function_handle')  || ...
                isa(self.AmbientTemperature, 'function_handle'));
        end
    end
    
    methods(Static, Access = private)
        function ok=ValidateBCVal(v)
            ok = isa(v, 'function_handle') || isnumeric(v);
            if(~ok)
                error(message('pde:thermalBCSpecification:invalidTermType'));
            end
            if isa(v, 'function_handle') && ~(nargin(v) == 2)
                error(message('pde:thermalBCSpecification:invalidFunctionHandle'));
            end
        end
        
        
        function ok=ValidateBCValNumericOnly(v)
            ok = isnumeric(v);
            if(~ok)
                error(message('pde:thermalBCSpecification:invalidTermTypeNumeric'));
            end
        end
        
        
        function ok=ValidateRegionID(rgnid)
            % Must be real(non-complex), full, natural number.
            if ~isreal(rgnid) || ~all(rgnid(:) > 0) || issparse(rgnid) || any(mod(rgnid(:),1)~=0)
                error(message('pde:pdeBoundaryConditions:invalidRegionID'));
            end
            ok = true;
        end
        
        function ok=ValidateRegionType(rgntype)
            nc = numel(rgntype);
            if ~(strncmpi(rgntype,'Face',nc) || strncmpi(rgntype,'Edge',nc))
                error(message('pde:pdeBoundaryConditions:invalidRegionType'));
            end
            ok = true;
        end
        
    end % static methods
    
    methods(Hidden=true)
        function delete(self)
            if ~isempty(self.RecordOwner) && isvalid(self.RecordOwner)
                self.RecordOwner.delistBoundaryConditions(self);
            end
        end
    end
    
    properties(Constant, Access=private)
        coefNames = {'Temperature', 'HeatFlux', 'ConvectionCoefficient', 'Emissivity', 'AmbientTemperature'};
        % Define Boolean arrays for the different option names
        % These will be used for checking valid combinations of options
        % using Boolean ops in the "set" functions defined above.
        noneSet                  = logical([0 0 0 0 0]);
        TemperatureSet           = logical([1 0 0 0 0]);
        HeatFluxSet              = logical([0 1 0 0 0]);
        ConvectionCoefficientSet = logical([0 0 1 0 0]);
        EmissivitySet            = logical([0 0 0 1 0]);
        AmbientTemperatureSet    = logical([0 0 0 0 1]);
    end
    
end

