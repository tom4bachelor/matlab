classdef TransientThermalResults < pde.ThermalResults
% pde.TransientThermalResults - Transient thermal model solution and its derived quantities
%   A TransientThermalResults object provides a convenient representation
%   of transient thermal model solution. It also provides functions to
%   interpolate temperature, evaluate temperature gradients, heat flux, and
%   heat rate.
%
% TransientThermalResults methods:
%   interpolateTemperature      - Interpolate temperature at specified spatial locations
%   evaluateTemperatureGradient - Evaluate gradients of temperature at specified spatial locations
%   evaluateHeatFlux            - Evaluate heat flux at specified spatial locations
%   evaluateHeatRate            - Evaluate integrated heat flux normal to a given boundary
%
% TransientThermalResults properties:
%   Temperature          - Temperature at nodal locations
%   SolutionTimes        - Solution times you used to solve the model
%   XGradients           - Spatial gradient of temperature along x-direction
%   YGradients           - Spatial gradient of temperature along y-direction
%   ZGradients           - Spatial gradient of temperature along z-direction
%   Mesh                 - Discretization of the domain
%
%    See also pde.SteadyStateThermalResults

% Copyright 2016 The MathWorks, Inc.

    properties(SetAccess = protected)
        % Temperature - Temperature at nodal locations
        % The Temperature matrix can be indexed into to extract a
        % sub-vector of interest.
        Temperature;
        
        % SolutionTimes - Solution times you used to solve the model
        SolutionTimes;
        
        % XGradients - Spatial gradient of temperature along x-direction 
        % The shape of XGradients is same as the shape of Temperature.
        XGradients;
        % YGradients - Spatial gradient of temperature along y-direction
        % The shape of YGradients is same as the shape of Temperature.
        YGradients;
        % ZGradients - Spatial gradient of temperature along z-direction
        % The shape of ZGradients is same as the shape of Temperature.
        ZGradients;
    end
    
    
    methods
        function obj = TransientThermalResults(varargin)
            obj@pde.ThermalResults(varargin{:});
            if nargin == 0
                %Will result in a default empty object.
                return
            end
            
            if isempty(obj.NumTimeEig)
                error(message('pde:PDEResults:solNotFromTimeDependent'))
            end
            
            narginchk(3,3);
            pdem = varargin{1};
            u = varargin{2};
            tlist = varargin{3};
            
            if ( ~isnumeric(tlist) || issparse(tlist) || ~isvector(tlist)...
                    || isscalar(tlist) || any(isnan(tlist)) || ...
                    ~any(isfinite(tlist)) )
                error(message('pde:PDEResults:invalidTimeVector'))
            end
            
            t0 = tlist(1);
            tfinal = tlist(end);
            if(t0 == tfinal)
                error(message('pde:PDEResults:tlistEndpointsNotDistinct'));
            end
            
            tdir = sign(tfinal - t0);
            if any( tdir*diff(tlist) <= 0 )
                error(message('pde:PDEResults:tlistUnordered'));
            end
            
            obj.SolutionTimes = tlist;
            %Since N = 1 for thermal model, no reshaping is required.
            ureshaped= u;
            
            if numel(obj.SolutionTimes) ~= obj.NumTimeEig
                error(message('pde:PDEResults:solTimesResultMismatch'))
            end
            obj.Temperature = ureshaped;
            
            if ~isempty(pdem.MaterialProperties)
                obj.Coefficients = pdem.MaterialProperties.packProperties;
            else
                obj.Coefficients = [];
            end
            obj.XGradients = [];
            obj.YGradients = [];
            obj.ZGradients = [];
            obj.InterpolantdUdx = [];
            obj.InterpolantdUdy = [];
            obj.InterpolantdUdz = [];
            if isempty(obj.Temperature)
                return;
            end
            
            obj.Interpolant   = pde.ThermalResults.constructInterpolat(pdem.Mesh,...
                ureshaped, pdem.PDESystemSize, obj.NumTimeEig);
            
            % Calculate gradients, assign nodal gradients as properties and
            % construct interpolants for gradients
            [ux,uy,uz] = nodalGradients(obj.Interpolant);
            obj.XGradients = pde.ThermalResults.reshapePDESolution(ux, ...
                obj.IsTimeEig, pdem.PDESystemSize, obj.NumTimeEig);
            obj.YGradients = pde.ThermalResults.reshapePDESolution(uy, ...
                obj.IsTimeEig, pdem.PDESystemSize, obj.NumTimeEig);
            obj.ZGradients = pde.ThermalResults.reshapePDESolution(uz, ...
                obj.IsTimeEig, pdem.PDESystemSize, obj.NumTimeEig);
            
            obj.InterpolantdUdx   = pde.ThermalResults.constructInterpolat(pdem.Mesh,...
                obj.XGradients, pdem.PDESystemSize, obj.NumTimeEig);
            
            obj.InterpolantdUdy   = pde.ThermalResults.constructInterpolat(pdem.Mesh,...
                obj.YGradients, pdem.PDESystemSize, obj.NumTimeEig);
            
            if ~obj.IsTwoD
                obj.InterpolantdUdz   = pde.ThermalResults.constructInterpolat(pdem.Mesh,...
                    obj.ZGradients, pdem.PDESystemSize, obj.NumTimeEig);
            end
            
        end
        
        
        % Methods
        function Tintrp = interpolateTemperature(obj,varargin)
            %interpolateTemperature - Interpolate temperature at specified spatial locations
            %
            % Tintrp = interpolateTemperature(R, QP, Ti) interpolates the
            % temperature in a transient thermal results object R, at the
            % query point locations QP at specified time-steps Ti. The
            % points QP are represented as a matrix of size Ndim-by-Numq,
            % where Ndim is the number of spatial dimensions and Numq is
            % the number of query points. Ti is a scalar or vector of
            % indices in the range 1 to number of elements in the
            % SolutionTimes vector. Tintrp is the interpolated temperature
            % at the query points QP and at specified time-steps Ti.
            %
            % Tintrp = interpolateTemperature(R, xq, yq, __ ) and
            % interpolateTemperature(R, xq, yq, zq, __ ) interpolates the
            % temperature using the query points QP specified as vectors of
            % coordinates or an N-D grid.
            %
            %
            % Example: Solve a transient thermal model to simulate heat
            % exchange between two identical cylindrical metal blocks,
            % stacked on top of each other, with one of the blocks set to a
            % high initial temperature.
            %
            % %Create a transient thermal model.
            % thermalmodel = createpde('thermal','transient');
            %
            % %Create the geometry of two cylindrical metal blocks stacked
            % %on top of each other. Assign the geometry to the thermal
            % %model and generate mesh.
            % gm = multicylinder(4, [2, 2], 'ZOffset', [0, 2]);
            % thermalmodel.Geometry = gm;
            % generateMesh(thermalmodel, 'Hmax', 0.5);
            %
            % %Assign material properties.
            % thermalProperties(thermalmodel,'ThermalConductivity',2,...
            %                       'MassDensity',0.0027,'SpecificHeat',920);
            %
            % %Apply insulated boundary condition on all boundary faces.
            % thermalBC(thermalmodel,'Face',1:thermalmodel.Geometry.NumFaces,'HeatFlux',0);
            %
            % %Set a high initial temperature to the bottom block and room
            % %temperature to the top block.
            % thermalIC(thermalmodel,800,'Cell',1);
            % thermalIC(thermalmodel,25,'Cell',2);
            % 
            % %Solve the thermal model.
            % tlist = 0:0.1:1;
            % result=solve(thermalmodel,tlist);
            %
            % %Interpolate and plot temperature along the cylinder axis.
            % zg = 0:0.1:4;
            % xg = zeros(size(zg));
            % yg = zeros(size(zg));
            % TaxialFinal = interpolateTemperature(result,xg,yg,zg,11);
            % figure
            % plot(zg,TaxialFinal)
            % title('Temperature along the cylinder axis')
            % xlabel('z')
            % ylabel('Temperature')
            %
            % See also pde.SteadyStateThermalResults/interpolateTemperature
            
            Tintrp = pde.ThermalResults.interpolateSolutionInternal(obj,varargin{:});
            
        end
        
        function [TxInt, TyInt, TzInt] = evaluateTemperatureGradient(obj,varargin)
            %evaluateTemperatureGradient - Evaluate gradients of temperature at specified spatial locations
            %
            % [TxInt, TyInt, TzInt] = evaluateTemperatureGradient(R,QP,Ti)
            % evaluates the spatial gradients of temperature in a
            % TransientThermalResults object R at the query point locations
            % QP and at specfied time-steps Ti. The points QP are
            % represented as a matrix of size Ndim-by-Numq, where Ndim is
            % the number of spatial dimensions and Numq is the number of
            % query points. Ti is a scalar or vector of indices in the
            % range 1 to number of elements in the SolutionTimes vector.
            % The output TxInt, TyInt, TzInt represents the components of
            % the temperature gradients at the query points QP at specified
            % time-steps Ti.
            %
            % [__] = evaluateTemperatureGradient(R, xq, yq, __ )  and
            % evaluateTemperatureGradient(R, xq, yq, zq, __ ) evaluates the
            % spatial gradients of temperature using the query points QP
            % specified as vectors of coordinates or an N-D grid.
            %
            % Example: Solve a transient thermal model to simulate heat
            % exchange between two identical cylindrical metal blocks,
            % stacked on top of each other, with one of the blocks set to a
            % high initial temperature.
            %
            % %Create a transient thermal model.
            % thermalmodel = createpde('thermal','transient');
            %
            % %Create the geometry of two cylindrical metal blocks stacked
            % %on top of each other. Assign the geometry to the thermal
            % %model and generate mesh.
            % gm = multicylinder(4, [2, 2], 'ZOffset', [0, 2]);
            % thermalmodel.Geometry = gm;
            % generateMesh(thermalmodel, 'Hmax', 0.5);
            %
            % %Assign material properties.
            % thermalProperties(thermalmodel,'ThermalConductivity',2,...
            %                       'MassDensity',0.0027,'SpecificHeat',920);
            %
            % %Apply insulated boundary condition on all boundary faces.
            % thermalBC(thermalmodel,'Face',1:thermalmodel.Geometry.NumFaces,'HeatFlux',0);
            %
            % %Set a high initial temperature to the bottom block and room
            % %temperature to the top block.
            % thermalIC(thermalmodel,800,'Cell',1);
            % thermalIC(thermalmodel,25,'Cell',2);
            % 
            % %Solve the thermal model.
            % tlist = 0:0.1:1;
            % result=solve(thermalmodel,tlist);
            %
            % %Evaluate temperature gradient along the cylinder axis for
            % %the last time-step.
            % zg = 0:0.1:2;
            % xg = zeros(size(zg));
            % yg = zeros(size(zg));
            % [Tx,Ty,Tz] = evaluateTemperatureGradient(result,xg,yg,zg,11);
            % plot(zg,Tz)
            % title('Temperature gradient along the cylinder axis')           
            %
            % See also pde.SteadyStateThermalResults/evaluateTemperatureGradient
            
            [TxInt, TyInt, TzInt] = pde.ThermalResults.evalGradInternal(obj,varargin{:});
        end
        
        function [qx, qy, qz] = evaluateHeatFlux(obj,varargin)
            % evaluateHeatFlux - Evaluate heat flux at specified spatial locations
            %
            % [qx,qy] = evaluateHeatFlux(R) evaluates the heat flux of a
            % thermal solution for a 2-D problem. R is an object of type
            % TransientThermalResults. The first dimension of qx and qy
            % corresponds to query points. The second dimension corresponds
            % time-steps.
            %
            % [qx,qy,qz] = evaluateHeatFlux(R) evaluates the heat flux of a
            % thermal solution for a 3-D problem.
            %
            % [__] = evaluateHeatFlux(R,QP,Ti) evaluates the heat flux
            % using results object R at time-steps Ti. The points QP is a
            % matrix of size Ndim-by-Numq, where Ndim is the number of
            % spatial dimensions and Numq is the number of query points. Ti
            % is a scalar or vector of indices in the range 1 to number of
            % elements in the SolutionTimes vector. The first array
            % dimension of each of the output array corresponds to the
            % number of spatial points defined by QP. The second array
            % dimension corresponds to time-steps Ti.
            %
            % [__] = evaluateHeatFlux(R,xq,yq,__ ) and [__] =
            % evaluateHeatFlux(R,xq,yq,zq, __ ) evaluates the heat flux
            % using the query points QP specified as vectors of coordinates
            % or an N-D grid. The first matrix dimension of each output
            % matrix corresponds to spatial points defined by [x(:),y(:)]
            % in 2-D space or [x(:),y(:),z(:)] in 3-D space. The second
            % array dimension corresponds to time-steps Ti.
            %
            % Example: Solve a transient thermal model to simulate heat
            % exchange between two identical cylindrical metal blocks,
            % stacked on top of each other, with one of the blocks set to a
            % high initial temperature.
            %
            % %Create a transient thermal model.
            % thermalmodel = createpde('thermal','transient');
            %
            % %Create the geometry of two cylindrical metal blocks stacked
            % %on top of each other. Assign the geometry to the thermal
            % %model and generate mesh.
            % gm = multicylinder(4, [2, 2], 'ZOffset', [0, 2]);
            % thermalmodel.Geometry = gm;
            % generateMesh(thermalmodel, 'Hmax', 0.5);
            %
            % %Assign material properties.
            % thermalProperties(thermalmodel,'ThermalConductivity',2,...
            %                       'MassDensity',0.0027,'SpecificHeat',920);
            %
            % %Apply insulated boundary condition on all boundary faces.
            % thermalBC(thermalmodel,'Face',1:thermalmodel.Geometry.NumFaces,'HeatFlux',0);
            %
            % %Set a high initial temperature to the bottom block and room
            % %temperature to the top block.
            % thermalIC(thermalmodel,800,'Cell',1);
            % thermalIC(thermalmodel,25,'Cell',2);
            % 
            % %Solve the thermal model.
            % tlist = 0:0.1:1;
            % result=solve(thermalmodel,tlist);
            %
            % %Evaluate heat flux throughout the domain. Plot the heat flux
            % %at the last time-step.
            % [qx,qy,qz] = evaluateHeatFlux(result);
            % pdeplot3D(thermalmodel,'ColormapData',qz(:,end))
            % title('Distribution of heat flux in the domain')
            %
            % See also pde.SteadyStateThermalResults/evaluateHeatFlux
            
            
            Nt = obj.NumTimeEig;
            Np = size(obj.Mesh.Nodes,2);
            
            qx=zeros(Np,Nt);
            qy=zeros(Np,Nt);
            qz=zeros(Np,Nt);
            
            for ti = 1:Nt
                cCoeffAtNodes = pde.ThermalResults.evalCCoeffAtNodes(obj,obj.SolutionTimes(ti));
                
                dudx = obj.XGradients;
                dudy = obj.YGradients;
                
                if obj.IsTwoD
                    % 2-D implementation code
                    [qx(:,ti), qy(:,ti)] = pde.ThermalResults.cgradImpl2D(obj,cCoeffAtNodes,dudx(:,ti)',dudy(:,ti)');
                    qz = [];
                else
                    dudz = obj.ZGradients;
                    % 3-D implementation code
                    [qx(:,ti), qy(:,ti), qz(:,ti)] = pde.ThermalResults.cgradImpl3D(obj,cCoeffAtNodes,dudx(:,ti)',dudy(:,ti)', dudz(:,ti)');
                end
            end % End of loop over time-steps
            
            % Perform interpolation if user specifies required additional arguments.
            if (nargin > 1)
                [qx, qy, qz] = pde.ThermalResults.evalCGradInternal(obj,qx,qy,qz,varargin{:});
            end
            
            %Negate c-gradients to get heat flux direction.
            qx = -qx;
            qy = -qy;
            qz = -qz;
        end
        
        %Declaration of method to compute heat flow rate.
        Qn = evaluateHeatRate(obj,varargin);
    end
    
    
end

