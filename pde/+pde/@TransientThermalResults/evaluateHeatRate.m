function Qn= evaluateHeatRate(self,varargin)
% evaluateHeatRate - Evaluate integrated heat flux normal to a given boundary
%
% Qn = evaluateHeatRate(R,'REGIONTYPE',REGIONID) evaluates the integrated
% heat flow normal to the boundary specified by using REGIONTYPE and
% REGIONID, for the TransientThermalResults object R. Here, REGIONTYPE is
% 'Face' for 3-D model, and 'Edge' for 2-D model. REGIONID is an integer
% specifying the ID of the geometric entity. The output, Qn, is a vector
% with number of elements equal to number of elements in SolutionTimes.
% Each entry in Qn represents the integrated heat flow rate, energy per
% unit time, flowing in the direction normal to the boundary, at a
% particular time-step. Qn is positive if the heat flows out of the domain,
% and negative if the heat flows into the domain.
% 
% Example: Convection cooling of a sphere from high temperature.
%   %Create a thermal model for transient analysis.
%   thermalmodel = createpde('thermal','transient'); 
%  
%   %Create a sphere of unit radius and assign it to the thermal model.
%   gm = multisphere(1);
%   thermalmodel.Geometry = gm;
%   
%   %Generate mesh.
%   generateMesh(thermalmodel,'GeometricOrder','linear');
%  
%   %Assign material properties.
%   thermalProperties(thermalmodel,'ThermalConductivity',0.80, ...
%                           'SpecificHeat',460,...
%                           'MassDensity',0.0078);
%  
%   %Apply convection from the surface of the sphere to atmosphere at 30 C.
%   thermalBC(thermalmodel,'Face',1,'ConvectionCoefficient',500,'AmbientTemperature',30); 
%  
%   %Apply an initial temperature of 800.
%   thermalIC(thermalmodel,800); 
%   
%   %Solve the thermal model
%   tlist = 0:0.1:1;
%   result = solve(thermalmodel,tlist); 
%  
%   %Compute the heat flow rate across the surface of the sphere over time.
%   Qn = evaluateHeatRate(result,'Face',1);
%
% See also pde.ThermalModel/solve,
% pde.TransientThermalResults/evaluateHeatFlux,
% pde.SteadyStateThermalResults/evaluateHeatRate
 
%       Copyright 2016 The MathWorks, Inc.


Qn = pde.ThermalResults.evaluateHeatRateInternal(self,varargin{:});