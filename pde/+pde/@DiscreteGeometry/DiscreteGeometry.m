% pde.DiscreteGeometry  Geometric representation of a facetted boundary
%    G = pde.DiscreteGeometry(FILENAME) Imports STL data from a file and 
%    creates an object-based geometry representation. A DiscreteGeometry
%    is typically created using the importGeometry method of the pde.PDEModel 
%    class with similar arguments. FILENAME is a string specifying the 
%    pathname and filename of the STL file including the file extension 
%    ending in 'stl' or 'STL'. If the file is in the current directory or 
%    in a directory on the MATLAB path, the pathname can be omitted. The 
%    returned value G is a DiscreteGeometry object that represents the 
%    boundary of the geometry in terms of logical faces, edges and vertices.
% 
% DiscreteGeometry properties:
%    NumCells - The number of volumetric cells in the geometry
%    NumFaces - The number of faces in the geometry
%    NumEdges - The number of edges in the geometry
%    NumVertices - The number of vertices in the geometry.
%
% See also CREATEPDE, pde.GeometricModel, pde.AnalyticGeometry, DECSG

% Copyright 2014-2015 The MathWorks, Inc.