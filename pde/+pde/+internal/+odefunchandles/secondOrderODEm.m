function m=secondOrderODEm(t, u)
%secondOrderODEm - Mass matrix function for second-order ODE system
%Computes the mass matrix of discretized PDE with second-order time
%derivative. 
%This undocumented function may be removed in a future release.
%
%       Copyright 2015-2016 The MathWorks, Inc.

  global femodel
  
  if(femodel.vd || femodel.vh)
    nu=femodel.numConstrainedEqns;
    if(nu == femodel.totalNumEqns)
      uFull = u;
    else
      uFull = femodel.B*u(1:nu) + femodel.ud;
    end
  end
  
  if(femodel.vd)
        if(femodel.nrp==2)
            femodel.Mass = formGlobalM2D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t,'m');
        elseif(femodel.nrp==3)
            femodel.Mass = formGlobalM3D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t,'m');
        end    
  end
  
  if(femodel.vh)
    [~,~,H,~]=femodel.thePde.assembleBoundary(uFull,t);
    femodel.B=pdenullorth(H);
  end
  
  nu=size(femodel.B,2);
  if(~isempty(femodel.C))
      % Make sure damping matrix and mass matrix have same size
      if(~all(size(femodel.Mass) == size(femodel.C)))
          error(message('pde:hyperbolic:dampsize'));
      end
      dampMat = femodel.B'*femodel.C*femodel.B;
  else
      dampMat = sparse(nu,nu);
  end
  
  
  m=[speye(nu,nu) sparse(nu,nu); dampMat femodel.B'*femodel.Mass*femodel.B];
  
end


