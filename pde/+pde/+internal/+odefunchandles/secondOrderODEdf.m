function df=secondOrderODEdf(t,u)
%secondOrderODEdf - Jacobian function for second-order ODE system
%Computes jacobian of discretized PDE with second-order time derivative.
%This undocumented function may be removed in a future release.
%
%       Copyright 2015-2016 The MathWorks, Inc.

global femodel

if ~(femodel.vq || femodel.vg || femodel.vh || femodel.vr || femodel.vc || femodel.va || femodel.vf)
    df=-femodel.K;
    return
end

nu=femodel.numConstrainedEqns;
if(nu == femodel.totalNumEqns)
    uFull = u(1:nu);
else
    uFull = femodel.B*u(1:nu) + femodel.ud;
end

if(femodel.vq || femodel.vh)
    [Q,~,H,~]=femodel.thePde.assembleBoundary(uFull,t);
end

if(femodel.vc || femodel.va)
    if(femodel.nrp==2)
        [K, ~] = formGlobalKF2D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t);
        A = formGlobalM2D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t,'a');
    elseif(femodel.nrp==3)
        [K, ~] = formGlobalKF3D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t);
        A = formGlobalM3D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t,'a');
    end
end

if femodel.vq
    femodel.Q=Q;
end

if femodel.vh
    femodel.B=pdenullorth(H);
end

if femodel.vc
    femodel.K=K;
end

if femodel.va
    femodel.A=A;
end

nu=size(femodel.B,2);
if ~femodel.vh
    df=-[sparse(nu,nu) -speye(nu,nu); ...
        femodel.B'*(femodel.K+femodel.A+femodel.Q)*femodel.B sparse(nu,nu)];
else
    dt=femodel.tspan*eps^(1/3);
    t1=t+dt;
    dt=t1-t;
    tm1=t-dt;
    if femodel.vd
        if(femodel.nrp==2)
            femodel.Mass = formGlobalM2D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t,'m');
        elseif(femodel.nrp==3)
            femodel.Mass = formGlobalM3D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t,'m');
        end
    end
    [~,~,H,~]=femodel.thePde.assembleBoundary(uFull,t1);
    N1=pdenullorth(H);
    [~,~,H,~]=femodel.thePde.assembleBoundary(uFull,tm1);
    Nm1=pdenullorth(H);
    BDot = (N1-Nm1)/(2*dt);
    BDotDot = (N1-2*femodel.B+Nm1)/(dt^2);
    if(isempty(femodel.C))
        nrFull = size(uFull,1);
        dampMat = sparse(nrFull, nrFull);
    else
        dampMat = femodel.C;
    end
    df=-[sparse(nu,nu) -speye(nu,nu); ...
        femodel.B'*((femodel.K+femodel.A+femodel.Q)*femodel.B+ ...
        femodel.Mass*BDotDot+dampMat*BDot) ...
        2*femodel.B'*femodel.Mass*BDot];
end

end

