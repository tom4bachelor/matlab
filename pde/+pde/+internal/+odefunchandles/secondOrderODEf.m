function f=secondOrderODEf(t,u)
%secondOrderODEf: Residue function for second-order ODE system
%Computes residue of discretized PDE with second-order time derivative.
%This undocumented function may be removed in a future release.
%
%       Copyright 2015-2016 The MathWorks, Inc.

global femodel

if ~(femodel.vq || femodel.vg || femodel.vh || femodel.vr || ...
        femodel.vc || femodel.va || femodel.vf)
    f=-femodel.K*u+femodel.F;
    return
end

nu=femodel.numConstrainedEqns;
if(nu == femodel.totalNumEqns)
    uFull = u(1:nu);
else
    uFull = femodel.B*u(1:nu) + femodel.ud;
end

if femodel.vq || femodel.vg || femodel.vh || femodel.vr
    [femodel.Q,femodel.G,femodel.H,femodel.R]=femodel.thePde.assembleBoundary(uFull,t);
end

if femodel.vc || femodel.va || femodel.vf
    if(femodel.nrp==2)
        [femodel.K, femodel.F] = formGlobalKF2D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t);
        A = formGlobalM2D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t,'a');
        femodel.A = A; % TEMP - Change the femodel to have a A matrix later.
    elseif(femodel.nrp==3)
        [femodel.K, femodel.F] = formGlobalKF3D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t);
        A = formGlobalM3D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t,'a');
        femodel.A = A;
    end
    %[femodel.K,femodel.A,femodel.F]=femodel.thePde.assema(femodel.p,femodel.t,femodel.c,femodel.a,femodel.f,uFull,t);
end

if ~(femodel.vh || femodel.vr)
    nu=femodel.numConstrainedEqns;
    KK = femodel.K + femodel.A + femodel.Q;
    K=[sparse(nu,nu) -speye(nu,nu); femodel.B'*KK*femodel.B sparse(nu,nu)];
    FF=femodel.B'*(femodel.F +femodel.G - KK*femodel.ud);
    F=[zeros(nu,1); FF];
else
    dt=femodel.tspan*eps^(1/3);
    t1=t+dt;
    dt=t1-t;
    tm1=t-dt;
    
    if femodel.vd
        if(femodel.nrp==2)
            femodel.Mass = formGlobalM2D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t,'d');
        elseif(femodel.nrp==3)
            femodel.Mass = formGlobalM3D(femodel.emptyPDEModel, femodel.p, femodel.t, femodel.coefstruct,uFull,t,'d');
        end
    end
    if(isempty(femodel.C))
        nrFull = size(uFull,1);
        dampForce = zeros(nrFull,1);
        dampMat = sparse(nrFull, nrFull);
    end
    
    KK = femodel.K + femodel.A + femodel.Q;
    FF = femodel.F+femodel.G;
    
    if femodel.vh
        [N,O]=pdenullorth(femodel.H);
        ud=O*((femodel.H*O)\femodel.R);
        [~,~,H,R]=femodel.thePde.assembleBoundary(uFull,t1);
        [N1,O]=pdenullorth(H);
        ud1=O*((H*O)\R);
        [~,~,H,R]=femodel.thePde.assembleBoundary(uFull,tm1);
        [Nm1,O]=pdenullorth(H);
        udm1=O*((H*O)\R);
        nu=size(N,2);
        udDot = (ud1 - udm1)/(2*dt);
        if(~isempty(femodel.C))
            dampMat = femodel.C;
            dampForce = dampMat*udDot;
        end
        BDot = (N1-Nm1)/(2*dt);
        BDotDot = (N1-2*N+Nm1)/(dt^2);
        udDotDot = (ud1-2*ud+udm1)/(dt^2);
        K=[sparse(nu,nu) -speye(nu,nu); ...
            N'*(KK*N+femodel.Mass*BDotDot+dampMat*BDot) ...
            2*N'*femodel.Mass*BDot];
        F=[zeros(nu,1); N'*(FF-...
            KK*ud - ...
            dampForce - femodel.Mass*udDotDot)];
    else
        nu=size(femodel.Nu,2);
        HH=femodel.H*femodel.Or;
        ud=femodel.Or*(HH\femodel.R);
        [~,~,~,R]=femodel.thePde.assembleBoundary(uFull,t1);
        ud1=femodel.Or*(HH\R);
        [~,~,~,R]=femodel.thePde.assembleBoundary(uFull,tm1);
        udm1=femodel.Or*(HH\R);
        udDot = (ud1 - udm1)/(2*dt);
        if(~isempty(femodel.C))
            dampForce = femodel.C*udDot;
        end
        udDotDot = (ud1-2*ud+udm1)/(dt^2);
        K=[sparse(nu,nu) -speye(nu,nu); ...
            femodel.Nu'*KK*femodel.Nu sparse(nu,nu)];
        F=[zeros(nu,1); ...
            femodel.Nu'*(FF - KK*ud - ...
            dampForce - femodel.Mass*udDotDot)];
    end
    femodel.ud = ud;
    femodel.dudt = udDot;
end

f=-K*u+F;

end
