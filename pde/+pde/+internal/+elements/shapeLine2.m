function shp  = shapeLine2(r)
%shapeLine2 Calculate shape functions for a 2-node line element
% Evaluate shape functions at parametric locations r, s
% This undocumented function may be changed or removed in a future release.

%       Copyright 2016 The MathWorks, Inc.

shp = [0.5*(1-r);
       0.5*(1+r)];
   
%%%% Node numbering %%%%%%
%   1_______________2
%%%%%%%%%%%%%%%%%%%%%%%%%%
   
end

