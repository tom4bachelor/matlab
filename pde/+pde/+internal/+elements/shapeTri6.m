function shp = shapeTri6( rs )
%SHAPETRI6 Calculate shape functions for a 6-node triangle
% Evaluate shape functions at parametric locations r, s
% This undocumented function may be changed or removed in a future release.

%       Copyright 2014 The MathWorks, Inc.

  L1 = rs(1,:); L2=rs(2,:); L3 = 1-L1-L2;
  shp = [
  L3.*(2.*L3 - 1)
  L1.*(2.*L1 - 1)
  L2.*(2.*L2 - 1)
  4.*L1.*L3
  4.*L1.*L2
  4.*L2.*L3];

end

