function shp=shapeTri3(rs)
%SHAPETRI3 Calculate shape functions for a 3-node triangle
% Evaluate shape functions at parametric locations r, s
% This undocumented function may be changed or removed in a future release.

%       Copyright 2014 The MathWorks, Inc.
r = rs(1,:);
s = rs(2,:);
shp = [r; s; 1-r-s];
end