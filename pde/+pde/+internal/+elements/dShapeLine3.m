function ds  = dShapeLine3(r)
%dShapeLine3 Calculate derivatives of shape functions for a 3-node quadratic line element
% Evaluate shape function derivatives wrt r and s at parametric locations
% r, s This undocumented function may be changed or removed in a future
% release.

%       Copyright 2015-2016 The MathWorks, Inc.

ds = [-(1-2*r)/2, -2*r,  (1+2*r)/2];
   
%%%% Node numbering %%%%%%
%   1_______2_______3
%%%%%%%%%%%%%%%%%%%%%%%%%%
   
end

