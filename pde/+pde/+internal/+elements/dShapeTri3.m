function ds = dShapeTri3(rs)
%DSHAPETRI3 Calculate derivatives of shape functions for a 3-node triangle
% Evaluate shape function derivatives wrt r and s. For 3-node triangle, these
% derivatives are constant but the rs entry is passed for consistency with the
% routines for elements with other topologies.
% This undocumented function may be changed or removed in a future release.

%       Copyright 2014 The MathWorks, Inc.

ds = [-1 -1; 1 0; 0 1];
end