function shp  = shapeLine3(r)
%shapeLine3 Calculate shape functions for a 3-node quadratic line element
% Evaluate shape functions at parametric locations r, s
% This undocumented function may be changed or removed in a future release.

%       Copyright 2015-2016 The MathWorks, Inc.

shp = [-0.5*r.*(1-r);
       (1+r).*(1-r);
        0.5*r.*(1+r)];
   
%%%% Node numbering %%%%%%
%   1_______2_______3
%%%%%%%%%%%%%%%%%%%%%%%%%%
   
end

