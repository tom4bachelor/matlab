function ds = dShapeTri6( rs )
%DSHAPETRI6 Calculate derivatives of shape functions for a 6-node triangle
% Evaluate shape function derivatives wrt r and s at parametric locations r, s
% This undocumented function may be changed or removed in a future release.

%       Copyright 2014 The MathWorks, Inc.

L1 = rs(1); L2 = rs(2); L3 = 1 - L1 - L2;
ds = [
 1.-4*L3  1.-4*L3
 4*L1-1. 0
 0   4*L2-1.
 4*(1. - L2 - 2.*L1)  -4*L1
 4*L2  4*L1
 -4*L2  4*(1. - L1 - 2.*L2)];

end

