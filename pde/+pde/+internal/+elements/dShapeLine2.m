function ds  = dShapeLine2(~)
%dShapeLine2 Calculate derivatives of shape functions for a 2-node linear element
% Evaluate shape function derivatives wrt r and s. For 2-node line element,
% these derivatives are constant but the rs entry is passed for consistency
% with the routines for elements with other topologies.
% This undocumented function may be changed or removed in a future release.

%       Copyright 2016 The MathWorks, Inc.

ds = [-0.5, 0.5];
   
%%%% Node numbering %%%%%%
%   1_______________2
%%%%%%%%%%%%%%%%%%%%%%%%%%
   
end

