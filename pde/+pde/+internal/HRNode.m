function obj=HRNode(N)
%HRNode Store r and h entries at a node for a value or dirichlet type BC
% This undocumented function may be changed or removed in a future release.

%       Copyright 2014 The MathWorks, Inc.
obj.r = zeros(N,1);
obj.h = zeros(N);
obj.nodeID=0;
end