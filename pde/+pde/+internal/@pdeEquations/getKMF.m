function [ K,M,F ] = getKMF(self)
% getKMF Calculate the K and M matrices and the F vector for tetrahedral models.
% Models may be composed of 4-node and 10-node tetrahedron elements.
% This undocumented function may be changed or removed in a future release.

%       Copyright 2014 The MathWorks, Inc.

% call builtin functions for actual computation
[K, F] = formGlobalKFTetImpl(self.thePde, self.Points, self.ElemConn, ...
  self.c, self.f, self.u, self.time);
M = formGlobalMTetImpl(self.thePde, self.Points, self.ElemConn, self.a, ...
  self.u, self.time);

end

