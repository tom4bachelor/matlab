classdef(Hidden) pdeBCImpl < handle
  %pdeBCImpl Implementation class for boundary conditions
  %
  % This undocumented function may be changed or removed in a future release.
  
  %       Copyright 2014-2016 The MathWorks, Inc.
  
  properties(SetAccess = protected)
    myPde; % reference to the pde object
    N;
  end
  
  methods(Access=protected)
    function obj=pdeBCImpl(myPde)
      obj.myPde = myPde;
      obj.N = myPde.PDESystemSize;
    end
  end
  
  properties(Constant, Access={?pde.internal.pde2DBCImpl, ?pde.internal.pde3DBCImpl,  ?pde.internal.pde2DBCSfnImpl, ?pde.internal.pdeEquations})
    % enumerations for allowable boundary condition types
    % (used internally by implementation classes)
    neumannBC=1; dirichletBC=2; valueBC=3;
  end
  
  methods(Access=protected)
  
    %
    % Utility, error checking functions. 
    % 
    function checkFuncEvalVecNxN(self, bcType, f, fVal, numFemEdges)
      % Check returned matrices where an N x N matrix is required at each
      % point. We allow the data to be returned as N^2 x numFemEdges or
      % N x N x numFemEdges.
      sz = size(fVal);
      nd = ndims(fVal);
      n = self.N;
      if(nd == 2)
        if(sz(1)~=n^2 || sz(2)~=numFemEdges)
          msg1 = message('pde:pde2DBCImpl:funcValNxN1', func2str(f), ...
            sz(1), sz(2), bcType);
          msg2 = message('pde:pde2DBCImpl:funcValNxN4', n^2, numFemEdges, ...
            n, n, numFemEdges);
          error('pde:pde2DBCImpl:funcValNxN', [msg1.getString() msg2.getString()]);
        end
      elseif(nd == 3)
        if(sz(1)~=self.N || sz(2)~=self.N || sz(3)~=numFemEdges)
          msg1 = message('pde:pde2DBCImpl:funcValNxN2', func2str(f), ...
            sz(1), sz(2), sz(3), bcType);
          msg2 = message('pde:pde2DBCImpl:funcValNxN4', n^2, numFemEdges, ...
            n, n, numFemEdges);
          error('pde:pde2DBCImpl:funcValNxN', [msg1.getString() msg2.getString()]);
        end
      else
        msg1 = message('pde:pde2DBCImpl:funcValNxN3', func2str(f), nd);
        msg2 = message('pde:pde2DBCImpl:funcValNxN4', n^2, numFemEdges, ...
          n, n, numFemEdges);
        error('pde:pde2DBCImpl:funcValNxN', [msg1.getString() msg2.getString()]);
      end
    end
    
    function checkFuncEvalVecN(self, bcType, f, fVal, numFemEdges)
      % Check returned matrices where an N-length vector is required at each
      % point. 
      sz = size(fVal);
      nd = ndims(fVal);
      n = self.N;
      if(nd == 2)
        if(sz(1)~=n || sz(2)~=numFemEdges)
          msg1 = message('pde:pde2DBCImpl:funcValNxN1', func2str(f), ...
            sz(1), sz(2), bcType);
          msg2 = message('pde:pde2DBCImpl:funcValN4', n, numFemEdges);
          error('pde:pde2DBCImpl:funcValN', [msg1.getString() msg2.getString()]);
        end
      else
        msg1 = message('pde:pde2DBCImpl:funcValNxN3', func2str(f), nd);
        msg2 = message('pde:pde2DBCImpl:funcValN4', n, numFemEdges);
        error('pde:pde2DBCImpl:funcValN', [msg1.getString() msg2.getString()]);
      end
    end
        
  end % methods
  
end % classdef

