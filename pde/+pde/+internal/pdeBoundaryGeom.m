classdef(Abstract) pdeBoundaryGeom
  %pdeBoundaryGeom Abstract class for boundary geometry
  %   Boundary conditions may be applied on these types of entities
  % pdeBoundaryGeom properties:
  %   ID - integer that uniquely identifies the geometry entity 

  % This function may be changed or removed in a future release.
  
  properties(Access=protected)
    myGeom;
  end
  
  properties(GetAccess=public)
  % ID unique integer identifying this entity
    ID;
  end
  
  methods
    function id = getId(self)
      id = self.ID;
    end
  end
  
end

