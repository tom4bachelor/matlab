classdef (Sealed) pdeEdge < pde.internal.pdeBoundaryGeom
  %pdeEdge Define a edge on a 2D geometry entity
  % pdeEdge properties:
  %   ID - integer that uniquely identifies the edge 
  
  %       Copyright 2014 The MathWorks, Inc.
  
  methods(Access = ?pde.GeometricModel)
    function self=pdeEdge(geom, id)
      import pde.internal.*
      if(~isa(geom, 'pde.GeometricModel'))
		error(message('pde:pdeEdge:invalidArg'));
      end
      self.myGeom = geom;
      self.ID = id;
    end
  end
  
end

