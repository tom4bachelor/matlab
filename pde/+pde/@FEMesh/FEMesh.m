% pde.FEMesh  Mesh representation for 2D and 3D meshes
%    A FEMesh represents the nodes and elements in a mesh, together with
%    attributes that describe the element size and geometric order.
%    A FEMesh is typically created using the generateMesh method of the 
%    pde.PDEModel class. 
%    
% FEMesh methods:
%    meshToPet      - Convert FEMesh data to P,E,T format
%
% FEMesh properties:
%    Nodes          - The coordinates of the nodes in the mesh
%    Elements       - The element connectivity 
%    MaxElementSize - Target maximum element size used to generate the mesh
%    MinElementSize - Target minimum element size used to generate the mesh
%    GeometricOrder - The geometric order of the elements in the mesh
%
% See also CREATEPDE, pde.PDEModel, pde.PDEModel/generateMesh

% Copyright 2014-2015 The MathWorks, Inc.
  
%{
properties
    % Nodes          - The coordinates of the nodes in the mesh 
    %    Nodes is a matrix of size Mdim-by-Nn where Mdim is the number of 
    %    dimensions, 2 <= Mdim <= 3. Nn is the number of nodes in the mesh.
    %    The i-th column of Nodes represents the coordinates of the i-th node.
        Nodes;

    % Elements       - The element connectivity     
    %    Elements is a matrix of size Mn-by-Ne, where Mn is the number of nodes
    %    per element and Ne is the number of elements in the mesh.
    %    The i-th column of Elements represents the i-th element in the mesh.
    %    The entries in the i-th column are the node IDs, the column numbers
    %    of Nodes.
        Elements; 

    % MaxElementSize - Target maximum element size used to generate the mesh
        MaxElementSize;  

    % MinElementSize - Target minimum element size used to generate the mesh    
        MinElementSize;

    % GeometricOrder - The geometric order of the elements in the mesh 
    %   The GeometricOrder is one of the following:
    %   'linear'      The geometric order of the elements is linear
    %   'quadratic'   The geometric order of the elements is quadratic
        GeometricOrder;    
end
%}

