% meshToPet  Convert FEMesh data to P,E,T format
%    [P,E,T] = meshToPet(MSH) Converts the FEMesh data contained in MSH
%    into P,E,T format defined in INITMESH. In the P,E,T format, P
%    represents the coordinates of the node points, E represents the
%    mesh-to-geometry associativity and T represents the elements that
%    make up the triangulation. See INITMESH for further information.
%    
%    See also INITMESH

% Copyright 2014-2015 The MathWorks, Inc.