% pde.GeometricModel  Geometric representation of model boundary
%   pde.GeometricModel is the superclass of all geometry classes in the PDE
%   toolbox. The PDE toolbox supports different geometry formats; namely, 
%   DECSG and PDEGEOM for 2D, and STL geometry for 3D. The object-based 
%   representation unifies these different types to promote consistency 
%   across the toolbox. For example, the DECSG and PDEGEOM format can be
%   represented by a pde.AnalyticGeometry and the 3D STL can be represented 
%   by a pde.DiscreteGeometry. Both of these types are geometric models, 
%   they share the same superclass, that is pde.GeometricModel
%
% See also pde.AnalyticGeometry, pde.DiscreteGeometry, pde.PDEModel

% Copyright 2014 The MathWorks, Inc.