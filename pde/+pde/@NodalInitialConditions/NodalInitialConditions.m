classdef (Sealed) NodalInitialConditions  < pde.PDEInitialConditions & matlab.mixin.internal.CompactDisplay 
% NodalInitialConditions Initial Conditions specified at mesh nodes
%     Initial conditions (ICs) for time dependent problems or the initial guess 
%     for nonlinear stationary problems. The PDE toolbox can solve equations 
%     of the form:
%    
%                m*d^2u/dt^2 + d*du/dt - div(c*grad(u)) + a*u = f
%  
%     The equation to solve is defined in terms of the coefficients m, d, c, 
%     a, f. If the PDE to solve has a non-zero 'm' or 'd' coefficient, then
%     the PDE is time-dependent and Initial Conditions must be specified. 
%     If the 'm' and 'd' coefficients are both zero and the 'c', 'a', or 'f'   
%     coefficient are a function of u or grad(u), then the PDE is a nonlinear 
%     stationary problem. An optional initial guess can be specified, otherwise 
%     a default initial guess of zero is assumed.
%  
%     Instances of this class can only be created by calling the 
%     setInitialConditions method of the PDEModel class. 
%  
%   See also pde.PDEModel, pde.PDEModel/setInitialConditions
 
% Copyright 2016 The MathWorks, Inc.


properties (SetAccess='private')

% InitialValue - The initial value or guess 
%    The initial value for time-dependent problems or the initial-guess for 
%    stationary nonlinear problems. The initial value is a
%    NumNodes-by-NumPDEs matrix, where NumNodes is the number of nodes
%    in the mesh and NumPDEs is the number of PDEs in the system.
    InitialValue;

% InitialDerivative	- The initial derivative	
%     The value of the initial derivative for time-dependent problems.
      
    InitialDerivative;
end

methods(Hidden=true, Access={?pde.PDEModel})
    % icr [varargin{1}, {timeindex}];
    function obj=NodalInitialConditions(varargin) 
      narginchk(2,4);            
      icr = varargin{1};
      results = varargin{2};
      if ~(isa(results, 'pde.StationaryResults') || isa(results, 'pde.TimeDependentResults'))
         error(message('pde:pdeInitialConditions:invalidResultsObject'));    
      end     
      systemsize = icr.ParentPdemodel.PDESystemSize;
      numnodes=0;
      if ~isempty(icr.ParentPdemodel.Mesh)
        numnodes = size(icr.ParentPdemodel.Mesh.Nodes,2);
      end
      
      timeindex = [];
      mcoefdefined = false;
      
      if numel(varargin) == 3
        mcoefdefined = varargin{3};       
      end  
      
      if numel(varargin) == 4
          timeindex = varargin{3};   
        mcoefdefined = varargin{4};       
      end        
      
      if isa(results, 'pde.StationaryResults')
        if ~isempty(timeindex)
            error(message('pde:pdeInitialConditions:timeIndexStationaryResult'));   
        end
        obj.InitialValue =  results.NodalSolution;                
      else 
        if isempty(timeindex)
            if results.PDESystemSize == 1
                timeindex = size(results.NodalSolution,2);
            else
                timeindex = size(results.NodalSolution,3);
            end
        else
            pde.NodalInitialConditions.validateTimeIndex(timeindex, size(results.NodalSolution));  
        end
        
        if results.PDESystemSize == 1
           obj.InitialValue =  results.NodalSolution(:,timeindex); 
        else
           obj.InitialValue =  squeeze(results.NodalSolution(:,:,timeindex)); 
        end
        
        if mcoefdefined && isempty(results.NodalTimeDerivative)
             error(message('pde:pdeInitialConditions:noTimeDerivativeInResults')); 
        end
        
        if ~isempty(results.NodalTimeDerivative)
            if results.PDESystemSize == 1
                obj.InitialDerivative =  results.NodalTimeDerivative(:,timeindex);
            else
                obj.InitialDerivative =  squeeze(results.NodalTimeDerivative(:,:,timeindex));
            end
            obj.checkIcSize(obj.InitialDerivative, systemsize, numnodes);
        end
        
      end
      obj.RecordOwner = icr;         
      obj.checkIcSize(obj.InitialValue, systemsize, numnodes);
    end
end

methods            
    function set.InitialValue(self, val)    
      self.precheckIC(val);     
      self.InitialValue = val;     
    end
    
    function set.InitialDerivative(self, val) 
      self.precheckIC(val);
      self.InitialDerivative = val;   
    end        
end

methods(Hidden=true, Access = {?pde.InitialConditionsRecords})                     
    function tf=numericIcValue(self)                
         tf = true;                                        
    end
    function tf=numericIcDerivativeValue(self)                
         tf = true;                                        
    end         
    function performSolverPrecheck(self, systemsize, numnodes)        
         validateICs(self, systemsize, numnodes);     
    end         
end

methods(Hidden=true)
    function tf = hasICValue(self) 
        tf = ~isempty(self.InitialValue);
    end
    function tf = hasICDerivativeValue(self)
        tf = ~isempty(self.InitialDerivative);
    end     
end

methods(Hidden=true, Access = private) 
    %
    % validateICs - check the size of the IC matrices
    % 
    function validateICs(self, systemsize, numnodes)
       if isempty(self.InitialValue)
         error(message('pde:pdeInitialConditions:emptyICValue'));      
       end      
       self.checkIcSize(self.InitialValue, systemsize, numnodes);
       if hasICDerivativeValue(self)
            self.checkIcSize(self.InitialDerivative, systemsize, numnodes);           
       end
    end            
end


methods(Static, Hidden=true)
    function preDelete(self,~)
        if isvalid(self.RecordOwner)
            self.RecordOwner.delistPDEInitialConditions(self);
        end
    end
end

methods(Static, Access = private)
                
    function precheckIC(icval)   
      if isempty(icval)          
          return
      end
      if isfloat(icval)          
        if issparse(icval)
            error(message('pde:pdeInitialConditions:invalidICValueSparse'));   
        elseif any(isnan(icval))
            error(message('pde:pdeInitialConditions:invalidICValueNaN'));
        elseif any(isinf(icval))
            error(message('pde:pdeInitialConditions:invalidICValueInf'));                 
        end
      end
    end
    function checkIcSize(icval, systemsize, numnodes)  
         pde.NodalInitialConditions.precheckIC(icval);
         if (size(icval,1) ~= numnodes)
            error(message('pde:pdeInitialConditions:nodalMismatch'));   
         end    
         if (size(icval,2) ~= systemsize)
            error(message('pde:pdeInitialConditions:sysSizeMismatch'));   
         end                              
    end    
    function validateTimeIndex(timeindex, ressize)
        validateattributes(timeindex,{'numeric'},{'integer','positive', 'nonzero','real', 'nonsparse'});         
        if (numel(ressize) == 2 && (timeindex > ressize(2)) || ...
            numel(ressize) == 3 && (timeindex > ressize(3)))           
            error(message('pde:pdeInitialConditions:outOfRangeT'));     
        end
    end                      
end    
   properties (Hidden = true, Access='private')
      RecordOwner;
   end   
end
