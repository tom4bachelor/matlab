classdef (Sealed) ThermalBCRecords  < handle & matlab.mixin.internal.CompactDisplay
% ThermalBCRecords  Assignments of the thermal boundary conditions
%    The ThermalBCRecords holds a record of all the assignments of thermal
%    boundary conditions across the geometric boundary. This may be a single
%    assignment that represents the same boundary conditions throughout the 
%    entire boundary or multiple assignments where the boundary conditions 
%    are different across various boundaries. 
%
% ThermalBCRecords methods:
%    findThermalBC  - Find thermal boundary condition assignment for a boundary
%
% BoundaryConditionRecords properties:
%    ThermalBCAssignments - Vector of boundary condition assignments
%
% See also pde.ThermalModel, pde.ThermalModel/thermalBC

% Copyright 2016 The MathWorks, Inc.

properties
    
% ThermalBCAssignments - Vector of thermal boundary condition assignments
%    A vector containing the thermal boundary condition assignments to the
%    boundaries. Each entry in the vector is a pde.ThermalBC object. This
%    object defines the boundary condition of a thermal model. Values at
%    boundaries and host boundaries are defined using the thermalBC method
%    of the ThermalModel.
    ThermalBCAssignments;
end

methods
   % Method declaration
   bc = findThermalBC(self, varargin)   
end

methods (Hidden = true, Access = private)
    tf = thermalBCSpecified(self, varargin)  
end

methods(Hidden=true)
    function obj=ThermalBCRecords(tpdem)
       obj.ThermalBCAssignments = [];
       obj.ParentPdemodel = tpdem;
    end 
end

methods(Hidden=true, Access={?pde.ThermalModel, ?pde.EquationModel, ?pde.PDEResults, ?pde.ThermalBC})
    function bcArray = packBoundaryConditions(self)
        if isempty(self)
            bcArray = [];
            return
        end
        if self.ParentPdemodel.IsTwoD
            nbc = self.ParentPdemodel.Geometry.NumEdges;
            region = 'edge';
        else
            nbc = self.ParentPdemodel.Geometry.NumFaces;
            region = 'face';
        end
        tf = thermalBCSpecified(self, region, 1:nbc);
        allbc = 1:nbc;
        nbctrue = allbc(tf);
        bc = pde.ThermalBC(self,'HeatFlux',0,region,1);
        bcArray = repmat(bc,nbc,1);
        for i = 1:length(nbctrue)
            bc = self.findThermalBC(region,nbctrue(i));
            bc = translateThermalBCToGeneric(self,bc);
            bcArray(nbctrue(i)) = bc;
        end
    end
    
     function bc = translateThermalBCToGeneric(self,bc)
            if ~isempty(bc.Temperature)
                bc.r = bc.Temperature;
                return
            end
            
            % Default zeros.
            Qn = 0;
            htc = 0;
            emiss = 0;
            Tambient = 0;
            SBC = 0;
            
            if ~isempty(bc.HeatFlux)
                Qn = bc.HeatFlux;
            end
            
            if ~isempty(bc.ConvectionCoefficient)
                if isempty(bc.AmbientTemperature)
                    error(message('pde:ThermalModel:missingAmbientTConvectionBC'));
                end
                htc = bc.ConvectionCoefficient;
            end
            
            if ~isempty(bc.Emissivity)
                if isempty(bc.AmbientTemperature)
                    error(message('pde:ThermalModel:missingAmbientTRadiationBC'));
                end
                
                if isempty(self.ParentPdemodel.StefanBoltzmannConstant)
                    error(message('pde:ThermalModel:missingSBCRadiationBC'));
                end
                
                emiss = bc.Emissivity;
            end
            
            if ~isempty(bc.AmbientTemperature)
                Tambient = bc.AmbientTemperature;
            end
            
            if ~isempty(self.ParentPdemodel.StefanBoltzmannConstant)
                SBC = self.ParentPdemodel.StefanBoltzmannConstant;
            end
            
            isFuncQn = isa(Qn, 'function_handle');
            isFuncHtc = isa(htc,'function_handle');
            isFuncEmiss = isa(emiss,'function_handle');
            qVal = htc; %q-value is always htc.
            % Define the g-value based on inputs. 
            if(~isFuncQn && ~isFuncHtc  && ~isFuncEmiss)
                if isempty(bc.Emissivity)
                    gVal =                 Qn               + htc          *Tambient;
                else
                    gVal =@(region,state)  Qn               + htc          *Tambient + emiss              *SBC*(Tambient.^4-state.u.^4);
                end
            elseif(~isFuncQn &&  isFuncHtc  && ~isFuncEmiss)
                gVal = @(region,state) Qn               + htc(region,state)*Tambient + emiss              *SBC*(Tambient.^4-state.u.^4);
            elseif( isFuncQn && ~isFuncHtc  && ~isFuncEmiss)
                gVal = @(region,state) Qn(region,state) + htc              *Tambient + emiss              *SBC*(Tambient.^4-state.u.^4);
            elseif( isFuncQn &&  isFuncHtc  && ~isFuncEmiss)
                gVal = @(region,state) Qn(region,state) + htc(region,state)*Tambient + emiss              *SBC*(Tambient.^4-state.u.^4);
            elseif(~isFuncQn && ~isFuncHtc  &&  isFuncEmiss)
                gVal = @(region,state) Qn               + htc              *Tambient + emiss(region,state)*SBC*(Tambient.^4-state.u.^4);
            elseif(~isFuncQn &&  isFuncHtc  &&  isFuncEmiss)
                gVal = @(region,state) Qn               + htc(region,state)*Tambient + emiss(region,state)*SBC*(Tambient.^4-state.u.^4);
            elseif( isFuncQn && ~isFuncHtc  &&  isFuncEmiss)
                gVal = @(region,state) Qn(region,state) + htc              *Tambient + emiss(region,state)*SBC*(Tambient.^4-state.u.^4);
            elseif( isFuncQn &&  isFuncHtc  &&  isFuncEmiss)
                gVal = @(region,state) Qn(region,state) + htc(region,state)*Tambient + emiss(region,state)*SBC*(Tambient.^4-state.u.^4);
            end

            bc.q = qVal;
            bc.g = gVal;
     end
    
end
  
methods(Hidden=true, Access={?pde.ThermalBC,?pde.ThermalModel})
    function delistBoundaryConditions(self, bctodelete)
        numbc = numel(self.ThermalBCAssignments);
        for i = 1:numbc
            thisbc = self.ThermalBCAssignments(i);
            if thisbc == bctodelete
                self.ThermalBCAssignments(i) = [];
                break
            end
        end  
        numbc = numel(self.ThermalBCAssignments);
        if numbc == 0 && ~isempty(self.ParentPdemodel) && isvalid(self.ParentPdemodel)
           self.ParentPdemodel.delistBoundaryConditions(); 
        end
    end 
end

methods(Hidden=true)
    function delete(self)
        numic = numel(self.ThermalBCAssignments);
        for i = 1:numic
            if isvalid(self.ThermalBCAssignments(i))           
                delete(self.ThermalBCAssignments(i));
            end
        end   
               
        if isvalid(self.ParentPdemodel)
            self.ParentPdemodel.delistBoundaryConditions();
        end        
    end    
end

properties (Hidden = true, Access={?pde.ThermalModel,?pde.ThermalBC})
    ParentPdemodel;
end  
  
end
