function tf = thermalBCSpecified(self, varargin)  
 % thermalBCSpecified True if boundary conditions are specified for domain or subdomain

% Copyright 2016 The MathWorks, Inc.

narginchk(3,3);  

if isempty(self.ParentPdemodel.Geometry)
    return;
end

parser = inputParser;
parser.addParameter('face', [], @pde.EquationModel.isValidEntityID); 
parser.addParameter('edge', [], @pde.EquationModel.isValidEntityID);  
parser.parse(varargin{:});

if ~ismember('face', parser.UsingDefaults)
    qregiontype = 'face';
    qregionid = parser.Results.face;
    maxid = self.ParentPdemodel.Geometry.NumFaces;       
    if any(qregionid > maxid)
       error(message('pde:pdeModel:invalidFaceIndex'));
    end 
    if self.ParentPdemodel.IsTwoD
      error(message('pde:thermalBCSpecification:noBoundaryQueryOnFace2D')); 
    end
else
    qregiontype = 'edge';
    qregionid = parser.Results.edge;
    maxid = self.ParentPdemodel.Geometry.NumEdges;
    if any(qregionid > maxid)
       error(message('pde:pdeModel:invalidEdgeIndex'));
    end 
    if ~self.ParentPdemodel.IsTwoD
      error(message('pde:thermalBCSpecification:noBoundaryQueryOnEdge3D'));  
    end
end

numassigns = numel(self.ThermalBCAssignments);
numqueries = numel(qregionid);
tf = false(size(qregionid)); 
for i = 1:numqueries
    rid = qregionid(i);
    for j = numassigns:-1:1
       thisbc =  self.ThermalBCAssignments(j);       
       if strcmpi(qregiontype,thisbc.RegionType) && ismember(rid, thisbc.RegionID)       
          tf(i) = true;
          break
       end
    end
end

end