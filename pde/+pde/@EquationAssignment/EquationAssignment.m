classdef EquationAssignment  < handle
% Abstract base class for representing equation assignments to (sub)domains.
%
% See also pde.PDEModel, pde.PDEModel/specifyCoefficients

% Copyright 2015 The MathWorks, Inc.
       
  methods(Hidden=true)   
    function obj=EquationAssignment()       
    end   
  end
end
