classdef (AllowedSubclasses = {?pde.GeometricInitialConditions, ?pde.NodalInitialConditions,?pde.NodalThermalICs, ?pde.GeometricThermalICs}) PDEInitialConditions  < handle & matlab.mixin.Heterogeneous
% Abstract base class for representing Initial Conditions
%
%   See also pde.PDEModel, pde.PDEModel/setInitialConditions
 
% Copyright 2015-2016 The MathWorks, Inc.
 
methods(Hidden=true)   
   function obj=PDEInitialConditions()       
   end
end
  
end
