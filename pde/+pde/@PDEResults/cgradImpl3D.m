function [cdudx, cdudy, cdudz] = cgradImpl3D(obj,c,dudx,dudy,dudz)
%cgradImpl3D - Internal function for evaluating c-grad for a 3-D problem.
%
% Copyright 2016 The MathWorks, Inc.

N = obj.PDESystemSize;
dim = pde.PDEResults.spatialDimension(obj);
dimHalf = dim*(dim+1)/2;
d2 = dim*dim;
numrowsC = size(c,1);


cdudx = zeros(size(dudx));
cdudy = zeros(size(dudy));
cdudz = zeros(size(dudz));


% Evaluate c-grad depending on the forms of c-coefficient.
switch numrowsC
    case 1
        for k=1:N
            cdudx(k,:)=c.*dudx(k,:);
            cdudy(k,:)=c.*dudy(k,:);
            cdudz(k,:)=c.*dudz(k,:);
        end
    case dim
        for k=1:N
            cdudx(k,:)=c(1,:).*dudx(k,:);
            cdudy(k,:)=c(2,:).*dudy(k,:);
            cdudz(k,:)=c(3,:).*dudz(k,:);
        end
    case dimHalf
        for k=1:N
            cdudx(k,:)=c(1,:).*dudx(k,:)+c(2,:).*dudy(k,:)+c(4,:).*dudz(k,:);
            cdudy(k,:)=c(2,:).*dudx(k,:)+c(3,:).*dudy(k,:)+c(5,:).*dudz(k,:);
            cdudz(k,:)=c(4,:).*dudx(k,:)+c(5,:).*dudy(k,:)+c(6,:).*dudz(k,:);
        end
    case d2
        for k=1:N
            cdudx(k,:)=c(1,:).*dudx(k,:)+c(4,:).*dudy(k,:)+c(7,:).*dudz(k,:);
            cdudy(k,:)=c(2,:).*dudx(k,:)+c(5,:).*dudy(k,:)+c(8,:).*dudz(k,:);
            cdudz(k,:)=c(3,:).*dudx(k,:)+c(6,:).*dudy(k,:)+c(9,:).*dudz(k,:);
        end
    case N
        for k=1:N
            cdudx(k,:)=c(1+1*(k-1),:).*dudx(k,:);
            cdudy(k,:)=c(1+1*(k-1),:).*dudy(k,:);
            cdudz(k,:)=c(1+1*(k-1),:).*dudz(k,:);
        end
    case dim*N
        for k=1:N
            cdudx(k,:)=c(1+3*(k-1),:).*dudx(k,:);
            cdudy(k,:)=c(2+3*(k-1),:).*dudy(k,:);
            cdudz(k,:)=c(3+3*(k-1),:).*dudz(k,:);
        end
    case dimHalf*N
        for k=1:N
            cdudx(k,:)=c(1+6*(k-1),:).*dudx(k,:)+c(2+6*(k-1),:).*dudy(k,:)+c(4+6*(k-1),:).*dudz(k,:);
            cdudy(k,:)=c(2+6*(k-1),:).*dudx(k,:)+c(3+6*(k-1),:).*dudy(k,:)+c(5+6*(k-1),:).*dudz(k,:);
            cdudz(k,:)=c(4+6*(k-1),:).*dudx(k,:)+c(5+6*(k-1),:).*dudy(k,:)+c(6+6*(k-1),:).*dudz(k,:);
        end
    case d2*N
        for k=1:N
            cdudx(k,:)=c(1+9*(k-1),:).*dudx(k,:)+c(4+9*(k-1),:).*dudy(k,:)+c(7+9*(k-1),:).*dudz(k,:);
            cdudy(k,:)=c(2+9*(k-1),:).*dudx(k,:)+c(5+9*(k-1),:).*dudy(k,:)+c(8+9*(k-1),:).*dudz(k,:);
            cdudz(k,:)=c(3+9*(k-1),:).*dudx(k,:)+c(6+9*(k-1),:).*dudy(k,:)+c(9+9*(k-1),:).*dudz(k,:);
        end
    case dim*N*(dim*N+1)/2
        m=1;
        for l=1:N
            for k=1:l-1
                % Fill the 3x3 matrices is upper triangle area.
                cdudx(k,:)=cdudx(k,:)+c(m  ,:).*dudx(l,:)+c(m+3,:).*dudy(l,:)+c(m+6,:).*dudz(l,:);
                cdudy(k,:)=cdudy(k,:)+c(m+1,:).*dudx(l,:)+c(m+4,:).*dudy(l,:)+c(m+7,:).*dudz(l,:);
                cdudz(k,:)=cdudz(k,:)+c(m+2,:).*dudx(l,:)+c(m+5,:).*dudy(l,:)+c(m+8,:).*dudz(l,:);
                % Fill 3x3 matrices is lower triangle area, coefficients are
                % transpose of the respective 3x3 matrix in the upper triangle
                % area.
                cdudx(l,:)=cdudx(l,:)+c(m  ,:).*dudx(k,:)+c(m+1,:).*dudy(k,:)+c(m+2,:).*dudz(k,:);
                cdudy(l,:)=cdudy(l,:)+c(m+3,:).*dudx(k,:)+c(m+4,:).*dudy(k,:)+c(m+5,:).*dudz(k,:);
                cdudz(l,:)=cdudz(l,:)+c(m+6,:).*dudx(k,:)+c(m+7,:).*dudy(k,:)+c(m+8,:).*dudz(k,:);
                m=m+9;
            end
            % Fill the matrices whose diagonals coincides with the diagnal of
            % the full coefficient matrix.
            cdudx(l,:)=cdudx(l,:)+c(m  ,:).*dudx(l,:)+c(m+1,:).*dudy(l,:)+c(m+3,:).*dudz(l,:);
            cdudy(l,:)=cdudy(l,:)+c(m+1,:).*dudx(l,:)+c(m+2,:).*dudy(l,:)+c(m+4,:).*dudz(l,:);
            cdudz(l,:)=cdudz(l,:)+c(m+3,:).*dudx(l,:)+c(m+4,:).*dudy(l,:)+c(m+5,:).*dudz(l,:);
            m=m+6;
        end
    case d2*N*N
        for k=1:N
            for l=1:N
                cdudx(k,:)=cdudx(k,:)+c(1+9*(k-1+N*(l-1)),:).*dudx(l,:)+ ...
                                      c(4+9*(k-1+N*(l-1)),:).*dudy(l,:)+ ...
                                      c(7+9*(k-1+N*(l-1)),:).*dudz(l,:);
                cdudy(k,:)=cdudy(k,:)+c(2+9*(k-1+N*(l-1)),:).*dudx(l,:)+ ...
                                      c(5+9*(k-1+N*(l-1)),:).*dudy(l,:)+ ...
                                      c(8+9*(k-1+N*(l-1)),:).*dudz(l,:);
                cdudz(k,:)=cdudz(k,:)+c(3+9*(k-1+N*(l-1)),:).*dudx(l,:)+ ...
                                      c(6+9*(k-1+N*(l-1)),:).*dudy(l,:)+ ...
                                      c(9+9*(k-1+N*(l-1)),:).*dudz(l,:);
            end
        end
    otherwise
        error(message('pde:pdeCoefficientSpecification:invalidCMatrixSize'))
end
% Transpose to shape aligne shape with the shape of NodalSolution property.
cdudx = cdudx';
cdudy = cdudy';
cdudz = cdudz';

end

