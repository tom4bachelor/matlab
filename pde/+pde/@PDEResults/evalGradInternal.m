function [dudxIntrp, dudyIntrp, dudzIntrp] = evalGradInternal(obj,varargin)
%evalGradInternal - Internal function for evaluating gradients
%
% Copyright 2015-2016 The MathWorks, Inc.

% If no arguments are passed in, then just return gradient properties.
if isempty(varargin)
    dudxIntrp = obj.XGradients;
    dudyIntrp = obj.YGradients;
    dudzIntrp = obj.ZGradients;
    return;
end

%validate number of arguments covering all types of PDE solutions and input
%formats for interpolation.
narginchk(2,6);
%valid number of output arguments
%nargoutchk(2,3);

%find valid number of arguments for p-matrix and xyz formats of inputs
[numargsp, numargsxyz] = pde.PDEResults.validnumargs(obj);
nargs = nargin;
if ((nargs ~= numargsp) && (nargs ~= numargsxyz))
    error(message('pde:PDEResults:invalidNumArgs'));
end

%sanity check for the input arguments
for n =1:nargs-1
    if (~isnumeric(varargin{n}) || ~isreal(varargin{n}) || ...
            isempty(varargin{n}) )
        error(message('pde:PDEResults:invalidArgs'));
    end
end

dim = pde.PDEResults.spatialDimension(obj); % dim = 2 for 2-D and dim = 3 for 3-D problems.

%validate the query point coordinates and store them in qcoords cell array.
if (nargs == numargsp) %p-matrix format of query points
    qcoords = pde.PDEResults.validatePointsMatrix(obj.IsTwoD, varargin{1}    );
else %xyz-arrays format of query points
    qcoords = pde.PDEResults.validateXYZArrays(    obj.IsTwoD, varargin(1:dim));
end

%validate the time-step/mode IDs and equation IDs, if any.
[qtimeID, qeqnID] = pde.PDEResults.validateVectorInputs(obj, varargin);

dudxIntrp = [];
dudyIntrp = [];
dudzIntrp = [];
if isempty(obj.Interpolant)
    return;
end

%interpolate the solution on to query points
dudxIntrpFull = evaluate(obj.InterpolantdUdx, qcoords{:});
dudyIntrpFull = evaluate(obj.InterpolantdUdy, qcoords{:});

% reshape the interpolated solution.
dudxIntrpFullFormatted = pde.PDEResults.reshapePDESolution(dudxIntrpFull, ...
    obj.IsTimeEig, obj.PDESystemSize, obj.NumTimeEig);

dudyIntrpFullFormatted = pde.PDEResults.reshapePDESolution(dudyIntrpFull, ...
    obj.IsTimeEig, obj.PDESystemSize, obj.NumTimeEig);


% select the required solution subset
dudxIntrp = pde.PDEResults.selectPDESolution(obj,dudxIntrpFullFormatted, qtimeID, qeqnID);
dudyIntrp = pde.PDEResults.selectPDESolution(obj,dudyIntrpFullFormatted, qtimeID, qeqnID);

dudzIntrp = []; % Evaluate z-gradient only if the solution is in 3-D sapce. 
if ~obj.IsTwoD
    dudzIntrpFull = evaluate(obj.InterpolantdUdz, qcoords{:});
    dudzIntrpFullFormatted = pde.PDEResults.reshapePDESolution(dudzIntrpFull, ...
        obj.IsTimeEig, obj.PDESystemSize, obj.NumTimeEig);
    dudzIntrp = pde.PDEResults.selectPDESolution(obj,dudzIntrpFullFormatted, qtimeID, qeqnID);
end

end % End of evalGradInternal



