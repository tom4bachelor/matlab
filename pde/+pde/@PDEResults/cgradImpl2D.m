function [cdudx, cdudy] = cgradImpl2D(obj,c,dudx,dudy)
%cgradImpl2D - Internal function for evaluating c-grad for a 2-D problem.
%
% Copyright 2016 The MathWorks, Inc.

N = obj.PDESystemSize;
dim = pde.PDEResults.spatialDimension(obj);
dimHalf = dim*(dim+1)/2;
d2 = dim*dim;
numrowsC = size(c,1);

cdudx = zeros(size(dudx));
cdudy = zeros(size(dudy));


% Evaluate c-grad depending on the forms of c-coefficient.
switch numrowsC
    case 1
        for k=1:N
            cdudx(k,:)=c.*dudx(k,:);
            cdudy(k,:)=c.*dudy(k,:);
        end
    case dim
        for k=1:N
            cdudx(k,:)=c(1,:).*dudx(k,:);
            cdudy(k,:)=c(2,:).*dudy(k,:);
        end
    case dimHalf
        for k=1:N
            cdudx(k,:)=c(1,:).*dudx(k,:)+c(2,:).*dudy(k,:);
            cdudy(k,:)=c(2,:).*dudx(k,:)+c(3,:).*dudy(k,:);
        end
    case d2
        for k=1:N
            cdudx(k,:)=c(1,:).*dudx(k,:)+c(3,:).*dudy(k,:);
            cdudy(k,:)=c(2,:).*dudx(k,:)+c(4,:).*dudy(k,:);
        end
    case N
        for k=1:N
            cdudx(k,:)=c(1+1*(k-1),:).*dudx(k,:);
            cdudy(k,:)=c(1+1*(k-1),:).*dudy(k,:);
        end
    case dim*N
        for k=1:N
            cdudx(k,:)=c(1+2*(k-1),:).*dudx(k,:);
            cdudy(k,:)=c(2+2*(k-1),:).*dudy(k,:);
        end
    case dimHalf*N
        for k=1:N
            cdudx(k,:)=c(1+3*(k-1),:).*dudx(k,:)+c(2+3*(k-1),:).*dudy(k,:);
            cdudy(k,:)=c(2+3*(k-1),:).*dudx(k,:)+c(3+3*(k-1),:).*dudy(k,:);
        end
    case d2*N
        for k=1:N
            cdudx(k,:)=c(1+4*(k-1),:).*dudx(k,:)+c(3+4*(k-1),:).*dudy(k,:);
            cdudy(k,:)=c(2+4*(k-1),:).*dudx(k,:)+c(4+4*(k-1),:).*dudy(k,:);
        end
    case dim*N*(dim*N+1)/2
        m=1;
        for l=1:N
            for k=1:l-1
                cdudx(k,:)=cdudx(k,:)+c(m,:).*dudx(l,:)+c(m+2,:).*dudy(l,:);
                cdudy(k,:)=cdudy(k,:)+c(m+1,:).*dudx(l,:)+c(m+3,:).*dudy(l,:);
                cdudx(l,:)=cdudx(l,:)+c(m,:).*dudx(k,:)+c(m+1,:).*dudy(k,:);
                cdudy(l,:)=cdudy(l,:)+c(m+2,:).*dudx(k,:)+c(m+3,:).*dudy(k,:);
                m=m+4;
            end
            cdudx(l,:)=cdudx(l,:)+c(m,:).*dudx(l,:)+c(m+1,:).*dudy(l,:);
            cdudy(l,:)=cdudy(l,:)+c(m+1,:).*dudx(l,:)+c(m+2,:).*dudy(l,:);
            m=m+3;
        end
    case d2*N*N
        for k=1:N
            for l=1:N
                cdudx(k,:)=cdudx(k,:)+c(1+4*(k-1+N*(l-1)),:).*dudx(l,:)+ ...
                    c(3+4*(k-1+N*(l-1)),:).*dudy(l,:);
                cdudy(k,:)=cdudy(k,:)+c(2+4*(k-1+N*(l-1)),:).*dudx(l,:)+ ...
                    c(4+4*(k-1+N*(l-1)),:).*dudy(l,:);
            end
        end
    otherwise
        error(message('pde:pdeCoefficientSpecification:invalidCMatrixSize'))
end

% Transpose to shape aligne shape with the shape of NodalSolution property.
cdudx = cdudx';
cdudy = cdudy';

end


