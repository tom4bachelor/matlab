function [cdudxIntrp, cdudyIntrp, cdudzIntrp] = evalCGradInternal(obj,nodalCGradX,nodalCGradY,nodalCGradZ,varargin)
%evalCGradInternal - Internal function for evaluating c-grad
%
% Copyright 2016 The MathWorks, Inc.

%valid number of arguments covering all types of PDE solutions and input formats
narginchk(5,9); 

%find valid number of arguments for p-matrix and xyz formats of inputs
[numargsp, numargsxyz] = pde.PDEResults.validnumargs(obj);

nargs = nargin-3; % Skip the three nodal c-grad arguments.
if ((nargs ~= numargsp) && (nargs ~= numargsxyz))
    error(message('pde:PDEResults:invalidNumArgs'));
end

%sanity check for the input arguments
for n =1:nargs-1
    if (~isnumeric(varargin{n}) || ~isreal(varargin{n}) || ...
            isempty(varargin{n}) )
        error(message('pde:PDEResults:invalidArgs'));
    end
end

N = obj.PDESystemSize;
if obj.IsTimeEig
    Nt = obj.NumTimeEig;
else
    Nt =1;
end
Np = size(obj.Mesh.Nodes,2);
dim = pde.PDEResults.spatialDimension(obj); % dim = 2 for 2-D and dim = 3 for 3-D problems.

%validate the query point coordinates and store them in qcoords cell array.
if (nargs == numargsp) %p-matrix format of query points
    qcoords = pde.PDEResults.validatePointsMatrix(obj.IsTwoD, varargin{1}    );
else %xyz-arrays format of query points
    qcoords = pde.PDEResults.validateXYZArrays(    obj.IsTwoD, varargin(1:dim));
end

%validate the time-step/mode IDs and equation IDs, if any.
[qtimeID, qeqnID] = pde.PDEResults.validateVectorInputs(obj, varargin);

cdudxIntrp = [];
cdudyIntrp = [];
cdudzIntrp = [];

if isempty(obj.Interpolant)
    return;
end

% construct interpolants with nodal c-gradients as field.
InterpolantCdUdx   = pde.PDEResults.constructInterpolat(obj.Mesh,...
    reshape(nodalCGradX,[Np*N,Nt]), N, Nt);

InterpolantCdUdy   = pde.PDEResults.constructInterpolat(obj.Mesh,...
    reshape(nodalCGradY,[Np*N,Nt]), N, Nt);

InterpolantCdUdz =[];

if ~obj.IsTwoD
    InterpolantCdUdz   = pde.PDEResults.constructInterpolat(obj.Mesh,...
        reshape(nodalCGradZ,[Np*N,Nt]), N, Nt);
end

%interpolate the solution on to query points
cdudxIntrpFull = evaluate(InterpolantCdUdx, qcoords{:});
cdudyIntrpFull = evaluate(InterpolantCdUdy, qcoords{:});

% reshape the interpolated solution.
cdudxIntrpFullFormatted = pde.PDEResults.reshapePDESolution(cdudxIntrpFull, ...
    obj.IsTimeEig, obj.PDESystemSize, obj.NumTimeEig);

cdudyIntrpFullFormatted = pde.PDEResults.reshapePDESolution(cdudyIntrpFull, ...
    obj.IsTimeEig, obj.PDESystemSize, obj.NumTimeEig);


% select the required solution subset
cdudxIntrp = pde.PDEResults.selectPDESolution(obj,cdudxIntrpFullFormatted, qtimeID, qeqnID);
cdudyIntrp = pde.PDEResults.selectPDESolution(obj,cdudyIntrpFullFormatted, qtimeID, qeqnID);

% Evaluate z-gradient only if the solution is in 3-D sapce. 
if ~obj.IsTwoD
    cdudzIntrpFull = evaluate(InterpolantCdUdz, qcoords{:});
    cdudzIntrpFullFormatted = pde.PDEResults.reshapePDESolution(cdudzIntrpFull, ...
        obj.IsTimeEig, obj.PDESystemSize, obj.NumTimeEig);
    cdudzIntrp = pde.PDEResults.selectPDESolution(obj,cdudzIntrpFullFormatted, qtimeID, qeqnID);
end

end % End of evalCGradInternal



