function pdeu = reshapePDESolution(u,IsTimeEig, PDESystemSize,NumTimeEig)
%reshape the solution
if    (PDESystemSize == 1)
    pdeu = u;
else
    if( ~IsTimeEig )
        pdeu = reshape(u,[], PDESystemSize);
    else
        pdeu = reshape(u,[], PDESystemSize, NumTimeEig);
    end
end

end