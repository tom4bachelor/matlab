function uintrp = selectPDESolution(obj,uintrpFullFormatted, qtimeID, qeqnID)
if  (obj.PDESystemSize == 1)
    if (~obj.IsTimeEig )
        uintrp = uintrpFullFormatted;
    else
        uintrp = uintrpFullFormatted(:,qtimeID);
    end
else
    if ( ~obj.IsTimeEig )
        uintrp = uintrpFullFormatted(:,qeqnID);
    else
        uintrp = uintrpFullFormatted(:,qeqnID,qtimeID);
    end
end

end