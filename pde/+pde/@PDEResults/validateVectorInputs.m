        function [qtimeID, qeqnID] = validateVectorInputs(obj, vectorargs)
            qeqnID  = [];
            qtimeID = [];
            
            if     ( obj.IsTimeEig )
                if (obj.PDESystemSize > 1 )
                    %required to specify two vector arguments
                    if (~isvector(vectorargs{end}) || ~isvector(vectorargs{end-1}) )
                        error(message('pde:PDEResults:nonVectorEqandTimeArgs'));
                    end
                    qeqnID  = vectorargs{end-1};
                    qtimeID = vectorargs{end  };
                else
                    %required to specify one vector argument of time-step IDs
                    if ( ~isvector(vectorargs{end}) )
                        error(message('pde:PDEResults:nonVectorTimeArgs'));
                    end
                    qtimeID = vectorargs{end};
                end
            else
                if (obj.PDESystemSize > 1 )
                    %required to specify one vector argument of equation IDs
                    if (~isvector(vectorargs{end}))
                        error(message('pde:PDEResults:nonVectorEqArgs'));
                    end
                    qeqnID = vectorargs{end};
                else
                    return % Not a system of PDEs or time-dependent/eigenvalue problem
                end
            end
            
            if ~isempty(qeqnID)
                validateattributes(qeqnID, {'numeric'},...
                    {'integer','positive'}, '', 'equation ID');
                % Check for range validity of equation ID
                if ( max(qeqnID) > obj.PDESystemSize )
                    error(message('pde:PDEResults:outofboundEq'));
                end
                
            end
            %
            if ~isempty(qtimeID)
                validateattributes(qtimeID, {'numeric'},...
                    {'integer','positive'}, '', 'time-step ID/ mode ID');
                % Check for range validity of time-step ID
                if ( max(qtimeID) > obj.NumTimeEig )
                    error(message('pde:PDEResults:outofboundTime'));
                end
            end
            
        end