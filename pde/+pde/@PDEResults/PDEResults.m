classdef (Abstract) PDEResults
    % pde.PDEResults PDE solution and its derived quantities
    %   A PDEResults object provides a convenient interface for
    %   postprocessing a PDE Toolbox solution. Create a PDEResults object
    %   using the createPDEResults function. PDEResults also provides the
    %   interpolateSolution method to interpolate the solution at specified
    %   spatial locations.
    %
    % PDEResults properties:   
    %   Mesh                 - Discretization of the domain
    %
    %    See also createPDEResults
    
    % Copyright 2015-2016 The MathWorks, Inc.
        
  
    
  properties(SetAccess = protected, Hidden=true, Abstract=true)
        % NodalSolution - Solution to PDE at nodal locations
        % Solution array which can be conveniently indexed into to extract
        % a sub-array of interest. The shape of NodalSolution depends on
        % the type of PDE and solver settings. It will be a:
        %
        %   column vector - for a single PDE with no time dependency
        %   matrix        - for a single hyperbolic or parabolic problem,
        %                   or a system of elliptic problems, or a single
        %                   eigenvalue problem
        %   3-D array     - for a system of hyperbolic, parabolic, or
        %                   eigenvalue problems
        %
        % The first array dimension of NodalSolution represents node index.
        % The second array dimension represents the time-step or
        % eigenvector index for a single PDE, or the equation index for a
        % system of PDEs. The third array dimension represents the
        % time-step index for a system of time-dependent PDEs, or the
        % eigenvect index for an eigenvalue problem involving a system of
        % PDEs.
        NodalSolution;           
    end
                
    methods
        function obj = PDEResults(varargin)        
             if nargin == 0
                return
             end
             narginchk(2,4);
             pdem = varargin{1};
             u = varargin{2};       
             if (~isa(pdem, 'pde.PDEModel'))
                error(message('pde:PDEResults:invalidPDEModel'))
            end
            
            if ( ~isnumeric(u) || issparse(u) || isrow(u) || isscalar(u) ...
                    || any(isnan(u(:))) || ~any(isfinite(u(:))))
                error(message('pde:PDEResults:invalidSolution'))
            end
            
            if ( ~ismatrix(u) && ~iscolumn(u))
                error(message('pde:PDEResults:invalidSolution'))
            end

            if ~isa(pdem.Mesh, 'pde.FEMesh')
                error(message('pde:pdeplot:UnmeshedPdeModel'))
            end
            
            [IsTimeEig, NumTimeEig] = classifyPDEModel(pdem, u);                       
            obj.Mesh = pde.PDEResults.cloneMesh(pdem.Mesh);
            
            % Metadata classifying the PDEmodel.
            obj.PDESystemSize = pdem.PDESystemSize;
            obj.IsTwoD        = pdem.IsTwoD;
            obj.IsTimeEig     = IsTimeEig; % true if time-dependent or eigenvectors
            obj.NumTimeEig    = NumTimeEig; % number of time-steps or eigenvectors                                                                                                                         
        end
        
        
        function obj = set.Mesh(obj,msh)
            if(~isempty(msh) && ~isa(msh, 'pde.FEMesh'))
                error(message('pde:pdeModel:invalidMesh'));
            end
            obj.Mesh = msh;
        end
       
    end
    properties(SetAccess = private)       
        % Mesh - Discretization of the domain
        % Mesh object containing the node and element data that were used
        % in solving the PDE.
        Mesh;      
    end
    
    methods (Hidden = true, Static = true, Access = protected)                      
         % Method declaration
        [dudxInt, dudyInt, dudzInt] = evalGradInternal(obj,varargin)
        [dudxInt, dudyInt, dudzInt] = evalCGradInternal(obj,nodalCGradX,nodalCGradY,nodalCGradZ,varargin)
        uinterp = interpolateSolutionInternal(obj,varargin)  
    end
    
    methods (Hidden = true, Static = true, Access = protected)        
        function obj = loadobj(obj)
            isderived = isa(obj, 'pde.StationaryResults') || ...
                        isa(obj, 'pde.EigenResults') || ...
                        isa(obj, 'pde.TimeDependentResults');
            if ~isderived 
                % Translate to stationary                               
                statobj = pde.StationaryResults();
                statobj.Mesh = obj.Mesh;
                statobj.NodalSolution = obj.NodalSolution;
                statobj.PDESystemSize = obj.PDESystemSize;
                statobj.IsTwoD        = obj.IsTwoD;
                statobj.IsTimeEig     = obj.IsTimeEig;
                statobj.NumTimeEig    = obj.NumTimeEig;                      
                obj = statobj;
            end                           
            obj.Interpolant = pde.PDEResults.constructInterpolat(obj.Mesh, ...
                   obj.NodalSolution, obj.PDESystemSize, obj.NumTimeEig);     
        end
        
        function resultsmsh = cloneMesh(msh)
            %Create an independent FEMesh object that has shared copies of the properties.
            p           = msh.Nodes;
            t           = msh.Elements;
            Hmax        = msh.MaxElementSize;
            Hmin        = msh.MinElementSize;
            geomOrder   = msh.GeometricOrder;
            assoc       = msh.MeshAssociation;
            resultsmsh  = pde.FEMesh(p, t, Hmax, Hmin, geomOrder, assoc);
        end
        
        function Interpolant = constructInterpolat(mesh, NodalValues, ...
                PDESystemSize, NumTimeEig)
            Interpolant = [];
            if isempty(NodalValues)
                return;
            end
            if (PDESystemSize ==1)
                u = NodalValues;
            else
                numnodes = size(mesh.Nodes,2);
                u = reshape(NodalValues, numnodes*PDESystemSize, NumTimeEig);
            end
            [p,~,t] = meshToPet(mesh);
            Interpolant = pdeInterpolant(p,t,u);
        end
        
        %Private methods declaration
        pdeu = reshapePDESolution(u,IsTimeEig, PDESystemSize,NumTimeEig)
        [numargsp, numargsxyz]  = validnumargs(obj)
        qcoords = validatePointsMatrix(IsTwoD, pQuery)
        qcoords = validateXYZArrays(IsTwoD, xyzdata)
        [qtimeID, qeqnID] = validateVectorInputs(obj, vectorargs)
        uintrp = selectPDESolution(obj,uintrpFullFormatted, qtimeID, qeqnID)
        dim = spatialDimension(obj)
        cCoeffAtNodes = evalCCoeffAtNodes(obj,varargin)
        [cdudx, cdudy] = cgradImpl2D(obj,c,dudx,dudy)
        [cdudx, cdudy, cdudz] = cgradImpl3D(obj,c,dudx,dudy,dudz)
        
    end
    
    properties (Hidden = true, SetAccess=protected)
        PDESystemSize;
        IsTwoD;
        IsTimeEig;
        NumTimeEig;
        Coefficients;
    end
    
    properties (Hidden = true, Transient=true)
        Interpolant;                              
        InterpolantdUdx;
        InterpolantdUdy;
        InterpolantdUdz;
    end
    
end


% clasify the type of PDE using the solution and PDEModel data.
function  [IsTimeEig, NumTimeEig] = classifyPDEModel(thepde, thesol)

numNodes = size(thepde.Mesh.Nodes,2);
N = thepde.PDESystemSize;

IsTimeEig = false;
NumTimeEig = [];
if isempty(thesol)
    return
end
if ( mod(numel(thesol), (numNodes*N)) ~= 0) % mismatch between the mesh and solution
    error(message('pde:PDEResults:modelSolMismatch'));
end

if (iscolumn(thesol) && (size(thesol,1) ~= numNodes*N))
    error(message('pde:PDEResults:modelSolMismatch'));
end

if (ismatrix(thesol) && ~iscolumn(thesol))
    if (size(thesol,1)== numNodes*N)
        IsTimeEig  = true;
        NumTimeEig = size(thesol,2);
    else
        error(message('pde:PDEResults:modelSolMismatch'));
    end
end

end