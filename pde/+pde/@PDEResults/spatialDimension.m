function dim = spatialDimension(obj)
if obj.IsTwoD
    dim = 2;
else
    dim = 3;
end
end