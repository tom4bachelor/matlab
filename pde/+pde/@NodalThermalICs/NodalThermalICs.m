classdef (Sealed) NodalThermalICs  < pde.PDEInitialConditions & matlab.mixin.internal.CompactDisplay 
% NodalInitialConditions Initial Temperature specified at mesh nodes
%     Initial condition (IC), temperature, for transient thermal problem or
%     the initial guess for nonlinear thermal problem. The thermal model in
%     PDE toolbox case solve equation of the form:
%    
%       (MassDensity*SpecificHeat)*dT/dt - div(ThermalConductivity*grad(T)) = HeatSource
%  
%     The equation to solve is defined in terms of the coefficients m, d, c, 
%     a, f. If the thermal model is set up as transient and valid non-zero
%     MassDensity and SpecificHeat are provided, then Initial Conditions
%     must be specified. Alternatively, if thermal model is set up to solve
%     steady state problem, however, if the ThermalConductivity and/or
%     HeatSource happens to be function of temperature then initial guess
%     for temperature must be provided as Initial Condition the the model.
%     If no initial guess is provided for a nonlinear steady state thermal
%     problem, then a default initial guess of zero is assumed.
%  
%     Instances of this class can only be created by calling the 
%     thermalIC method of the ThermalModel class. 
%  
%   See also pde.ThermalModel, pde.ThermalModel/thermalIC
 
% Copyright 2016 The MathWorks, Inc.


properties (SetAccess='private')

% InitialTemperature - The initial temperature or guess 
%    The initial temperature for a transient thermal problem or the initial-guess for 
%    steady state thermal nonlinear problems. The initial temperature is a
%    NumNodes-by-1 vector, where NumNodes is the number of nodes
%    in the mesh.
    InitialTemperature;

end


methods(Hidden=true, Access={?pde.ThermalModel})
    function obj=NodalThermalICs(varargin)
        narginchk(2,4);
        icr = varargin{1};
        results = varargin{2};
        if ~(isa(results, 'pde.SteadyStateThermalResults') || isa(results, 'pde.TransientThermalResults'))
            error(message('pde:thermalInitialConditions:invalidResultsObject'));
        end
        systemsize = 1;
        numnodes=0;
        if ~isempty(icr.ParentPdemodel.Mesh)
            numnodes = size(icr.ParentPdemodel.Mesh.Nodes,2);
        end
        
        timeindex = [];
        
        if numel(varargin) == 3
            timeindex = varargin{3};
        end
        
        if isa(results, 'pde.SteadyStateThermalResults')
            if ~isempty(timeindex)
                error(message('pde:thermalInitialConditions:timeIndexStationaryResult'));
            end
            obj.InitialTemperature =  results.Temperature;
        else
            if isempty(timeindex)
                timeindex = size(results.Temperature,2);
            else
                pde.NodalThermalICs.validateTimeIndex(timeindex, size(results.Temperature));
            end
            
            obj.InitialTemperature =  results.Temperature(:,timeindex);
        end
        obj.RecordOwner = icr;
        obj.checkIcSize(obj.InitialTemperature, systemsize, numnodes);
    end
end

methods            
    function set.InitialTemperature(self, val)    
      self.precheckIC(val);     
      self.InitialTemperature = val;     
    end
   
end

methods(Hidden=true, Static = true, Access = {?pde.ThermalICRecords})                     
    function tf=numericIcTemperature()
        % Nodal initial conditions are always numeric.
         tf = true;                                        
    end
end

methods(Hidden=true, Access = {?pde.ThermalICRecords})
    
    function performSolverPrecheck(self, systemsize, numnodes)        
         validateICs(self, systemsize, numnodes);     
    end         
end

methods(Hidden=true)
    function tf = hasICValue(self) 
        tf = ~isempty(self.InitialTemperature);
    end
  
end

methods(Hidden=true, Access = private) 
    %
    % validateICs - check the size of the IC matrices
    % 
    function validateICs(self, systemsize, numnodes)
       if isempty(self.InitialTemperature)
         error(message('pde:pdeInitialConditions:emptyICValue'));      
       end      
       self.checkIcSize(self.InitialTemperature, systemsize, numnodes);
    end            
end


methods(Static, Access = private)
                
    function precheckIC(icval)   
      if isempty(icval)          
          return
      end
      if isfloat(icval)          
        if issparse(icval)
            error(message('pde:thermalInitialConditions:invalidICValueSparse'));   
        elseif any(isnan(icval))
            error(message('pde:thermalInitialConditions:invalidICValueNaN'));
        elseif any(isinf(icval))
            error(message('pde:thermalInitialConditions:invalidICValueInf'));                 
        elseif any(~isreal(icval))
            error(message('pde:thermalInitialConditions:invalidICValueComplex'));                 
        end
      end
    end
    
    function checkIcSize(icval, systemsize, numnodes)  
         pde.NodalThermalICs.precheckIC(icval);
         if (size(icval,1) ~= numnodes)
            error(message('pde:thermalInitialConditions:nodalMismatch'));   
         end    
         if (size(icval,2) ~= systemsize)
            error(message('pde:thermalInitialConditions:sysSizeMismatch'));   
         end                              
    end  
    
    function validateTimeIndex(timeindex, ressize)
        validateattributes(timeindex,{'numeric'},{'integer','positive', 'nonzero','real', 'nonsparse'});         
        if (numel(ressize) == 2 && (timeindex > ressize(2)))           
            error(message('pde:pdeInitialConditions:outOfRangeT'));     
        end
    end   
    
end    

methods(Static, Hidden=true)
    function delete(self,~)
        if isvalid(self.RecordOwner)
            self.RecordOwner.delistThermalICAssignments(self);
        end
    end
end


properties (Hidden = true, Access='private')
      RecordOwner;
   end   
end

