function tf = globalCoefficients(self, varargin)  
% globalCoefficients  True if consistent across the geometric domain
%    tf = globalCoefficients(CC) returns TRUE if all coefficients are 
%    consistent across the geometric domain, otherwise returns FALSE. 
%  
%    tf = globalCoefficients(CC, COEFNAME) returns TRUE if the coefficient of
%    type COEFNAME is consistent across the geometric domain, otherwise 
%    returns FALSE.
%  
%   See also  pde.PDEModel/specifyCoefficients

% Copyright 2015-2016 The MathWorks, Inc.

narginchk(1,2); 
tf = true;
% numassigns = numel(self.CoefficientAssignments);
% if numassigns == 0
%     tf = false;
%     return;
% end

if ~self.coefficientsSpecified()   
    tf = false;
    return;
end

if self.ParentPdemodel.Geometry.NumCells==1
    % 3-D geometry with one cell
    return;
elseif self.ParentPdemodel.Geometry.NumFaces==1
    % 2-D geometry with one face
    return;
end

if self.ParentPdemodel.IsTwoD
    qregiontype = 'face';
    nRegion = self.ParentPdemodel.Geometry.NumFaces;
else
    qregiontype = 'cell';
    nRegion = self.ParentPdemodel.Geometry.NumCells;
end



% if isempty(self.ParentPdemodel.Geometry)
%     % Coefficients defined, but no geometry
%     return;
% end



if nargin == 2
   coefname =  varargin{1};
   iscoef = false;
   if (ischar(coefname) && numel(coefname)==1)
       iscoef = (strcmp(coefname, 'm') ||  strcmp(coefname, 'd') || ...
                 strcmp(coefname, 'c') || strcmp(coefname, 'a') || ...
                 strcmp(coefname, 'f'));
   end
   if ~iscoef
      error(message('pde:pdeCoefficientSpecification:invalidCoefName')); 
   end
end
    



thiscoef = self.findCoefficients(qregiontype, 1);
for i = 2:nRegion
    othercoef = self.findCoefficients(qregiontype, i);           
    if ~thiscoef.sameCoefficients(othercoef, varargin{:})
        tf = false;
        return;
    end            
end   

end