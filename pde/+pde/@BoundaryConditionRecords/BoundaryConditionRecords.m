classdef (Sealed) BoundaryConditionRecords  < handle & matlab.mixin.internal.CompactDisplay
% BoundaryConditionRecords  Assignments of the boundary conditions
%    The BoundaryConditionRecords holds a record of all the assignments of 
%    boundary conditions across the geometric boundary. This may be a single
%    assignment that represents the same boundary conditions throughout the 
%    entire boundary or multiple assignments where the boundary conditions 
%    are different across various boundaries. 
%
% BoundaryConditionRecords methods:
%    findBoundaryConditions  - Find boundary condition assignment for a
%    boundary
%
% BoundaryConditionRecords properties:
%    BoundaryConditionAssignments - Vector of boundary condition assignments
%
% See also pde.PDEModel, pde.PDEModel/applyBoundaryCondition

% Copyright 2016 The MathWorks, Inc.

properties
    
% BoundaryConditionAssignments - Vector of boundary condition assignments
%    A vector containing the boundary condition assignments to the boundaries.
%    Each entry in the vector is a pde.BoundaryCondition object. 
%    This object defines the boundary condition of the PDE. Coefficients and 
%    host boundaries are defined using the applyBoundaryCondition
%    method of the PDEModel.
    BoundaryConditionAssignments;
end

methods
   % Method declaration
   bc = findBoundaryConditions(self, varargin)   
end

methods (Hidden = true, Access = private)
    tf = boundaryConditionsSpecified(self, varargin)  
end

methods(Hidden=true)
    function obj=BoundaryConditionRecords(pdem)
       obj.BoundaryConditionAssignments = [];
       obj.ParentPdemodel = pdem;
    end 
end

methods(Hidden=true, Access={?pde.EquationModel, ?pde.PDEResults, ?pde.BoundaryCondition})
    function bcArray = packBoundaryConditions(self)
        if isempty(self)
            bcArray = [];
            return
        end
        if self.ParentPdemodel.IsTwoD
            nbc = self.ParentPdemodel.Geometry.NumEdges;
            region = 'edge';
        else
            nbc = self.ParentPdemodel.Geometry.NumFaces;
            region = 'face';
        end
        tf = boundaryConditionsSpecified(self, region, 1:nbc);
        allbc = 1:nbc;
        nbctrue = allbc(tf);
        bc = pde.BoundaryCondition(self,'neumann',region,1,'g',0);
        bcArray = repmat(bc,nbc,1);
        for i = 1:length(nbctrue)
            bc = self.findBoundaryConditions(region,nbctrue(i));
            bcArray(nbctrue(i)) = bc;
        end
    end
end
  
methods(Hidden=true, Access={?pde.BoundaryCondition,?pde.EquationModel})
    function delistBoundaryConditions(self, bctodelete)
        numbc = numel(self.BoundaryConditionAssignments);
        for i = 1:numbc
            thisbc = self.BoundaryConditionAssignments(i);
            if thisbc == bctodelete
                self.BoundaryConditionAssignments(i) = [];
                break
            end
        end  
        numbc = numel(self.BoundaryConditionAssignments);
        if numbc == 0 && ~isempty(self.ParentPdemodel) && isvalid(self.ParentPdemodel)
           self.ParentPdemodel.delistBoundaryConditions(); 
        end
    end 
end

methods(Hidden=true)
    function delete(self)
        numic = numel(self.BoundaryConditionAssignments);
        for i = 1:numic
            if isvalid(self.BoundaryConditionAssignments(i))           
                delete(self.BoundaryConditionAssignments(i));
            end
        end   
               
        if isvalid(self.ParentPdemodel)
            self.ParentPdemodel.delistBoundaryConditions();
        end        
    end    
end

properties (Hidden = true, Access=?pde.PDEModel)
%     (Hidden = true, SetAccess='private')
    ParentPdemodel;
end  
  
end
