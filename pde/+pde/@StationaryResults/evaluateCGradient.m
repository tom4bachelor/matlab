function [cdudx, cdudy, cdudz] = evaluateCGradient(obj,varargin)
%evaluateCGradient - Evaluate flux of a stationary PDE solution
%
% [cdudx,cdudy] = evaluateCGradient(R) computes the tensor product of
% c-coefficient and spatial gradients of solution for a 2-D problem. R is
% an object of type StationaryResults. The shape of output arrays, cdudx
% and cdudy, depends on the number of PDEs for which R is the results
% object. The first array dimension of each of the output array represents
% node index. The second array dimension represents the equation index for
% a system of PDEs. The c-coefficients used to obtain the results object R
% are used internally to compute the tensor product.
%
% [cdudx,cdudy,cdudz] = evaluateCGradient(R) evaluates flux for a 3-D
% problem.
%
% [__] = evaluateCGradient(R,QP) evaluates flux for a single PDE with
% results object R at query points QP. Here, QP is a matrix of size
% dim-by-Numq, where dim represents spatial dimensions, dim is 2 for 2-D
% and 3 for 3-D problems, and Numq is the number of query points. The first
% array dimension of each of the output array corresponds to the number of
% spatial points defined by QP.
%
% [__] = evaluateCGradient(R,QP,EQi) evaluates flux for a system of PDEs
% with results object R at the query point locations QP and for specified
% equations EQi. EQi is a scalar or vector of equation indices in the range
% 1 to PDESystemSize, the total number of equation in the system of PDEs.
% The first array dimension of each of the output array corresponds to the
% number of spatial points defined by QP. The second array dimension
% corresponds to equation indices EQi.
%
% [__] = evaluateCGradient(R,xq,yq,__) and [__] =
% evaluateCGradient(R,xq,yq,zq,__) allow the query points QP to be specified
% as coordinate vector or N-D grid. The first array dimension of
% each of the output array corresponds to spatial points defined by
% [x(:),y(:)] in 2-D space or [x(:),y(:),z(:)] in 3-D space.
%
% Example: Compute stresses in a cantilevered beam subject to shear loading
% at free end.
%
%     %Create a PDEModel and import geometry.
%     N = 3;
%     pdem = createpde(N);
%     importGeometry(pdem,'SquareBeam.STL');
%     pdegplot(pdem,'FaceLabels','on');
%
%     %Specify material properties.
%     E = 2.1e11;
%     nu = 0.3;
%     c = elasticityC3D(E, nu);
%     a = 0;
%     f = [0;0;0];
%     specifyCoefficients(pdem,'m',0,'d',0,'c',c,'a',a','f',f);
%
%     % Apply fixed boundary condition on one end of the beam.
%     applyBoundaryCondition(pdem,'dirichlet','Face',6,'u',[0 0 0]);
%     % Apply a shear load on the end opposite to the fixed end.
%     applyBoundaryCondition(pdem,'neumann','Face',5,'g',[0,0,-3e3]);
%
%     %Generate mesh and solve.
%     generateMesh(pdem,'Hmax',25,'GeometricOrder','quadratic');
%     R=solvepde(pdem);
%
%     %Compute stress: product of c-coefficient and gradients of
%     %displacement.
%     [sig_x,sig_y,sig_z] = R.evaluateCGradient;
%     %Plot normal component of stress along x-direction and observe that
%     %top portion of the beam experiences tension and the bottom portion
%     %experiences compression.
%     figure
%     pdeplot3D(pdem,'colormapdata',sig_x(:,1))
%
%     %Define a line across the beam from bottom to top at mid-span and
%     %mid-width.
%     zg = linspace(0, 100, 10);
%     xg = 250*ones(size(zg));
%     yg = 50*ones(size(zg));
%
%     %Compute stresses along the line.
%     [sig_xx,sig_xy,sig_xz] = R.evaluateCGradient(xg,yg,zg,1);
%
%     %Plot x-direction normal stress along the line.
%     figure
%     plot(sig_xx,zg)
%     grid on
%     xlabel('\sigma_{xx}')
%     ylabel('z')
%
%    See also solvepde, pde.StationaryResults, createPDEResults

% Copyright 2016 The MathWorks, Inc.


cCoeffAtNodes = pde.PDEResults.evalCCoeffAtNodes(obj);

dudx = obj.XGradients';
dudy = obj.YGradients';

if obj.IsTwoD
    % 2-D implementation code
    [cdudx, cdudy] = pde.PDEResults.cgradImpl2D(obj,cCoeffAtNodes,dudx,dudy);
    cdudz = [];
else
    dudz = obj.ZGradients';
    % 3-D implementation code
    [cdudx, cdudy, cdudz] = pde.PDEResults.cgradImpl3D(obj,cCoeffAtNodes,dudx,dudy,dudz);
end

% Perform interpolation if user specifies required additional arguments.
if (nargin > 1)
    [cdudx, cdudy, cdudz] = pde.PDEResults.evalCGradInternal(obj,cdudx,cdudy,cdudz,varargin{:});
else
    
end
end