function [dudxIntrp, dudyIntrp, dudzIntrp] = evaluateGradient(obj,varargin)
%evaluateGradient - evaluate gradients of solution at specified spatial locations
%
% [dudxIntrp, dudyIntrp, dudzIntrp] = evaluateGradient(R,QP, EQj) 
% evaluates the gradients in a PDEResults object R, for a system of equations, 
% at the query point locations QP. The points QP are represented as a matrix 
% of size 3-by-Numq, where 3 represents 3 spatial dimensions and Numq is the 
% number of query points. EQj is a scalar or vector of equation indices in 
% the range 1 to PDESystemSize, the total number of equation in the system 
% of PDEs. The output dudxIntrp, dudyIntrp, dudzIntrp represents the components 
% of the gradients at the query points QP.
%
% [dudxIntrp, dudyIntrp, dudzIntrp] = evaluateGradient(R,QP, EQj) 
% evaluates the gradients in a PDEResults object R, for a system of equations, 
% at the query point locations QP. The points QP are represented as a matrix 
% of size 2-by-Numq, where 2 represents 2 spatial dimensions and Numq is the 
% number of query points. EQj is a scalar or vector of equation indices in 
% the range 1 to PDESystemSize, the total number of equation in the system 
% of PDEs. The output dudxIntrp, dudyIntrp, represents the components of the 
% gradients at the query points QP.
%
% [__] = evaluateGradient(R, QP) evaluates the 
% gradients in a PDEResults object R, for a scalar PDE, at the query point 
% locations QP. 
%
% [__] = evaluateGradient(R, xq, yq, __ ) 
% and [__] =  evaluateGradient(R, xq, yq, zq, __ ) allow the query points QP 
% to be specified in coordinate vector format.
%
%   Example: Solve a elliptic PDE on a block and visualize the
%   gradients of solution on a mid-plane.
%
%     %Create a PDEModel, import geometry, and specify coefficients.
%     N = 1; % single PDE
%     pdem = createpde(N);
%     g = importGeometry(pdem,'Block.stl');
%     c = 1;
%     a = 0;
%     f = 0.1;
%     specifyCoefficients(pdem,'m',0,'d',0,'c',c,'a',a,'f',f);
%
%     %Apply boundary conditions and generate the mesh.
%     applyBoundaryCondition(pdem,'dirichlet','Face',1:4,'u',0);
%     applyBoundaryCondition(pdem,'neumann','Face',6,'g',-1);
%     applyBoundaryCondition(pdem,'neumann','Face',5,'g',1);
%
%     generateMesh(pdem);
%
%     %Solve the PDE.
%     R = solvepde(pdem);
%
%     %Define a grid of points representing the mid-plane of the plate.
%     xp = 0:2:100; yp = 0:2:50;
%     [XX, ZZ] = meshgrid(xp,yp);
%     YY = 2*ones(size(XX));
%
%     %Evaluate gradients of the solution on the mid-plane grid.
%     [dudx, dudy, dudz] = evaluateGradient(R,XX,YY,ZZ);
%
%     %Reshape the gradients to visualize using the MATLAB surf function.
%     dudxres = reshape(dudx,size(XX));
%     dudyres = reshape(dudy,size(XX));
%     dudzres = reshape(dudz,size(XX));
%
%     %Visualize the gradients of solution on the mid-plane.
%     figure
%     subplot(1,3,1)
%     surf(XX,ZZ,dudxres)
%     xlabel('x'); ylabel('y'); zlabel('du/dx');
%
%     subplot(1,3,2)
%     surf(XX,ZZ,dudyres)
%     xlabel('x'); ylabel('y'); zlabel('du/dy');
%
%     subplot(1,3,3)
%     surf(XX,ZZ,dudzres)
%     xlabel('x'); ylabel('y'); zlabel('du/dz');
%
%    See also pde.StationaryResults, createPDEResults, solvepde, meshgrid, surf

% Copyright 2015-2016 The MathWorks, Inc.

if (isempty(obj.InterpolantdUdx))
    error(message('pde:PDEResults:gradientNotSupported15bObj'));
end

[dudxIntrp, dudyIntrp, dudzIntrp] = pde.PDEResults.evalGradInternal(obj,varargin{:}) ;
 
end       

