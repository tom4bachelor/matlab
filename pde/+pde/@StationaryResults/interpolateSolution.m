function uintrp = interpolateSolution(obj,varargin)
%interpolateSolution - Interpolate solution at specified spatial locations
%
% uintrp = interpolateSolution(R, QP, EQj) interpolates the data in a results 
% object R, for a system of equations, at the query point locations QP. 
% The points QP is represented as a matrix of size Ndim-by-Numq, where Ndim 
% is the number of spatial dimensions and Numq is the number of query points. 
% EQj is a scalar or vector of equation indices in the range 1 to PDESystemSize, 
% where PDESystemSize is the total number of equation in the system of PDEs. 
% uintrp is the interpolated solution at the query points QP.
%
% uintrp = interpolateSolution(R, QP) interpolates the data in a 
% results object R, for a scalar PDE, at the query point locations QP. 
% The points QP is represented as a matrix of size Ndim-by-Numq, where Ndim 
% is the number of spatial dimensions and Numq is the number of query points.
% uintrp is the interpolated solution at the query points QP.
%
% uintrp = interpolateSolution(R, xq, yq, __ ) 
% and uintrp =  interpolateSolution(R, xq, yq, zq, __ ) allow the query 
% points QP to be specified in coordinate vector format.
%
%  Example: Solve a scalar stationary problem and interpolate the
%  solution to a dense grid.
%
%     %Create a PDEModel, use L-shaped 2-D geometry, and specify coefficients.
%     model = createpde;
%     geometryFromEdges(model,@lshapeg);
%     specifyCoefficients(model,'m',0,'d',0,'c',1,'a',0,'f',1);
%
%     %Apply boundary conditions, generate the mesh and solve the PDE.
%     applyBoundaryCondition(model,'dirichlet','edge',1:model.Geometry.NumEdges,'u',0);
%     generateMesh(model,'Hmax',0.05);
%     results = solvepde(model);
%     %Interpolate the solution on the grid from �1 to 1 in each direction.
%     v = linspace(-1,1,101);
%     [X,Y] = meshgrid(v);
%     querypoints = [X(:),Y(:)]';
%     uintrp = interpolateSolution(results,querypoints);
%
%     %Plot the resulting interpolation on a mesh.
%     uintrp = reshape(uintrp,size(X));
%     mesh(X,Y,uintrp)
%     xlabel('x')
%     ylabel('y')
%
%   See also pde.StationaryResults, createPDEResults, solvepde, meshgrid, surf

% Copyright 2015-2016 The MathWorks, Inc.

uintrp = pde.PDEResults.interpolateSolutionInternal(obj,varargin{:});

end




