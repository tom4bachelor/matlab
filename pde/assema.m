function [ks,km,fm]=assema(varargin) %(p,t,c,a,f,u,time,sdl)
%ASSEMA Assembles area integral contributions in a PDE problem.
%
%       [K,M,F1]=ASSEMA(P,T,C,A,F) assembles the stiffness matrix K,
%       the mass matrix M, and the right-hand side vector F1.
%
%       The following call sequences are also allowed:
%       [K,M,F1]=ASSEMA(P,T,C,A,F,U0) 
%       [K,M,F1]=ASSEMA(P,T,C,A,F,U0,TIME) 
%       [K,M,F1]=ASSEMA(P,T,C,A,F,U0,TIME,SDL) 
%       [K,M,F1]=ASSEMA(P,T,C,A,F,TIME) 
%       [K,M,F1]=ASSEMA(P,T,C,A,F,TIME,SDL) 
%
%       [K,M,F1]=ASSEMA(PDEM,C,A,F...) Allows the mesh (P,T) to
%       be defined by PDEM, a PDEModel object that contains a mesh.
%
%       The input parameters P, T, C, A, F, U0, TIME, and SDL have the
%       same meaning as in ASSEMPDE.
%
%       Note: This function does not support quadratic triangular elements
%
%       See also CREATEPDE, pde.PDEMODEL, ASSEMPDE

%       Copyright 1994-2016 The MathWorks, Inc.

narginchk(4,8);

p = varargin{1};  
t = varargin{2};  
c = varargin{3};
a = varargin{4};
f = varargin{5};    

   
gotu=false;
gottime=false;
gotsdl=false;

if nargin==5
  % No action
elseif nargin==6
  if size(varargin{6},1)>1
    u = varargin{6};
    gotu=true;
  else
    time=varargin{6};
    gottime=true;
  end
elseif nargin==7
  if size(varargin{6},1)>1
    gotu=true;
    gottime=true;
    u = varargin{6};
    time = varargin{7};
  else
    sdl=varargin{7};
    time=varargin{6};
    gottime=true;
    gotsdl=true;
  end
elseif nargin==8
  u = varargin{6};
  time = varargin{7};
  sdl = varargin{8};
  gotu=true;
  gottime=true;
  gotsdl=true;
end

quadraticTriNotSupported(t);

if ~gottime
  time=[];
end

if ~gotu
  u = [];
end

if ~gotsdl
  sdl = [];
end

thePde = getPde( p, [], t, [], f, u, time);

if(size(p,1) == 3)
  if ~isempty(sdl)
    error(message('pde:pdeModel:sdl3D'));
  end
  % For 3D geometry the class method handles the implementation
  [ks,km,fm] = thePde.assema(p,t,c,a,f,u,time);
else
  % 2D geometry
  [ks,km,fm] = pde.internal.assema2DImpl(thePde, p, t, c, a, f,  u, time, sdl); 
end

end


% LocalWords:  sdl PDEM CREATEPDE PDEMODEL
