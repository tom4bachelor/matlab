classdef pdeParabolicInfo < pdeODEInfo
  %pdeParabolicInfo Internal class used to communicate data to ode15s helper functions.
  % This undocumented function may be removed in a future release.
  
  %       Copyright 2012 The MathWorks, Inc.
  
methods
  function obj=pdeParabolicInfo(u0,tlist,b,p,e,t,c,a,f,d)
    obj=obj@pdeODEInfo(tlist, nargin<=9);
    if(nargin >= 10)
      % Handle case where coefficients are passed to parabolic
      obj.b=b; obj.p=p; obj.e=e; obj.t=t; obj.c=c; obj.a=a; obj.f=f; obj.d=d;
      obj=obj.checkFuncDepen(u0, tlist);
      if(~(obj.vq || obj.vg || obj.vh || obj.vr || ...
          obj.vc || obj.va || obj.vf))
        [KK,FF,B,ud]=assempde(obj.K,obj.M,obj.F,obj.Q,obj.G,obj.H,obj.R);
        obj.K=KK;
        obj.F=FF;
      end
    elseif(nargin >= 7)
      % Handle case where global matrices are passed to parabolic
      obj.K=b; obj.F=p; obj.B=e; obj.ud=t; obj.MM=c;
      obj.numConstrainedEqns = size(obj.K, 1);
    else
      error(message('pde:parabolic:nargin'))
    end
  end
end
  
  
end

