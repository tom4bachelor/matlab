function [ dl ] = drawWelle(sWelle, sLabel)
%UNTITLED5 Summary of this function goes here
%   alpha: Winkel (Bogenma�)


dDurchmesser = sWelle.dDurchmesser;
dPassfederBreite = sWelle.dPassfederBreite;
dPassfederHoehe = sWelle.dPassfederHoehe;
    
dPassfederWinkel = asind(dPassfederBreite/2 / (dDurchmesser/2));

yKbE = dPassfederBreite/2;
xKbE = -dDurchmesser/2 * cosd(dPassfederWinkel); %Koordinaten Ende Kreisbogen

yKbA = -dPassfederBreite/2; 
xKbA = -dDurchmesser/2 * cosd(dPassfederWinkel); %Koordinaten Anfang Kreisbogen

yEckeE = dPassfederBreite/2;
xEckeE = xKbE - dPassfederHoehe;
Kreisteilerx = dDurchmesser/2;
Kreisteilery = 0;
yEckeA = -dPassfederBreite/2;
xEckeA = xKbE - dPassfederHoehe;



dl = [...
    drawLine(xKbA,xEckeA,yEckeA,yEckeA,sLabel.Rotor,sLabel.Welle),...
    drawLine(xEckeA,xEckeA,yEckeA,yEckeE,sLabel.Rotor,sLabel.Welle),...
    drawLine(xEckeE,xKbE,yEckeE,yEckeE,sLabel.Rotor,sLabel.Welle),...
    drawArc(0,0,dDurchmesser/2,180-dPassfederWinkel,-(360-(2*dPassfederWinkel)),sLabel.Rotor,sLabel.Welle)
    ];


%dl = [dlKreisbogen dlKreisbogen2];

end

