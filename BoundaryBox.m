function [ dl ] = BoundaryBox( dBoundaryBox_Kantenlaenge, sLabel)
%BOUNDARYBOX Summary of this function goes here
%   Detailed explanation goes here

    dl = drawLine(-dBoundaryBox_Kantenlaenge/2, dBoundaryBox_Kantenlaenge/2,dBoundaryBox_Kantenlaenge/2, dBoundaryBox_Kantenlaenge/2,sLabel.Bound,sLabel.Air);
    dl = [dl,drawLine(dBoundaryBox_Kantenlaenge/2, dBoundaryBox_Kantenlaenge/2,dBoundaryBox_Kantenlaenge/2, -dBoundaryBox_Kantenlaenge/2,sLabel.Bound,sLabel.Air)];
    dl = [dl,drawLine(dBoundaryBox_Kantenlaenge/2, -dBoundaryBox_Kantenlaenge/2,-dBoundaryBox_Kantenlaenge/2, -dBoundaryBox_Kantenlaenge/2,sLabel.Bound,sLabel.Air)];
    dl = [dl,drawLine(-dBoundaryBox_Kantenlaenge/2, -dBoundaryBox_Kantenlaenge/2,-dBoundaryBox_Kantenlaenge/2, dBoundaryBox_Kantenlaenge/2,sLabel.Bound,sLabel.Air)];

end

