function [ dlR ] = copyRotateN(x, y, phi, nc, dl)
%
    dlR = copyRotate(x,y,phi,dl);
    for j=2:1:nc
        dlR = [dlR, copyRotate(x,y,(j*phi),dl)];
    end
end       


