function [ g ] = drawArc( x, y, r, phi_s, phi_e, el, er )
%DRAWARC Summary of this function goes here
%   Detailed explanation goes here
%   Circle
%   1
%   Starting x coordinate
%   Ending x coordinate
%   Starting y coordinate	
%   Ending y coordinate
%   Region label to left of segment, with direction induced by start and end points (0 is exterior label)
%   Region label to right of segment, with direction induced by start and end points (0 is exterior label)
%   x coordinate of circle center
%   y coordinate of circle center
%   Length of first semiaxis (Here radius)
%   0 0 
    

    %Definition mit den Uhrzeigersinn um die 
    xs = cosd(phi_s)*r + x;
    ys = sind(phi_s)*r + y;
    
    xe = cosd(phi_e)*r +x;
    ye = sind(phi_e)*r +y;
    
%     if abs(phi_s-phi_e) > 180   %Split the circle in 2 halfs
%         if phi_e > phi_s
%             xm = cosd(((phi_e-phi_s)/2) + phi_s)*r + x;
%             ym = sind(((phi_e-phi_s)/2) + phi_s)*r + y;
%         else
%             xm = cosd(((phi_s-phi_e)/2) + phi_s)*r + x;
%             ym = sind(((phi_s-phi_e)/2) + phi_s)*r + y;
%         end
%         g = [1 , xs, xm, ys, ym, el, er, x, y, r, 0, 0]';
%         g = [g,[1 , xm, xe, ym, ye, el, er, x, y, r, 0, 0]'];
%     else
        g = [1 , xs, xe, ys, ye, el, er, x, y, r, 0, 0]';
    end
    
end
