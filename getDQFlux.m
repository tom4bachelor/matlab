function [ psi_d, psi_q ] = getDQFlux( result, angle, sLabel )
%GETDQINDUCTANCE Summary of this function goes here
%   Detailed explanation goes here
    %Strang A
    [ ~, ~, u_mean, ~] = pdesolutionsub(result.Mesh,result.NodalSolution,sLabel.WicklungA_p);
    [ ~, ~, u_mean2, ~] = pdesolutionsub(result.Mesh,result.NodalSolution,sLabel.WicklungA_m);
    [ ~, ~, u_mean3, ~] = pdesolutionsub(result.Mesh,result.NodalSolution,sLabel.WicklungA2_p);
    [ ~, ~, u_mean4, ~] = pdesolutionsub(result.Mesh,result.NodalSolution,sLabel.WicklungA2_m);
    psi_a = (u_mean-u_mean2+u_mean3-u_mean4)*0.1;
    %Strang B
    [ ~, ~, u_mean, ~] = pdesolutionsub(result.Mesh,result.NodalSolution,sLabel.WicklungB_p);
    [ ~, ~, u_mean2, ~] = pdesolutionsub(result.Mesh,result.NodalSolution,sLabel.WicklungB_m);
    [ ~, ~, u_mean3, ~] = pdesolutionsub(result.Mesh,result.NodalSolution,sLabel.WicklungB2_p);
    [ ~, ~, u_mean4, ~] = pdesolutionsub(result.Mesh,result.NodalSolution,sLabel.WicklungB2_m);
    psi_b = (u_mean-u_mean2+u_mean3-u_mean4)*0.1;
    %Strang C
    [ ~, ~, u_mean, ~] = pdesolutionsub(result.Mesh,result.NodalSolution,sLabel.WicklungC_p);
    [ ~, ~, u_mean2, ~] = pdesolutionsub(result.Mesh,result.NodalSolution,sLabel.WicklungC_m);
    [ ~, ~, u_mean3, ~] = pdesolutionsub(result.Mesh,result.NodalSolution,sLabel.WicklungC2_p);
    [ ~, ~, u_mean4, ~] = pdesolutionsub(result.Mesh,result.NodalSolution,sLabel.WicklungC2_m);
    psi_c = (u_mean-u_mean2+u_mean3-u_mean4)*0.1;
    
    %Rechne Flüsse in DQ-Koordinaten um
    [psi_alpha,psi_beta]=TransformABCToAlphaBeta(psi_a,psi_b,psi_c);
    [psi_d, psi_q] = TransformAlphaBetaToDQ(psi_alpha,psi_beta,angle);
    
end

