function [dXAlpha, dXBeta] = TransformDQToAlphaBeta(dXd, dXq, dAngle)

  dAngle_el = deg2rad(dAngle);  

  dXAlpha = cos(dAngle_el)*dXd - sin(dAngle_el)*dXq;
  dXBeta = sin(dAngle_el)*dXd + cos(dAngle_el)*dXq;
  
end