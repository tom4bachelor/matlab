%% Main Skript 

close all
clear 
clc



tic

Abmessungen
addpath(genpath('./pde'));   %Pfad für modifizierte PDE-Skripte

output = './calc/';
mkdir(output);


%% Zuordung FaceLabels 
sLabel.Bound = 0;
sLabel.Air = 1;
sLabel.Stator = 2;
sLabel.Rotorluft = 3;
sLabel.Rotor = 4;
sLabel.Welle = 5;
sLabel.Magnet1 = 6;
sLabel.Magnet2 = 7;
sLabel.Magnet1Luft_r = 8;
sLabel.Magnet1Luft_l = 9;
sLabel.Magnet2Luft_r = 10;
sLabel.Magnet2Luft_l = 11;
sLabel.WicklungA_p = 12;
sLabel.WicklungA_m = 13;
sLabel.WicklungB_m = 14;
sLabel.WicklungB_p = 15;
sLabel.WicklungC_p = 16;
sLabel.WicklungC_m = 17;
sLabel.WicklungA2_m = 18;
sLabel.WicklungA2_p = 19;
sLabel.WicklungB2_p = 20;
sLabel.WicklungB2_m = 21;
sLabel.WicklungC2_m = 22;
sLabel.WicklungC2_p = 23;
sLabel.Bohrung1 = 24;
sLabel.Bohrung2 = 25;
sLabel.Bohrung3 = 26;
sLabel.Bohrung4 = 27;
sLabel.Bohrung5 = 28;
sLabel.Bohrung6 = 29;
sLabel.Bohrung7 = 30;
sLabel.Bohrung8 = 31;
sLabel.Magnet1Erregung1 = 0;
sLabel.Magnet1Erregung2 = 0;
sLabel.Magnet2Erregung1 = 0;
sLabel.Magnet2Erregung2 = 0;
sLabel = initializeLabels(sLabel);



%% Motor errichten

motor = createpde(1);
dl = [BoundaryBox(dBoundaryBox_Kantenlaenge,sLabel),Stator(sStator,sWicklung,sLabel), copyRotate(0,0,0,[drawWelle(sWelle, sLabel), Rotor(sRotor,sLabel)])];%
dl = gScale(dl, 0.001); %Scale to m from mm values
geometryFromEdges(motor,dl);



% figure('Name', 'Finite Elements','units','centimeters','position',[0 0 8 8]);
% [p,e,t]=initmesh(dl);
% [p,e,t] = refinemesh(dl,p,e,t,[sLabel.Rotorluft sLabel.Rotor]);
% %refinemesh kann nicht mit dem neuen Workflow benutzt werden:
% %https://de.mathworks.com/matlabcentral/answers/329568-convert-p-e-t-to-femesh-for-pdemodel
% % -> Mesh kann nicht in einzelnen Gebieten angepasst werde
% % p,e,t kann nicht in PDEModel integreiert werden -> nur direkt
% % generateMesh
% pdeplot(p,e,t);
% axis equal;

applyBoundaryCondition(motor,'Edge',1:4,'r',0,'h',1);
%h*u=r -> A=0 Vektorpotential an der Boundary ist 0 -> kein Magnetischer
%Fluss durch die Boundary
%applyBoundaryCondition(motor,'dirichlet','Edge',1:4,'r',0,'h',1); %>R2016b







%% Materialbeschreibung für die einzelnen Gebiete
sMaterial.mue0 = 4*pi*10^(-7);
sMaterial.Air = 1*sMaterial.mue0;
sMaterial.Cu = 1*sMaterial.mue0;
sMaterial.NeFeB32 = [1.045*sMaterial.mue0 883310 0];
%sMaterial.Fe = 4*pi*10^(-7)*(5000./(1+0.05*(ux.^2+uy.^2))+200);
%fe=@(region,state)(1./(4*pi*10^(-7)*(5000./(1+0.05*(state.ux.^2+state.uy.^2))+200)));
%Wie beschreibe ich den Magneten? direkt eine Lösung u vorgeben!!

%fe=@(region,state)(1./(getmuer(B,H,(sqrt(state.ux.^2+state.uy.^2)))*4*pi*10^(-7)));
muealt;
%% Koeffizienten auf Gebiete anwenden
%https://de.mathworks.com/help/pde/ug/specifycoefficients.html
specifyCoefficients(motor,'m',0,'d',0,'c',(1/sMaterial.Air),'a',0,'f',0,'Face',[...
    sLabel.Air...
    sLabel.Bohrung1...
    sLabel.Bohrung2...
    sLabel.Bohrung3...
    sLabel.Bohrung4...
    sLabel.Bohrung5...
    sLabel.Bohrung5...
    sLabel.Bohrung6...
    sLabel.Bohrung7...
    sLabel.Bohrung8...
    sLabel.Rotorluft...
    sLabel.Magnet1Luft_l...
    sLabel.Magnet1Luft_r...
    sLabel.Magnet2Luft_l...
    sLabel.Magnet2Luft_r...
    sLabel.Welle...
    ]);



%Stator
specifyCoefficients(motor,'m',0,'d',0,'c',fe,'a',0,'f',0,'Face',sLabel.Stator);

%Rotor
specifyCoefficients(motor,'m',0,'d',0,'c',fe,'a',0,'f',0,'Face',[...
    sLabel.Rotor...
    ]);
%Magneten
J_pm = pm2j(sRotor.dPM_Hoehe/1000,sRotor.dPM_Breite/1000,sRotor.dCurrentSheet/1000,sMaterial.NeFeB32);
%J_pm = 0;
specifyCoefficients(motor,'m',0,'d',0,'c',1/sMaterial.NeFeB32(1),'a',0,'f',0,'Face',[...
    sLabel.Magnet1...
    sLabel.Magnet2...
    ]);
specifyCoefficients(motor,'m',0,'d',0,'c',1/sMaterial.NeFeB32(1),'a',0,'f',J_pm,'Face',[...
    sLabel.Magnet1Erregung1...
    sLabel.Magnet2Erregung1...
    ]);
specifyCoefficients(motor,'m',0,'d',0,'c',1/sMaterial.NeFeB32(1),'a',0,'f',-J_pm,'Face',[...
    sLabel.Magnet1Erregung2...
    sLabel.Magnet2Erregung2...
    ]);
%(-1.4701)*(10^11) für J_pm geht nicht.. stepsize to small

%Boundary
%specifyCoefficients(motor,'m',0,'d',0,'c',fe,'a',0,'f',0,'Face',[...

motor.SolverOptions.ResidualNorm = 2;%timeit= 27.2338 für J=[0,0,0]
%Residual Norm nicht auf 2 gesetzt braucht die L�sung ewig! Warum? Wie
%funktionieren FiniteElemente
motor.SolverOptions.MinStep = 0;% So klein wie m�glich, damit der Solver nicht abbricht
%motor.SolverOptions.RelativeTolerance = 1e-02;  
%motor.SolverOptions.AbsoluteTolerance = 5.0000e-01; 
%Machen keinen Unterschied
motor.SolverOptions.ResidualTolerance = 1.0000e-04;%1e-04 ist standart Relativ gro�en Fehler zulassen, damit der Solver eine L�sung findet
motor.SolverOptions.ReportStatistics = 'on';
motor.SolverOptions.MaxIterations = 60; %Etwas mehr Iterationen wg dem neuen Residuum/Abbruchbedingungen


Id = 0;  
Iq = 7.5;
%Id = [-20:3:-2, 0, 2:3:20];
%Iq = [-20:3:-2, 0, 2:3:20];
dI = 0;
if dI~=0
    dI = [-dI dI 0];
end

sCalc.angle = 0;
%sCalc.angle = sCalc.angle(1:end-1);
sCalc.Iq = Iq;
sCalc.Id = Id;
sCalc.dI = dI;
%sMotor=zeros(length(sCalc.angle)-1);
delete(motor.Geometry);
motor.Geometry=[];
save('motor','motor');      %Basismodell Strom und Rotorpositionsunabhängig

for idI = 1:length(dI)
    if dI(idI)<0
        Iq = Iq-dI(idI);
    elseif dI(idI)>0
        Iq = sCalc.Iq;
        Id = Id+dI(idI);
    else
        Id = sCalc.Id;
        Iq = sCalc.Iq;
    end
        
for iId = 1:length(Id)
for iIq = 1:length(Iq)
for iPhi=1:1:(length(sCalc.angle))
    clear motor;
    load('motor');    
    angle = sCalc.angle(iPhi);
    dl = [BoundaryBox(dBoundaryBox_Kantenlaenge,sLabel),Stator(sStator,sWicklung,sLabel), copyRotate(0,0,angle,[drawWelle(sWelle, sLabel), Rotor(sRotor,sLabel)])];%
    dl = gScale(dl, 0.001); %Scale to m from mm values
    geometryFromEdges(motor,dl);

    %% Strombeschlag


    [Ialpha,Ibeta] = TransformDQToAlphaBeta(Id(iId),Iq(iIq),angle);
    [Ia,Ib,Ic] = TransformAlphaBetaToABC(Ialpha,Ibeta);
    J = ItoJ([Ia,Ib,Ic],sWicklung.dWickelfensterBreite*sWicklung.dWickelfensterHoehe*1e-06)*sWicklung.iN;
    %2*10^8 funktioniert nicht 2*10^9 geht wieder ^^
    % Error using pde.EquationModel/solveStationaryNonlinear (line 104)
    % Stepsize too small.
    % 
    % Error in pde.PDEModel/solvepde (line 77)
    %             u = self.solveStationaryNonlinear(coefstruct, u0);
    % 
    % Error in Main (line 153)
    % result = solvepde(motor);
    %Wicklung A:
    specifyCoefficients(motor,'m',0,'d',0,'c',(1/sMaterial.Cu),'a',0,'f',J(1),'Face',[...
        sLabel.WicklungA_p...
        sLabel.WicklungA2_p...
        ]);
    specifyCoefficients(motor,'m',0,'d',0,'c',(1/sMaterial.Cu),'a',0,'f',-J(1),'Face',[...
        sLabel.WicklungA_m...
        sLabel.WicklungA2_m...
        ]);
    %Wicklung B:
    specifyCoefficients(motor,'m',0,'d',0,'c',(1/sMaterial.Cu),'a',0,'f',J(2),'Face',[...
        sLabel.WicklungB_p...
        sLabel.WicklungB2_p...
        ]);
    specifyCoefficients(motor,'m',0,'d',0,'c',(1/sMaterial.Cu),'a',0,'f',-J(2),'Face',[...
        sLabel.WicklungB_m...
        sLabel.WicklungB2_m...
        ]);
    %Wicklung C:
    specifyCoefficients(motor,'m',0,'d',0,'c',(1/sMaterial.Cu),'a',0,'f',J(3),'Face',[...
        sLabel.WicklungC_p...
        sLabel.WicklungC2_p...
        ]);
    specifyCoefficients(motor,'m',0,'d',0,'c',(1/sMaterial.Cu),'a',0,'f',-J(3),'Face',[...
        sLabel.WicklungC_m...
        sLabel.WicklungC2_m...
        ]);
    generateMesh(motor,'Jiggle','on','JiggleIter',10,'GeometricOrder','linear','Hmax',0.004);% 'quadratic',

    save([output 'model_' num2str(angle) 'phi_' num2str(Id(iId)) 'Id_' num2str(Iq(iIq)) 'Iq.mat'],'motor');
end
end
end
end


%% Berechnungsblock
Torque=zeros(length(Id),length(Iq),length(dI),(length(sCalc.angle)));
Error=zeros(length(Id),length(Iq),length(dI),(length(sCalc.angle)));
res = zeros(length(Id),length(Iq),length(dI),(length(sCalc.angle)));
E = zeros(length(Id),length(Iq),length(dI),(length(sCalc.angle)));
Ec = zeros(length(Id),length(Iq),length(dI),(length(sCalc.angle)));
angle = sCalc.angle;
for idI = 1:length(dI)
    
    %Falls zwischendrinnen abgebrochen, werte neu zuweisen
    Iq = sCalc.Iq;
    Id = sCalc.Id;
    
    if dI(idI)<0
        Iq = Iq-dI(idI);
    elseif dI(idI)>0
        Iq = sCalc.Iq;
        Id = Id+dI(idI);
    else
        Id = sCalc.Id;
        Iq = sCalc.Iq;
    end
    
for iId = 1:length(Id)
for iIq = 1:length(Iq)
for iPhi=1:1:(length(sCalc.angle))
    try
      result = parsolvepde([output 'model_' num2str(angle(iPhi)) 'phi_' num2str(Id(iId)) 'Id_' num2str(Iq(iIq)) 'Iq.mat']);
      Torque(iId, iIq, idI, iPhi) = getTorque2(result);
      res(iId, iIq, idI, iPhi) = result.Residual(end);
      [E(iId, iIq, idI, iPhi),Ec(iId, iIq, idI, iPhi)] = getEEcTotal(result, sLabel, sMaterial );
      if strcmp(result.Failure,'maxiterations')
          Error(iId, iIq, idI, iPhi)=1;
      elseif strcmp(result.Failure,'residualconverge')
          Error(iId, iIq, idI, iPhi)=2;
      end
      
      parsave([output 'result_' num2str(angle(iPhi)) 'phi_' num2str(Id(iId)) 'Id_' num2str(Iq(iIq)) 'Iq.mat'],result);
    catch
      Error(iId, iIq, idI, iPhi)=3;
    end
end
end
end
end
sCalc.Torque=Torque;
sCalc.E = E;
sCalc.Ec = Ec;
sCalc.Error=Error;
sCalc.Residuum = res;
save([output 'sCalc'],'sCalc');
% u=result.NodalSolution;
% ux=result.XGradients;
% uy=result.YGradients;
% pdeplot(motor,'XYData',sqrt(uy.^2+ux.^2),'Mesh','off','Contour','off','FlowData',[uy,-ux],'ColorMap','jet');%'ZData',u,'ZData',sqrt(uy.^2+ux.^2)
%pdeplot(motor,'FlowData',[uy,-ux]);
%'ColorMap','jet'


axis equal;
toc;



%% Ende





