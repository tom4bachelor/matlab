function [ Emagc ] = getmagEc( result, mue, sdl )
%GETMAGE Summary of this function goes here
%   Detailed explanation goes here
    [p,~,t] = meshToPet(result.Mesh);
    [ux,uy] = pdegrad(p,t,result.NodalSolution,sdl);
    ut = sqrt(ux.^2+uy.^2);
    tsub=t(:,pdesdt(t,sdl));
    [a,~,~,~] = pdetrg(p,tsub);
    if isa(mue, 'function_handle')
        int=@(x)(x./(mue(x)*4*pi*10^-7));
        Emagc = ((ut.*int(ut))-matrixIntegral(int,0,ut)).*a;
        
    else
        %Emag = 0.5*ut.^2.*a./mue;
        int=@(x)(x./mue);
        Emagc = matrixIntegral(int,0,ut);
        Emagc = Emagc.*a;
    end
    Emagc=sum(Emagc);
end

function [out] = matrixIntegral( fun, xmin, xmax )
    out = zeros(1,length(xmax));
    for i=1:length(xmax)
        out(i) = integral(fun,xmin,xmax(i));
    end
end