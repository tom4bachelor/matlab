function [ dl ] = gScale( dl, scale )
%GSCLAE Scale the geometry
%   Scale the geometry to convert from cm, mm to the si unit m
    [~,n]=size(dl);
    for i=1:1:n
        dl(2,i)=dl(2,i)*scale;
        dl(3,i)=dl(3,i)*scale;
        dl(4,i)=dl(4,i)*scale;
        dl(5,i)=dl(5,i)*scale;
        dl(8,i)=dl(8,i)*scale;
        dl(9,i)=dl(9,i)*scale;
        dl(10,i)=dl(10,i)*scale;
    end
    

end

