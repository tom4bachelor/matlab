%%Idee: Teile den Kreis in diskrete Punkte. Finde die Gradienten an den
%%Punkten, rechne sie um und integriere darüber!

%Finde Punkte:
function [T] =  getTorque2(result)
r=0.0506;
phi=[0 2*pi];
qp=64000;
sp=phi(2)/qp;
phi=[0:sp:2*pi];

Te = getTorqueVol(result,r,phi);
T=sum(Te)*(2*pi/qp)/(4*pi*10^-7);
T=T*0.1;
end
%cumtrapz((2*pi*r/qp),Te)/(4*pi*10^-7);