function [ ut, a, u_mean, a_sum] = pdesolutionsub( mesh, u, sdl )
%pdesolutionsub 
%   Detailed explanation goes here
    %Convert FEMesh to pet
    [p,~,t] = meshToPet(mesh);
    %Get the right area for integration for all named areas in sdl
    tsub = t(:,t(4,:)==sdl(1));
    for j=2:1:length(sdl)
        tsub=[tsub t(:,t(4,:)==sdl(j))];
    end
    %Get the interpolated midpoint
    ut = pdeintrp(p,tsub,u);
    %Interpolate the area of the triangle
    [a,~,~,~] = pdetrg(p,tsub);
    a_sum=sum(a);
    %Get mean value of solution in the area
    u_mean = sum(ut.*a)/a_sum;
    
end

