function [ Emag ] = getmagEpm( result, Material, sdl )
%GETMAGE Summary of this function goes here
%   Detailed explanation goes here
    [p,~,t] = meshToPet(result.Mesh);
    [ux,uy] = pdegrad(p,t,result.NodalSolution,sdl);
    ut = sqrt(ux.^2+uy.^2);
    tsub=t(:,pdesdt(t,sdl));
    [a,~,~,~] = pdetrg(p,tsub);
    mue = Material(1);
    Hc = Material(2);
    
    if isa(mue, 'function_handle')

    else
        %Emag = 0.5*ut.^2.*a./mue;
        int=@(x)(x./mue);
        Emag = ((Hc^2*mue) - Hc*ut)-matrixIntegral(int,ut,Hc*mue);
        Emag = Emag.*a;
    end
    Emag=sum(Emag);
end

function [out] = matrixIntegral( fun, xmin, xmax )
    out = zeros(1,length(xmin));
    for i=1:length(xmin)
        out(i) = integral(fun,xmin(i),xmax);
    end
end