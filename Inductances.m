close all
clear
clc

sLabel.WicklungA_p = 12;
sLabel.WicklungA_m = 13;
sLabel.WicklungB_m = 14;
sLabel.WicklungB_p = 15;
sLabel.WicklungC_p = 16;
sLabel.WicklungC_m = 17;
sLabel.WicklungA2_m = 18;
sLabel.WicklungA2_p = 19;
sLabel.WicklungB2_p = 20;
sLabel.WicklungB2_m = 21;
sLabel.WicklungC2_m = 22;
sLabel.WicklungC2_p = 23;


addpath(genpath('./pde'));   %Pfad fÃ¼r modifizierte PDE-Skripte


output = './calc/';
load([output 'sCalc.mat']);
Iq = sCalc.Iq;
Id = sCalc.Id;
dI = sCalc.dI(2);
angle = sCalc.angle;


Ldd = zeros(length(Id),length(Iq),length(sCalc.angle));
Lqq = zeros(length(Id),length(Iq),length(sCalc.angle));
Ldq = zeros(length(Id),length(Iq),length(sCalc.angle));
Lqd = zeros(length(Id),length(Iq),length(sCalc.angle));
aPsi_dd = zeros(length(Id),length(Iq),length(sCalc.angle));
aPsi_qq = zeros(length(Id),length(Iq),length(sCalc.angle));
aPsi_dq = zeros(length(Id),length(Iq),length(sCalc.angle));
aPsi_qd = zeros(length(Id),length(Iq),length(sCalc.angle));
aPsi_d0 = zeros(length(Id),length(Iq),length(sCalc.angle));
aPsi_q0 = zeros(length(Id),length(Iq),length(sCalc.angle));

Error = zeros(length(Id),length(Iq));
for iId = 1:length(Id)
for iIq = 1:length(Iq)
for iPhi = 1:length(sCalc.angle)

    %get Fluxes
    try
        load([output 'result_' num2str(angle(iPhi)) 'phi_' num2str(Id(iId)) 'Id_' num2str(Iq(iIq)) 'Iq.mat'],'result');
        [psi_d0, psi_q0] = getDQFlux(result,angle(iPhi),sLabel);
        load([output 'result_' num2str(angle(iPhi)) 'phi_' num2str(Id(iId)+dI) 'Id_' num2str(Iq(iIq)) 'Iq.mat'],'result');
        [psi_dd, psi_qd] = getDQFlux(result,angle(iPhi),sLabel);
        load([output 'result_' num2str(angle(iPhi)) 'phi_' num2str(Id(iId)) 'Id_' num2str(Iq(iIq)+dI) 'Iq.mat'],'result');
        [psi_dq, psi_qq] = getDQFlux(result,angle(iPhi),sLabel);
    catch
        Error(iId,iIq,iPhi) = 1;
        Ldd(iId,iIq,iPhi) = nan;
        Lqq(iId,iIq,iPhi) = nan;
        Ldq(iId,iIq,iPhi) = nan;
        Lqd(iId,iIq,iPhi) = nan;
        continue;
    end
    
    
    %get Inductances
    Ldd(iId,iIq,iPhi) = (psi_dd-psi_d0)/dI;
    Lqq(iId,iIq,iPhi) = (psi_qq-psi_q0)/dI;
    Ldq(iId,iIq,iPhi) = (psi_dq-psi_d0)/dI;
    Lqd(iId,iIq,iPhi) = (psi_qd-psi_q0)/dI;
    aPsi_dd(iId,iIq,iPhi) = psi_dd;
    aPsi_dq(iId,iIq,iPhi) = psi_dq;
    aPsi_qq(iId,iIq,iPhi) = psi_qq;
    aPsi_qd(iId,iIq,iPhi) = psi_qd;
    aPsi_d0(iId,iIq,iPhi) = psi_d0;
    aPsi_q0(iId,iIq,iPhi) = psi_q0;

end    
end
end

sInductance.Ldd = Ldd;
sInductance.Lqq = Lqq;
sInductance.Ldq = Ldq;
sInductance.Lqd = Lqd;
sInductance.Psi_dd = aPsi_dd;
sInductance.Psi_qq = aPsi_qq;
sInductance.Psi_dq = aPsi_dq;
sInductance.Psi_qd = aPsi_qd;
sInductance.Psi_d0 = aPsi_d0;
sInductance.Psi_q0 = aPsi_q0;
sInductance.Error = Error;
sInductance.Id = Id;
sInductance.Iq = Iq;
sInductance.dI = dI;
sInductance.angle = sCalc.angle;

save([output 'sInductance.mat'], 'sInductance');
