function [ dl ] = Rotor( sRotor, sLabel )
%ROTOR Summary of this function goes here
%   Detailed explanation goes here

    dDurchmesser = sRotor.dDurchmesser;
    dWelleDurchmesser = sRotor.dWelleDurchmesser;
    dPassfederHoehe = sRotor.dPassfederHoehe;
    dBohrungsdiskretisierungLuftspalt = sRotor.dBohrungsdiskretisierungLuftspalt;
    dBohrungsdiskretisierungPM = sRotor.dBohrungsdiskretisierungPM;   
    dJ = sRotor.dCurrentSheet;
        
    dPM_Hoehe = sRotor.dPM_Hoehe;
    dPM_Breite = sRotor.dPM_Breite;
    dPM_Winkel = sRotor.dPM_Winkel;
    dPM_Abstand = sRotor.dPM_Abstand;
    szMaterialPM = sRotor.szMaterialPM;
    
    
    dl = [...
        drawArc(0,0,dDurchmesser/2,0,360,sLabel.Rotor,sLabel.Rotorluft),...
        drawSlot(0, dDurchmesser/2 - dPM_Abstand - dPM_Hoehe/2, dPM_Breite, dPM_Hoehe, dPM_Winkel, dJ, sLabel.Magnet1, sLabel.Magnet1Erregung1, sLabel.Magnet1Erregung2, sLabel.Magnet1Luft_l, sLabel.Magnet1Luft_r, sLabel.Rotor),...
        drawSlot(0, -(dDurchmesser/2 - dPM_Abstand - dPM_Hoehe/2), dPM_Breite, dPM_Hoehe, dPM_Winkel, dJ, sLabel.Magnet2, sLabel.Magnet2Erregung1, sLabel.Magnet2Erregung2, sLabel.Magnet2Luft_l, sLabel.Magnet2Luft_r, sLabel.Rotor),...
        ];
    %AddSlot(0, -(dDurchmesser/2 - dPM_Abstand - dPM_Hoehe/2), dPM_Breite, dPM_Hoehe, dPM_Winkel, iRotor, szMaterialPM, iRotorBohrungen, 'Air', dBohrungsdiskretisierungPM);

    
    
end

