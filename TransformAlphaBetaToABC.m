function [dXa, dXb, dXc] = TransformAlphaBetaToABC(dXAlpha, dXBeta)
  
  dXa = dXAlpha;
  dXb = 3/2*(-1/3*dXAlpha + sqrt(3)/3*dXBeta);
  dXc = 3/2*(-1/3*dXAlpha - sqrt(3)/3*dXBeta);

end