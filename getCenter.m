function [ x,y,r ] = getCenter( x1,y1,x2,y2,phi )
    
%GETCENTER Berechnung des Kreismittelpunktes und dessen Radius aus 2
%Punkten und einem Winkel zwischen den Punkten
%     double(x1);
%     double(y1);
%     double(x2);
%     double(y2);
%     double(phi);
%     double(l);
%     double(r);
%     double(dy);
    
    dx=x2-x1;
    dy=y2-y1;
    
    l = sqrt(dx.^2+dy.^2);
    r = (l/2)/sind(phi/2);
    
    %H�he im gleichschenkligen Dreieck
    h = sqrt((r.^2)+(l.^2)/4);
    
    dex= dx/l;
    dey= dy/l;
    
    x = x1+(dx/2)+ h*(-dey);
    y = y1+(dy/2)+ h*(dex);
      
    
    
end

