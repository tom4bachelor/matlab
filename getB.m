function [B] = getB(result, r, phi)
   x = cos(phi).*r;
   y = sin(phi).*r;
   [gradx,grady] = evaluateGradient(result,x,y);
   gradx = gradx';
   grady = grady';
   Br = cos(phi).*-gradx + sin(phi).*grady;
   Bp = -sin(phi).*-gradx + cos(phi).*grady;
   B = Br.*Bp.*r.^2;
end