function [ dl ] = functWelle(alpha)
%UNTITLED5 Summary of this function goes here
%   alpha: Winkel (Bogenma�)


% Eingangsparameter Welle
sWelle.dDurchmesser = 35;
sWelle.dPassfederBreite = 10;
sWelle.dPassfederHoehe = 3.3;


dDurchmesser = sWelle.dDurchmesser;
dPassfederBreite = sWelle.dPassfederBreite;
dPassfederHoehe = sWelle.dPassfederHoehe;
    
dPassfederWinkel = asin(dPassfederBreite/2 / (dDurchmesser/2));

yKbE = dPassfederBreite/2;
xKbE = -dDurchmesser/2 * cos(dPassfederWinkel); %Koordinaten Ende Kreisbogen

yKbA = -dPassfederBreite/2; 
xKbA = -dDurchmesser/2 * cos(dPassfederWinkel); %Koordinaten Anfang Kreisbogen

yEckeE = dPassfederBreite/2;
xEckeE = xKbE - dPassfederHoehe;
Kreisteilerx = dDurchmesser/2;
Kreisteilery = 0;
yEckeA = -dPassfederBreite/2;
xEckeA = xKbE - dPassfederHoehe;

%Rotation Welle um Winkel alpha, berechne neue Koordinaten Anfang und Ende Kreisbogen

yKbEr = sin(alpha) * xKbE + cos(alpha) * yKbE;
xKbEr = cos(alpha) * xKbE - sin(alpha) * yKbE;
yKbAr = sin(alpha) * xKbA + cos(alpha) * yKbA;
xKbAr = cos(alpha) * xKbA - sin(alpha) * yKbA;

yEckeEr = sin(alpha) * xEckeE + cos(alpha) * yEckeE;
xEckeEr = cos(alpha) * xEckeE - sin(alpha) * yEckeE;
yEckeAr = sin(alpha) * xEckeA + cos(alpha) * yEckeA;
xEckeAr = cos(alpha) * xEckeA - sin(alpha) * yEckeA;

dlKreisbogen = [1 xKbAr Kreisteilerx yKbAr Kreisteilery 0 1 0 0 dDurchmesser/2 0 0]';
dlKreisbogen2 = [1 Kreisteilerx xKbEr Kreisteilery yKbEr 0 1 0 0 dDurchmesser/2 0 0]';
dlLinie1 = [2 xEckeAr xKbAr yEckeAr yKbAr 0 1 0 0 0 0 0]';
dlLinie2 = [2 xEckeEr xEckeAr yEckeEr yEckeAr 0 1 0 0 0 0 0]';
dlLinie3 = [2 xKbEr xEckeEr yKbEr yEckeEr 0 1 0 0 0 0 0]';
dl = [dlKreisbogen dlKreisbogen2 dlLinie1 dlLinie2 dlLinie3];
%dl = [dlKreisbogen dlKreisbogen2];

end

