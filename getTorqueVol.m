function [ Te ] = getTorqueVol( result, r, phi )
%GETTORQUEVOL Summary of this function goes here
%   Detailed explanation goes here
   x = cos(phi).*r;
   y = sin(phi).*r;
   [gradx,grady] = evaluateGradient(result,x,y);
   gradx = reshape(gradx,size(phi));
   grady = reshape(grady,size(phi));
   By = -gradx;
   Bx = grady;
   Br = cos(phi).*Bx + sin(phi).*By;
   Bp = -sin(phi).*Bx + cos(phi).*By;
   Te = Br.*Bp.*r.^2;

end

