function [ mu ] = getmuer(Bv, Hv, B)
%LINEARINTERP Summary of this function goes here
%   Detailed explanation goes here
    mu_rs=1;
    [~,n]=size(B);
    [m,~]=size(Bv);
    mu=zeros(size(B));
    mu_v=Bv./(Hv*4*pi*10^(-7));
    if isnan(mu_v(1))
        mu_v(1)=mu_v(2);
    end
    for i=1:1:n   
        if B(1,i) == 0
            mu(1,i)=mu_v(1);   
        elseif B(1,i) < Bv(m)
            j=1;
            %mu(1,i) = interp1(Bv,mu_v,B(1,i));
            while B(1,i)>Bv(j)
                j=j+1;
            end
            j=j-1;
            mu(1,i) = mu_v(j)+(B(1,i)-Bv(j))*(mu_v(j+1)-mu_v(j))/(Bv(j+1)-Bv(j));
            
        else
            H = Hv(m) + (B(1,i)-Bv(m))/(4*pi*10^(-7)*mu_rs);
            mu(1,i) = B(1,i)/(H*4*pi*10^(-7));
        end
    end
    
end

%y = Yd(m)+((Yd(m)-Yd(m-1))/(Xd(m)-Xd(m-1))*(x-Xd(m)));