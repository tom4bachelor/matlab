
%% Zeichnet slots auf Rotor

function AddSlot(dX_Center, dY_Center, dWidth, dHeight, dAngle, iGruppe, szMaterialPM, iRadiusGruppe, szRadiusMaterial, dDiskretisierung, szInCircuit)
    
	mi_clearselected();
	
	dX_l = dX_Center - dWidth/2;
	dX_r = dX_Center + dWidth/2;
	dY_o = dY_Center + dHeight/2;
	dY_u = dY_Center - dHeight/2;
	
	mi_addnode(dX_l, dY_o);
	mi_addnode(dX_l, dY_u);
	mi_addnode(dX_r, dY_o);
	mi_addnode(dX_r, dY_u);
	
	mi_addsegment(dX_l, dY_o, dX_l, dY_u);
	mi_addsegment(dX_l, dY_u, dX_r, dY_u);
	mi_addsegment(dX_r, dY_u, dX_r, dY_o);
	mi_addsegment(dX_r, dY_o, dX_l, dY_o);
	
    if exist('iRadiusGruppe', 'var') == 1
	
		mi_addarc(dX_l, dY_o, dX_l, dY_u, 180, dDiskretisierung);           %Zeichnet kleines Luftgebiet am Ende des PM
		mi_addarc(dX_r, dY_u, dX_r, dY_o, 180, dDiskretisierung);
        
    end
	
	mi_selectnode(dX_l, dY_o);
	mi_selectnode(dX_l, dY_u);
	mi_selectnode(dX_r, dY_o);
	mi_selectnode(dX_r, dY_u);
	
	mi_selectsegment(dX_l, (dY_o + dY_u)/2);
	mi_selectsegment(dX_r, (dY_o + dY_u)/2);
	mi_selectsegment((dX_l + dX_r)/2, dY_o);
	mi_selectsegment((dX_l + dX_r)/2, dY_u);
	
	if exist('iRadiusGruppe', 'var') == 1
        
		mi_selectarcsegment(dX_l, dY_o);
		mi_selectarcsegment(dX_r, dY_u);
        
	end
	
	mi_setgroup(13);
	mi_clearselected();
	
	% Material Slot	
	mi_addblocklabel(dX_Center, dY_Center);
	mi_selectlabel(dX_Center, dY_Center);
    
    if exist('szInCircuit', 'var') == 0                                     % F�gt PM mit Material und M,agnetisierungsrichtung ein
	        
		mi_setblockprop(szMaterialPM, 1, 1, [], 90, 13, 0);
        
    else
        
		mi_setblockprop(szMaterialPM, 1, 1, szInCircuit, 90, 13, 0);
        
    end
    
	mi_clearselected()

	% Material Radien
    
    if exist('iRadiusGruppe', 'var') == 1                                   % F�gt Material f�r Aussparungen neben PM ein
	        
		mi_addblocklabel(dX_l - dHeight/4, dY_Center);
		mi_selectlabel(dX_l - dHeight/4, dY_Center);
		mi_setblockprop(szRadiusMaterial, 1, 1, [], 0, 17, 0);
	
		mi_addblocklabel(dX_r + dHeight/4, dY_Center);
		mi_selectlabel(dX_r + dHeight/4, dY_Center);
		mi_setblockprop(szRadiusMaterial, 1, 1, [], 0, 17, 0);
	
		mi_clearselected();
        
    end
	
	% Drehen
	mi_selectgroup(13);
	
    if exist('iRadiusGruppe', 'var') == 1
	        
		mi_selectgroup(17);
        
    end    
	
	mi_moverotate(dX_Center, dY_Center, rad2deg(dAngle));
	
	% Gruppen zuweisen
	mi_clearselected();
	mi_selectgroup(13);
	mi_setgroup(iGruppe);
	
    if exist('iRadiusGruppe', 'var') == 1
	        
		mi_clearselected();
		mi_selectgroup(17);
		mi_setgroup(iRadiusGruppe);
        
    end
    	
	mi_clearselected()
	
end