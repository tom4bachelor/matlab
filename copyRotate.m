function [ dlR ] = copyRotate(x, y, phi, dl)
%COPYROTATE Summary of this function goes here
%   Abgekupfert aus Sourcecode von FEMM MOVECOPY:Rotatecopy
    dlR=dl;
    [~,n]=size(dl);
    c=x + y*1i;
    rot = exp(1i*phi*pi/180);
    for j=1:1:n
        p1=dl(2,j)+dl(4,j)*1i;
        p2=dl(3,j)+dl(5,j)*1i;
        p3=dl(8,j)+dl(9,j)*1i;
        
        p4=(p1-c)*rot+c;
        p5=(p2-c)*rot+c;
        p6=(p3-c)*rot+c;
        
        dlR(2,j)=real(p4);
        dlR(4,j)=imag(p4);
        dlR(3,j)=real(p5);
        dlR(5,j)=imag(p5);
        dlR(8,j)=real(p6);
        dlR(9,j)=imag(p6);
        
    end
    
    
        
end

