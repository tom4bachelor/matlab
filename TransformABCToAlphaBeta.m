function [dXAlpha, dXBeta] = TransformABCToAlphaBeta(dXa, dXb, dXc)

  dXAlpha = 2/3*(dXa - 1/2*dXb - 1/2*dXc);
  dXBeta = 2/3*(sqrt(3)/2*dXb - sqrt(3)/2*dXc);
 
end
