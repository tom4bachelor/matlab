function [dl] =  Stator(sStator,sWicklung,sLabel)

    %% Parameter f�r Stator und Wicklung bereitstellen
    dPaketBreite = sStator.dPaketBreite;
    dLochkreis = sStator.dLochkreis;
    dLochkreis2 = sStator.dLochkreis2;
    dBohrungDurchmesser = sStator.dBohrungDurchmesser;
    dBohrungDurchmesser2 = sStator.dBohrungDurchmesser2;
    dInnendurchmesser = sStator.dInnendurchmesser;
    dPolbedeckung = sStator.dPolbedeckung;
    dPolschuhHoeheAussen = sStator.dPolschuhHoeheAussen;
    dPolschuhHoeheInnen = sStator.dPolschuhHoeheInnen;
    dPolschuhWinkel = sStator.dPolschuhWinkel;
    dZahnBreite = sStator.dZahnBreite;
    dZahnHoehe = sStator.dZahnHoehe;
    dRadiusIn = sStator.dRadiusIn;                                          %% �bergang Zahn auf Joch
    dRadiusOut = sStator.dRadiusOut;                                        %% Radius Joch;
    
%     dBohrungsdiskretisierungBohrung = sStator.dBohrungsdiskretisierungBohrung;
%     dBohrungsdiskretisierungPolschuh = sStator.dBohrungsdiskretisierungPolschuh;
%     dBohrungsdiskretisierungJoch = sStator.dBohrungsdiskretisierungJoch;
%     dBohrungsdiskretisierungLuftspalt = sStator.dBohrungsdiskretisierungLuftspalt;
%     
     dWickelfensterBreite = sWicklung.dWickelfensterBreite;
     dWickelfensterHoehe = sWicklung.dWickelfensterHoehe;
%     
     iWickelfenster_ol = sWicklung.iWickelfenster_ol;
     iWickelfenster_or = sWicklung.iWickelfenster_or;
     iWickelfenster_olo = sWicklung.iWickelfenster_olo;
     iWickelfenster_olu = sWicklung.iWickelfenster_olu;
     iWickelfenster_ulo = sWicklung.iWickelfenster_ulo;
     iWickelfenster_ulu = sWicklung.iWickelfenster_ulu;
     iWickelfenster_ul = sWicklung.iWickelfenster_ul;
     iWickelfenster_ur = sWicklung.iWickelfenster_ur;
     iWickelfenster_uro = sWicklung.iWickelfenster_uro;
     iWickelfenster_uru = sWicklung.iWickelfenster_uru;
     iWickelfenster_oro = sWicklung.iWickelfenster_oro;
     iWickelfenster_oru = sWicklung.iWickelfenster_oru;

                                                   
	%% Zeichnen des Außenpakets
    
    dl = drawLine(-dPaketBreite/2, dPaketBreite/2, dPaketBreite/2, dPaketBreite/2, 1,2);
    dl = [dl,drawLine(dPaketBreite/2, dPaketBreite/2, dPaketBreite/2, -dPaketBreite/2, 1,2)];
    dl = [dl,drawLine(dPaketBreite/2, -dPaketBreite/2, -dPaketBreite/2, -dPaketBreite/2, 1,2)];
    dl = [dl,drawLine(-dPaketBreite/2, -dPaketBreite/2, -dPaketBreite/2, dPaketBreite/2, 1,2)];
    
    
    %% Bohrungen hinzufügen
    dX_Stator_Bohrung = dLochkreis/2 * cos(pi/4);
    dl = [dl,drawArc(-dX_Stator_Bohrung, dX_Stator_Bohrung, dBohrungDurchmesser2/2,0,360,sLabel.Bohrung1,sLabel.Stator)];
    dl = [dl,drawArc(dX_Stator_Bohrung, dX_Stator_Bohrung, dBohrungDurchmesser2/2,0,360,sLabel.Bohrung2,sLabel.Stator)];
    dl = [dl,drawArc(dX_Stator_Bohrung, -dX_Stator_Bohrung, dBohrungDurchmesser2/2,0,360,sLabel.Bohrung3,sLabel.Stator)];
    dl = [dl,drawArc(-dX_Stator_Bohrung, -dX_Stator_Bohrung, dBohrungDurchmesser2/2,0,360,sLabel.Bohrung4,sLabel.Stator)];
    
    
    dX_Stator_Bohrung = dLochkreis2/2 * cos(pi/4);
    dl = [dl,drawArc(-dX_Stator_Bohrung, dX_Stator_Bohrung, dBohrungDurchmesser2/2,0,360,sLabel.Bohrung5,sLabel.Stator)];
    dl = [dl,drawArc(dX_Stator_Bohrung, dX_Stator_Bohrung, dBohrungDurchmesser2/2,0,360,sLabel.Bohrung6,sLabel.Stator)];
    dl = [dl,drawArc(dX_Stator_Bohrung, -dX_Stator_Bohrung, dBohrungDurchmesser2/2,0,360,sLabel.Bohrung7,sLabel.Stator)];
    dl = [dl,drawArc(-dX_Stator_Bohrung, -dX_Stator_Bohrung, dBohrungDurchmesser2/2,0,360,sLabel.Bohrung8,sLabel.Stator)];
    
    
    %% Statorzähne zeichnen
    
    %Eckpunkte des ersten Statorzahns berechnen von unten nach oben
    dX_SZ_1l = -dInnendurchmesser/2 * sin(dPolbedeckung * pi/6);
	dX_SZ_1r = -dX_SZ_1l;
	dY_SZ_1 = dInnendurchmesser/2 * cos(dPolbedeckung * pi/6);
    
	dX_SZ_2l = -(dInnendurchmesser/2 + dPolschuhHoeheAussen) * sin(dPolbedeckung * pi/6);
	dX_SZ_2r = -dX_SZ_2l;
	dY_SZ_2 = (dInnendurchmesser/2 + dPolschuhHoeheAussen) * cos(dPolbedeckung * pi/6);
    
    dX_SZ_3l = -dZahnBreite/2;
	dX_SZ_3r = -dX_SZ_3l;
	dY_SZ_3 = dInnendurchmesser/2 + dPolschuhHoeheInnen;
    
    %Mittelpunkt und Radius des Kreisbogens f�r den Polschuh berechnen
    %[dX_SZ_Cl, dY_SZ_Cl, dR_SZ] = getCenter(dX_SZ_3l, dY_SZ_3, dX_SZ_2l, dY_SZ_2, dPolschuhWinkel);
    %[dX_SZ_Cr, dY_SZ_Cr, dR_SZ] = getCenter(dX_SZ_2r, dY_SZ_2, dX_SZ_3r, dY_SZ_3, dPolschuhWinkel);
    
    
    dX_SZ_4l = dX_SZ_3l;
	dX_SZ_4r = dX_SZ_3r;
	dY_SZ_4 = dY_SZ_3 + dZahnHoehe - dRadiusIn;
    
    dX_SZ_4Cl = dX_SZ_4l - dRadiusIn;
    
    
	dX_SZ_5l = dX_SZ_4l - dRadiusIn;
	dX_SZ_5r = -dX_SZ_5l;
	dY_SZ_5 = dY_SZ_4 + dRadiusIn*0.7;
    
    
    dX_SZ_6l = -dRadiusOut/2*sin(pi/6);
	dX_SZ_6r = -dX_SZ_6l;
	dY_SZ_6 = dRadiusOut/2*cos(pi/6);
    

    dl_SZ=[...
    drawArc(0,0,dInnendurchmesser/2,dPolbedeckung *(30)+90,dPolbedeckung*(-60),sLabel.Stator,sLabel.Rotorluft),...
    drawLine(dX_SZ_1l, dX_SZ_2l, dY_SZ_1, dY_SZ_2,sLabel.Rotorluft,sLabel.Stator),...
    drawLine(dX_SZ_1r, dX_SZ_2r, dY_SZ_1, dY_SZ_2,sLabel.Stator,sLabel.Rotorluft),...
    drawLine(dX_SZ_2l, dX_SZ_3l, dY_SZ_2, dY_SZ_3, sLabel.Rotorluft, sLabel.Stator),...
    drawLine(dX_SZ_2r, dX_SZ_3r, dY_SZ_2, dY_SZ_3, sLabel.Stator, sLabel.Rotorluft),...
    ...%drawLine(dX_SZ_3r, dX_SZ_4r, dY_SZ_3, dY_SZ_4, sLabel.Stator, sLabel.WicklungA_m),...
    ...%drawLine(dX_SZ_3l, dX_SZ_4l, dY_SZ_3, dY_SZ_4,sLabel.WicklungA_p, sLabel.Stator),...
    drawLine(dX_SZ_4l, dX_SZ_5l, dY_SZ_4, dY_SZ_5, sLabel.Rotorluft, sLabel.Stator),...
    drawLine(dX_SZ_4r, dX_SZ_5r, dY_SZ_4, dY_SZ_5, sLabel.Stator, sLabel.Rotorluft),...
    drawLine(dX_SZ_5l, dX_SZ_6l, dY_SZ_5, dY_SZ_6, sLabel.Rotorluft, sLabel.Stator),...
    drawLine(dX_SZ_5r, dX_SZ_6r, dY_SZ_5, dY_SZ_6, sLabel.Stator, sLabel.Rotorluft),...
    ];
% Unstetigkeit beim übergang der Kreise
%     drawArc(dX_SZ_4l-(dY_SZ_5-dY_SZ_4), dY_SZ_4,dY_SZ_5-dY_SZ_4, 0, 90, sLabel.Rotorluft, sLabel.Stator),...
%     drawArc(dX_SZ_4r+(dY_SZ_5-dY_SZ_4), dY_SZ_4,dY_SZ_5-dY_SZ_4, 180, -90, sLabel.Stator, sLabel.Rotorluft),...
%     drawArc(0,0,norm([dX_SZ_4l-(dY_SZ_5-dY_SZ_4),dY_SZ_5]),acosd((dX_SZ_4l-(dY_SZ_5-dY_SZ_4))/norm([dX_SZ_4l-(dY_SZ_5-dY_SZ_4),dY_SZ_5])),acosd(dX_SZ_6l/norm([dX_SZ_4l-(dY_SZ_5-dY_SZ_4),dY_SZ_5]))-acosd((dX_SZ_4l-(dY_SZ_5-dY_SZ_4))/norm([dX_SZ_4l-(dY_SZ_5-dY_SZ_4),dY_SZ_5])),sLabel.Rotorluft, sLabel.Stator),...
%     drawArc(0,0,norm([dX_SZ_4r+(dY_SZ_5-dY_SZ_4),dY_SZ_5]),acosd((dX_SZ_4r+(dY_SZ_5-dY_SZ_4))/norm([dX_SZ_4r+(dY_SZ_5-dY_SZ_4),dY_SZ_5])),acosd(dX_SZ_6r/norm([dX_SZ_4r+(dY_SZ_5-dY_SZ_4),dY_SZ_5]))-acosd((dX_SZ_4r+(dY_SZ_5-dY_SZ_4))/norm([dX_SZ_4r-(dY_SZ_5-dY_SZ_4),dY_SZ_5])), sLabel.Stator, sLabel.Rotorluft),...


    %drawArcb(dX_SZ_Cl, dY_SZ_Cl, dR_SZ, dX_SZ_3l, dX_SZ_2l, dY_SZ_3, dY_SZ_2, sLabel.Stator, sLabel.Rotorluft),...
    %drawArcb(dX_SZ_Cr, dY_SZ_Cr, dR_SZ, dX_SZ_2r, dX_SZ_3r, dY_SZ_2, dY_SZ_3, sLabel.Stator, sLabel.Rotorluft),...
    %Vereinfachung wg nicht genauem finden des Radius und des
    %Kreismittelpunkts
    
    %drawArcb(0,0,(dRadiusOut/2), dX_SZ_5l, dX_SZ_6l, dY_SZ_5, dY_SZ_6, sLabel.Rotorluft, sLabel.Stator)
    
    
    dl_SZ=[dl_SZ,copyRotateN(0,0,60,5,dl_SZ)];
    
    
    
    
    
    
    
    
    %%Wicklungen
    dX_CWF = 0;
    dY_CWF = dInnendurchmesser/2 + dPolschuhHoeheInnen + (dZahnHoehe - dRadiusIn)/2;
    dl_WF = [...
        drawWicklungen(dX_CWF,dY_CWF,dWickelfensterHoehe,dWickelfensterBreite,0,dZahnBreite,sLabel.WicklungA_p,sLabel.WicklungA_m,sLabel.Stator,sLabel.Rotorluft),...
        drawWicklungen(dX_CWF,dY_CWF,dWickelfensterHoehe,dWickelfensterBreite,-60,dZahnBreite,sLabel.WicklungB_m,sLabel.WicklungB_p,sLabel.Stator,sLabel.Rotorluft),...
        drawWicklungen(dX_CWF,dY_CWF,dWickelfensterHoehe,dWickelfensterBreite,-120,dZahnBreite,sLabel.WicklungC_p,sLabel.WicklungC_m,sLabel.Stator,sLabel.Rotorluft),...
        drawWicklungen(dX_CWF,dY_CWF,dWickelfensterHoehe,dWickelfensterBreite,-180,dZahnBreite,sLabel.WicklungA2_m,sLabel.WicklungA2_p,sLabel.Stator,sLabel.Rotorluft),...
        drawWicklungen(dX_CWF,dY_CWF,dWickelfensterHoehe,dWickelfensterBreite,-240,dZahnBreite,sLabel.WicklungB2_p,sLabel.WicklungB2_m,sLabel.Stator,sLabel.Rotorluft),...
        drawWicklungen(dX_CWF,dY_CWF,dWickelfensterHoehe,dWickelfensterBreite,-300,dZahnBreite,sLabel.WicklungC2_m,sLabel.WicklungC2_p,sLabel.Stator,sLabel.Rotorluft),...
        ];
        
    
    dl=[dl,dl_SZ, dl_WF];

end