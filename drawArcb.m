function [ g ] = drawArcb( x, y, r, xs, xe, ys, ye, el, er )
%DRAWARC Summary of this function goes here
%   Detailed explanation goes here
%   Circle
%   1
%   Starting x coordinate
%   Ending x coordinate
%   Starting y coordinate	
%   Ending y coordinate
%   Region label to left of segment, with direction induced by start and end points (0 is exterior label)
%   Region label to right of segment, with direction induced by start and end points (0 is exterior label)
%   x coordinate of circle center
%   y coordinate of circle center
%   Length of first semiaxis (Here radius)
%   0 0 

    
    g = [1 , xs, xe, ys, ye, el, er, x, y, r, 0, 0]';
    
end
