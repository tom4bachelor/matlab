% function [ J ] = pm2j( dHeight, dWidth, dCSThickness, Material)
% %PM2J Calculates a equivalent sheet-currentdensity for a magnet
% %   This function calculates a currentdensity that is needed to model a pm
% %   magnet appropiatly
%         if Material(2)~=0
%             %Permanent magnet given through Hc and µ_r
%             %cylindrical coil?
%             J = Material(2)*dWidth/(dHeight*dCSThickness);%sqrt((dHeight^2)+(dWidth^2))
%             
%             
%         else
%             %Material given by energy density
%             %Here it is important to know if the energyproduct is given in
%             %MGOe (Mega Gaus Oersteds) or in BH kJ/m^3
%             Hc = sqrt(Material(3)/Material(1))*10^6/(4*pi);
%             J = dHeight*dCSThickness*Hc*sqrt((dHeight^2)+(dWidth^2));
%             
%         end
%         
% 
% end

%%By a mean H value calculated over the Biot-Savart-Law
% function [ J ] = pm2j( dHeight, dWidth, dCSThickness, Material)
% %PM2J Calculates a equivalent sheet-currentdensity for a magnet
% %   This function calculates a currentdensity that is needed to model a pm
% %   magnet appropiatly
%         if Material(2)~=0
%             %Permanent magnet given through Hc and µ_r
%             %cylindrical coil?
%             I = Material(2)*pi*(dWidth-2*dCSThickness)/(log((dWidth-dCSThickness)/(dCSThickness/2)));
%             J = I/(dCSThickness*dHeight);
%             
%             
%         else
%             %Material given by energy density
%             %Here it is important to know if the energyproduct is given in
%             %MGOe (Mega Gaus Oersteds) or in BH kJ/m^3
%             Hc = sqrt(Material(3)/Material(1))*10^6/(4*pi);
%             J = dHeight*dCSThickness*Hc*sqrt((dHeight^2)+(dWidth^2));
%             
%         end
%         
% 
% end

%MMF representation... doesnt make to much sense
function [ J ] = pm2j( dHeight, dWidth, dCSThickness, Material)
%PM2J Calculates a equivalent sheet-currentdensity for a magnet
%   This function calculates a currentdensity that is needed to model a pm
%   magnet appropiatly
        if Material(2)~=0
            I = Material(2)*dHeight;
            J = I/(dCSThickness*dHeight);
        end
end