%% Matlab

addpath(genpath('./pde'));   %Pfad fÃ¼r modifizierte PDE-Skripte

x_start = -0.115;
x_ende = 0.115;
y_start = 0;
y_ende = 0;

%Number of points

qp = 230;

xq = linspace(x_start,x_ende,qp);
yq = linspace(y_start,y_ende,qp);

%get B
[bx,by] = evaluateGradient(result,xq,yq);
b = sqrt(bx.^2+by.^2);

%plot
%Länge der Linie
l=sqrt((xq-x_start).^2+(yq-y_start).^2);
%scale
l=l*1000;

plot(l-115,b,'LineWidth',1);

%% FEMM
m = 1000; %Umrechnungsfaktor m zu cm
Bfx = zeros(1,qp);
Bfy = zeros(1,qp);
for i=1:qp
    Bf=mo_getb(xq(i)*m,yq(i)*m);
    Bfx(i)=Bf(1);
    Bfy(i)=Bf(2);
end
bf = sqrt(Bfx.^2+Bfy.^2);

hold on;
plot(l-115,bf,'LineWidth',1);
xlim([l(1)-115,l(end)-115]);
legend('\pdet','\femm','Location','northeast');%best
ylabel('Flussdichte $|B|$ [T]','Interpreter','latex');
xlabel('x [mm]','Interpreter','latex')
grid on;
set(gca,'FontSize',20)
%matlab2tikz('width','0.7\columnwidth')
 