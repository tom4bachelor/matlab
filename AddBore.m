function AddBore(dX_Center, dY_Center, dDiameter, iGroup, dDiskretisierung, szMaterial, szInCircuit)
	
	mi_clearselected();
	mi_addnode(dX_Center - dDiameter/2, dY_Center);
	mi_addnode(dX_Center + dDiameter/2, dY_Center);
	mi_addarc(dX_Center - dDiameter/2, dY_Center, dX_Center + dDiameter/2, dY_Center, 180, dDiskretisierung);
	mi_addarc(dX_Center + dDiameter/2, dY_Center, dX_Center - dDiameter/2, dY_Center, 180, dDiskretisierung);
    
    g=
    
	
	if exist('szMaterial', 'var') == 1
        
        if exist('szInCircuit', 'var') == 0
            
			mi_addblocklabel(dX_Center, dY_Center);
			mi_selectlabel(dX_Center, dY_Center);
			mi_setblockprop(szMaterial, 1, 0, 0, 0, 0, 0);
            
        else
            
			mi_addblocklabel(dX_Center, dY_Center);
			mi_selectlabel(dX_Center, dY_Center);
			mi_setblockprop(szMaterial, 1, 0, szInCircuit, 0, 0, 0);
            
        end
        
	end
		
	mi_selectnode(dX_Center - dDiameter/2, dY_Center);
	mi_selectnode(dX_Center + dDiameter/2, dY_Center);
	mi_selectarcsegment(dX_Center, dY_Center + dDiameter/2);
	mi_selectarcsegment(dX_Center, dY_Center - dDiameter/2);
	
	if exist('iGroup', 'var')
        
		mi_setgroup(iGroup);
        
	end
	
	mi_clearselected();
		
end






