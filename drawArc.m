function [ g ] = drawArc( x, y, r, phi_s, dphi, el, er )
%Zeichnet einen Kreisbogen um x, y mit dem Radius r, dem Startwinkel phi_s
%und der Winkellänge von dphi

%   Geometriematrix Kreiselement:
%   1
%   Starting x coordinate
%   Ending x coordinate
%   Starting y coordinate	
%   Ending y coordinate
%   Region label to left of segment, with direction induced by start and end points (0 is exterior label)
%   Region label to right of segment, with direction induced by start and end points (0 is exterior label)
%   x coordinate of circle center
%   y coordinate of circle center
%   Length of first semiaxis (Here radius)
%   0 0 
    

    
    if(dphi>=0)
        xs = cosd(phi_s)*r + x;
        ys = sind(phi_s)*r + y;
    
        xe = cosd(phi_s+dphi)*r +x;
        ye = sind(phi_s+dphi)*r +y;
    else
        xs = cosd(phi_s+dphi)*r + x;
        ys = sind(phi_s+dphi)*r + y;
    
        xe = cosd(phi_s)*r +x;
        ye = sind(phi_s)*r +y;
    end
    if abs(dphi) > 180   %Split the circle in 2 halfs
        if dphi > 0
            xm = cosd((dphi/2) + phi_s)*r + x;
            ym = sind((dphi/2) + phi_s)*r + y;
        else
            xm = cosd((dphi/2) + phi_s)*r + x;
            ym = sind((dphi/2) + phi_s)*r + y;
        end
        g = [1 , xs, xm, ys, ym, el, er, x, y, r, 0, 0]';
        g = [g,[1 , xm, xe, ym, ye, el, er, x, y, r, 0, 0]'];
    else
        g = [1 , xs, xe, ys, ye, el, er, x, y, r, 0, 0]';
    end
    
end
